Imports FTS_Reconcile.App.Dao
Imports FTS_Reconcile.App.Utility
Imports System.IO
Imports System.Messaging
Imports FTS_Reconcile.Helper.Services
Imports FTS_Reconcile.fts.util
Imports FTS_Reconcile.fts.service
Imports System.Net.NetworkInformation
Imports System.Threading
Imports System.Globalization
Imports FTS_Reconcile.HQComponent.BO
Imports System.ComponentModel
Imports FTS_Reconcile.fts.constant

Public Class frmEOD
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

    Private isInit As Boolean = False

    Dim resultSource As New BindingSource()

    Dim pSiteNo As String
    Dim pFromDate As Date
    Dim pToDate As Date
    Dim sumR As New SummaryReconcileCollection
    Dim isError As Boolean = False
    Dim errMsg As String

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        init()
    End Sub

    Private Sub frmEOD_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        init()
    End Sub

    Private Sub init()
        ' Sets the culture to English
        Thread.CurrentThread.CurrentCulture = New CultureInfo("en-US")
        ' Sets the UI culture to English
        Thread.CurrentThread.CurrentUICulture = New CultureInfo("en-US")

        log.Info("Request data from SITE app is init v." + (GetType(frmMain).Assembly.GetName().Version).ToString)

        Me.Text = "Request Data From Site v." + (GetType(frmMain).Assembly.GetName().Version).ToString

        LoadSite(StationConstant.STATION_TYPE_OID)

        'generate status dropdown list
        cbbStatus.DataSource = GlobalInfo.getStatusList
        cbbStatus.DisplayMember = "Value"
        cbbStatus.ValueMember = "Key"

        isInit = True
    End Sub

    Private Sub LoadSite(ByVal SiteTypeId As Integer)
        Try
            cbbSite.DataSource = SiteService.getsForComboBox(SiteTypeId)
            cbbSite.DisplayMember = "site_name"
            cbbSite.ValueMember = "site_code"
            cbbSite.Show()
        Catch ex As Exception
            log.Error("load site master", ex)
            Throw ex
        End Try
    End Sub

    Private Sub rbOil_CheckedChanged(sender As Object, e As EventArgs) Handles rbOil.CheckedChanged
        If isInit AndAlso rbOil.Checked Then
            cbbSite.Enabled = True
            LoadSite(StationConstant.STATION_TYPE_OID)
        End If
    End Sub

    Private Sub rbNgv_CheckedChanged(sender As Object, e As EventArgs) Handles rbNgv.CheckedChanged
        If isInit AndAlso rbNgv.Checked Then
            cbbSite.Enabled = True
            LoadSite(StationConstant.STATION_TYPE_NGV)
        End If
    End Sub

    Private Sub btnRefreshEOD_Click(sender As Object, e As EventArgs) Handles btnRefreshEOD.Click

        'display loading screen
        BeginAsyncIndication()

        If Not bgwShowData.IsBusy = True Then
            'map search criteria
            pSiteNo = cbbSite.SelectedValue
            pFromDate = dtpStartDate.Value.Date
            pToDate = dtpToDate.Value.Date

            bgwShowData.RunWorkerAsync()
        End If

    End Sub

    'inquiery EOD transaction and also update status
    Private Sub loadEODData()
        Try
            isError = False
            sumR.Clear()

            sumR = ReconcileService.checkLatestSummary(pSiteNo, pFromDate, pToDate)
        Catch ex As Exception
            log.Error("Error retreive data from HQ : " + ex.Message, ex)
            isError = True
            errMsg = "Error retreive data from HQ : " + ex.Message
        End Try
    End Sub

    Private Sub bgwShowData_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bgwShowData.DoWork
        loadEODData()
    End Sub

    Private Sub bgwShowData_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bgwShowData.RunWorkerCompleted
        'disable loading
        EndAsyncIndication()

        If isError Then
            MessageBox.Show(errMsg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            sumR = ReconcileService.checkLatestSummary(pSiteNo, pFromDate, pToDate)
            resultSource.DataSource = sumR
            dgwResult.DataSource = resultSource
            dgwResult.Refresh()

            Dim diff As Integer = 0, complate As Integer = 0, notFound As Integer = 0, waiting As Integer = 0, other As Integer = 0
            Try
                '=== Paint Red Item to Grid ===
                Dim i As Integer = 0

                For i = 0 To sumR.Count - 1
                    If sumR.Item(i).Status = FTSConstant.StatusConstant.COMPATE Then
                        complate += 1
                    ElseIf sumR.Item(i).Status = FTSConstant.StatusConstant.DIFF Then
                        diff += 1
                        PaintDiffRow(i)
                    ElseIf sumR.Item(i).Status = FTSConstant.StatusConstant.NOT_SEND_EOD Then
                        notFound += 1
                    ElseIf sumR.Item(i).Status = FTSConstant.StatusConstant.WAITING Then
                        waiting += 1
                    Else
                        other += 1
                    End If
                Next
            Catch ex As Exception
                log.Error("error : " + ex.Message, ex)
            End Try

            Dim baseStatus As String = "Total: {0:D3}      Diff: {1:D3}      Not found EOD: {2:D3}      Complete: {3:D3}       Waiting: {4:D3}      Other: {5:D3}"
            lblStatus.Text = String.Format(baseStatus, sumR.Count, diff, notFound, complate, waiting, other)
        End If

        isError = False
    End Sub

    Private Sub PaintDiffRow(ByVal Rowindex As Integer)
        '==Function Paint Red Item==
        'dgwResult.Rows(Rowindex).DefaultCellStyle.BackColor = Color.Red
        dgwResult.Rows(Rowindex).DefaultCellStyle.ForeColor = Color.Red
    End Sub

    Private Sub cbbStatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbbStatus.SelectedIndexChanged
        'this process is not work yet
        If isInit Then
            If cbbStatus.SelectedValue = "-" Then
                'resultSource.Filter = ""
                SummaryReconcileBindingSource.Filter = ""
                dgwResult.Refresh()
            Else
                resultSource.Filter = "[STATUS] = '" + cbbStatus.SelectedValue + "'"
                dgwResult.Refresh()
            End If
        End If
    End Sub

End Class