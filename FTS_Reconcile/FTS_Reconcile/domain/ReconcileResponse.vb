﻿Imports FTS_Services.Domain.BO.UI
Imports Ie.IeComponent.BO

Namespace Domain.BO.UI
    Public Class ReconcileResponse
        Public Property processStatus As Integer
        Public Property processMsg As String
        Public Property totalSite As Integer
        Public Property totalHq As Integer
        Public Property siteMissingList As List(Of FullTaxReceipt)
        Public Property diffList As List(Of FullTaxReceiptUI)
        Public Property siteList As List(Of FullTaxReceiptUI)
        Public Property hqList As List(Of HQReceipt)

        Public Sub New()
            processStatus = -1
            siteMissingList = New List(Of FullTaxReceipt)
            diffList = New List(Of FullTaxReceiptUI)
            siteList = New List(Of FullTaxReceiptUI)
            hqList = New List(Of HQReceipt)
        End Sub

        Public Sub reset()
            processStatus = -1
            processMsg = ""
            totalSite = 0
            totalHq = 0

            siteMissingList.Clear()
            diffList.Clear()
            siteList.Clear()
            hqList.Clear()
        End Sub
    End Class

End Namespace
