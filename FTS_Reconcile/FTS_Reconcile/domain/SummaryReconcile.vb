﻿Namespace HQComponent.BO
    <Serializable()>
    Public Class SummaryReconcile
        Inherits BaseBO

#Region "Private Fields"
        Private _siteTypeId As Integer
        Private _siteNo As String = ""
        Private _siteId As Integer
        Private _stationId As Integer
        Private _shiftNo As String
        Private _docDate As Date
        Private _createDate As Date
        Private _fullTaxQty As Integer
        Private _rFullTaxQty As Integer
        Private _dFullTaxQty As Integer
        Private _cancelQty As Integer
        Private _rCancelQty As Integer
        Private _dCancelQty As Integer
        Private _voidQty As Integer
        Private _rVoidQty As Integer
        Private _dVoidQty As Integer
        Private _replaceQty As Integer
        Private _rReplaceQty As Integer
        Private _dReplaceQty As Integer
        Private _cnQty As Integer
        Private _rCnQty As Integer
        Private _dCnQty As Integer
        Private _fleetQty As Integer
        Private _rFleetQty As Integer
        Private _dFleetQty As Integer
        Private _manualQty As Integer
        Private _rManualQty As Integer
        Private _dManualQty As Integer
        Private _scanQty As Integer
        Private _rScanQty As Integer
        Private _dScanQty As Integer
        Private _fuelQty As Integer
        Private _rFuelQty As Integer
        Private _dFuelQty As Integer
        Private _goodsQty As Integer
        Private _rGoodsQty As Integer
        Private _dGoodsQty As Integer
        Private _status As String
        Private _startFullTaxNbr As String
        Private _endFullTaxNbr As String
        Private _startManualNbr As String
        Private _endManualNbr As String
        Private _startCnNbr As String
        Private _endCnNbr As String
        Private _customerQty As String
        Private _custPlateQty As String

        Private _customerHqQty As Long
        Private _custPlateHqQty As Long

#End Region
#Region "Properties"
        Property SiteTypeId() As Integer
            Get
                Return _siteTypeId
            End Get
            Set(ByVal Value As Integer)
                _siteTypeId = Value
            End Set
        End Property

        Property SiteNo() As String
            Get
                Return _siteNo
            End Get
            Set(ByVal Value As String)
                _siteNo = Value
            End Set
        End Property

        Property SiteId() As Integer
            Get
                Return _siteId
            End Get
            Set(ByVal Value As Integer)
                _siteId = Value
            End Set
        End Property

        Property StationId() As Integer
            Get
                Return _stationId
            End Get
            Set(ByVal Value As Integer)
                _stationId = Value
            End Set
        End Property

        Property ShiftNo() As String
            Get
                Return _shiftNo
            End Get
            Set(ByVal Value As String)
                _shiftNo = Value
            End Set
        End Property

        Property DocDate() As Date
            Get
                Return _docDate
            End Get
            Set(ByVal Value As Date)
                _docDate = Value
            End Set
        End Property

        Property CreateDate() As Date
            Get
                Return _createDate
            End Get
            Set(ByVal Value As Date)
                _createDate = Value
            End Set
        End Property

        Property FullTaxQty() As Integer
            Get
                Return _fullTaxQty
            End Get
            Set(ByVal Value As Integer)
                _fullTaxQty = Value
            End Set
        End Property

        Property RFullTaxQty() As Integer
            Get
                Return _rFullTaxQty
            End Get
            Set(ByVal Value As Integer)
                _rFullTaxQty = Value
            End Set
        End Property

        Property DFullTaxQty() As Integer
            Get
                Return _dFullTaxQty
            End Get
            Set(ByVal Value As Integer)
                _dFullTaxQty = Value
            End Set
        End Property

        Property CancelQty() As Integer
            Get
                Return _cancelQty
            End Get
            Set(ByVal Value As Integer)
                _cancelQty = Value
            End Set
        End Property

        Property RCancelQty() As Integer
            Get
                Return _rCancelQty
            End Get
            Set(ByVal Value As Integer)
                _rCancelQty = Value
            End Set
        End Property

        Property DCancelQty() As Integer
            Get
                Return _dCancelQty
            End Get
            Set(ByVal Value As Integer)
                _dCancelQty = Value
            End Set
        End Property

        Property VoidQty() As Integer
            Get
                Return _voidQty
            End Get
            Set(ByVal Value As Integer)
                _voidQty = Value
            End Set
        End Property

        Property RVoidQty() As Integer
            Get
                Return _rVoidQty
            End Get
            Set(ByVal Value As Integer)
                _rVoidQty = Value
            End Set
        End Property

        Property DVoidQty() As Integer
            Get
                Return _dVoidQty
            End Get
            Set(ByVal Value As Integer)
                _dVoidQty = Value
            End Set
        End Property

        Property ReplaceQty() As Integer
            Get
                Return _replaceQty
            End Get
            Set(ByVal Value As Integer)
                _replaceQty = Value
            End Set
        End Property

        Property RReplaceQty() As Integer
            Get
                Return _rReplaceQty
            End Get
            Set(ByVal Value As Integer)
                _rReplaceQty = Value
            End Set
        End Property

        Property DReplaceQty() As Integer
            Get
                Return _dReplaceQty
            End Get
            Set(ByVal Value As Integer)
                _dReplaceQty = Value
            End Set
        End Property

        Property CnQty() As Integer
            Get
                Return _cnQty
            End Get
            Set(ByVal Value As Integer)
                _cnQty = Value
            End Set
        End Property

        Property RCnQty() As Integer
            Get
                Return _rCnQty
            End Get
            Set(ByVal Value As Integer)
                _rCnQty = Value
            End Set
        End Property

        Property DCnQty() As Integer
            Get
                Return _dCnQty
            End Get
            Set(ByVal Value As Integer)
                _dCnQty = Value
            End Set
        End Property

        Property FleetQty() As Integer
            Get
                Return _fleetQty
            End Get
            Set(ByVal Value As Integer)
                _fleetQty = Value
            End Set
        End Property

        Property RFleetQty() As Integer
            Get
                Return _rFleetQty
            End Get
            Set(ByVal Value As Integer)
                _rFleetQty = Value
            End Set
        End Property

        Property DFleetQty() As Integer
            Get
                Return _dFleetQty
            End Get
            Set(ByVal Value As Integer)
                _dFleetQty = Value
            End Set
        End Property

        Property ManualQty() As Integer
            Get
                Return _manualQty
            End Get
            Set(ByVal Value As Integer)
                _manualQty = Value
            End Set
        End Property

        Property RManualQty() As Integer
            Get
                Return _rManualQty
            End Get
            Set(ByVal Value As Integer)
                _rManualQty = Value
            End Set
        End Property

        Property DManualQty() As Integer
            Get
                Return _dManualQty
            End Get
            Set(ByVal Value As Integer)
                _dManualQty = Value
            End Set
        End Property

        Property ScanQty() As Integer
            Get
                Return _scanQty
            End Get
            Set(ByVal Value As Integer)
                _scanQty = Value
            End Set
        End Property

        Property RScanQty() As Integer
            Get
                Return _rScanQty
            End Get
            Set(ByVal Value As Integer)
                _rScanQty = Value
            End Set
        End Property

        Property DScanQty() As Integer
            Get
                Return _dScanQty
            End Get
            Set(ByVal Value As Integer)
                _dScanQty = Value
            End Set
        End Property

        Property FuelQty() As Integer
            Get
                Return _fuelQty
            End Get
            Set(ByVal Value As Integer)
                _fuelQty = Value
            End Set
        End Property

        Property RFuelQty() As Integer
            Get
                Return _rFuelQty
            End Get
            Set(ByVal Value As Integer)
                _rFuelQty = Value
            End Set
        End Property

        Property DFuelQty() As Integer
            Get
                Return _dFuelQty
            End Get
            Set(ByVal Value As Integer)
                _dFuelQty = Value
            End Set
        End Property

        Property GoodsQty() As Integer
            Get
                Return _goodsQty
            End Get
            Set(ByVal Value As Integer)
                _goodsQty = Value
            End Set
        End Property

        Property RGoodsQty() As Integer
            Get
                Return _rGoodsQty
            End Get
            Set(ByVal Value As Integer)
                _rGoodsQty = Value
            End Set
        End Property

        Property DGoodsQty() As Integer
            Get
                Return _dGoodsQty
            End Get
            Set(ByVal Value As Integer)
                _dGoodsQty = Value
            End Set
        End Property

        Property Status() As String
            Get
                Return _status
            End Get
            Set(ByVal Value As String)
                _status = Value
            End Set
        End Property

        Property StartFullTaxNbr() As String
            Get
                Return _startFullTaxNbr
            End Get
            Set(ByVal Value As String)
                _startFullTaxNbr = Value
            End Set
        End Property

        Property EndFullTaxNbr() As String
            Get
                Return _endFullTaxNbr
            End Get
            Set(ByVal Value As String)
                _endFullTaxNbr = Value
            End Set
        End Property

        Property StartManualNbr() As String
            Get
                Return _startManualNbr
            End Get
            Set(ByVal Value As String)
                _startManualNbr = Value
            End Set
        End Property

        Property EndManualNbr() As String
            Get
                Return _endManualNbr
            End Get
            Set(ByVal Value As String)
                _endManualNbr = Value
            End Set
        End Property

        Property StartCnNbr() As String
            Get
                Return _startCnNbr
            End Get
            Set(ByVal Value As String)
                _startCnNbr = Value
            End Set
        End Property

        Property EndCnNbr() As String
            Get
                Return _endCnNbr
            End Get
            Set(ByVal Value As String)
                _endCnNbr = Value
            End Set
        End Property

        Property CustomerQty() As String
            Get
                Return _customerQty
            End Get
            Set(ByVal Value As String)
                _customerQty = Value
            End Set
        End Property

        Property custPlateQty() As String
            Get
                Return _custPlateQty
            End Get
            Set(ByVal Value As String)
                _custPlateQty = Value
            End Set
        End Property

        Property CustomerHqQty() As Long
            Get
                Return _customerHqQty
            End Get
            Set(ByVal Value As Long)
                _customerHqQty = Value
            End Set
        End Property

        Property custPlateHqQty() As Long
            Get
                Return _custPlateHqQty
            End Get
            Set(ByVal Value As Long)
                _custPlateHqQty = Value
            End Set
        End Property


        Public Sub New()
            MyBase.New()
        End Sub

#End Region

    End Class

    <Serializable()>
    Public Class SummaryReconcileCollection
        Inherits BaseBOCollection

        Default Property Item(ByVal index As Integer) As SummaryReconcile
            Get
                Return CType(innerList.Item(index), SummaryReconcile)
            End Get
            Set(ByVal value As SummaryReconcile)
                InnerList.Item(index) = value
            End Set
        End Property

        Sub Add(ByVal value As SummaryReconcile)
            InnerList.Add(value)
        End Sub

    End Class

End Namespace
