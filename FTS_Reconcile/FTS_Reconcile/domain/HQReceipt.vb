﻿Namespace Domain.BO.UI

    Public Class HQReceipt
        Property receiptNbr As String
        Property receiptDate As Date
        Property customerTaxId As String
        Property plateNo As String
        Property netAmt As Double
        Property shiftNo As String
        Property siteNo As String

    End Class

End Namespace
