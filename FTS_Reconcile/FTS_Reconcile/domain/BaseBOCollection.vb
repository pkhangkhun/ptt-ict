﻿Namespace HQComponent.BO

    <Serializable()>
    Public Class BaseBOCollection
        Inherits CollectionBase
        Implements System.IDisposable

        ' Implement IDisposable
        ' This is the method called by the client to dispose the object.
        Public Overloads Sub Dispose() _
            Implements System.IDisposable.Dispose

            ' Call the actual dispose method, specify manual dispose
            Dispose(True)
            ' Take the object out of GC
            GC.SuppressFinalize(Me)
        End Sub

        ' This is the method called by the GC
        Protected Overrides Sub Finalize()
            ' Call the actual dispose method, specify GC dispose
            Dispose(False)
        End Sub

        ' This is to be implemented by the subclass
        Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                ' Manually called
                ' Call Dispose() on the contained object
            End If
            ' Call Dispose() on the base object
        End Sub

    End Class

End Namespace
