<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnSend = New System.Windows.Forms.Button()
        Me.lbStatus = New System.Windows.Forms.Label()
        Me.pbLoad = New System.Windows.Forms.ProgressBar()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblD = New System.Windows.Forms.Label()
        Me.lblC = New System.Windows.Forms.Label()
        Me.cbbSite = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.rbOil = New System.Windows.Forms.RadioButton()
        Me.rbNgv = New System.Windows.Forms.RadioButton()
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dtpToDate = New System.Windows.Forms.DateTimePicker()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.lbNoEod = New System.Windows.Forms.Label()
        Me.chkNoEOD = New System.Windows.Forms.CheckBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.rdoIp = New System.Windows.Forms.RadioButton()
        Me.rdoSiteNo = New System.Windows.Forms.RadioButton()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtDestAddress = New System.Windows.Forms.TextBox()
        Me.btnSendByReceiptNBR = New System.Windows.Forms.Button()
        Me.txtReceiptNBR = New System.Windows.Forms.TextBox()
        Me.chbFixIp = New System.Windows.Forms.CheckBox()
        Me.cbbFixIp = New System.Windows.Forms.ComboBox()
        Me.chkNoQ = New System.Windows.Forms.CheckBox()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnSend
        '
        Me.btnSend.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSend.Location = New System.Drawing.Point(510, 95)
        Me.btnSend.Name = "btnSend"
        Me.btnSend.Size = New System.Drawing.Size(118, 55)
        Me.btnSend.TabIndex = 7
        Me.btnSend.Text = "Send Request / Reconcile"
        Me.btnSend.UseVisualStyleBackColor = True
        '
        'lbStatus
        '
        Me.lbStatus.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.lbStatus.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.lbStatus.Location = New System.Drawing.Point(34, 170)
        Me.lbStatus.Name = "lbStatus"
        Me.lbStatus.Size = New System.Drawing.Size(594, 27)
        Me.lbStatus.TabIndex = 6
        Me.lbStatus.Text = " "
        Me.lbStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'pbLoad
        '
        Me.pbLoad.Location = New System.Drawing.Point(34, 288)
        Me.pbLoad.Name = "pbLoad"
        Me.pbLoad.Size = New System.Drawing.Size(594, 12)
        Me.pbLoad.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.Label1.Location = New System.Drawing.Point(58, 55)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 19)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Site:"
        '
        'lblD
        '
        Me.lblD.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.lblD.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.lblD.Location = New System.Drawing.Point(34, 257)
        Me.lblD.Name = "lblD"
        Me.lblD.Size = New System.Drawing.Size(594, 27)
        Me.lblD.TabIndex = 8
        Me.lblD.Text = " "
        Me.lblD.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblC
        '
        Me.lblC.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.lblC.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.lblC.Location = New System.Drawing.Point(34, 228)
        Me.lblC.Name = "lblC"
        Me.lblC.Size = New System.Drawing.Size(594, 27)
        Me.lblC.TabIndex = 9
        Me.lblC.Text = " "
        Me.lblC.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cbbSite
        '
        Me.cbbSite.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cbbSite.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbbSite.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbbSite.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.cbbSite.FormattingEnabled = True
        Me.cbbSite.Location = New System.Drawing.Point(105, 52)
        Me.cbbSite.Name = "cbbSite"
        Me.cbbSite.Size = New System.Drawing.Size(375, 28)
        Me.cbbSite.TabIndex = 10
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.Location = New System.Drawing.Point(10, 20)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(104, 19)
        Me.Label2.TabIndex = 13
        Me.Label2.Text = "Station Type:"
        '
        'rbOil
        '
        Me.rbOil.AutoSize = True
        Me.rbOil.Checked = True
        Me.rbOil.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.rbOil.Location = New System.Drawing.Point(120, 20)
        Me.rbOil.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.rbOil.Name = "rbOil"
        Me.rbOil.Size = New System.Drawing.Size(40, 21)
        Me.rbOil.TabIndex = 12
        Me.rbOil.TabStop = True
        Me.rbOil.Text = "Oil"
        Me.rbOil.UseVisualStyleBackColor = True
        '
        'rbNgv
        '
        Me.rbNgv.AutoSize = True
        Me.rbNgv.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.rbNgv.Location = New System.Drawing.Point(180, 20)
        Me.rbNgv.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.rbNgv.Name = "rbNgv"
        Me.rbNgv.Size = New System.Drawing.Size(51, 21)
        Me.rbNgv.TabIndex = 11
        Me.rbNgv.Text = "Ngv"
        Me.rbNgv.UseVisualStyleBackColor = True
        '
        'dtpStartDate
        '
        Me.dtpStartDate.CustomFormat = "yyyy-MM-dd"
        Me.dtpStartDate.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpStartDate.Location = New System.Drawing.Point(106, 86)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.Size = New System.Drawing.Size(168, 27)
        Me.dtpStartDate.TabIndex = 14
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.Label3.Location = New System.Drawing.Point(52, 92)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(47, 19)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "Date:"
        '
        'dtpToDate
        '
        Me.dtpToDate.CustomFormat = "yyyy-MM-dd"
        Me.dtpToDate.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpToDate.Location = New System.Drawing.Point(313, 86)
        Me.dtpToDate.Name = "dtpToDate"
        Me.dtpToDate.Size = New System.Drawing.Size(167, 27)
        Me.dtpToDate.TabIndex = 17
        '
        'Timer1
        '
        '
        'lbNoEod
        '
        Me.lbNoEod.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.lbNoEod.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.lbNoEod.Location = New System.Drawing.Point(34, 199)
        Me.lbNoEod.Name = "lbNoEod"
        Me.lbNoEod.Size = New System.Drawing.Size(594, 27)
        Me.lbNoEod.TabIndex = 18
        Me.lbNoEod.Text = " "
        Me.lbNoEod.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'chkNoEOD
        '
        Me.chkNoEOD.AutoSize = True
        Me.chkNoEOD.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.chkNoEOD.Location = New System.Drawing.Point(105, 126)
        Me.chkNoEOD.Name = "chkNoEOD"
        Me.chkNoEOD.Size = New System.Drawing.Size(144, 24)
        Me.chkNoEOD.TabIndex = 19
        Me.chkNoEOD.Text = "Update No EOD"
        Me.chkNoEOD.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.rdoIp)
        Me.GroupBox1.Controls.Add(Me.rdoSiteNo)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txtDestAddress)
        Me.GroupBox1.Controls.Add(Me.btnSendByReceiptNBR)
        Me.GroupBox1.Controls.Add(Me.txtReceiptNBR)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(38, 322)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(590, 151)
        Me.GroupBox1.TabIndex = 26
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Send by Receipt NBR"
        '
        'rdoIp
        '
        Me.rdoIp.AutoSize = True
        Me.rdoIp.Location = New System.Drawing.Point(285, 79)
        Me.rdoIp.Name = "rdoIp"
        Me.rdoIp.Size = New System.Drawing.Size(80, 24)
        Me.rdoIp.TabIndex = 28
        Me.rdoIp.Text = "IP Addr"
        Me.rdoIp.UseVisualStyleBackColor = True
        '
        'rdoSiteNo
        '
        Me.rdoSiteNo.AutoSize = True
        Me.rdoSiteNo.Checked = True
        Me.rdoSiteNo.Location = New System.Drawing.Point(184, 80)
        Me.rdoSiteNo.Name = "rdoSiteNo"
        Me.rdoSiteNo.Size = New System.Drawing.Size(79, 24)
        Me.rdoSiteNo.TabIndex = 27
        Me.rdoSiteNo.TabStop = True
        Me.rdoSiteNo.Text = "Site No"
        Me.rdoSiteNo.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.Label4.Location = New System.Drawing.Point(27, 112)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(132, 19)
        Me.Label4.TabIndex = 32
        Me.Label4.Text = "Destination Value"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.Label6.Location = New System.Drawing.Point(31, 81)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(128, 19)
        Me.Label6.TabIndex = 30
        Me.Label6.Text = "Destination Type"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.Label5.Location = New System.Drawing.Point(64, 42)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(95, 19)
        Me.Label5.TabIndex = 29
        Me.Label5.Text = "Receipt NBR"
        '
        'txtDestAddress
        '
        Me.txtDestAddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtDestAddress.Location = New System.Drawing.Point(181, 109)
        Me.txtDestAddress.Name = "txtDestAddress"
        Me.txtDestAddress.Size = New System.Drawing.Size(238, 26)
        Me.txtDestAddress.TabIndex = 29
        '
        'btnSendByReceiptNBR
        '
        Me.btnSendByReceiptNBR.Location = New System.Drawing.Point(455, 73)
        Me.btnSendByReceiptNBR.Name = "btnSendByReceiptNBR"
        Me.btnSendByReceiptNBR.Size = New System.Drawing.Size(93, 58)
        Me.btnSendByReceiptNBR.TabIndex = 30
        Me.btnSendByReceiptNBR.Text = "Send"
        Me.btnSendByReceiptNBR.UseVisualStyleBackColor = True
        '
        'txtReceiptNBR
        '
        Me.txtReceiptNBR.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtReceiptNBR.Location = New System.Drawing.Point(181, 42)
        Me.txtReceiptNBR.Name = "txtReceiptNBR"
        Me.txtReceiptNBR.Size = New System.Drawing.Size(238, 26)
        Me.txtReceiptNBR.TabIndex = 26
        '
        'chbFixIp
        '
        Me.chbFixIp.AutoSize = True
        Me.chbFixIp.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.chbFixIp.Location = New System.Drawing.Point(264, 126)
        Me.chbFixIp.Name = "chbFixIp"
        Me.chbFixIp.Size = New System.Drawing.Size(71, 24)
        Me.chbFixIp.TabIndex = 27
        Me.chbFixIp.Text = "Fix IP:"
        Me.chbFixIp.UseVisualStyleBackColor = True
        '
        'cbbFixIp
        '
        Me.cbbFixIp.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cbbFixIp.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbbFixIp.Enabled = False
        Me.cbbFixIp.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.cbbFixIp.FormattingEnabled = True
        Me.cbbFixIp.Location = New System.Drawing.Point(341, 126)
        Me.cbbFixIp.Name = "cbbFixIp"
        Me.cbbFixIp.Size = New System.Drawing.Size(139, 28)
        Me.cbbFixIp.TabIndex = 28
        '
        'chkNoQ
        '
        Me.chkNoQ.AutoSize = True
        Me.chkNoQ.Checked = True
        Me.chkNoQ.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkNoQ.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.chkNoQ.Location = New System.Drawing.Point(510, 56)
        Me.chkNoQ.Name = "chkNoQ"
        Me.chkNoQ.Size = New System.Drawing.Size(152, 24)
        Me.chkNoQ.TabIndex = 29
        Me.chkNoQ.Text = "Update Summary"
        Me.chkNoQ.UseVisualStyleBackColor = True
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(680, 502)
        Me.Controls.Add(Me.chkNoQ)
        Me.Controls.Add(Me.cbbFixIp)
        Me.Controls.Add(Me.chbFixIp)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.chkNoEOD)
        Me.Controls.Add(Me.lbNoEod)
        Me.Controls.Add(Me.dtpToDate)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.dtpStartDate)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.rbOil)
        Me.Controls.Add(Me.rbNgv)
        Me.Controls.Add(Me.cbbSite)
        Me.Controls.Add(Me.lblC)
        Me.Controls.Add(Me.lblD)
        Me.Controls.Add(Me.btnSend)
        Me.Controls.Add(Me.lbStatus)
        Me.Controls.Add(Me.pbLoad)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "FTS Reconcile By Site"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnSend As System.Windows.Forms.Button
    Friend WithEvents lbStatus As System.Windows.Forms.Label
    Friend WithEvents pbLoad As System.Windows.Forms.ProgressBar
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblD As System.Windows.Forms.Label
    Friend WithEvents lblC As System.Windows.Forms.Label
    Friend WithEvents cbbSite As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents rbOil As System.Windows.Forms.RadioButton
    Friend WithEvents rbNgv As System.Windows.Forms.RadioButton
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dtpToDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents lbNoEod As System.Windows.Forms.Label
    Friend WithEvents chkNoEOD As CheckBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents txtDestAddress As TextBox
    Friend WithEvents btnSendByReceiptNBR As Button
    Friend WithEvents txtReceiptNBR As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents rdoIp As RadioButton
    Friend WithEvents rdoSiteNo As RadioButton
    Friend WithEvents chbFixIp As CheckBox
    Friend WithEvents cbbFixIp As ComboBox
    Friend WithEvents chkNoQ As CheckBox
End Class
