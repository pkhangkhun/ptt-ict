Imports System.Data.SqlClient

Namespace App.Dao

    Public Class QueryDao
        Inherits BaseDao

        Public Shared Sub sqlUpdate(ByVal sql As String)

            Dim cn As SqlConnection = GetDBConnection
            cn.Open()
            Try
                Dim command As New SqlCommand(sql, cn)
                command.CommandType = CommandType.Text
                command.ExecuteNonQuery()
            Catch ex As Exception
                Throw ex
            Finally
                cn.Close()
            End Try

        End Sub

        Public Shared Function sqlInquiry(ByVal sql As String) As DataSet
            Dim ds As New DataSet
            Dim cn As SqlConnection = GetDBConnection()
            cn.Open()

            Dim command As New SqlCommand(sql, cn)
            command.CommandType = CommandType.Text


            Dim dap As SqlDataAdapter = New SqlDataAdapter()
            dap.SelectCommand = command

            Try
                dap.Fill(ds, "MASTER")
            Catch ex As Exception
                Throw ex
            Finally
                dap.Dispose()
                cn.Close()
            End Try

            Return ds
        End Function

    End Class

End Namespace