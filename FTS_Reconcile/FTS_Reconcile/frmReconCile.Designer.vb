<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReconCile
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnSend = New System.Windows.Forms.Button
        Me.btnExit = New System.Windows.Forms.Button
        Me.dgvReconcile = New System.Windows.Forms.DataGridView
        Me.site_code = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.site_name = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.SHIFT_NO = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Diff = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.btnSeach = New System.Windows.Forms.Button
        Me.rbNgv = New System.Windows.Forms.RadioButton
        Me.rbOil = New System.Windows.Forms.RadioButton
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker
        Me.cbbSite = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar
        Me.Label4 = New System.Windows.Forms.Label
        CType(Me.dgvReconcile, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnSend
        '
        Me.btnSend.Location = New System.Drawing.Point(439, 13)
        Me.btnSend.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnSend.Name = "btnSend"
        Me.btnSend.Size = New System.Drawing.Size(106, 36)
        Me.btnSend.TabIndex = 0
        Me.btnSend.Text = "�觢�����"
        Me.btnSend.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(439, 57)
        Me.btnExit.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(106, 36)
        Me.btnExit.TabIndex = 1
        Me.btnExit.Text = "�͡"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'dgvReconcile
        '
        Me.dgvReconcile.AllowUserToAddRows = False
        Me.dgvReconcile.AllowUserToDeleteRows = False
        Me.dgvReconcile.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvReconcile.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.site_code, Me.site_name, Me.SHIFT_NO, Me.Diff})
        Me.dgvReconcile.Location = New System.Drawing.Point(12, 190)
        Me.dgvReconcile.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.dgvReconcile.Name = "dgvReconcile"
        Me.dgvReconcile.Size = New System.Drawing.Size(728, 282)
        Me.dgvReconcile.TabIndex = 2
        '
        'site_code
        '
        Me.site_code.HeaderText = "site_code"
        Me.site_code.Name = "site_code"
        '
        'site_name
        '
        Me.site_name.FillWeight = 200.0!
        Me.site_name.HeaderText = "site_name"
        Me.site_name.Name = "site_name"
        Me.site_name.Width = 200
        '
        'SHIFT_NO
        '
        Me.SHIFT_NO.FillWeight = 150.0!
        Me.SHIFT_NO.HeaderText = "SHIFT_NO"
        Me.SHIFT_NO.Name = "SHIFT_NO"
        Me.SHIFT_NO.Width = 150
        '
        'Diff
        '
        Me.Diff.HeaderText = "Diff"
        Me.Diff.Name = "Diff"
        '
        'btnSeach
        '
        Me.btnSeach.Location = New System.Drawing.Point(436, 133)
        Me.btnSeach.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnSeach.Name = "btnSeach"
        Me.btnSeach.Size = New System.Drawing.Size(151, 49)
        Me.btnSeach.TabIndex = 3
        Me.btnSeach.Text = "��Ǩ�ͺ"
        Me.btnSeach.UseVisualStyleBackColor = True
        '
        'rbNgv
        '
        Me.rbNgv.AutoSize = True
        Me.rbNgv.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.rbNgv.Location = New System.Drawing.Point(131, 7)
        Me.rbNgv.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.rbNgv.Name = "rbNgv"
        Me.rbNgv.Size = New System.Drawing.Size(51, 21)
        Me.rbNgv.TabIndex = 5
        Me.rbNgv.Text = "Ngv"
        Me.rbNgv.UseVisualStyleBackColor = True
        '
        'rbOil
        '
        Me.rbOil.AutoSize = True
        Me.rbOil.Checked = True
        Me.rbOil.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.rbOil.Location = New System.Drawing.Point(85, 7)
        Me.rbOil.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.rbOil.Name = "rbOil"
        Me.rbOil.Size = New System.Drawing.Size(40, 21)
        Me.rbOil.TabIndex = 6
        Me.rbOil.TabStop = True
        Me.rbOil.Text = "Oil"
        Me.rbOil.UseVisualStyleBackColor = True
        '
        'dtpStartDate
        '
        Me.dtpStartDate.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.dtpStartDate.Location = New System.Drawing.Point(83, 66)
        Me.dtpStartDate.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.Size = New System.Drawing.Size(326, 24)
        Me.dtpStartDate.TabIndex = 7
        '
        'cbbSite
        '
        Me.cbbSite.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.cbbSite.FormattingEnabled = True
        Me.cbbSite.Location = New System.Drawing.Point(83, 36)
        Me.cbbSite.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.cbbSite.Name = "cbbSite"
        Me.cbbSite.Size = New System.Drawing.Size(326, 24)
        Me.cbbSite.TabIndex = 8
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.Label1.Location = New System.Drawing.Point(26, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 17)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "������"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.Label2.Location = New System.Drawing.Point(43, 72)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(34, 17)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "�ѹ���"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.Label3.Location = New System.Drawing.Point(36, 39)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 17)
        Me.Label3.TabIndex = 11
        Me.Label3.Text = "ʶҹ�"
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(12, 513)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(422, 13)
        Me.ProgressBar1.TabIndex = 12
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.Label4.Location = New System.Drawing.Point(12, 493)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(105, 17)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "���ѧ�觢����� ..."
        '
        'frmReconCile
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(557, 107)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cbbSite)
        Me.Controls.Add(Me.dtpStartDate)
        Me.Controls.Add(Me.rbOil)
        Me.Controls.Add(Me.rbNgv)
        Me.Controls.Add(Me.btnSeach)
        Me.Controls.Add(Me.dgvReconcile)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnSend)
        Me.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmReconCile"
        Me.Text = "frmReconCile"
        CType(Me.dgvReconcile, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnSend As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents dgvReconcile As System.Windows.Forms.DataGridView
    Friend WithEvents btnSeach As System.Windows.Forms.Button
    Friend WithEvents rbNgv As System.Windows.Forms.RadioButton
    Friend WithEvents rbOil As System.Windows.Forms.RadioButton
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents cbbSite As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents site_code As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents site_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SHIFT_NO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Diff As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
