Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Runtime.InteropServices

Namespace App.Utility

    Public Class DBUtil

        Public Shared Function GetDBConnection() As SqlConnection
            ' Return the SQL Server DB Connection
            ' To be changed to get this from a config file.
            Dim connectionString As String = System.Configuration.ConfigurationManager.AppSettings.Get("ConnectionString")
            Return New SqlConnection(connectionString)
        End Function

        Public Shared Function SqlQuote(ByVal sqlStr As String) As String
            'takes SQL wildcard and quote characters and doubles them to make literal
            'e.g. "O'Brian" >> "O''Brian"
            Dim s As String = ""
            If sqlStr.Length > 0 Then
                If sqlStr.Substring(0, 1) <> "'" Then
                    sqlStr = "'" + sqlStr
                End If
                If sqlStr.Substring(sqlStr.Length - 1, 1) <> "'" Then
                    sqlStr += "'"
                End If
                Dim i As Integer
                Dim tempChar As String

                For i = 2 To Len(sqlStr) - 1
                    tempChar = Mid$(sqlStr, i, 1)
                    Select Case tempChar
                        Case "'", "*", "?" 'modify this list for your db specific version of SQL
                            s = s & tempChar & tempChar
                        Case Else
                            s = s & tempChar
                    End Select
                Next
            End If
            Return "'" + s + "'"
        End Function

        Public Shared Function sqlLikeText(ByVal searchText As String) As String
            Return SqlQuote("%" + searchText.Trim().Replace(" ", "%") + "%")
        End Function

        Public Shared Function sqlLikeParm(ByVal searchText As String) As String
            Return "%" + searchText.Trim().Replace(" ", "%") + "%"
        End Function
    End Class

End Namespace
