﻿Namespace fts.constant
    Public Class FTSConstant

        Public Enum statusLoad As Integer
            NotSelect
            NotFound
            FoundDiff
            NoEOD
            Fail
            Complate
        End Enum

        Public Class StatusConstant
            Shared ReadOnly Property COMPATE As String = "C"
            Shared ReadOnly Property DIFF As String = "D"
            Shared ReadOnly Property NOT_SEND_EOD As String = "X"
            Shared ReadOnly Property WAITING As String = "W"
        End Class
    End Class
End Namespace
