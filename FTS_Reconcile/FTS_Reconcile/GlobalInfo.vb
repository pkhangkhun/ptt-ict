﻿Module GlobalInfo

    Public SITE_LIST As Hashtable

    Public isUI As Boolean = False

    Public Class StationConstant
        Public Shared STATION_TYPE_OID As Integer = 1
        Public Shared STATION_TYPE_NGV As Integer = 2
    End Class

    Public Function getStatusList() As BindingSource
        Dim comboSource As New Dictionary(Of String, String)
        comboSource.Add("-", "All")
        comboSource.Add("C", "Complete")
        comboSource.Add("X", "Not found EOD")
        comboSource.Add("D", "Diff Site <--> HQ")

        Return New BindingSource(comboSource, Nothing)
    End Function
End Module
