<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInsertNoEod
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.Button1 = New System.Windows.Forms.Button
        Me.lbStatus = New System.Windows.Forms.Label
        Me.pbLoad = New System.Windows.Forms.ProgressBar
        Me.Label1 = New System.Windows.Forms.Label
        Me.lblD = New System.Windows.Forms.Label
        Me.lblC = New System.Windows.Forms.Label
        Me.cbbSite = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.rbOil = New System.Windows.Forms.RadioButton
        Me.rbNgv = New System.Windows.Forms.RadioButton
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker
        Me.Label3 = New System.Windows.Forms.Label
        Me.dtpToDate = New System.Windows.Forms.DateTimePicker
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.Button1.Location = New System.Drawing.Point(485, 29)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(109, 74)
        Me.Button1.TabIndex = 7
        Me.Button1.Text = "reconcile"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'lbStatus
        '
        Me.lbStatus.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.lbStatus.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.lbStatus.Location = New System.Drawing.Point(11, 140)
        Me.lbStatus.Name = "lbStatus"
        Me.lbStatus.Size = New System.Drawing.Size(594, 27)
        Me.lbStatus.TabIndex = 6
        Me.lbStatus.Text = " "
        Me.lbStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'pbLoad
        '
        Me.pbLoad.Location = New System.Drawing.Point(11, 228)
        Me.pbLoad.Name = "pbLoad"
        Me.pbLoad.Size = New System.Drawing.Size(594, 12)
        Me.pbLoad.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.Label1.Location = New System.Drawing.Point(23, 55)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(57, 19)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "ʶҹ� :"
        '
        'lblD
        '
        Me.lblD.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.lblD.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.lblD.Location = New System.Drawing.Point(11, 196)
        Me.lblD.Name = "lblD"
        Me.lblD.Size = New System.Drawing.Size(594, 27)
        Me.lblD.TabIndex = 8
        Me.lblD.Text = " "
        Me.lblD.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblC
        '
        Me.lblC.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.lblC.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.lblC.Location = New System.Drawing.Point(11, 168)
        Me.lblC.Name = "lblC"
        Me.lblC.Size = New System.Drawing.Size(594, 27)
        Me.lblC.TabIndex = 9
        Me.lblC.Text = " "
        Me.lblC.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cbbSite
        '
        Me.cbbSite.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.cbbSite.FormattingEnabled = True
        Me.cbbSite.Location = New System.Drawing.Point(83, 52)
        Me.cbbSite.Name = "cbbSite"
        Me.cbbSite.Size = New System.Drawing.Size(375, 28)
        Me.cbbSite.TabIndex = 10
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.Label2.Location = New System.Drawing.Point(10, 20)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(70, 19)
        Me.Label2.TabIndex = 13
        Me.Label2.Text = "������ :"
        '
        'rbOil
        '
        Me.rbOil.AutoSize = True
        Me.rbOil.Checked = True
        Me.rbOil.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.rbOil.Location = New System.Drawing.Point(83, 20)
        Me.rbOil.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.rbOil.Name = "rbOil"
        Me.rbOil.Size = New System.Drawing.Size(40, 21)
        Me.rbOil.TabIndex = 12
        Me.rbOil.TabStop = True
        Me.rbOil.Text = "Oil"
        Me.rbOil.UseVisualStyleBackColor = True
        '
        'rbNgv
        '
        Me.rbNgv.AutoSize = True
        Me.rbNgv.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.rbNgv.Location = New System.Drawing.Point(144, 21)
        Me.rbNgv.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.rbNgv.Name = "rbNgv"
        Me.rbNgv.Size = New System.Drawing.Size(51, 21)
        Me.rbNgv.TabIndex = 11
        Me.rbNgv.Text = "Ngv"
        Me.rbNgv.UseVisualStyleBackColor = True
        '
        'dtpStartDate
        '
        Me.dtpStartDate.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.dtpStartDate.Location = New System.Drawing.Point(83, 86)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.Size = New System.Drawing.Size(177, 27)
        Me.dtpStartDate.TabIndex = 14
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.Label3.Location = New System.Drawing.Point(33, 92)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(49, 19)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "�ѹ��� :"
        '
        'dtpToDate
        '
        Me.dtpToDate.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.dtpToDate.Location = New System.Drawing.Point(281, 86)
        Me.dtpToDate.Name = "dtpToDate"
        Me.dtpToDate.Size = New System.Drawing.Size(177, 27)
        Me.dtpToDate.TabIndex = 17
        '
        'Timer1
        '
        '
        'frmInsertNoEod
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(617, 252)
        Me.Controls.Add(Me.dtpToDate)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.dtpStartDate)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.rbOil)
        Me.Controls.Add(Me.rbNgv)
        Me.Controls.Add(Me.cbbSite)
        Me.Controls.Add(Me.lblC)
        Me.Controls.Add(Me.lblD)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.lbStatus)
        Me.Controls.Add(Me.pbLoad)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.Name = "frmInsertNoEod"
        Me.Text = "frmInsertNoEod"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents lbStatus As System.Windows.Forms.Label
    Friend WithEvents pbLoad As System.Windows.Forms.ProgressBar
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblD As System.Windows.Forms.Label
    Friend WithEvents lblC As System.Windows.Forms.Label
    Friend WithEvents cbbSite As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents rbOil As System.Windows.Forms.RadioButton
    Friend WithEvents rbNgv As System.Windows.Forms.RadioButton
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dtpToDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
End Class
