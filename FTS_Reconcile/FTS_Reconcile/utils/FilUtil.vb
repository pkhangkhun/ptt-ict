﻿Imports System.IO

Public Class FileUtil
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

    Public Shared Sub DeleteAllFile(ByVal folderDir As String)
        'delete all file in folder
        DeleteAllFile(folderDir, "*.*")
    End Sub

    Public Shared Sub DeleteAllFile(ByVal folderDir As String, ByVal pattern As String)
        Dim fileName As String
        Dim files() As String
        Dim i As Integer

        Try
            If Not Directory.Exists(folderDir) Then
                log.Warn("Not found delete folder")
                Exit Sub
            End If

            files = Directory.GetFiles(folderDir, pattern)
            i = files.Length
            While i > 0
                fileName = files(files.Length - i)

                If File.Exists(fileName) Then
                    File.Delete(fileName)
                End If
                i -= 1
            End While
        Catch ex As Exception
            log.Error("Error delete log file : {0}", ex)
        End Try
    End Sub

    Public Shared Sub DeleteLogFile(ByVal logDir As String, ByVal numOfKept As Integer)
        Dim fileName As String
        Dim files() As String
        Dim i As Integer

        Try
            If Not Directory.Exists(logDir) Then
                log.Warn("Not found delete log folder")
                Exit Sub
            End If

            files = Directory.GetFiles(logDir)
            Array.Sort(files)

            i = files.Length
            While i > numOfKept AndAlso numOfKept > 0
                fileName = files(files.Length - i)

                If File.Exists(fileName) Then
                    File.Delete(fileName)
                End If
                i -= 1
            End While
        Catch ex As Exception
            log.Error("Error delete log file : {0}", ex)
        End Try
    End Sub
End Class
