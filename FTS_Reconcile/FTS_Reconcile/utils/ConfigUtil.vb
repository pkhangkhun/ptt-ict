﻿Imports System.Collections.Specialized

Namespace fts.util
    Public Class ConfigUtil
        Public Shared Function getString(key As String) As String
            Dim str As String = System.Configuration.ConfigurationManager.AppSettings.Get(key)
            Return str
        End Function

        Public Shared Function getString(section As String, key As String) As String
            'System.Configuration.ConfigurationManager.RefreshSection(section)
            Dim value As NameValueCollection = System.Configuration.ConfigurationManager.GetSection(section)

            Dim str As String = value(key)
            Return str
        End Function
    End Class

End Namespace
