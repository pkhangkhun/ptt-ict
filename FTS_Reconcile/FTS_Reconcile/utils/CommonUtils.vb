﻿Imports FTS_Reconcile.Helper.Services

Namespace fts.util

    Public Class CommonUtils

        Public Shared Function IsIpAddressValid(IpAddress As String) As Boolean
            Return System.Net.IPAddress.TryParse(IpAddress, Nothing)
        End Function

        Public Shared Sub checkRemoteAddrIsValid(ipAddress As String, port As Integer)
            Dim helper As New HelperService
            Dim statusChk As Integer
            Dim msg(4) As String
            msg(0) = "success"
            msg(1) = "occure on: wrong ip, server wrong port, server service stoped"
            msg(2) = "occure on: local wrong port, local service stoped"
            msg(3) = "Server Down"
            msg(4) = "NetworkUnreachable"

            '1: connect success And service started
            '2: cannot connect Or firewall blocked
            '3: connect success And service stoped
            '4: NetworkUnreachable
            statusChk = helper.CheckServiceStatus(ipAddress, port)

            If statusChk <> 0 Then
                Throw New Exception(String.Format("{0} - {1}", statusChk, msg(statusChk)))
            End If
        End Sub
    End Class

End Namespace
