﻿Imports Ie.IeComponent.BO
Imports Ie.UI.Utility
Imports System.Data.SqlClient
Imports FTS_Reconcile.HQComponent.BO
Imports FTS_Reconcile.App.Utility

Namespace App.Dao

    Public Class SummaryReconcileDao
        Inherits BaseDao

        Private Class ColumnName
            Public Const ObjectId = "OBJECT_ID"
            Public Const SiteTypeId = "SITE_TYPE_ID"
            Public Const SiteNo = "SITE_NO"
            Public Const SiteId = "SITE_ID"
            Public Const StationId = "STATION_ID"
            Public Const ShiftNo = "SHIFT_NO"
            Public Const DocDate = "DOC_DATE"
            Public Const CreateDate = "CREATE_DATE"
            Public Const FullTaxQty = "FULL_TAX_QTY"
            Public Const RFullTaxQty = "R_FULL_TAX_QTY"
            Public Const DFullTaxQty = "D_FULL_TAX_QTY"
            Public Const CancelQty = "CANCEL_QTY"
            Public Const RCancelQty = "R_CANCEL_QTY"
            Public Const DCancelQty = "D_CANCEL_QTY"
            Public Const VoidQty = "VOID_QTY"
            Public Const RVoidQty = "R_VOID_QTY"
            Public Const DVoidQty = "D_VOID_QTY"
            Public Const ReplaceQty = "REPLACE_QTY"
            Public Const RReplaceQty = "R_REPLACE_QTY"
            Public Const DReplaceQty = "D_REPLACE_QTY"
            Public Const CnQty = "CN_QTY"
            Public Const RCnQty = "R_CN_QTY"
            Public Const DCnQty = "D_CN_QTY"
            Public Const FleetQty = "FLEET_QTY"
            Public Const RFleetQty = "R_FLEET_QTY"
            Public Const DFleetQty = "D_FLEET_QTY"
            Public Const ManualQty = "MANUAL_QTY"
            Public Const RManualQty = "R_MANUAL_QTY"
            Public Const DManualQty = "D_MANUAL_QTY"
            Public Const ScanQty = "SCAN_QTY"
            Public Const RScanQty = "R_SCAN_QTY"
            Public Const DScanQty = "D_SCAN_QTY"
            Public Const FuelQty = "FUEL_QTY"
            Public Const RFuelQty = "R_FUEL_QTY"
            Public Const DFuelQty = "D_FUEL_QTY"
            Public Const GoodsQty = "GOODS_QTY"
            Public Const RGoodsQty = "R_GOODS_QTY"
            Public Const DGoodsQty = "D_GOODS_QTY"
            Public Const Status = "STATUS"
            Public Const StartFullTaxNbr = "START_FULL_TAX_NBR"
            Public Const EndFullTaxNbr = "END_FULL_TAX_NBR"
            Public Const StartManualNbr = "START_MANUAL_NBR"
            Public Const EndManualNbr = "END_MANUAL_NBR"
            Public Const StartCnNbr = "START_CN_NBR"
            Public Const EndCnNbr = "END_CN_NBR"

            Public Const CustomerQty = "CUSTOMER_QTY"
            Public Const CustPlateQty = "CUST_PLATE_QTY"

            Public Const CustomerHqQty = "Customer_Hq_Qty"
            Public Const CustPlateHqQty = "Cust_Plate_Hq_Qty"
        End Class

        Private Class ParamName
            Public Const ObjectId = "@ObjectId"
            Public Const SiteTypeId = "@SiteTypeId"
            Public Const SiteNo = "@SiteNo"
            Public Const SiteId = "@SiteId"
            Public Const StationId = "@StationId"
            Public Const ShiftNo = "@ShiftNo"
            Public Const DocDate = "@DocDate"
            Public Const CreateDate = "@CreateDate"
            Public Const FullTaxQty = "@FullTaxQty"
            Public Const RFullTaxQty = "@RFullTaxQty"
            Public Const DFullTaxQty = "@DFullTaxQty"
            Public Const CancelQty = "@CancelQty"
            Public Const RCancelQty = "@RCancelQty"
            Public Const DCancelQty = "@DCancelQty"
            Public Const VoidQty = "@VoidQty"
            Public Const RVoidQty = "@RVoidQty"
            Public Const DVoidQty = "@DVoidQty"
            Public Const ReplaceQty = "@ReplaceQty"
            Public Const RReplaceQty = "@RReplaceQty"
            Public Const DReplaceQty = "@DReplaceQty"
            Public Const CnQty = "@CnQty"
            Public Const RCnQty = "@RCnQty"
            Public Const DCnQty = "@DCnQty"
            Public Const FleetQty = "@FleetQty"
            Public Const RFleetQty = "@RFleetQty"
            Public Const DFleetQty = "@DFleetQty"
            Public Const ManualQty = "@ManualQty"
            Public Const RManualQty = "@RManualQty"
            Public Const DManualQty = "@DManualQty"
            Public Const ScanQty = "@ScanQty"
            Public Const RScanQty = "@RScanQty"
            Public Const DScanQty = "@DScanQty"
            Public Const FuelQty = "@FuelQty"
            Public Const RFuelQty = "@RFuelQty"
            Public Const DFuelQty = "@DFuelQty"
            Public Const GoodsQty = "@GoodsQty"
            Public Const RGoodsQty = "@RGoodsQty"
            Public Const DGoodsQty = "@DGoodsQty"
            Public Const Status = "@Status"
            Public Const StartFullTaxNbr = "@StartFullTaxNbr"
            Public Const EndFullTaxNbr = "@EndFullTaxNbr"
            Public Const StartManualNbr = "@StartManualNbr"
            Public Const EndManualNbr = "@EndManualNbr"
            Public Const StartCnNbr = "@StartCnNbr"
            Public Const EndCnNbr = "@EndCnNbr"

            Public Const CustomerQty = "@CustomerQty"
            Public Const CustPlateQty = "@CustPlateQty"
            Public Const CustomerHqQty = "@CustomerHqQty"
            Public Const CustPlateHqQty = "@CustPlateHqQty"
        End Class

        Private Shared Sub MapCommandParameter(ByVal command As SqlCommand, ByVal summaryReconcile As SummaryReconcile)

            command.Parameters.AddWithValue(ParamName.ObjectId, 0)
            If IsDBNull(summaryReconcile.SiteTypeId) Or IsNothing(summaryReconcile.SiteTypeId) Then
                command.Parameters.AddWithValue(ParamName.SiteTypeId, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.SiteTypeId, summaryReconcile.SiteTypeId)
            End If
            If IsDBNull(summaryReconcile.SiteNo) Or IsNothing(summaryReconcile.SiteNo) Then
                command.Parameters.AddWithValue(ParamName.SiteNo, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.SiteNo, summaryReconcile.SiteNo)
            End If
            If IsDBNull(summaryReconcile.SiteId) Or IsNothing(summaryReconcile.SiteId) Then
                command.Parameters.AddWithValue(ParamName.SiteId, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.SiteId, summaryReconcile.SiteId)
            End If
            If IsDBNull(summaryReconcile.StationId) Or IsNothing(summaryReconcile.StationId) Then
                command.Parameters.AddWithValue(ParamName.StationId, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.StationId, summaryReconcile.StationId)
            End If
            If IsDBNull(summaryReconcile.ShiftNo) Or IsNothing(summaryReconcile.ShiftNo) Then
                command.Parameters.AddWithValue(ParamName.ShiftNo, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.ShiftNo, summaryReconcile.ShiftNo)
            End If
            If Not InputValidator.IsDate(summaryReconcile.DocDate) Then
                command.Parameters.AddWithValue(ParamName.DocDate, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.DocDate, summaryReconcile.DocDate)
            End If
            If Not InputValidator.IsDate(summaryReconcile.CreateDate) Then
                command.Parameters.AddWithValue(ParamName.CreateDate, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.CreateDate, summaryReconcile.CreateDate)
            End If
            If IsDBNull(summaryReconcile.FullTaxQty) Or IsNothing(summaryReconcile.FullTaxQty) Then
                command.Parameters.AddWithValue(ParamName.FullTaxQty, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.FullTaxQty, summaryReconcile.FullTaxQty)
            End If
            If IsDBNull(summaryReconcile.RFullTaxQty) Or IsNothing(summaryReconcile.RFullTaxQty) Then
                command.Parameters.AddWithValue(ParamName.RFullTaxQty, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.RFullTaxQty, summaryReconcile.RFullTaxQty)
            End If
            If IsDBNull(summaryReconcile.DFullTaxQty) Or IsNothing(summaryReconcile.DFullTaxQty) Then
                command.Parameters.AddWithValue(ParamName.DFullTaxQty, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.DFullTaxQty, summaryReconcile.DFullTaxQty)
            End If
            If IsDBNull(summaryReconcile.CancelQty) Or IsNothing(summaryReconcile.CancelQty) Then
                command.Parameters.AddWithValue(ParamName.CancelQty, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.CancelQty, summaryReconcile.CancelQty)
            End If
            If IsDBNull(summaryReconcile.RCancelQty) Or IsNothing(summaryReconcile.RCancelQty) Then
                command.Parameters.AddWithValue(ParamName.RCancelQty, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.RCancelQty, summaryReconcile.RCancelQty)
            End If
            If IsDBNull(summaryReconcile.DCancelQty) Or IsNothing(summaryReconcile.DCancelQty) Then
                command.Parameters.AddWithValue(ParamName.DCancelQty, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.DCancelQty, summaryReconcile.DCancelQty)
            End If
            If IsDBNull(summaryReconcile.VoidQty) Or IsNothing(summaryReconcile.VoidQty) Then
                command.Parameters.AddWithValue(ParamName.VoidQty, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.VoidQty, summaryReconcile.VoidQty)
            End If
            If IsDBNull(summaryReconcile.RVoidQty) Or IsNothing(summaryReconcile.RVoidQty) Then
                command.Parameters.AddWithValue(ParamName.RVoidQty, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.RVoidQty, summaryReconcile.RVoidQty)
            End If
            If IsDBNull(summaryReconcile.DVoidQty) Or IsNothing(summaryReconcile.DVoidQty) Then
                command.Parameters.AddWithValue(ParamName.DVoidQty, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.DVoidQty, summaryReconcile.DVoidQty)
            End If
            If IsDBNull(summaryReconcile.ReplaceQty) Or IsNothing(summaryReconcile.ReplaceQty) Then
                command.Parameters.AddWithValue(ParamName.ReplaceQty, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.ReplaceQty, summaryReconcile.ReplaceQty)
            End If
            If IsDBNull(summaryReconcile.RReplaceQty) Or IsNothing(summaryReconcile.RReplaceQty) Then
                command.Parameters.AddWithValue(ParamName.RReplaceQty, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.RReplaceQty, summaryReconcile.RReplaceQty)
            End If
            If IsDBNull(summaryReconcile.DReplaceQty) Or IsNothing(summaryReconcile.DReplaceQty) Then
                command.Parameters.AddWithValue(ParamName.DReplaceQty, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.DReplaceQty, summaryReconcile.DReplaceQty)
            End If
            If IsDBNull(summaryReconcile.CnQty) Or IsNothing(summaryReconcile.CnQty) Then
                command.Parameters.AddWithValue(ParamName.CnQty, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.CnQty, summaryReconcile.CnQty)
            End If
            If IsDBNull(summaryReconcile.RCnQty) Or IsNothing(summaryReconcile.RCnQty) Then
                command.Parameters.AddWithValue(ParamName.RCnQty, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.RCnQty, summaryReconcile.RCnQty)
            End If
            If IsDBNull(summaryReconcile.DCnQty) Or IsNothing(summaryReconcile.DCnQty) Then
                command.Parameters.AddWithValue(ParamName.DCnQty, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.DCnQty, summaryReconcile.DCnQty)
            End If
            If IsDBNull(summaryReconcile.FleetQty) Or IsNothing(summaryReconcile.FleetQty) Then
                command.Parameters.AddWithValue(ParamName.FleetQty, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.FleetQty, summaryReconcile.FleetQty)
            End If
            If IsDBNull(summaryReconcile.RFleetQty) Or IsNothing(summaryReconcile.RFleetQty) Then
                command.Parameters.AddWithValue(ParamName.RFleetQty, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.RFleetQty, summaryReconcile.RFleetQty)
            End If
            If IsDBNull(summaryReconcile.DFleetQty) Or IsNothing(summaryReconcile.DFleetQty) Then
                command.Parameters.AddWithValue(ParamName.DFleetQty, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.DFleetQty, summaryReconcile.DFleetQty)
            End If
            If IsDBNull(summaryReconcile.ManualQty) Or IsNothing(summaryReconcile.ManualQty) Then
                command.Parameters.AddWithValue(ParamName.ManualQty, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.ManualQty, summaryReconcile.ManualQty)
            End If
            If IsDBNull(summaryReconcile.RManualQty) Or IsNothing(summaryReconcile.RManualQty) Then
                command.Parameters.AddWithValue(ParamName.RManualQty, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.RManualQty, summaryReconcile.RManualQty)
            End If
            If IsDBNull(summaryReconcile.DManualQty) Or IsNothing(summaryReconcile.DManualQty) Then
                command.Parameters.AddWithValue(ParamName.DManualQty, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.DManualQty, summaryReconcile.DManualQty)
            End If
            If IsDBNull(summaryReconcile.ScanQty) Or IsNothing(summaryReconcile.ScanQty) Then
                command.Parameters.AddWithValue(ParamName.ScanQty, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.ScanQty, summaryReconcile.ScanQty)
            End If
            If IsDBNull(summaryReconcile.RScanQty) Or IsNothing(summaryReconcile.RScanQty) Then
                command.Parameters.AddWithValue(ParamName.RScanQty, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.RScanQty, summaryReconcile.RScanQty)
            End If
            If IsDBNull(summaryReconcile.DScanQty) Or IsNothing(summaryReconcile.DScanQty) Then
                command.Parameters.AddWithValue(ParamName.DScanQty, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.DScanQty, summaryReconcile.DScanQty)
            End If
            If IsDBNull(summaryReconcile.FuelQty) Or IsNothing(summaryReconcile.FuelQty) Then
                command.Parameters.AddWithValue(ParamName.FuelQty, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.FuelQty, summaryReconcile.FuelQty)
            End If
            If IsDBNull(summaryReconcile.RFuelQty) Or IsNothing(summaryReconcile.RFuelQty) Then
                command.Parameters.AddWithValue(ParamName.RFuelQty, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.RFuelQty, summaryReconcile.RFuelQty)
            End If
            If IsDBNull(summaryReconcile.DFuelQty) Or IsNothing(summaryReconcile.DFuelQty) Then
                command.Parameters.AddWithValue(ParamName.DFuelQty, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.DFuelQty, summaryReconcile.DFuelQty)
            End If
            If IsDBNull(summaryReconcile.GoodsQty) Or IsNothing(summaryReconcile.GoodsQty) Then
                command.Parameters.AddWithValue(ParamName.GoodsQty, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.GoodsQty, summaryReconcile.GoodsQty)
            End If
            If IsDBNull(summaryReconcile.RGoodsQty) Or IsNothing(summaryReconcile.RGoodsQty) Then
                command.Parameters.AddWithValue(ParamName.RGoodsQty, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.RGoodsQty, summaryReconcile.RGoodsQty)
            End If
            If IsDBNull(summaryReconcile.DGoodsQty) Or IsNothing(summaryReconcile.DGoodsQty) Then
                command.Parameters.AddWithValue(ParamName.DGoodsQty, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.DGoodsQty, summaryReconcile.DGoodsQty)
            End If
            If IsDBNull(summaryReconcile.Status) Or IsNothing(summaryReconcile.Status) Then
                command.Parameters.AddWithValue(ParamName.Status, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.Status, summaryReconcile.Status)
            End If
            If IsDBNull(summaryReconcile.StartFullTaxNbr) Or IsNothing(summaryReconcile.StartFullTaxNbr) Then
                command.Parameters.AddWithValue(ParamName.StartFullTaxNbr, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.StartFullTaxNbr, summaryReconcile.StartFullTaxNbr)
            End If
            If IsDBNull(summaryReconcile.EndFullTaxNbr) Or IsNothing(summaryReconcile.EndFullTaxNbr) Then
                command.Parameters.AddWithValue(ParamName.EndFullTaxNbr, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.EndFullTaxNbr, summaryReconcile.EndFullTaxNbr)
            End If
            If IsDBNull(summaryReconcile.StartManualNbr) Or IsNothing(summaryReconcile.StartManualNbr) Then
                command.Parameters.AddWithValue(ParamName.StartManualNbr, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.StartManualNbr, summaryReconcile.StartManualNbr)
            End If
            If IsDBNull(summaryReconcile.EndManualNbr) Or IsNothing(summaryReconcile.EndManualNbr) Then
                command.Parameters.AddWithValue(ParamName.EndManualNbr, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.EndManualNbr, summaryReconcile.EndManualNbr)
            End If
            If IsDBNull(summaryReconcile.StartCnNbr) Or IsNothing(summaryReconcile.StartCnNbr) Then
                command.Parameters.AddWithValue(ParamName.StartCnNbr, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.StartCnNbr, summaryReconcile.StartCnNbr)
            End If
            If IsDBNull(summaryReconcile.EndCnNbr) Or IsNothing(summaryReconcile.EndCnNbr) Then
                command.Parameters.AddWithValue(ParamName.EndCnNbr, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.EndCnNbr, summaryReconcile.EndCnNbr)
            End If

            If IsDBNull(summaryReconcile.CustomerQty) Or IsNothing(summaryReconcile.CustomerQty) Then
                command.Parameters.AddWithValue(ParamName.CustomerQty, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.CustomerQty, summaryReconcile.CustomerQty)
            End If

            If IsDBNull(summaryReconcile.custPlateQty) Or IsNothing(summaryReconcile.custPlateQty) Then
                command.Parameters.AddWithValue(ParamName.CustPlateQty, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.CustPlateQty, summaryReconcile.custPlateQty)
            End If

            If IsDBNull(summaryReconcile.CustomerHqQty) Or IsNothing(summaryReconcile.CustomerHqQty) Then
                command.Parameters.AddWithValue(ParamName.CustomerHqQty, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.CustomerHqQty, summaryReconcile.CustomerHqQty)
            End If

            If IsDBNull(summaryReconcile.custPlateHqQty) Or IsNothing(summaryReconcile.custPlateHqQty) Then
                command.Parameters.AddWithValue(ParamName.CustPlateHqQty, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.CustPlateHqQty, summaryReconcile.custPlateHqQty)
            End If
        End Sub

        Private Shared Sub Populate2(sumR As SummaryReconcile, dr As SqlDataReader)
            If Not (dr.Item("R_Full_Tax_Qty") Is System.DBNull.Value) Then sumR.RFullTaxQty = CInt(dr.Item("R_Full_Tax_Qty"))
            If Not (dr.Item("R_Scan_Qty") Is System.DBNull.Value) Then sumR.RScanQty = CInt(dr.Item("R_Scan_Qty"))
            If Not (dr.Item("R_Void_Qty") Is System.DBNull.Value) Then sumR.RVoidQty = CInt(dr.Item("R_Void_Qty"))
            If Not (dr.Item("R_Replace_Qty") Is System.DBNull.Value) Then sumR.RReplaceQty = CInt(dr.Item("R_Replace_Qty"))
            If Not (dr.Item("R_Cancel_Qty") Is System.DBNull.Value) Then sumR.RCancelQty = CInt(dr.Item("R_Cancel_Qty"))
            If Not (dr.Item("R_Manual_Qty") Is System.DBNull.Value) Then sumR.RManualQty = CInt(dr.Item("R_Manual_Qty"))
            If Not (dr.Item("R_Fleet_Qty") Is System.DBNull.Value) Then sumR.RFleetQty = CInt(dr.Item("R_Fleet_Qty"))
            If Not (dr.Item("R_Fuel_Qty") Is System.DBNull.Value) Then sumR.RFuelQty = CInt(dr.Item("R_Fuel_Qty"))
            If Not (dr.Item("R_Goods_Qty") Is System.DBNull.Value) Then sumR.RGoodsQty = CInt(dr.Item("R_Goods_Qty"))
            If Not (dr.Item("R_Cn_Qty") Is System.DBNull.Value) Then sumR.RCnQty = CInt(dr.Item("R_Cn_Qty"))
            If Not (dr.Item("D_Full_Tax_Qty") Is System.DBNull.Value) Then sumR.DFullTaxQty = CInt(dr.Item("D_Full_Tax_Qty"))
            If Not (dr.Item("D_Scan_Qty") Is System.DBNull.Value) Then sumR.DScanQty = CInt(dr.Item("D_Scan_Qty"))
            If Not (dr.Item("D_Void_Qty") Is System.DBNull.Value) Then sumR.DVoidQty = CInt(dr.Item("D_Void_Qty"))
            If Not (dr.Item("D_Replace_Qty") Is System.DBNull.Value) Then sumR.DReplaceQty = CInt(dr.Item("D_Replace_Qty"))
            If Not (dr.Item("D_Cancel_Qty") Is System.DBNull.Value) Then sumR.DCancelQty = CInt(dr.Item("D_Cancel_Qty"))
            If Not (dr.Item("D_Manual_Qty") Is System.DBNull.Value) Then sumR.DManualQty = CInt(dr.Item("D_Manual_Qty"))
            If Not (dr.Item("D_Fleet_Qty") Is System.DBNull.Value) Then sumR.DFleetQty = CInt(dr.Item("D_Fleet_Qty"))
            If Not (dr.Item("D_Fuel_Qty") Is System.DBNull.Value) Then sumR.DFuelQty = CInt(dr.Item("D_Fuel_Qty"))
            If Not (dr.Item("D_Goods_Qty") Is System.DBNull.Value) Then sumR.DGoodsQty = CInt(dr.Item("D_Goods_Qty"))
            If Not (dr.Item("D_Cn_Qty") Is System.DBNull.Value) Then sumR.DCnQty = CInt(dr.Item("D_Cn_Qty"))
            If Not (dr.Item("STATUS") Is System.DBNull.Value) Then sumR.Status = CStr(dr.Item("STATUS"))
            If Not (dr.Item("SITE_NO") Is System.DBNull.Value) Then sumR.SiteNo = CStr(dr.Item("SITE_NO"))
            If Not (dr.Item("DOC_DATE") Is System.DBNull.Value) Then sumR.DocDate = CDate(dr.Item("DOC_DATE"))

        End Sub

        Private Shared Sub Populate(sumR As SummaryReconcile, dr As SqlDataReader)
            If Not (dr.Item(ColumnName.DocDate) Is System.DBNull.Value) Then sumR.DocDate = CDate(dr.Item(ColumnName.DocDate))
            If Not (dr.Item(ColumnName.CreateDate) Is System.DBNull.Value) Then sumR.CreateDate = CDate(dr.Item(ColumnName.CreateDate))
            If Not (dr.Item(ColumnName.SiteTypeId) Is System.DBNull.Value) Then sumR.SiteTypeId = CStr(dr.Item(ColumnName.SiteTypeId))
            If Not (dr.Item(ColumnName.SiteNo) Is System.DBNull.Value) Then sumR.SiteNo = CStr(dr.Item(ColumnName.SiteNo))
            If Not (dr.Item(ColumnName.SiteId) Is System.DBNull.Value) Then sumR.SiteId = CStr(dr.Item(ColumnName.SiteId))
            If Not (dr.Item(ColumnName.StationId) Is System.DBNull.Value) Then sumR.StationId = CStr(dr.Item(ColumnName.StationId))
            If Not (dr.Item(ColumnName.ShiftNo) Is System.DBNull.Value) Then sumR.ShiftNo = CStr(dr.Item(ColumnName.ShiftNo))
            If Not (dr.Item(ColumnName.FullTaxQty) Is System.DBNull.Value) Then sumR.FullTaxQty = CInt(dr.Item(ColumnName.FullTaxQty))
            If Not (dr.Item(ColumnName.RFullTaxQty) Is System.DBNull.Value) Then sumR.RFullTaxQty = CInt(dr.Item(ColumnName.RFullTaxQty))
            If Not (dr.Item(ColumnName.DFullTaxQty) Is System.DBNull.Value) Then sumR.DFullTaxQty = CInt(dr.Item(ColumnName.DFullTaxQty))
            If Not (dr.Item(ColumnName.CancelQty) Is System.DBNull.Value) Then sumR.CancelQty = CInt(dr.Item(ColumnName.CancelQty))
            If Not (dr.Item(ColumnName.RCancelQty) Is System.DBNull.Value) Then sumR.RCancelQty = CInt(dr.Item(ColumnName.RCancelQty))
            If Not (dr.Item(ColumnName.DCancelQty) Is System.DBNull.Value) Then sumR.DCancelQty = CInt(dr.Item(ColumnName.DCancelQty))
            If Not (dr.Item(ColumnName.VoidQty) Is System.DBNull.Value) Then sumR.VoidQty = CInt(dr.Item(ColumnName.VoidQty))
            If Not (dr.Item(ColumnName.RVoidQty) Is System.DBNull.Value) Then sumR.RVoidQty = CInt(dr.Item(ColumnName.RVoidQty))
            If Not (dr.Item(ColumnName.DVoidQty) Is System.DBNull.Value) Then sumR.DVoidQty = CInt(dr.Item(ColumnName.DVoidQty))
            If Not (dr.Item(ColumnName.ReplaceQty) Is System.DBNull.Value) Then sumR.ReplaceQty = CInt(dr.Item(ColumnName.ReplaceQty))
            If Not (dr.Item(ColumnName.RReplaceQty) Is System.DBNull.Value) Then sumR.RReplaceQty = CInt(dr.Item(ColumnName.RReplaceQty))
            If Not (dr.Item(ColumnName.DReplaceQty) Is System.DBNull.Value) Then sumR.DReplaceQty = CInt(dr.Item(ColumnName.DReplaceQty))
            If Not (dr.Item(ColumnName.CnQty) Is System.DBNull.Value) Then sumR.CnQty = CInt(dr.Item(ColumnName.CnQty))
            If Not (dr.Item(ColumnName.RCnQty) Is System.DBNull.Value) Then sumR.RCnQty = CInt(dr.Item(ColumnName.RCnQty))
            If Not (dr.Item(ColumnName.DCnQty) Is System.DBNull.Value) Then sumR.DCnQty = CInt(dr.Item(ColumnName.DCnQty))
            If Not (dr.Item(ColumnName.FleetQty) Is System.DBNull.Value) Then sumR.FleetQty = CInt(dr.Item(ColumnName.FleetQty))
            If Not (dr.Item(ColumnName.RFleetQty) Is System.DBNull.Value) Then sumR.RFleetQty = CInt(dr.Item(ColumnName.RFleetQty))
            If Not (dr.Item(ColumnName.DFleetQty) Is System.DBNull.Value) Then sumR.DFleetQty = CInt(dr.Item(ColumnName.DFleetQty))
            If Not (dr.Item(ColumnName.ManualQty) Is System.DBNull.Value) Then sumR.ManualQty = CInt(dr.Item(ColumnName.ManualQty))
            If Not (dr.Item(ColumnName.RManualQty) Is System.DBNull.Value) Then sumR.RManualQty = CInt(dr.Item(ColumnName.RManualQty))
            If Not (dr.Item(ColumnName.DManualQty) Is System.DBNull.Value) Then sumR.DManualQty = CInt(dr.Item(ColumnName.DManualQty))
            If Not (dr.Item(ColumnName.ScanQty) Is System.DBNull.Value) Then sumR.ScanQty = CInt(dr.Item(ColumnName.ScanQty))
            If Not (dr.Item(ColumnName.RScanQty) Is System.DBNull.Value) Then sumR.RScanQty = CInt(dr.Item(ColumnName.RScanQty))
            If Not (dr.Item(ColumnName.DScanQty) Is System.DBNull.Value) Then sumR.DScanQty = CInt(dr.Item(ColumnName.DScanQty))
            If Not (dr.Item(ColumnName.FuelQty) Is System.DBNull.Value) Then sumR.FuelQty = CInt(dr.Item(ColumnName.FuelQty))
            If Not (dr.Item(ColumnName.RFuelQty) Is System.DBNull.Value) Then sumR.RFuelQty = CInt(dr.Item(ColumnName.RFuelQty))
            If Not (dr.Item(ColumnName.DFuelQty) Is System.DBNull.Value) Then sumR.DFuelQty = CInt(dr.Item(ColumnName.DFuelQty))
            If Not (dr.Item(ColumnName.GoodsQty) Is System.DBNull.Value) Then sumR.GoodsQty = CInt(dr.Item(ColumnName.GoodsQty))
            If Not (dr.Item(ColumnName.RGoodsQty) Is System.DBNull.Value) Then sumR.RGoodsQty = CInt(dr.Item(ColumnName.RGoodsQty))
            If Not (dr.Item(ColumnName.DGoodsQty) Is System.DBNull.Value) Then sumR.DGoodsQty = CInt(dr.Item(ColumnName.DGoodsQty))
            If Not (dr.Item(ColumnName.Status) Is System.DBNull.Value) Then sumR.Status = CStr(dr.Item(ColumnName.Status))
            If Not (dr.Item(ColumnName.StartFullTaxNbr) Is System.DBNull.Value) Then sumR.StartFullTaxNbr = CStr(dr.Item(ColumnName.StartFullTaxNbr))
            If Not (dr.Item(ColumnName.EndFullTaxNbr) Is System.DBNull.Value) Then sumR.EndFullTaxNbr = CStr(dr.Item(ColumnName.EndFullTaxNbr))
            If Not (dr.Item(ColumnName.StartManualNbr) Is System.DBNull.Value) Then sumR.StartManualNbr = CStr(dr.Item(ColumnName.StartManualNbr))
            If Not (dr.Item(ColumnName.EndManualNbr) Is System.DBNull.Value) Then sumR.EndManualNbr = CStr(dr.Item(ColumnName.EndManualNbr))
            If Not (dr.Item(ColumnName.StartCnNbr) Is System.DBNull.Value) Then sumR.StartCnNbr = CStr(dr.Item(ColumnName.StartCnNbr))
            If Not (dr.Item(ColumnName.EndCnNbr) Is System.DBNull.Value) Then sumR.EndCnNbr = CStr(dr.Item(ColumnName.EndCnNbr))
            If Not (dr.Item(ColumnName.CustomerQty) Is System.DBNull.Value) Then sumR.CustomerQty = CInt(dr.Item(ColumnName.CustomerQty))
            If Not (dr.Item(ColumnName.CustPlateQty) Is System.DBNull.Value) Then sumR.custPlateQty = CInt(dr.Item(ColumnName.CustPlateQty))
            If Not (dr.Item(ColumnName.CustomerHqQty) Is System.DBNull.Value) Then sumR.CustomerHqQty = CInt(dr.Item(ColumnName.CustomerHqQty))
            If Not (dr.Item(ColumnName.CustPlateHqQty) Is System.DBNull.Value) Then sumR.custPlateHqQty = CInt(dr.Item(ColumnName.CustPlateHqQty))

        End Sub



#Region "Common Function Method"

        Public Shared Function getSummary(ByVal siteNo As String, docDate As Date, ByVal cn As SqlConnection) As SummaryReconcileCollection
            Dim sql As String = "SELECT * FROM SUMMARY_RECONCILE"
            sql += " where 1 = 1"
            sql += String.Format(" And {0}={1}", ColumnName.SiteNo, ParamName.SiteNo)
            sql += String.Format(" And {0}={1}", ColumnName.DocDate, ParamName.DocDate)

            Dim command As New SqlCommand(sql, cn)
            command.CommandType = CommandType.Text
            command.Parameters.AddWithValue(ParamName.SiteNo, siteNo)
            command.Parameters.AddWithValue(ParamName.DocDate, docDate)
            Dim dr As SqlDataReader = command.ExecuteReader
            Dim sumRList As New SummaryReconcileCollection
            Dim sumR As SummaryReconcile = Nothing

            Do While dr.Read
                sumR = New SummaryReconcile
                Populate(sumR, dr)
                sumRList.Add(sumR)
                System.Threading.Thread.Sleep(1)
            Loop
            dr.Close()

            Return sumRList
        End Function

        Public Shared Function getSummary(ByVal siteNo As String, fromDate As Date, toDate As Date, ByVal cn As SqlConnection) As SummaryReconcileCollection
            Dim sql As String = "SELECT * FROM SUMMARY_RECONCILE"
            sql += " where 1 = 1"
            sql += String.Format(" And {0} between {1} and {2}", ColumnName.DocDate, "@fromDate", "@toDate")
            If Not String.IsNullOrEmpty(siteNo) AndAlso siteNo <> "0" Then
                sql += String.Format(" And {0}={1}", ColumnName.SiteNo, ParamName.SiteNo)
            End If

            Dim command As New SqlCommand(sql, cn)
            command.CommandType = CommandType.Text
            command.Parameters.AddWithValue("@fromDate", fromDate)
            command.Parameters.AddWithValue("@toDate", toDate)
            If Not String.IsNullOrEmpty(siteNo) AndAlso siteNo <> "0" Then
                command.Parameters.AddWithValue(ParamName.SiteNo, siteNo)
            End If

            Dim dr As SqlDataReader = command.ExecuteReader
            Dim sumRList As New SummaryReconcileCollection
            Dim sumR As SummaryReconcile = Nothing

            Do While dr.Read
                sumR = New SummaryReconcile
                Populate(sumR, dr)
                sumRList.Add(sumR)
            Loop
            dr.Close()

            Return sumRList
        End Function

        Public Shared Sub updateStatus(sumR As SummaryReconcile)

            '// update reconcile to lastest data
            Dim sql As String
            sql = "update SUMMARY_RECONCILE set " &
            "R_Full_Tax_Qty=" + sumR.RFullTaxQty.ToString + "," &
            "R_Scan_Qty=" + sumR.RFullTaxQty.ToString + "," &
            "R_Void_Qty=" + sumR.RVoidQty.ToString + "," &
            "R_Replace_Qty=" + sumR.RReplaceQty.ToString + "," &
            "R_Cancel_Qty=" + sumR.RCancelQty.ToString + "," &
            "R_Manual_Qty=" + sumR.RManualQty.ToString + "," &
            "R_Fleet_Qty=" + sumR.RFleetQty.ToString + "," &
            "R_Fuel_Qty=" + sumR.RFuelQty.ToString + "," &
            "R_Goods_Qty=" + sumR.RGoodsQty.ToString + "," &
            "R_Cn_Qty=" + sumR.RCnQty.ToString + "," &
            "D_Full_Tax_Qty=" + sumR.DFullTaxQty.ToString + "," &
            "D_Scan_Qty=" + sumR.DFullTaxQty.ToString + "," &
            "D_Void_Qty=" + sumR.DVoidQty.ToString + "," &
            "D_Replace_Qty=" + sumR.DReplaceQty.ToString + "," &
            "D_Cancel_Qty=" + sumR.DCancelQty.ToString + "," &
            "D_Manual_Qty=" + sumR.DManualQty.ToString + "," &
            "D_Fleet_Qty=" + sumR.DFleetQty.ToString + "," &
            "D_Fuel_Qty=" + sumR.DFuelQty.ToString + "," &
            "D_Goods_Qty=" + sumR.DGoodsQty.ToString + "," &
            "D_Cn_Qty=" + sumR.DCnQty.ToString + "," &
            "Status='" + sumR.Status + "'" &
            " where 1 = 1" &
            " and site_no = " + DBUtil.SqlQuote(sumR.SiteNo) &
            " and DOC_DATE = " + DBUtil.SqlQuote(sumR.DocDate.ToString("yyyy-MM-dd"))

            QueryDao.sqlUpdate(sql)
        End Sub
#End Region

    End Class

End Namespace
