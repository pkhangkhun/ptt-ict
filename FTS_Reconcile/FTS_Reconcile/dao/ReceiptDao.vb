﻿Imports Ie.IeComponent.BO
Imports System.Data.SqlClient
Imports Ie.UI.Utility
Imports FTS_Reconcile.Domain.BO.UI

Namespace App.Dao

    Public Class ReceiptDao
        Inherits BaseDao

        Private Class ColumnName
            Public Const ObjectId = "OBJECT_ID"
            Public Const ReceiptNbr = "RECEIPT_NBR"
            Public Const PlateNumber = "PLATE_NUMBER"
            Public Const PlateProvinceId = "PLATE_PROVINCE_ID"
            Public Const ReceiptType = "RECEIPT_TYPE"
            Public Const ReceiptDate = "RECEIPT_DATE"
            Public Const UnitPrice = "UNIT_PRICE"
            Public Const UnitName = "UNIT_NAME"
            Public Const ProdCode = "PROD_CODE"
            Public Const Qty = "QTY"
            Public Const ProdName = "PROD_NAME"
            Public Const TotalAmt = "TOTAL_AMT"
            Public Const DiscountAmt = "DISCOUNT_AMT"
            Public Const TotalAfterDiscountAmt = "TOTAL_AFTER_DISCOUNT_AMT"
            Public Const VatAmt = "VAT_AMT"
            Public Const NetAmt = "NET_AMT"
            Public Const PrintNo = "PRINT_NO"
            Public Const VoidReason = "VOID_REASON"
            Public Const IsScaned = "IS_SCANED"
            Public Const VoidUserId = "VOID_USER_ID"
            Public Const RefNbr = "REF_NBR"
            Public Const IsManualEntry = "IS_MANUAL_ENTRY"
            Public Const StationId = "STATION_ID"
            Public Const VoidDttm = "VOID_DTTM"
            Public Const CustType = "CUST_TYPE"
            Public Const IssueDate = "ISSUE_DATE"
            Public Const Status = "STATUS"
            Public Const ShiftNo = "SHIFT_NO"
            Public Const IsConverted = "IS_CONVERTED"
            Public Const ReplaceNo = "REPLACE_NO"
            Public Const ReplaceDate = "REPLACE_DATE"
            Public Const SiteId = "SITE_ID"
            Public Const DistrictCode = "DISTRICT_CODE"
            Public Const RegionCode = "REGION_CODE"
            Public Const LogoImg = "LOGO_IMG"
            Public Const CompanyName = "COMPANY_NAME"
            Public Const CompanyTaxId = "COMPANY_TAX_ID"
            Public Const CompanyBranchNo = "COMPANY_BRANCH_NO"
            Public Const CompanyBranchName = "COMPANY_BRANCH_NAME"
            Public Const CompanyAddress = "COMPANY_ADDRESS"
            Public Const RdId = "RD_ID"
            Public Const PosNbr = "POS_NBR"
            Public Const TranNbr = "TRAN_NBR"
            Public Const PumpNbr = "PUMP_NBR"
            Public Const HashTag = "HASH_TAG"
            Public Const EmployeeName = "EMPLOYEE_NAME"
            Public Const ReplaceRef = "REPLACE_REF"
            Public Const Lcustomer = "LCUSTOMER"
            Public Const Lcardno = "LCARDNO"
            Public Const Ltotalpoint = "LTOTALPOINT"
            Public Const Ltrnpoint = "LTRNPOINT"
            Public Const Lbalance = "LBALANCE"
            Public Const Lrepoint = "LREPOINT"
            Public Const Lpointtoday = "LPOINTTODAY"
            Public Const IsFleetCard = "IS_FLEET_CARD"
            Public Const HostId = "HOST_ID"
            Public Const SaleTypeDesc = "SALE_TYPE_DESC"
            Public Const SiteTypeId = "SITE_TYPE_ID"
            Public Const IsManualFill = "IS_MANUAL_FILL"
            Public Const SiteNo = "SITE_NO"
            Public Const Version = "VERSION"
            Public Const CreateUser = "CREATE_USER"
            Public Const CreateDttm = "CREATE_DTTM"
            Public Const UpdateUser = "UPDATE_USER"
            Public Const UpdateDttm = "UPDATE_DTTM"

            Public Const ReceiptNbrRef = "RECEIPT_NBR_REF"
            Public Const IsDuplicate = "IS_DUPLICATE"
            Public Const CnRef = "CN_REF"
            Public Const FullTaxNbrRef = "FULL_TAX_NBR_REF"
            Public Const ReasonDesc = "REASON_DESC"
        End Class

        Private Class ParamName
            Public Const ObjectId = "@ObjectId"
            Public Const ReceiptNbr = "@ReceiptNbr"
            Public Const PlateNumber = "@PlateNumber"
            Public Const PlateProvinceId = "@PlateProvinceId"
            Public Const ReceiptType = "@ReceiptType"
            Public Const ReceiptDate = "@ReceiptDate"
            Public Const UnitPrice = "@UnitPrice"
            Public Const UnitName = "@UnitName"
            Public Const ProdCode = "@ProdCode"
            Public Const Qty = "@Qty"
            Public Const ProdName = "@ProdName"
            Public Const TotalAmt = "@TotalAmt"
            Public Const DiscountAmt = "@DiscountAmt"
            Public Const TotalAfterDiscountAmt = "@TotalAfterDiscountAmt"
            Public Const VatAmt = "@VatAmt"
            Public Const NetAmt = "@NetAmt"
            Public Const PrintNo = "@PrintNo"
            Public Const VoidReason = "@VoidReason"
            Public Const IsScaned = "@IsScaned"
            Public Const VoidUserId = "@VoidUserId"
            Public Const RefNbr = "@RefNbr"
            Public Const IsManualEntry = "@IsManualEntry"
            Public Const StationId = "@StationId"
            Public Const VoidDttm = "@VoidDttm"
            Public Const CustType = "@CustType"
            Public Const IssueDate = "@IssueDate"
            Public Const Status = "@Status"
            Public Const ShiftNo = "@ShiftNo"
            Public Const IsConverted = "@IsConverted"
            Public Const ReplaceNo = "@RePlaceNo"
            Public Const ReplaceDate = "@RePlaceDate"
            Public Const SiteId = "@SiteId"
            Public Const DistrictCode = "@DistrictCode"
            Public Const RegionCode = "@RegionCode"
            Public Const LogoImg = "@LogoImg"
            Public Const CompanyName = "@CompanyName"
            Public Const CompanyTaxId = "@CompanyTaxId"
            Public Const CompanyBranchNo = "@CompanyBranchNo"
            Public Const CompanyBranchName = "@CompanyBranchName"
            Public Const CompanyAddress = "@CompanyAddress"
            Public Const RdId = "@RdId"
            Public Const PosNbr = "@PosNbr"
            Public Const TranNbr = "@TranNbr"
            Public Const PumpNbr = "@PumpNbr"
            Public Const HashTag = "@HashTag"
            Public Const EmployeeName = "@EmployeeName"
            Public Const ReplaceRef = "@ReplaceRef"
            Public Const Lcustomer = "@Lcustomer"
            Public Const Lcardno = "@Lcardno"
            Public Const Ltotalpoint = "@Ltotalpoint"
            Public Const Ltrnpoint = "@Ltrnpoint"
            Public Const Lbalance = "@Lbalance"
            Public Const Lrepoint = "@Lrepoint"
            Public Const Lpointtoday = "@Lpointtoday"
            Public Const IsFleetCard = "@IsFleetCard"
            Public Const HostId = "@HostId"
            Public Const SaleTypeDesc = "@SaleTypeDesc"
            Public Const SiteTypeId = "@SiteTypeId"
            Public Const IsManualFill = "@IsManualFill"
            Public Const SiteNo = "@SiteNo"
            Public Const Version = "@Version"
            Public Const CreateUser = "@CreateUser"
            Public Const CreateDttm = "@CreateDttm"
            Public Const UpdateUser = "@UpdateUser"
            Public Const UpdateDttm = "@UpdateDttm"

            Public Const ReceiptNbrRef = "@ReceiptNbrRef"
            Public Const IsDuplicate = "@IsDuplicate"

            Public Const CnRef = "@CnRef"
            Public Const FullTaxNbrRef = "@FullTaxNbrRef"
            Public Const ReasonDesc = "@ReasonDesc"
        End Class

        Private Shared Sub PopulateData(ByVal receipt As Receipt, ByVal dr As SqlDataReader)
            receipt.ObjectID.ID = CType(dr.Item(ColumnName.ObjectId), Long)
            If Not (dr.Item(ColumnName.ReceiptNbr) Is System.DBNull.Value) Then receipt.ReceiptNbr = CStr(dr.Item(ColumnName.ReceiptNbr))
            If Not (dr.Item(ColumnName.PlateNumber) Is System.DBNull.Value) Then receipt.PlateNumber = CStr(dr.Item(ColumnName.PlateNumber))
            If Not (dr.Item(ColumnName.PlateProvinceId) Is System.DBNull.Value) Then receipt.PlateProvinceId = CInt(dr.Item(ColumnName.PlateProvinceId))
            If Not (dr.Item(ColumnName.ReceiptType) Is System.DBNull.Value) Then receipt.ReceiptType = CStr(dr.Item(ColumnName.ReceiptType))
            If Not (dr.Item(ColumnName.ReceiptDate) Is System.DBNull.Value) Then receipt.ReceiptDate = CDate(dr.Item(ColumnName.ReceiptDate))
            If Not (dr.Item(ColumnName.UnitPrice) Is System.DBNull.Value) Then receipt.UnitPrice = CDec(dr.Item(ColumnName.UnitPrice))
            If Not (dr.Item(ColumnName.UnitName) Is System.DBNull.Value) Then receipt.UnitName = CStr(dr.Item(ColumnName.UnitName))
            If Not (dr.Item(ColumnName.ProdCode) Is System.DBNull.Value) Then receipt.ProdCode = CStr(dr.Item(ColumnName.ProdCode))
            If Not (dr.Item(ColumnName.Qty) Is System.DBNull.Value) Then receipt.Qty = CDec(dr.Item(ColumnName.Qty))
            If Not (dr.Item(ColumnName.ProdName) Is System.DBNull.Value) Then receipt.ProdName = CStr(dr.Item(ColumnName.ProdName))
            If Not (dr.Item(ColumnName.TotalAmt) Is System.DBNull.Value) Then receipt.TotalAmt = CDec(dr.Item(ColumnName.TotalAmt))
            If Not (dr.Item(ColumnName.DiscountAmt) Is System.DBNull.Value) Then receipt.DiscountAmt = CDec(dr.Item(ColumnName.DiscountAmt))
            If Not (dr.Item(ColumnName.TotalAfterDiscountAmt) Is System.DBNull.Value) Then receipt.TotalAfterDiscountAmt = CDec(dr.Item(ColumnName.TotalAfterDiscountAmt))
            If Not (dr.Item(ColumnName.VatAmt) Is System.DBNull.Value) Then receipt.VatAmt = CDec(dr.Item(ColumnName.VatAmt))
            If Not (dr.Item(ColumnName.NetAmt) Is System.DBNull.Value) Then receipt.NetAmt = CDec(dr.Item(ColumnName.NetAmt))
            If Not (dr.Item(ColumnName.PrintNo) Is System.DBNull.Value) Then receipt.PrintNo = CInt(dr.Item(ColumnName.PrintNo))
            If Not (dr.Item(ColumnName.VoidReason) Is System.DBNull.Value) Then receipt.VoidReason = CStr(dr.Item(ColumnName.VoidReason))
            If Not (dr.Item(ColumnName.IsScaned) Is System.DBNull.Value) Then receipt.IsScaned = CBool(dr.Item(ColumnName.IsScaned))
            If Not (dr.Item(ColumnName.VoidUserId) Is System.DBNull.Value) Then receipt.VoidUserId = CStr(dr.Item(ColumnName.VoidUserId))
            If Not (dr.Item(ColumnName.RefNbr) Is System.DBNull.Value) Then receipt.RefNbr = CStr(dr.Item(ColumnName.RefNbr))
            If Not (dr.Item(ColumnName.IsManualEntry) Is System.DBNull.Value) Then receipt.IsManualEntry = CBool(dr.Item(ColumnName.IsManualEntry))
            If Not (dr.Item(ColumnName.StationId) Is System.DBNull.Value) Then receipt.StationId = CInt(dr.Item(ColumnName.StationId))
            If Not (dr.Item(ColumnName.VoidDttm) Is System.DBNull.Value) Then receipt.VoidDttm = CDate(dr.Item(ColumnName.VoidDttm))
            If Not (dr.Item(ColumnName.CustType) Is System.DBNull.Value) Then receipt.CustType = CStr(dr.Item(ColumnName.CustType))
            If Not (dr.Item(ColumnName.IssueDate) Is System.DBNull.Value) Then receipt.IssueDate = CDate(dr.Item(ColumnName.IssueDate))
            If Not (dr.Item(ColumnName.Status) Is System.DBNull.Value) Then receipt.Status = CStr(dr.Item(ColumnName.Status))
            If Not (dr.Item(ColumnName.ShiftNo) Is System.DBNull.Value) Then receipt.ShiftId = CLng(dr.Item(ColumnName.ShiftNo))
            If Not (dr.Item(ColumnName.IsConverted) Is System.DBNull.Value) Then receipt.IsConverted = CBool(dr.Item(ColumnName.IsConverted))
            If Not (dr.Item(ColumnName.ReplaceNo) Is System.DBNull.Value) Then receipt.ReplaceNo = CInt(dr.Item(ColumnName.ReplaceNo))
            If Not (dr.Item(ColumnName.ReplaceDate) Is System.DBNull.Value) Then receipt.ReplaceDate = CDate(dr.Item(ColumnName.ReplaceDate))
            If Not (dr.Item(ColumnName.SiteId) Is System.DBNull.Value) Then receipt.SiteId = CLng(dr.Item(ColumnName.SiteId))
            If Not (dr.Item(ColumnName.DistrictCode) Is System.DBNull.Value) Then receipt.DistrictCode = CStr(dr.Item(ColumnName.DistrictCode))
            If Not (dr.Item(ColumnName.RegionCode) Is System.DBNull.Value) Then receipt.RegionCode = CStr(dr.Item(ColumnName.RegionCode))
            If Not (dr.Item(ColumnName.LogoImg) Is System.DBNull.Value) Then receipt.LogoImg = dr.Item(ColumnName.LogoImg)

            If Not (dr.Item(ColumnName.CompanyName) Is System.DBNull.Value) Then receipt.CompanyName = CStr(dr.Item(ColumnName.CompanyName))
            If Not (dr.Item(ColumnName.CompanyTaxId) Is System.DBNull.Value) Then receipt.CompanyTaxId = CStr(dr.Item(ColumnName.CompanyTaxId))
            If Not (dr.Item(ColumnName.CompanyBranchNo) Is System.DBNull.Value) Then receipt.CompanyBranchNo = CStr(dr.Item(ColumnName.CompanyBranchNo))
            If Not (dr.Item(ColumnName.CompanyBranchName) Is System.DBNull.Value) Then receipt.CompanyBranchName = CStr(dr.Item(ColumnName.CompanyBranchName))
            If Not (dr.Item(ColumnName.CompanyAddress) Is System.DBNull.Value) Then receipt.CompanyAddress = CStr(dr.Item(ColumnName.CompanyAddress))
            If Not (dr.Item(ColumnName.RdId) Is System.DBNull.Value) Then receipt.RdId = CStr(dr.Item(ColumnName.RdId))
            If Not (dr.Item(ColumnName.PosNbr) Is System.DBNull.Value) Then receipt.PosNbr = CInt(dr.Item(ColumnName.PosNbr))
            If Not (dr.Item(ColumnName.TranNbr) Is System.DBNull.Value) Then receipt.TranNbr = CInt(dr.Item(ColumnName.TranNbr))
            If Not (dr.Item(ColumnName.PumpNbr) Is System.DBNull.Value) Then receipt.PumpNbr = CInt(dr.Item(ColumnName.PumpNbr))
            If Not (dr.Item(ColumnName.HashTag) Is System.DBNull.Value) Then receipt.HashTag = CStr(dr.Item(ColumnName.HashTag))
            If Not (dr.Item(ColumnName.EmployeeName) Is System.DBNull.Value) Then receipt.EmployeeName = CStr(dr.Item(ColumnName.EmployeeName))
            If Not (dr.Item(ColumnName.ReplaceRef) Is System.DBNull.Value) Then receipt.ReplaceRef = CStr(dr.Item(ColumnName.ReplaceRef))
            If Not (dr.Item(ColumnName.Lcustomer) Is System.DBNull.Value) Then receipt.Lcustomer = CStr(dr.Item(ColumnName.Lcustomer))
            If Not (dr.Item(ColumnName.Lcardno) Is System.DBNull.Value) Then receipt.Lcardno = CStr(dr.Item(ColumnName.Lcardno))
            If Not (dr.Item(ColumnName.Ltotalpoint) Is System.DBNull.Value) Then receipt.Ltotalpoint = CDec(dr.Item(ColumnName.Ltotalpoint))
            If Not (dr.Item(ColumnName.Ltrnpoint) Is System.DBNull.Value) Then receipt.Ltrnpoint = CDec(dr.Item(ColumnName.Ltrnpoint))
            If Not (dr.Item(ColumnName.Lbalance) Is System.DBNull.Value) Then receipt.Lbalance = CDec(dr.Item(ColumnName.Lbalance))
            If Not (dr.Item(ColumnName.Lrepoint) Is System.DBNull.Value) Then receipt.Lrepoint = CDec(dr.Item(ColumnName.Lrepoint))
            If Not (dr.Item(ColumnName.Lpointtoday) Is System.DBNull.Value) Then receipt.Lpointtoday = CDec(dr.Item(ColumnName.Lpointtoday))
            If Not (dr.Item(ColumnName.IsFleetCard) Is System.DBNull.Value) Then receipt.IsFleetCard = CBool(dr.Item(ColumnName.IsFleetCard))
            If Not (dr.Item(ColumnName.HostId) Is System.DBNull.Value) Then receipt.HostId = CStr(dr.Item(ColumnName.HostId))
            If Not (dr.Item(ColumnName.SaleTypeDesc) Is System.DBNull.Value) Then receipt.SaleTypeDesc = CStr(dr.Item(ColumnName.SaleTypeDesc))
            If Not (dr.Item(ColumnName.SiteTypeId) Is System.DBNull.Value) Then receipt.SiteTypeId = CStr(dr.Item(ColumnName.SiteTypeId))
            If Not (dr.Item(ColumnName.IsManualFill) Is System.DBNull.Value) Then receipt.IsManualFill = CBool(dr.Item(ColumnName.IsManualFill))
            If Not (dr.Item(ColumnName.SiteNo) Is System.DBNull.Value) Then receipt.SiteNo = CStr(dr.Item(ColumnName.SiteNo))

            If Not (dr.Item(ColumnName.Version) Is System.DBNull.Value) Then receipt.Version = CLng(dr.Item(ColumnName.Version))
            If Not (dr.Item(ColumnName.CreateUser) Is System.DBNull.Value) Then receipt.CreateUser = CStr(dr.Item(ColumnName.CreateUser))
            If Not (dr.Item(ColumnName.CreateDttm) Is System.DBNull.Value) Then receipt.UpdateDttm = CDate(dr.Item(ColumnName.CreateDttm))
            If Not (dr.Item(ColumnName.UpdateUser) Is System.DBNull.Value) Then receipt.UpdateUser = CStr(dr.Item(ColumnName.UpdateUser))
            If Not (dr.Item(ColumnName.UpdateDttm) Is System.DBNull.Value) Then receipt.UpdateDttm = CDate(dr.Item(ColumnName.UpdateDttm))
            'If Not (dr.Item(ColumnName.ReceiptNbrRef) Is System.DBNull.Value) Then receipt.ReceiptNbrRef = CStr(dr.Item(ColumnName.ReceiptNbrRef))
            'If Not (dr.Item(ColumnName.IsDuplicate) Is System.DBNull.Value) Then receipt.IsDuplicate = CBool(dr.Item(ColumnName.UpdateDttm))
            If Not (dr.Item(ColumnName.CnRef) Is System.DBNull.Value) Then receipt.CnRef = CStr(dr.Item(ColumnName.CnRef))
            If Not (dr.Item(ColumnName.FullTaxNbrRef) Is System.DBNull.Value) Then receipt.FullTaxNbrRef = CStr(dr.Item(ColumnName.FullTaxNbrRef))
            'If Not (dr.Item(ColumnName.ReasonDesc) Is System.DBNull.Value) Then receipt.ReasonDesc = CStr(dr.Item(ColumnName.ReasonDesc))

        End Sub

        Private Shared Sub PopulateCollection(ByVal receipts As ReceiptCollection, ByVal dr As SqlDataReader)
            Do While (dr.Read())
                Dim receipt As New Receipt
                PopulateData(receipt, dr)
                receipts.Add(receipt)
            Loop
        End Sub

        Private Shared Sub MapCommandParameter(ByVal command As SqlCommand, ByVal receipt As Receipt, ByVal receiptNbrRef As String, ByVal isDuplicate As Boolean)

            command.Parameters.AddWithValue(ParamName.ObjectId, receipt.ObjectID.ID)
            If IsDBNull(receipt.ReceiptNbr) Or IsNothing(receipt.ReceiptNbr) Then
                command.Parameters.AddWithValue(ParamName.ReceiptNbr, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.ReceiptNbr, receipt.ReceiptNbr)
            End If
            If IsDBNull(receipt.PlateNumber) Or IsNothing(receipt.PlateNumber) Then
                command.Parameters.AddWithValue(ParamName.PlateNumber, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.PlateNumber, receipt.PlateNumber)
            End If
            If IsDBNull(receipt.PlateProvinceId) Or IsNothing(receipt.PlateProvinceId) Then
                command.Parameters.AddWithValue(ParamName.PlateProvinceId, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.PlateProvinceId, receipt.PlateProvinceId)
            End If
            If IsDBNull(receipt.ReceiptType) Or IsNothing(receipt.ReceiptType) Then
                command.Parameters.AddWithValue(ParamName.ReceiptType, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.ReceiptType, receipt.ReceiptType)
            End If
            If Not InputValidator.IsDate(receipt.ReceiptDate) Then
                command.Parameters.AddWithValue(ParamName.ReceiptDate, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.ReceiptDate, receipt.ReceiptDate)
            End If
            If IsDBNull(receipt.UnitPrice) Or IsNothing(receipt.UnitPrice) Then
                command.Parameters.AddWithValue(ParamName.UnitPrice, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.UnitPrice, receipt.UnitPrice)
            End If
            If IsDBNull(receipt.UnitName) Or IsNothing(receipt.UnitName) Then
                command.Parameters.AddWithValue(ParamName.UnitName, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.UnitName, receipt.UnitName)
            End If
            '=== ไม่เก็บ Product Code เนื่องจากที่ส่งมาเป็น Shift No =================================
            If IsDBNull(receipt.ProdCode) Or IsNothing(receipt.ProdCode) Then
                command.Parameters.AddWithValue(ParamName.ProdCode, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.ProdCode, "")
            End If
            '==============================================================================
            If IsDBNull(receipt.Qty) Or IsNothing(receipt.Qty) Then
                command.Parameters.AddWithValue(ParamName.Qty, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.Qty, receipt.Qty)
            End If
            If IsDBNull(receipt.ProdName) Or IsNothing(receipt.ProdName) Then
                command.Parameters.AddWithValue(ParamName.ProdName, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.ProdName, receipt.ProdName)
            End If
            If IsDBNull(receipt.TotalAmt) Or IsNothing(receipt.TotalAmt) Then
                command.Parameters.AddWithValue(ParamName.TotalAmt, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.TotalAmt, receipt.TotalAmt)
            End If
            If IsDBNull(receipt.DiscountAmt) Or IsNothing(receipt.DiscountAmt) Then
                command.Parameters.AddWithValue(ParamName.DiscountAmt, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.DiscountAmt, receipt.DiscountAmt)
            End If
            If IsDBNull(receipt.TotalAfterDiscountAmt) Or IsNothing(receipt.TotalAfterDiscountAmt) Then
                command.Parameters.AddWithValue(ParamName.TotalAfterDiscountAmt, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.TotalAfterDiscountAmt, receipt.TotalAfterDiscountAmt)
            End If
            If IsDBNull(receipt.VatAmt) Or IsNothing(receipt.VatAmt) Then
                command.Parameters.AddWithValue(ParamName.VatAmt, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.VatAmt, receipt.VatAmt)
            End If
            If IsDBNull(receipt.NetAmt) Or IsNothing(receipt.NetAmt) Then
                command.Parameters.AddWithValue(ParamName.NetAmt, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.NetAmt, receipt.NetAmt)
            End If
            If IsDBNull(receipt.PrintNo) Or IsNothing(receipt.PrintNo) Then
                command.Parameters.AddWithValue(ParamName.PrintNo, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.PrintNo, receipt.PrintNo)
            End If
            If IsDBNull(receipt.VoidReason) Or IsNothing(receipt.VoidReason) Then
                command.Parameters.AddWithValue(ParamName.VoidReason, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.VoidReason, receipt.VoidReason)
            End If
            If IsDBNull(receipt.IsScaned) Or IsNothing(receipt.IsScaned) Then
                command.Parameters.AddWithValue(ParamName.IsScaned, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.IsScaned, receipt.IsScaned)
            End If
            If IsDBNull(receipt.VoidUserId) Or IsNothing(receipt.VoidUserId) Then
                command.Parameters.AddWithValue(ParamName.VoidUserId, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.VoidUserId, receipt.VoidUserId)
            End If
            If IsDBNull(receipt.RefNbr) Or IsNothing(receipt.RefNbr) Then
                command.Parameters.AddWithValue(ParamName.RefNbr, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.RefNbr, receipt.RefNbr)
            End If
            If IsDBNull(receipt.IsManualEntry) Or IsNothing(receipt.IsManualEntry) Then
                command.Parameters.AddWithValue(ParamName.IsManualEntry, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.IsManualEntry, receipt.IsManualEntry)
            End If
            If IsDBNull(receipt.StationId) Or IsNothing(receipt.StationId) Then
                command.Parameters.AddWithValue(ParamName.StationId, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.StationId, receipt.StationId)
            End If
            If Not InputValidator.IsDate(receipt.VoidDttm) Then
                command.Parameters.AddWithValue(ParamName.VoidDttm, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.VoidDttm, receipt.VoidDttm)
            End If
            If IsDBNull(receipt.CustType) Or IsNothing(receipt.CustType) Then
                command.Parameters.AddWithValue(ParamName.CustType, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.CustType, receipt.CustType)
            End If
            If Not InputValidator.IsDate(receipt.IssueDate) Then
                command.Parameters.AddWithValue(ParamName.IssueDate, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.IssueDate, receipt.IssueDate)
            End If
            If IsDBNull(receipt.Status) Or IsNothing(receipt.Status) Then
                command.Parameters.AddWithValue(ParamName.Status, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.Status, receipt.Status)
            End If
            '==== เก็บข้อมูล Sift No ที่ฝากมากับ Product Code จาก Client ========================
            If IsDBNull(receipt.ProdCode) Or IsNothing(receipt.ProdCode) Then
                command.Parameters.AddWithValue(ParamName.ShiftNo, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.ShiftNo, receipt.ProdCode)
            End If
            '============================================================================
            If IsDBNull(receipt.IsConverted) Or IsNothing(receipt.IsConverted) Then
                command.Parameters.AddWithValue(ParamName.IsConverted, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.IsConverted, receipt.IsConverted)
            End If
            If IsDBNull(receipt.ReplaceNo) Or IsNothing(receipt.ReplaceNo) Then
                command.Parameters.AddWithValue(ParamName.ReplaceNo, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.ReplaceNo, receipt.ReplaceNo)
            End If
            If Not InputValidator.IsDate(receipt.ReplaceDate) Then
                command.Parameters.AddWithValue(ParamName.ReplaceDate, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.ReplaceDate, receipt.ReplaceDate)
            End If
            If IsDBNull(receipt.SiteId) Or IsNothing(receipt.SiteId) Then
                command.Parameters.AddWithValue(ParamName.SiteId, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.SiteId, receipt.SiteId)
            End If
            If IsDBNull(receipt.DistrictCode) Or IsNothing(receipt.DistrictCode) Then
                command.Parameters.AddWithValue(ParamName.DistrictCode, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.DistrictCode, receipt.DistrictCode)
            End If
            If IsDBNull(receipt.RegionCode) Or IsNothing(receipt.RegionCode) Then
                command.Parameters.AddWithValue(ParamName.RegionCode, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.RegionCode, receipt.RegionCode)
            End If


            'If IsDBNull(receipt.LogoImg) Or IsNothing(receipt.LogoImg) Then
            '    command.Parameters.AddWithValue(ParamName.LogoImg, System.DBNull.Value)
            'Else
            '    command.Parameters.Add(ParamName.LogoImg, SqlDbType.Image, receipt.LogoImg.Length).Value = receipt.LogoImg
            'End If

            If IsDBNull(receipt.LogoImg) Or IsNothing(receipt.LogoImg) Then
                command.Parameters.AddWithValue(ParamName.LogoImg, System.DBNull.Value)
            Else
                Dim img As Byte() = receipt.LogoImg

                Dim imgPara As SqlParameter = New SqlParameter("@LogoImg", SqlDbType.Image)

                imgPara.Value = img

                command.Parameters.Add(imgPara)
            End If


            If IsDBNull(receipt.CompanyName) Or IsNothing(receipt.CompanyName) Then
                command.Parameters.AddWithValue(ParamName.CompanyName, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.CompanyName, receipt.CompanyName)
            End If
            If IsDBNull(receipt.CompanyTaxId) Or IsNothing(receipt.CompanyTaxId) Then
                command.Parameters.AddWithValue(ParamName.CompanyTaxId, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.CompanyTaxId, receipt.CompanyTaxId)
            End If
            If IsDBNull(receipt.CompanyBranchNo) Or IsNothing(receipt.CompanyBranchNo) Then
                command.Parameters.AddWithValue(ParamName.CompanyBranchNo, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.CompanyBranchNo, receipt.CompanyBranchNo)
            End If
            If IsDBNull(receipt.CompanyBranchName) Or IsNothing(receipt.CompanyBranchName) Then
                command.Parameters.AddWithValue(ParamName.CompanyBranchName, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.CompanyBranchName, receipt.CompanyBranchName)
            End If
            If IsDBNull(receipt.CompanyAddress) Or IsNothing(receipt.CompanyAddress) Then
                command.Parameters.AddWithValue(ParamName.CompanyAddress, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.CompanyAddress, receipt.CompanyAddress)
            End If
            If IsDBNull(receipt.RdId) Or IsNothing(receipt.RdId) Then
                command.Parameters.AddWithValue(ParamName.RdId, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.RdId, receipt.RdId)
            End If
            If IsDBNull(receipt.PosNbr) Or IsNothing(receipt.PosNbr) Then
                command.Parameters.AddWithValue(ParamName.PosNbr, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.PosNbr, receipt.PosNbr)
            End If
            If IsDBNull(receipt.TranNbr) Or IsNothing(receipt.TranNbr) Then
                command.Parameters.AddWithValue(ParamName.TranNbr, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.TranNbr, receipt.TranNbr)
            End If
            If IsDBNull(receipt.PumpNbr) Or IsNothing(receipt.PumpNbr) Then
                command.Parameters.AddWithValue(ParamName.PumpNbr, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.PumpNbr, receipt.PumpNbr)
            End If
            If IsDBNull(receipt.HashTag) Or IsNothing(receipt.HashTag) Then
                command.Parameters.AddWithValue(ParamName.HashTag, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.HashTag, receipt.HashTag)
            End If
            If IsDBNull(receipt.EmployeeName) Or IsNothing(receipt.EmployeeName) Then
                command.Parameters.AddWithValue(ParamName.EmployeeName, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.EmployeeName, receipt.EmployeeName)
            End If
            If IsDBNull(receipt.ReplaceRef) Or IsNothing(receipt.ReplaceRef) Then
                command.Parameters.AddWithValue(ParamName.ReplaceRef, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.ReplaceRef, receipt.ReplaceRef)
            End If
            If IsDBNull(receipt.Lcustomer) Or IsNothing(receipt.Lcustomer) Then
                command.Parameters.AddWithValue(ParamName.Lcustomer, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.Lcustomer, receipt.Lcustomer)
            End If
            If IsDBNull(receipt.Lcardno) Or IsNothing(receipt.Lcardno) Then
                command.Parameters.AddWithValue(ParamName.Lcardno, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.Lcardno, receipt.Lcardno)
            End If
            If IsDBNull(receipt.Ltotalpoint) Or IsNothing(receipt.Ltotalpoint) Then
                command.Parameters.AddWithValue(ParamName.Ltotalpoint, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.Ltotalpoint, receipt.Ltotalpoint)
            End If
            If IsDBNull(receipt.Ltrnpoint) Or IsNothing(receipt.Ltrnpoint) Then
                command.Parameters.AddWithValue(ParamName.Ltrnpoint, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.Ltrnpoint, receipt.Ltrnpoint)
            End If
            If IsDBNull(receipt.Lbalance) Or IsNothing(receipt.Lbalance) Then
                command.Parameters.AddWithValue(ParamName.Lbalance, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.Lbalance, receipt.Lbalance)
            End If
            If IsDBNull(receipt.Lrepoint) Or IsNothing(receipt.Lrepoint) Then
                command.Parameters.AddWithValue(ParamName.Lrepoint, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.Lrepoint, receipt.Lrepoint)
            End If
            If IsDBNull(receipt.Lpointtoday) Or IsNothing(receipt.Lpointtoday) Then
                command.Parameters.AddWithValue(ParamName.Lpointtoday, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.Lpointtoday, receipt.Lpointtoday)
            End If
            If IsDBNull(receipt.IsFleetCard) Or IsNothing(receipt.IsFleetCard) Then
                command.Parameters.AddWithValue(ParamName.IsFleetCard, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.IsFleetCard, receipt.IsFleetCard)
            End If
            If IsDBNull(receipt.HostId) Or IsNothing(receipt.HostId) Then
                command.Parameters.AddWithValue(ParamName.HostId, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.HostId, receipt.HostId)
            End If
            If IsDBNull(receipt.SaleTypeDesc) Or IsNothing(receipt.SaleTypeDesc) Then
                command.Parameters.AddWithValue(ParamName.SaleTypeDesc, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.SaleTypeDesc, receipt.SaleTypeDesc)
            End If
            If IsDBNull(receipt.SiteTypeId) Or IsNothing(receipt.SiteTypeId) Then
                command.Parameters.AddWithValue(ParamName.SiteTypeId, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.SiteTypeId, receipt.SiteTypeId)
            End If
            If IsDBNull(receipt.IsManualFill) Or IsNothing(receipt.IsManualFill) Then
                command.Parameters.AddWithValue(ParamName.IsManualFill, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.IsManualFill, receipt.IsManualFill)
            End If
            If IsDBNull(receipt.SiteNo) Or IsNothing(receipt.SiteNo) Then
                command.Parameters.AddWithValue(ParamName.SiteNo, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.SiteNo, receipt.SiteNo)
            End If
            If IsDBNull(receipt.Version) Or IsNothing(receipt.Version) Then
                command.Parameters.AddWithValue(ParamName.Version, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.Version, receipt.Version)
            End If
            If IsDBNull(receipt.CreateUser) Or IsNothing(receipt.CreateUser) Then
                command.Parameters.AddWithValue(ParamName.CreateUser, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.CreateUser, receipt.CreateUser)
            End If
            If Not InputValidator.IsDate(receipt.UpdateDttm) Then
                command.Parameters.AddWithValue(ParamName.CreateDttm, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.CreateDttm, receipt.UpdateDttm)
            End If
            If IsDBNull(receipt.UpdateUser) Or IsNothing(receipt.UpdateUser) Then
                command.Parameters.AddWithValue(ParamName.UpdateUser, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.UpdateUser, receipt.UpdateUser)
            End If
            If Not InputValidator.IsDate(receipt.UpdateDttm) Then
                command.Parameters.AddWithValue(ParamName.UpdateDttm, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.UpdateDttm, receipt.UpdateDttm)
            End If
            If IsDBNull(receiptNbrRef) Or IsNothing(receiptNbrRef) Then
                command.Parameters.AddWithValue(ParamName.ReceiptNbrRef, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.ReceiptNbrRef, receiptNbrRef)
            End If
            If IsDBNull(isDuplicate) Or IsNothing(isDuplicate) Then
                command.Parameters.AddWithValue(ParamName.IsDuplicate, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.IsDuplicate, isDuplicate)
            End If
            If IsDBNull(receipt.CnRef) Or IsNothing(receipt.CnRef) Then
                command.Parameters.AddWithValue(ParamName.CnRef, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.CnRef, receipt.CnRef)
            End If

            If IsDBNull(receipt.FullTaxNbrRef) Or IsNothing(receipt.FullTaxNbrRef) Then
                command.Parameters.AddWithValue(ParamName.FullTaxNbrRef, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.FullTaxNbrRef, receipt.FullTaxNbrRef)
            End If

            'If IsDBNull(receipt.ReasonDesc) Or IsNothing(receipt.ReasonDesc) Then
            '    command.Parameters.AddWithValue(ParamName.ReasonDesc, System.DBNull.Value)
            'Else
            '    command.Parameters.AddWithValue(ParamName.ReasonDesc, receipt.ReasonDesc)
            'End If

        End Sub

        Private Shared Function PopulateDataToHqReceipt(ByVal dr As SqlDataReader) As HQReceipt
            Dim hqObj As New HQReceipt
            hqObj.receiptNbr = CStr(dr.Item(ColumnName.ReceiptNbrRef))
            hqObj.plateNo = CStr(dr.Item(ColumnName.PlateNumber))
            hqObj.customerTaxId = CStr(dr.Item(ColumnName.CompanyTaxId))
            hqObj.siteNo = CStr(dr.Item(ColumnName.SiteNo))

            'If Not (dr.Item(ColumnName.ReceiptNbrRef) Is System.DBNull.Value) Then hqObj.receiptNbr = CStr(dr.Item(ColumnName.ReceiptNbrRef))
            'If Not (dr.Item(ColumnName.PlateNumber) Is System.DBNull.Value) Then hqObj.plateNo = CStr(dr.Item(ColumnName.PlateNumber))
            If Not (dr.Item(ColumnName.ReceiptDate) Is System.DBNull.Value) Then hqObj.receiptDate = CDate(dr.Item(ColumnName.ReceiptDate))
            If Not (dr.Item(ColumnName.NetAmt) Is System.DBNull.Value) Then hqObj.netAmt = CDec(dr.Item(ColumnName.NetAmt))
            If Not (dr.Item(ColumnName.ShiftNo) Is System.DBNull.Value) Then hqObj.shiftNo = CLng(dr.Item(ColumnName.ShiftNo))
            'If Not (dr.Item(ColumnName.CompanyTaxId) Is System.DBNull.Value) Then hqObj.customerTaxId = CStr(dr.Item(ColumnName.CompanyTaxId))
            'If Not (dr.Item(ColumnName.SiteNo) Is System.DBNull.Value) Then hqObj.siteNo = CStr(dr.Item(ColumnName.SiteNo))

            Return hqObj
        End Function

#Region "Common Function Method"

        Public Shared Function GetByShift(ByVal ShiftNo As String, ByVal siteNo As String, ByVal cn As SqlConnection) As ReceiptCollection
            Dim receipts As New ReceiptCollection

            Dim sql As String = "select * from RECEIPT " &
            "where Shift_no=@ShiftNo" &
            " and site_no=@SiteNo"

            Dim command As New SqlCommand(sql, cn)
            command.CommandType = CommandType.Text
            command.Parameters.AddWithValue(ParamName.ShiftNo, ShiftNo)
            command.Parameters.AddWithValue(ParamName.SiteNo, siteNo)
            Dim dr As SqlDataReader = command.ExecuteReader

            PopulateCollection(receipts, dr)

            dr.Close()

            Return receipts
        End Function

        Public Shared Function GetByShiftForDictionary(ByVal shiftNo As String, ByVal siteNo As String, ByVal cn As SqlConnection) As Dictionary(Of String, HQReceipt)
            Dim receipts As New Dictionary(Of String, HQReceipt)
            Dim outStr As String

            Dim sql As String = "select * from RECEIPT " &
            "where Shift_no=@ShiftNo" &
            " and site_no=@SiteNo"

            Dim command As New SqlCommand(sql, cn)
            command.CommandType = CommandType.Text
            command.Parameters.AddWithValue(ParamName.ShiftNo, shiftNo)
            command.Parameters.AddWithValue(ParamName.SiteNo, siteNo)
            Dim dr As SqlDataReader = command.ExecuteReader

            Do While (dr.Read())
                Dim hqObj As HQReceipt

                outStr = CStr(dr.Item(ColumnName.ReceiptNbrRef))
                hqObj = PopulateDataToHqReceipt(dr)

                If Not receipts.ContainsKey(outStr) Then
                    receipts.Add(outStr, hqObj)
                End If
            Loop

            dr.Close()

            Return receipts
        End Function

        Public Shared Function GetByReceiptDate(ByVal siteNo As String, ByVal receiptDate As Date, ByVal cn As SqlConnection) As ReceiptCollection
            Dim receipts As New ReceiptCollection

            Dim sql As String
            '// query transaction that already receive in HQ
            sql = "select * "
            sql += " from RECEIPT "
            sql += " where 1 = 1"
            sql += String.Format(" And {0}={1}", ColumnName.SiteNo, ParamName.SiteNo)
            sql += String.Format(" And {0} between '{1}' and '{1} 23:59:58'", ColumnName.ReceiptDate, receiptDate.ToString("yyyy-MM-dd"))

            Dim command As New SqlCommand(sql, cn)
            command.CommandType = CommandType.Text
            command.Parameters.AddWithValue(ParamName.SiteNo, siteNo)
            Dim dr As SqlDataReader = command.ExecuteReader

            PopulateCollection(receipts, dr)

            dr.Close()

            Return receipts
        End Function

        Public Shared Function GetByDateForDictionary(ByVal siteNo As String, ByVal receiptDate As Date, ByVal cn As SqlConnection) As Dictionary(Of String, HQReceipt)
            Dim receipts As New Dictionary(Of String, HQReceipt)
            Dim outStr As String

            Dim sql As String = String.Format("select {0} from RECEIPT ", ColumnName.ReceiptNbrRef)
            sql += " where 1 = 1"
            sql += String.Format(" And {0}={1}", ColumnName.SiteNo, ParamName.SiteNo)
            sql += String.Format(" And {0} between '{1}' and '{1} 23:59:58'", ColumnName.ReceiptDate, receiptDate.ToString("yyyy-MM-dd"))

            Dim command As New SqlCommand(sql, cn)
            command.CommandType = CommandType.Text
            command.Parameters.AddWithValue(ParamName.SiteNo, siteNo)
            Dim dr As SqlDataReader = command.ExecuteReader

            Do While (dr.Read())
                Dim hqObj As HQReceipt

                outStr = CStr(dr.Item(ColumnName.ReceiptNbrRef))
                hqObj = PopulateDataToHqReceipt(dr)

                If Not receipts.ContainsKey(outStr) Then
                    receipts.Add(outStr, hqObj)
                End If

                System.Threading.Thread.Sleep(1)
            Loop

            dr.Close()

            Return receipts
        End Function

        Public Shared Function CheckExists(ByVal receiptNbr As String, ByVal cn As SqlConnection, ByVal txn As SqlTransaction) As Boolean

            Dim isExists As Boolean = False

            Dim sql As String = "select RECEIPT_NBR from RECEIPT " &
            "where receipt_nbr_ref=@ReceiptNbr"

            Dim command As New SqlCommand(sql, cn, txn)
            command.CommandType = CommandType.Text
            command.Parameters.AddWithValue(ParamName.ReceiptNbr, receiptNbr)

            Dim dr As SqlDataReader = command.ExecuteReader
            If dr.Read Then
                isExists = True
            End If

            dr.Close()

            Return isExists
        End Function

#End Region

    End Class

End Namespace
