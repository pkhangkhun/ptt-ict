﻿Imports System.Messaging
Imports Ie.IeComponent.BO

Namespace fts.service
    Public Class MQService
        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        Private Const qPrefix As String = "FormatName:DIRECT=TCP:"
        Private Const qName As String = "\private$\FTS_POS_NT_IB"

        Private Shared Function getQPath(destinationIP As String) As String
            Return qPrefix + destinationIP + qName
        End Function

        Public Shared Sub MSMQ_SendRequest(destinationIP As String, ByVal shiftNo As String, ByVal siteNo As String)
            log.Info("Send Q SiteNo:" + siteNo + " ShiftNo:" + shiftNo + " IP-Address:" + destinationIP)

            Dim messageQ As MessageQueue
            Dim message As System.Messaging.Message

            Dim qPath As String = getQPath(destinationIP)
            messageQ = New MessageQueue(qPath)

            message = New System.Messaging.Message(shiftNo, New BinaryMessageFormatter)
            message.Label = "REFT:" + " ShiftNo:" + shiftNo + " SiteNo:" + siteNo + " Date:" + Format(Date.Now, "yyMMdd-HHmm")
            message.AcknowledgeType = AcknowledgeTypes.None
            message.UseJournalQueue = False
            message.UseDeadLetterQueue = True

            Dim success As Boolean = True
            Try
                messageQ.Send(message)
            Catch ex As Exception
                success = False
                log.Error("Error Send Request", ex)
            Finally
                messageQ.Close()
            End Try
        End Sub

        Public Shared Sub MSMQ_SendRequestReceiptNbrByIP(destinationIP As String, ByVal receiptNbr As String)
            Dim messageQ As MessageQueue
            Dim message As System.Messaging.Message

            Dim qPath As String = getQPath(destinationIP)
            messageQ = New MessageQueue(qPath)
            message = New System.Messaging.Message(receiptNbr, New BinaryMessageFormatter)
            message.Label = "REFN:" + " ReceiptNbr:" + receiptNbr
            message.AcknowledgeType = AcknowledgeTypes.None
            message.UseJournalQueue = False
            message.UseDeadLetterQueue = True

            Dim success As Boolean = True
            Try
                messageQ.Send(message)
            Catch ex As Exception
                success = False
                log.Error("Error Send Request", ex)
            Finally
                messageQ.Close()
            End Try
        End Sub

        Public Shared Sub MSMQ_SendRequestReceiptNbrBySite(siteNo As String, ByVal receiptNbr As String)
            Dim destIp As String
            destIp = SiteService.getSiteIP(siteNo)

            MSMQ_SendRequestReceiptNbrByIP(destIp, receiptNbr)
        End Sub

        Public Shared Sub MSMQ_SendRequestEOD(ByVal siteNo As String, ByVal startDate As Date, ByVal toDate As Date)
            Dim destIp As String
            destIp = SiteService.getSiteIP(siteNo)

            MSMQ_SendRequestEOD(destIp, siteNo, startDate, toDate)
        End Sub

        Public Shared Sub MSMQ_SendRequestEOD(destinationIP As String, ByVal siteNo As String, ByVal startDate As Date, ByVal toDate As Date)
            Dim startDateStr As String = startDate.ToString("yyyyMMdd")
            Dim toDateStr As String = toDate.ToString("yyyyMMdd")

            log.Info("Start Send Request")
            log.Info("SiteNo:" + siteNo + " shift Date:" + startDateStr + " - " + toDateStr + " IP-Address:" + destinationIP)

            Dim messageQ As MessageQueue
            Dim message As System.Messaging.Message

            Dim qPath As String = getQPath(destinationIP)
            messageQ = New MessageQueue(qPath)
            '     Dim startD As String = startDate.Substring(6, 4) + startDate.Substring(3, 2) + startDate.Substring(0, 2)
            '     Dim toD As String = startDate.Substring(6, 4) + startDate.Substring(3, 2) + startDate.Substring(0, 2)
            '01/12/2015
            message = New System.Messaging.Message(startDateStr + toDateStr, New BinaryMessageFormatter)
            message.Label = "RESD:" + " shift Date:" + startDateStr + " - " + startDateStr + " SiteNo:" + siteNo + " Date:" + Format(Date.Now, "yyMMdd-HHmm")
            message.AcknowledgeType = AcknowledgeTypes.None
            message.UseJournalQueue = False
            message.UseDeadLetterQueue = True

            Dim success As Boolean = True
            Try
                messageQ.Send(message)
            Catch ex As Exception
                success = False
                log.Error("Error MQ : " + ex.Message, ex)
            Finally
                messageQ.Close()
            End Try
        End Sub

        'Send Recept & Full Tax to HQ
        Public Shared Sub MSMQ_FullTaxTrans(ByVal qlabel As String, ByVal qFullTax As Q_FullTax)
            Dim qDestination As String = System.Configuration.ConfigurationManager.AppSettings.Get("QNAME")
            If qDestination = "" Then
                Exit Sub
            End If
            Dim qPath As String = "FormatName:DIRECT=TCP:" + System.Configuration.ConfigurationManager.AppSettings.Get("QHOST")
            Dim qName As String = "\private$\" + qDestination
            Dim mqSource As String = "POS"
            Dim messageQ As MessageQueue
            Dim message As Message
            'Dim mqTran As New MessageQueueTransaction()

            qPath += qName
            messageQ = New MessageQueue(qPath)
            message = New Message(qFullTax, New BinaryMessageFormatter)

            message.Label = qlabel + ":" + Format(Date.Now, "yyMMdd-HHmm")

            message.AcknowledgeType = AcknowledgeTypes.None
            message.UseJournalQueue = False
            message.UseDeadLetterQueue = True
            message.Priority = MessagePriority.Highest

            Dim success As Boolean = True
            Try
                messageQ.Send(message)
            Catch ex As Exception
                success = False
            Finally
                messageQ.Close()
            End Try

        End Sub
    End Class

End Namespace
