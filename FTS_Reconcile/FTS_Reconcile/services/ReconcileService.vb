﻿Imports System.Data.SqlClient
Imports FTS_Reconcile.App.Dao
Imports FTS_Reconcile.App.Utility
Imports FTS_Reconcile.Domain.BO.UI
Imports FTS_Reconcile.fts.constant
Imports FTS_Reconcile.fts.util
Imports FTS_Reconcile.HQComponent.BO
Imports FTS_Services
Imports FTS_Services.App.Service
Imports FTS_Services.Domain.BO.UI
Imports FTS_Services.UI.Factory
Imports Ie.IeComponent.BO

Namespace fts.service
    Public Class ReconcileService
        Inherits BaseDao

        Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        'reset value that receive at HQ
        Private Shared Function resetHqSummaryValue(sumR As SummaryReconcile) As SummaryReconcile
            sumR.RFullTaxQty = 0
            sumR.RScanQty = 0
            sumR.RVoidQty = 0
            sumR.RReplaceQty = 0
            sumR.RCancelQty = 0
            sumR.RManualQty = 0
            sumR.RFleetQty = 0
            sumR.RGoodsQty = 0
            sumR.RCnQty = 0

            Return sumR
        End Function

        Private Shared Function updateSummary(siteNo As String, fromDate As Date, toDate As Date, cn As SqlConnection) As SummaryReconcileCollection
            Dim receipts As ReceiptCollection
            Dim sumRList As SummaryReconcileCollection

            Try
                'get summary status
                sumRList = SummaryReconcileDao.getSummary(siteNo, fromDate, toDate, cn)

                If sumRList Is Nothing Then
                    Return Nothing
                Else
                    For Each sumR As SummaryReconcile In sumRList

                        If sumR.Status = "C" OrElse sumR.Status = "X" Then
                            'summary is complate or not found, no need to any process, skip its
                            Continue For
                        End If

                        'get hq tran data
                        receipts = ReceiptDao.GetByShift(sumR.ShiftNo, sumR.SiteNo, cn)

                        'recalculate receive data
                        sumR = resetHqSummaryValue(sumR)
                        Dim obj As Receipt
                        Dim cnt As Integer = 0

                        For cnt = 0 To receipts.Count - 1
                            obj = receipts.Item(cnt)

                            If obj.ReceiptType = "F" Then
                                sumR.RFullTaxQty += 1

                                If obj.IsScaned Then
                                    sumR.RScanQty += 1
                                End If

                                If obj.Status = "V" Then
                                    sumR.RVoidQty += 1
                                    sumR.RReplaceQty += 1
                                ElseIf obj.Status = "C" Then
                                    sumR.RCancelQty += 1
                                End If

                                If obj.IsManualEntry Then
                                    sumR.RManualQty += 1
                                End If
                                If obj.IsFleetCard Then
                                    sumR.RFleetQty += 1
                                End If
                                If obj.Qty > 0 Then
                                    sumR.RFuelQty += 1
                                Else
                                    sumR.RGoodsQty += 1
                                End If
                            ElseIf obj.ReceiptType = "C" Then
                                sumR.RCnQty += 1
                            End If
                        Next

                        If sumR.RFullTaxQty > sumR.FullTaxQty Then
                            sumR.FullTaxQty = sumR.RFullTaxQty
                        End If

                        sumR.DFullTaxQty = sumR.RFullTaxQty - sumR.FullTaxQty
                        sumR.DScanQty = sumR.RScanQty - sumR.ScanQty
                        sumR.DVoidQty = sumR.RVoidQty - sumR.VoidQty
                        sumR.DReplaceQty = sumR.RReplaceQty - sumR.ReplaceQty
                        sumR.DCancelQty = sumR.RCancelQty - sumR.CancelQty
                        sumR.DManualQty = sumR.RManualQty - sumR.ManualQty
                        sumR.DFleetQty = sumR.RFleetQty - sumR.FleetQty
                        sumR.DFuelQty = sumR.RFuelQty - sumR.FuelQty
                        sumR.DGoodsQty = sumR.RGoodsQty - sumR.GoodsQty
                        sumR.DCnQty = sumR.RCnQty - sumR.CnQty

                        sumR.Status = "C"

                        If sumR.DFullTaxQty <> 0 Then
                            sumR.Status = "D"
                        End If
                        If sumR.DCnQty <> 0 Then
                            sumR.Status = "D"
                        End If

                        SummaryReconcileDao.updateStatus(sumR)

                        log.Info("Reconcile Status : " + sumR.Status)
                        System.Threading.Thread.Sleep(1)
                    Next
                End If
            Finally
                cn.Close()
            End Try

            Return sumRList
        End Function

        Private Shared Function updateSummary(siteNo As String, receiptDate As Date, cn As SqlConnection) As SummaryReconcileCollection
            Return updateSummary(siteNo, receiptDate, receiptDate, cn)
        End Function

        Public Shared Function checkLatestSummary(siteNo As String, receiptDate As Date) As SummaryReconcileCollection
            Dim sumRList As SummaryReconcileCollection

            Dim cn As SqlConnection = GetDBConnection()
            cn.Open()

            Try
                'get summary status
                sumRList = updateSummary(siteNo, receiptDate, cn)
            Finally
                cn.Close()
            End Try

            Return sumRList
        End Function

        Public Shared Function checkLatestSummary(siteNo As String, fromDate As Date, toDate As date) As SummaryReconcileCollection
            Dim sumRList As SummaryReconcileCollection = Nothing

            Dim cn As SqlConnection = GetDBConnection()
            cn.Open()

            Try
                'get summary status
                sumRList = updateSummary(siteNo, fromDate, toDate, cn)
            Finally
                cn.Close()
            End Try

            Return sumRList
        End Function

        Public Shared Function autoResendMissingData(siteNo As String, dataDate As Date, Optional isAlive As Boolean = True) As ReconcileResponse
            Dim recRs As New ReconcileResponse

            'check HQ alread receive all data from summary status
            Dim sumRList As SummaryReconcileCollection = checkLatestSummary(siteNo, dataDate)
            Dim siteIp As String = ""

            Dim str As String

            If sumRList Is Nothing OrElse sumRList.Count = 0 Then
                str = String.Format("Not found EOD Site:{0}, Date:{1}", siteNo, dataDate.ToString("yyyy-MM-dd"))
                log.Warn(str)
                recRs.processStatus = FTSConstant.statusLoad.NotFound
                Return recRs
            Else
                str = String.Format("EOD Site:{0}, Date:{1}", siteNo, dataDate.ToString("yyyy-MM-dd"))
                log.Info(str)
            End If

            Try
                siteIp = SiteService.getSiteIP(siteNo)
                'Check postgres is available or not

                'skip checking for slow process
                'CommonUtils.checkRemoteAddrIsValid(siteIp, 5432)
            Catch ex As Exception
                str = String.Format("error check ip:{0} for Site:{1}", siteIp, siteNo)
                log.Error(str, ex)
            End Try

            Dim baseConStr As String
            baseConStr = ConfigUtil.getString("postgresConStr")
            FTS_Services.App.Utility.DBUtil.conStr = String.Format(baseConStr, siteIp)

            'initial data list
            Dim list As New List(Of FullTaxReceiptUI)

            Dim diffCount As Integer = 0
            Dim noEODCount As Integer = 0

            For Each sumR As SummaryReconcile In sumRList
                str = String.Format("EOD shift_no:{0} - status:{1}", sumR.ShiftNo, sumR.Status)
                log.Info(str)
                If sumR Is Nothing OrElse sumR.Status = "X" Then
                    noEODCount += 1
                ElseIf sumR.Status = "C" Then
                    'skip complate summary
                    Continue For
                Else
                    diffCount += 1

                    'check data at site when still alive
                    If isAlive Then
                        '// load data from site
                        Dim FullTaxServ As IFullTaxService = BaseServiceFactory.GetInstance(GetType(FullTaxService))
                        Dim siteData As FullTaxReceiptCollection
                        Dim hqData As Dictionary(Of String, HQReceipt)

                        hqData = HQFullTaxSvc.getFullTaxByShiftNo(siteNo, sumR.ShiftNo)
                        siteData = FullTaxServ.GetByShiftNo(sumR.ShiftNo)

                        recRs.totalSite += siteData.Count
                        recRs.totalHq += hqData.Count

                        For Each obj As FullTaxReceipt In siteData
                            recRs.siteList.Add(New FullTaxReceiptUI(obj))

                            Dim outObj As FullTaxReceiptUI

                            If Not hqData.ContainsKey(obj.ReceiptNbr) Then
                                outObj = New FullTaxReceiptUI(obj) '// UI object
                                list.Add(outObj)
                                recRs.siteMissingList.Add(obj)  'store data into memory
                            End If
                            System.Threading.Thread.Sleep(1)
                        Next

                        For Each obj As HQReceipt In hqData.Values
                            recRs.hqList.Add(obj)
                        Next
                    End If
                End If
            Next

            Dim msg As String
            If diffCount > 0 Then
                recRs.processStatus = FTSConstant.statusLoad.FoundDiff

                'store data to global value for display
                recRs.diffList = list

                If (recRs.siteMissingList.Count > 0) Then
                    'Found diff transaction
                    msg = String.Format("Total data at site {0} of {1}", recRs.siteMissingList.Count, recRs.totalSite)
                Else
                    msg = String.Format("Not found diff data, found {0} records at site", recRs.totalSite)
                End If

            ElseIf noEODCount > 0 Then
                recRs.processStatus = FTSConstant.statusLoad.NoEOD
                msg = "Site not send EOD"
            Else
                recRs.processStatus = FTSConstant.statusLoad.Complate
                msg = "Reconcile was matched"
            End If

            recRs.processMsg = msg

            Return recRs
        End Function

        Public Shared Function inquiryMissingDataByDate(siteNo As String, dataDate As Date, Optional isAlive As Boolean = True) As ReconcileResponse
            Dim recRs As New ReconcileResponse

            'check HQ alread receive all data from summary status
            Dim sumRList As SummaryReconcileCollection = checkLatestSummary(siteNo, dataDate)
            Dim siteIp As String = ""

            Dim str As String

            If sumRList Is Nothing OrElse sumRList.Count = 0 Then
                str = String.Format("Not found EOD Site:{0}, Date:{1}", siteNo, dataDate.ToString("yyyy-MM-dd"))
                log.Warn(str)
                recRs.processStatus = FTSConstant.statusLoad.NotFound
                Return recRs
            Else
                str = String.Format("EOD Site:{0}, Date:{1}", siteNo, dataDate.ToString("yyyy-MM-dd"))
                log.Info(str)
            End If

            Try
                siteIp = SiteService.getSiteIP(siteNo)
                'Check postgres is available or not

                'skip checking for slow process
                'CommonUtils.checkRemoteAddrIsValid(siteIp, 5432)
            Catch ex As Exception
                str = String.Format("error check ip:{0} for Site:{1}", siteIp, siteNo)
                log.Error(str, ex)
            End Try

            Dim baseConStr As String
            baseConStr = ConfigUtil.getString("postgresConStr")
            FTS_Services.App.Utility.DBUtil.conStr = String.Format(baseConStr, siteIp)

            'initial data list
            Dim list As New List(Of FullTaxReceiptUI)

            Dim diffCount As Integer = 0
            Dim noEODCount As Integer = 0

            For Each sumR As SummaryReconcile In sumRList
                str = String.Format("EOD shift_no:{0} - status:{1}", sumR.ShiftNo, sumR.Status)
                log.Info(str)
                If sumR Is Nothing OrElse sumR.Status = "X" Then
                    noEODCount += 1
                ElseIf sumR.Status = "C" Then
                    'skip complate summary
                    Continue For
                Else
                    diffCount += 1

                    'check data at site when still alive
                    If isAlive Then
                        '// load data from site
                        Dim FullTaxServ As IFullTaxService = BaseServiceFactory.GetInstance(GetType(FullTaxService))
                        Dim siteData As FullTaxReceiptCollection
                        Dim hqData As Dictionary(Of String, HQReceipt)

                        hqData = HQFullTaxSvc.getFullTaxByShiftNo(siteNo, sumR.ShiftNo)
                        siteData = FullTaxServ.GetByShiftNo(sumR.ShiftNo)

                        recRs.totalSite += siteData.Count
                        recRs.totalHq += hqData.Count

                        For Each obj As FullTaxReceipt In siteData
                            recRs.siteList.Add(New FullTaxReceiptUI(obj))

                            Dim outObj As FullTaxReceiptUI

                            If Not hqData.ContainsKey(obj.ReceiptNbr) Then
                                outObj = New FullTaxReceiptUI(obj) '// UI object
                                list.Add(outObj)
                                recRs.siteMissingList.Add(obj)  'store data into memory
                            End If
                            System.Threading.Thread.Sleep(1)
                        Next

                        For Each obj As HQReceipt In hqData.Values
                            recRs.hqList.Add(obj)
                        Next
                    End If
                End If
            Next

            Dim msg As String
            If diffCount > 0 Then
                recRs.processStatus = FTSConstant.statusLoad.FoundDiff

                'store data to global value for display
                recRs.diffList = list

                If (recRs.siteMissingList.Count > 0) Then
                    'Found diff transaction
                    msg = String.Format("Total data at site {0} of {1}", recRs.siteMissingList.Count, recRs.totalSite)
                Else
                    msg = String.Format("Not found diff data, found {0} records at site", recRs.totalSite)
                End If

            ElseIf noEODCount > 0 Then
                recRs.processStatus = FTSConstant.statusLoad.NoEOD
                msg = "Site not send EOD"
            Else
                recRs.processStatus = FTSConstant.statusLoad.Complate
                msg = "Reconcile was matched"
            End If

            recRs.processMsg = msg

            Return recRs
        End Function

        Public Shared Sub syncDataToHq(siteList As List(Of FullTaxReceipt))

            Dim PaymentTranServ As IPaymentTransService = BaseServiceFactory.GetInstance(GetType(PaymentTransService))

            Try
                Dim msg As String
                'Put data on data table into local queue and send to HQ
                log.Info("Total resend data : " + siteList.Count.ToString)

                For Each fullTax As FullTaxReceipt In siteList
                    msg = String.Format("Resend {0}:{1}:{2}", fullTax.SiteNo, fullTax.ReceiptDate.ToString("yyyyMMdd"), fullTax.ReceiptNbr)
                    log.Info(msg)

                    Dim mLabel As String = ""

                    If fullTax.Status = "C" Then
                        mLabel = "FT_C_" + fullTax.ReceiptNbr
                    ElseIf fullTax.PrintNo > 0 Then
                        mLabel = "FT_P_" + fullTax.ReceiptNbr
                    ElseIf fullTax.ReplaceRef <> "" Then
                        mLabel = "FT_R_" + fullTax.ReceiptNbr + ":" + fullTax.ReplaceRef
                    ElseIf fullTax.IsManualEntry Then
                        mLabel = "FT_M_" + fullTax.ReceiptNbr
                    Else
                        mLabel = "FT_N_" + fullTax.ReceiptNbr
                    End If

                    Dim BitmapConverter As System.ComponentModel.TypeConverter = System.ComponentModel.TypeDescriptor.GetConverter(My.Resources.FTS_Logo.[GetType]())
                    fullTax.LogoImg = DirectCast(BitmapConverter.ConvertTo(My.Resources.FTS_Logo, GetType(Byte())), Byte())

                    Dim qFullTax As New Q_FullTax
                    qFullTax.RePost = 1
                    qFullTax.FullTax = fullTax
                    qFullTax.PaymentTrans = PaymentTranServ.GetByFullTaxNbr(qFullTax.FullTax.ReceiptNbr)
                    If qFullTax.FullTax.IsScaned Then
                        qFullTax.FullTax.CardImg = FTS_Services.App.Dao.CardImgDao.Retrieve(qFullTax.FullTax.ReceiptNbr)
                    End If

                    MQService.MSMQ_FullTaxTrans(mLabel, qFullTax)
                Next
                msg = String.Format("Resend {0} records, complate", siteList.Count)
                log.Info(msg)

            Catch ex As Exception
                Dim msg As String
                msg = String.Format("Error send q : ", ex.Message)
                log.Error(msg, ex)
                Throw ex
            End Try
        End Sub
    End Class
End Namespace
