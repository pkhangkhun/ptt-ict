﻿Imports FTS_Reconcile.App.Dao
Imports FTS_Reconcile.App.Utility

Public Class SiteService
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

    Public Shared Function gets(Optional siteNo As String = "", Optional status As String = "") As DataTable
        Dim retDs As DataSet = New DataSet()
        Try
            Dim sql As String = "select s.site_code, s.site_code + ' - ' + s.site_name as site_name "
            sql += " from site s "
            sql += "INNER JOIN STATION ST ON S.SITE_CODE = ST.SITE_NO"
            sql += " where 1 = 1"
            sql += "   and s.site_code not in ('0', '4', '5')" 'ignore master & test environment

            If Not IsNothing(siteNo) AndAlso siteNo.Length > 0 Then
                sql += " and s.site_code = '" + siteNo + "'"
            End If

            If Not IsNothing(status) AndAlso status.Length > 0 Then
                sql += " and st.status = '" + status + "'"
            End If

            sql += " order by s.site_code"

            Dim dsPrefix As DataSet = QueryDao.sqlInquiry(sql)
            retDs.Tables.Add(dsPrefix.Tables(0).Copy)
            dsPrefix.Dispose()
        Catch ex As Exception
            Throw ex
        End Try

        Return retDs.Tables(0)
    End Function

    Public Shared Function getSiteIP(ByVal siteNo As String) As String
        Dim ipAddr As String = ""

        Try
            Dim sql As String = "select IP_ADDRESS " &
                                " from STATION where SITE_NO = " + DBUtil.SqlQuote(siteNo)

            Dim ds As DataSet = QueryDao.sqlInquiry(sql)

            If ds.Tables(0).Rows.Count > 0 Then
                ipAddr = ds.Tables(0).Rows(0).Item("IP_ADDRESS")
            End If

            ds.Dispose()

        Catch ex As Exception
            log.Error("Unable get client ip", ex)
            Throw ex
        End Try

        Return ipAddr
    End Function

    Public Shared Function gets(ByVal SiteTypeId As Integer) As DataTable
        Dim retDs As DataSet = New DataSet()
        Try
            Dim sql As String = "select site_code, site_code + ' - ' + site_name as site_name " &
                                " from site where SITE_TYPE_ID = " & SiteTypeId.ToString &
                                " order by site_code"

            Dim dsPrefix As DataSet = QueryDao.sqlInquiry(sql)
            retDs.Tables.Add(dsPrefix.Tables(0).Copy)
            dsPrefix.Dispose()
        Catch ex As Exception
            Throw ex
        End Try

        Return retDs.Tables(0)
    End Function

    Public Shared Function getsForComboBox(ByVal SiteTypeId As Integer) As DataTable
        Dim retDs As DataSet = New DataSet()
        Try
            Dim sql As String = "select site_code, site_code + ' - ' + site_name as site_name " &
                                " from site where SITE_TYPE_ID = " & SiteTypeId.ToString &
                                " union select '0',' - All -'" &
                                " order by site_code"

            Dim dsPrefix As DataSet = QueryDao.sqlInquiry(sql)
            retDs.Tables.Add(dsPrefix.Tables(0).Copy)
            dsPrefix.Dispose()
        Catch ex As Exception
            Throw ex
        End Try

        Return retDs.Tables(0)
    End Function

    '// Add summary reconcile for any site that not send EOD data
    Public Shared Sub createSiteSummaryNotSendEOD(dataDate As Date, Optional ByVal siteNo As String = "")
        Try
            Dim sql As String = "  select a.site_no,b.SITE_TYPE_ID from STATION a left join site b on a.site_no = b.site_code "
            sql += " where STATUS = 'A'  and a.site_no not in ('0','4','5') and a.site_no not in "
            sql += "( select SITE_NO from SUMMARY_RECONCILE where DOC_DATE = '" + dataDate.ToString("yyyy-MM-dd") + "'"
            sql += ")"
            If siteNo <> "" Then
                sql += " and site_no = '" + siteNo + "'"
            End If

            Dim dsDate As DataSet = QueryDao.sqlInquiry(sql)
            Dim totalRec As Integer = dsDate.Tables(0).Rows.Count

            If totalRec > 0 Then
                If siteNo <> "" Then
                    log.Debug("Site: " + siteNo + " missing EOD on " + dataDate.ToString("yyyy-MM-dd"))
                Else
                    log.Debug("total missing EOD on " + dataDate.ToString("yyyy-MM-dd") + " " + totalRec + " records")
                End If

                For i As Integer = 0 To totalRec - 1
                    Application.DoEvents()
                    With dsDate.Tables(0).Rows(i)
                        sql = "INSERT INTO [dbo].[SUMMARY_RECONCILE]"
                        sql += " ([OBJECT_ID]"
                        sql += " ,[SITE_TYPE_ID]"
                        sql += " ,[SITE_NO]"
                        sql += " ,[SHIFT_NO]"
                        sql += " ,[STATION_ID]"
                        sql += " ,[DOC_DATE]"
                        sql += " ,[CREATE_DATE]"
                        sql += "  ,[STATUS])"
                        sql += "   VALUES"
                        sql += "  (0"
                        sql += " ," + .Item("SITE_TYPE_ID").ToString
                        sql += "  ," + DBUtil.SqlQuote(.Item("SITE_NO").ToString)
                        sql += "  ,0"
                        sql += "  ,1"
                        sql += "  ," + DBUtil.SqlQuote(dataDate.ToString("yyyy-MM-dd"))
                        sql += "  ,getdate()"
                        sql += "   ,'X')"
                    End With
                    QueryDao.sqlUpdate(sql)
                Next
            End If
        Catch ex As Exception
            log.Error("Error update EOD", ex)
            Throw ex
        End Try
    End Sub
End Class
