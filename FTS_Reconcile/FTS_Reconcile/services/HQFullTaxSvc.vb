﻿Imports System.Data.SqlClient
Imports FTS_Reconcile.App.Dao
Imports FTS_Reconcile.App.Utility
Imports FTS_Reconcile.Domain.BO.UI
Imports FTS_Reconcile.HQComponent.BO
Imports Ie.IeComponent.BO

Public Class HQFullTaxSvc
    Inherits BaseDao

    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

    Public Shared Function getFullTaxByReceiptDate(siteNo As String, receiptDate As Date) As Dictionary(Of String, HQReceipt)
        Dim ds As Dictionary(Of String, HQReceipt)
        Dim cn As SqlConnection = GetDBConnection()
        cn.Open()

        Try
            ds = ReceiptDao.GetByDateForDictionary(siteNo, receiptDate, cn)
        Catch ex As Exception
            Throw ex
        Finally
            cn.Close()
        End Try

        Return ds
    End Function

    Public Shared Function getFullTaxByShiftNo(siteNo As String, shiftNo As String) As Dictionary(Of String, HQReceipt)
        Dim ds As Dictionary(Of String, HQReceipt)
        Dim cn As SqlConnection = GetDBConnection()
        cn.Open()

        Try
            ds = ReceiptDao.GetByShiftForDictionary(shiftNo, siteNo, cn)
        Catch ex As Exception
            Throw ex
        Finally
            cn.Close()
        End Try

        Return ds
    End Function

End Class
