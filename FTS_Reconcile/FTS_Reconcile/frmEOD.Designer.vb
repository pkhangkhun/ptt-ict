<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmEOD
    Inherits AsyncDialog.AsyncBaseDialog

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnRefreshEOD = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbbSite = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.rbOil = New System.Windows.Forms.RadioButton()
        Me.rbNgv = New System.Windows.Forms.RadioButton()
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dtpToDate = New System.Windows.Forms.DateTimePicker()
        Me.dgwResult = New System.Windows.Forms.DataGridView()
        Me.StatusDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SiteNoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DocDateDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DFullTaxQtyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FullTaxQtyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RFullTaxQtyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ShiftNoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SiteTypeIdDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SiteIdDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.StationIdDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CreateDateDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CancelQtyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RCancelQtyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DCancelQtyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VoidQtyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RVoidQtyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DVoidQtyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ReplaceQtyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RReplaceQtyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DReplaceQtyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CnQtyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RCnQtyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DCnQtyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FleetQtyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RFleetQtyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DFleetQtyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ManualQtyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RManualQtyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DManualQtyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ScanQtyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RScanQtyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DScanQtyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FuelQtyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RFuelQtyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DFuelQtyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GoodsQtyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RGoodsQtyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DGoodsQtyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.StartFullTaxNbrDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EndFullTaxNbrDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.StartManualNbrDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EndManualNbrDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.StartCnNbrDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EndCnNbrDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CustomerQtyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CustPlateQtyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CustomerHqQtyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CustPlateHqQtyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VersionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UpdateUserDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UpdateDttmDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SummaryReconcileBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.cbbStatus = New System.Windows.Forms.ComboBox()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.bgwShowData = New System.ComponentModel.BackgroundWorker()
        CType(Me.dgwResult, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SummaryReconcileBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnRefreshEOD
        '
        Me.btnRefreshEOD.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRefreshEOD.Location = New System.Drawing.Point(524, 52)
        Me.btnRefreshEOD.Name = "btnRefreshEOD"
        Me.btnRefreshEOD.Size = New System.Drawing.Size(118, 55)
        Me.btnRefreshEOD.TabIndex = 7
        Me.btnRefreshEOD.Text = "Show Data"
        Me.btnRefreshEOD.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.Label1.Location = New System.Drawing.Point(58, 55)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 19)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Site:"
        '
        'cbbSite
        '
        Me.cbbSite.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cbbSite.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbbSite.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbbSite.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.cbbSite.FormattingEnabled = True
        Me.cbbSite.Location = New System.Drawing.Point(105, 52)
        Me.cbbSite.Name = "cbbSite"
        Me.cbbSite.Size = New System.Drawing.Size(375, 28)
        Me.cbbSite.TabIndex = 10
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.Location = New System.Drawing.Point(10, 20)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(104, 19)
        Me.Label2.TabIndex = 13
        Me.Label2.Text = "Station Type:"
        '
        'rbOil
        '
        Me.rbOil.AutoSize = True
        Me.rbOil.Checked = True
        Me.rbOil.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.rbOil.Location = New System.Drawing.Point(120, 20)
        Me.rbOil.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.rbOil.Name = "rbOil"
        Me.rbOil.Size = New System.Drawing.Size(40, 21)
        Me.rbOil.TabIndex = 12
        Me.rbOil.TabStop = True
        Me.rbOil.Text = "Oil"
        Me.rbOil.UseVisualStyleBackColor = True
        '
        'rbNgv
        '
        Me.rbNgv.AutoSize = True
        Me.rbNgv.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.rbNgv.Location = New System.Drawing.Point(180, 20)
        Me.rbNgv.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.rbNgv.Name = "rbNgv"
        Me.rbNgv.Size = New System.Drawing.Size(51, 21)
        Me.rbNgv.TabIndex = 11
        Me.rbNgv.Text = "Ngv"
        Me.rbNgv.UseVisualStyleBackColor = True
        '
        'dtpStartDate
        '
        Me.dtpStartDate.CustomFormat = "yyyy-MM-dd"
        Me.dtpStartDate.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpStartDate.Location = New System.Drawing.Point(106, 86)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.Size = New System.Drawing.Size(168, 27)
        Me.dtpStartDate.TabIndex = 14
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.Label3.Location = New System.Drawing.Point(52, 92)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(47, 19)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "Date:"
        '
        'dtpToDate
        '
        Me.dtpToDate.CustomFormat = "yyyy-MM-dd"
        Me.dtpToDate.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.dtpToDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpToDate.Location = New System.Drawing.Point(313, 86)
        Me.dtpToDate.Name = "dtpToDate"
        Me.dtpToDate.Size = New System.Drawing.Size(167, 27)
        Me.dtpToDate.TabIndex = 17
        '
        'dgwResult
        '
        Me.dgwResult.AllowUserToAddRows = False
        Me.dgwResult.AllowUserToDeleteRows = False
        Me.dgwResult.AllowUserToOrderColumns = True
        Me.dgwResult.AllowUserToResizeRows = False
        Me.dgwResult.AutoGenerateColumns = False
        Me.dgwResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgwResult.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.StatusDataGridViewTextBoxColumn, Me.SiteNoDataGridViewTextBoxColumn, Me.DocDateDataGridViewTextBoxColumn, Me.DFullTaxQtyDataGridViewTextBoxColumn, Me.FullTaxQtyDataGridViewTextBoxColumn, Me.RFullTaxQtyDataGridViewTextBoxColumn, Me.ShiftNoDataGridViewTextBoxColumn, Me.SiteTypeIdDataGridViewTextBoxColumn, Me.SiteIdDataGridViewTextBoxColumn, Me.StationIdDataGridViewTextBoxColumn, Me.CreateDateDataGridViewTextBoxColumn, Me.CancelQtyDataGridViewTextBoxColumn, Me.RCancelQtyDataGridViewTextBoxColumn, Me.DCancelQtyDataGridViewTextBoxColumn, Me.VoidQtyDataGridViewTextBoxColumn, Me.RVoidQtyDataGridViewTextBoxColumn, Me.DVoidQtyDataGridViewTextBoxColumn, Me.ReplaceQtyDataGridViewTextBoxColumn, Me.RReplaceQtyDataGridViewTextBoxColumn, Me.DReplaceQtyDataGridViewTextBoxColumn, Me.CnQtyDataGridViewTextBoxColumn, Me.RCnQtyDataGridViewTextBoxColumn, Me.DCnQtyDataGridViewTextBoxColumn, Me.FleetQtyDataGridViewTextBoxColumn, Me.RFleetQtyDataGridViewTextBoxColumn, Me.DFleetQtyDataGridViewTextBoxColumn, Me.ManualQtyDataGridViewTextBoxColumn, Me.RManualQtyDataGridViewTextBoxColumn, Me.DManualQtyDataGridViewTextBoxColumn, Me.ScanQtyDataGridViewTextBoxColumn, Me.RScanQtyDataGridViewTextBoxColumn, Me.DScanQtyDataGridViewTextBoxColumn, Me.FuelQtyDataGridViewTextBoxColumn, Me.RFuelQtyDataGridViewTextBoxColumn, Me.DFuelQtyDataGridViewTextBoxColumn, Me.GoodsQtyDataGridViewTextBoxColumn, Me.RGoodsQtyDataGridViewTextBoxColumn, Me.DGoodsQtyDataGridViewTextBoxColumn, Me.StartFullTaxNbrDataGridViewTextBoxColumn, Me.EndFullTaxNbrDataGridViewTextBoxColumn, Me.StartManualNbrDataGridViewTextBoxColumn, Me.EndManualNbrDataGridViewTextBoxColumn, Me.StartCnNbrDataGridViewTextBoxColumn, Me.EndCnNbrDataGridViewTextBoxColumn, Me.CustomerQtyDataGridViewTextBoxColumn, Me.CustPlateQtyDataGridViewTextBoxColumn, Me.CustomerHqQtyDataGridViewTextBoxColumn, Me.CustPlateHqQtyDataGridViewTextBoxColumn, Me.VersionDataGridViewTextBoxColumn, Me.UpdateUserDataGridViewTextBoxColumn, Me.UpdateDttmDataGridViewTextBoxColumn})
        Me.dgwResult.DataSource = Me.SummaryReconcileBindingSource
        Me.dgwResult.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dgwResult.Location = New System.Drawing.Point(25, 134)
        Me.dgwResult.Name = "dgwResult"
        Me.dgwResult.Size = New System.Drawing.Size(634, 301)
        Me.dgwResult.TabIndex = 18
        '
        'StatusDataGridViewTextBoxColumn
        '
        Me.StatusDataGridViewTextBoxColumn.DataPropertyName = "Status"
        Me.StatusDataGridViewTextBoxColumn.HeaderText = "Status"
        Me.StatusDataGridViewTextBoxColumn.Name = "StatusDataGridViewTextBoxColumn"
        Me.StatusDataGridViewTextBoxColumn.Width = 50
        '
        'SiteNoDataGridViewTextBoxColumn
        '
        Me.SiteNoDataGridViewTextBoxColumn.DataPropertyName = "SiteNo"
        Me.SiteNoDataGridViewTextBoxColumn.HeaderText = "SiteNo"
        Me.SiteNoDataGridViewTextBoxColumn.Name = "SiteNoDataGridViewTextBoxColumn"
        Me.SiteNoDataGridViewTextBoxColumn.Width = 70
        '
        'DocDateDataGridViewTextBoxColumn
        '
        Me.DocDateDataGridViewTextBoxColumn.DataPropertyName = "DocDate"
        Me.DocDateDataGridViewTextBoxColumn.HeaderText = "DocDate"
        Me.DocDateDataGridViewTextBoxColumn.Name = "DocDateDataGridViewTextBoxColumn"
        '
        'DFullTaxQtyDataGridViewTextBoxColumn
        '
        Me.DFullTaxQtyDataGridViewTextBoxColumn.DataPropertyName = "DFullTaxQty"
        Me.DFullTaxQtyDataGridViewTextBoxColumn.HeaderText = "Diff Tax"
        Me.DFullTaxQtyDataGridViewTextBoxColumn.Name = "DFullTaxQtyDataGridViewTextBoxColumn"
        Me.DFullTaxQtyDataGridViewTextBoxColumn.Width = 80
        '
        'FullTaxQtyDataGridViewTextBoxColumn
        '
        Me.FullTaxQtyDataGridViewTextBoxColumn.DataPropertyName = "FullTaxQty"
        Me.FullTaxQtyDataGridViewTextBoxColumn.HeaderText = "Site Tax"
        Me.FullTaxQtyDataGridViewTextBoxColumn.Name = "FullTaxQtyDataGridViewTextBoxColumn"
        Me.FullTaxQtyDataGridViewTextBoxColumn.Width = 80
        '
        'RFullTaxQtyDataGridViewTextBoxColumn
        '
        Me.RFullTaxQtyDataGridViewTextBoxColumn.DataPropertyName = "RFullTaxQty"
        Me.RFullTaxQtyDataGridViewTextBoxColumn.HeaderText = "HQ Tax"
        Me.RFullTaxQtyDataGridViewTextBoxColumn.Name = "RFullTaxQtyDataGridViewTextBoxColumn"
        Me.RFullTaxQtyDataGridViewTextBoxColumn.Width = 80
        '
        'ShiftNoDataGridViewTextBoxColumn
        '
        Me.ShiftNoDataGridViewTextBoxColumn.DataPropertyName = "ShiftNo"
        Me.ShiftNoDataGridViewTextBoxColumn.HeaderText = "ShiftNo"
        Me.ShiftNoDataGridViewTextBoxColumn.Name = "ShiftNoDataGridViewTextBoxColumn"
        '
        'SiteTypeIdDataGridViewTextBoxColumn
        '
        Me.SiteTypeIdDataGridViewTextBoxColumn.DataPropertyName = "SiteTypeId"
        Me.SiteTypeIdDataGridViewTextBoxColumn.HeaderText = "SiteTypeId"
        Me.SiteTypeIdDataGridViewTextBoxColumn.Name = "SiteTypeIdDataGridViewTextBoxColumn"
        '
        'SiteIdDataGridViewTextBoxColumn
        '
        Me.SiteIdDataGridViewTextBoxColumn.DataPropertyName = "SiteId"
        Me.SiteIdDataGridViewTextBoxColumn.HeaderText = "SiteId"
        Me.SiteIdDataGridViewTextBoxColumn.Name = "SiteIdDataGridViewTextBoxColumn"
        '
        'StationIdDataGridViewTextBoxColumn
        '
        Me.StationIdDataGridViewTextBoxColumn.DataPropertyName = "StationId"
        Me.StationIdDataGridViewTextBoxColumn.HeaderText = "StationId"
        Me.StationIdDataGridViewTextBoxColumn.Name = "StationIdDataGridViewTextBoxColumn"
        '
        'CreateDateDataGridViewTextBoxColumn
        '
        Me.CreateDateDataGridViewTextBoxColumn.DataPropertyName = "CreateDate"
        Me.CreateDateDataGridViewTextBoxColumn.HeaderText = "CreateDate"
        Me.CreateDateDataGridViewTextBoxColumn.Name = "CreateDateDataGridViewTextBoxColumn"
        '
        'CancelQtyDataGridViewTextBoxColumn
        '
        Me.CancelQtyDataGridViewTextBoxColumn.DataPropertyName = "CancelQty"
        Me.CancelQtyDataGridViewTextBoxColumn.HeaderText = "CancelQty"
        Me.CancelQtyDataGridViewTextBoxColumn.Name = "CancelQtyDataGridViewTextBoxColumn"
        '
        'RCancelQtyDataGridViewTextBoxColumn
        '
        Me.RCancelQtyDataGridViewTextBoxColumn.DataPropertyName = "RCancelQty"
        Me.RCancelQtyDataGridViewTextBoxColumn.HeaderText = "RCancelQty"
        Me.RCancelQtyDataGridViewTextBoxColumn.Name = "RCancelQtyDataGridViewTextBoxColumn"
        '
        'DCancelQtyDataGridViewTextBoxColumn
        '
        Me.DCancelQtyDataGridViewTextBoxColumn.DataPropertyName = "DCancelQty"
        Me.DCancelQtyDataGridViewTextBoxColumn.HeaderText = "DCancelQty"
        Me.DCancelQtyDataGridViewTextBoxColumn.Name = "DCancelQtyDataGridViewTextBoxColumn"
        '
        'VoidQtyDataGridViewTextBoxColumn
        '
        Me.VoidQtyDataGridViewTextBoxColumn.DataPropertyName = "VoidQty"
        Me.VoidQtyDataGridViewTextBoxColumn.HeaderText = "VoidQty"
        Me.VoidQtyDataGridViewTextBoxColumn.Name = "VoidQtyDataGridViewTextBoxColumn"
        '
        'RVoidQtyDataGridViewTextBoxColumn
        '
        Me.RVoidQtyDataGridViewTextBoxColumn.DataPropertyName = "RVoidQty"
        Me.RVoidQtyDataGridViewTextBoxColumn.HeaderText = "RVoidQty"
        Me.RVoidQtyDataGridViewTextBoxColumn.Name = "RVoidQtyDataGridViewTextBoxColumn"
        '
        'DVoidQtyDataGridViewTextBoxColumn
        '
        Me.DVoidQtyDataGridViewTextBoxColumn.DataPropertyName = "DVoidQty"
        Me.DVoidQtyDataGridViewTextBoxColumn.HeaderText = "DVoidQty"
        Me.DVoidQtyDataGridViewTextBoxColumn.Name = "DVoidQtyDataGridViewTextBoxColumn"
        '
        'ReplaceQtyDataGridViewTextBoxColumn
        '
        Me.ReplaceQtyDataGridViewTextBoxColumn.DataPropertyName = "ReplaceQty"
        Me.ReplaceQtyDataGridViewTextBoxColumn.HeaderText = "ReplaceQty"
        Me.ReplaceQtyDataGridViewTextBoxColumn.Name = "ReplaceQtyDataGridViewTextBoxColumn"
        '
        'RReplaceQtyDataGridViewTextBoxColumn
        '
        Me.RReplaceQtyDataGridViewTextBoxColumn.DataPropertyName = "RReplaceQty"
        Me.RReplaceQtyDataGridViewTextBoxColumn.HeaderText = "RReplaceQty"
        Me.RReplaceQtyDataGridViewTextBoxColumn.Name = "RReplaceQtyDataGridViewTextBoxColumn"
        '
        'DReplaceQtyDataGridViewTextBoxColumn
        '
        Me.DReplaceQtyDataGridViewTextBoxColumn.DataPropertyName = "DReplaceQty"
        Me.DReplaceQtyDataGridViewTextBoxColumn.HeaderText = "DReplaceQty"
        Me.DReplaceQtyDataGridViewTextBoxColumn.Name = "DReplaceQtyDataGridViewTextBoxColumn"
        '
        'CnQtyDataGridViewTextBoxColumn
        '
        Me.CnQtyDataGridViewTextBoxColumn.DataPropertyName = "CnQty"
        Me.CnQtyDataGridViewTextBoxColumn.HeaderText = "CnQty"
        Me.CnQtyDataGridViewTextBoxColumn.Name = "CnQtyDataGridViewTextBoxColumn"
        '
        'RCnQtyDataGridViewTextBoxColumn
        '
        Me.RCnQtyDataGridViewTextBoxColumn.DataPropertyName = "RCnQty"
        Me.RCnQtyDataGridViewTextBoxColumn.HeaderText = "RCnQty"
        Me.RCnQtyDataGridViewTextBoxColumn.Name = "RCnQtyDataGridViewTextBoxColumn"
        '
        'DCnQtyDataGridViewTextBoxColumn
        '
        Me.DCnQtyDataGridViewTextBoxColumn.DataPropertyName = "DCnQty"
        Me.DCnQtyDataGridViewTextBoxColumn.HeaderText = "DCnQty"
        Me.DCnQtyDataGridViewTextBoxColumn.Name = "DCnQtyDataGridViewTextBoxColumn"
        '
        'FleetQtyDataGridViewTextBoxColumn
        '
        Me.FleetQtyDataGridViewTextBoxColumn.DataPropertyName = "FleetQty"
        Me.FleetQtyDataGridViewTextBoxColumn.HeaderText = "FleetQty"
        Me.FleetQtyDataGridViewTextBoxColumn.Name = "FleetQtyDataGridViewTextBoxColumn"
        '
        'RFleetQtyDataGridViewTextBoxColumn
        '
        Me.RFleetQtyDataGridViewTextBoxColumn.DataPropertyName = "RFleetQty"
        Me.RFleetQtyDataGridViewTextBoxColumn.HeaderText = "RFleetQty"
        Me.RFleetQtyDataGridViewTextBoxColumn.Name = "RFleetQtyDataGridViewTextBoxColumn"
        '
        'DFleetQtyDataGridViewTextBoxColumn
        '
        Me.DFleetQtyDataGridViewTextBoxColumn.DataPropertyName = "DFleetQty"
        Me.DFleetQtyDataGridViewTextBoxColumn.HeaderText = "DFleetQty"
        Me.DFleetQtyDataGridViewTextBoxColumn.Name = "DFleetQtyDataGridViewTextBoxColumn"
        '
        'ManualQtyDataGridViewTextBoxColumn
        '
        Me.ManualQtyDataGridViewTextBoxColumn.DataPropertyName = "ManualQty"
        Me.ManualQtyDataGridViewTextBoxColumn.HeaderText = "ManualQty"
        Me.ManualQtyDataGridViewTextBoxColumn.Name = "ManualQtyDataGridViewTextBoxColumn"
        '
        'RManualQtyDataGridViewTextBoxColumn
        '
        Me.RManualQtyDataGridViewTextBoxColumn.DataPropertyName = "RManualQty"
        Me.RManualQtyDataGridViewTextBoxColumn.HeaderText = "RManualQty"
        Me.RManualQtyDataGridViewTextBoxColumn.Name = "RManualQtyDataGridViewTextBoxColumn"
        '
        'DManualQtyDataGridViewTextBoxColumn
        '
        Me.DManualQtyDataGridViewTextBoxColumn.DataPropertyName = "DManualQty"
        Me.DManualQtyDataGridViewTextBoxColumn.HeaderText = "DManualQty"
        Me.DManualQtyDataGridViewTextBoxColumn.Name = "DManualQtyDataGridViewTextBoxColumn"
        '
        'ScanQtyDataGridViewTextBoxColumn
        '
        Me.ScanQtyDataGridViewTextBoxColumn.DataPropertyName = "ScanQty"
        Me.ScanQtyDataGridViewTextBoxColumn.HeaderText = "ScanQty"
        Me.ScanQtyDataGridViewTextBoxColumn.Name = "ScanQtyDataGridViewTextBoxColumn"
        '
        'RScanQtyDataGridViewTextBoxColumn
        '
        Me.RScanQtyDataGridViewTextBoxColumn.DataPropertyName = "RScanQty"
        Me.RScanQtyDataGridViewTextBoxColumn.HeaderText = "RScanQty"
        Me.RScanQtyDataGridViewTextBoxColumn.Name = "RScanQtyDataGridViewTextBoxColumn"
        '
        'DScanQtyDataGridViewTextBoxColumn
        '
        Me.DScanQtyDataGridViewTextBoxColumn.DataPropertyName = "DScanQty"
        Me.DScanQtyDataGridViewTextBoxColumn.HeaderText = "DScanQty"
        Me.DScanQtyDataGridViewTextBoxColumn.Name = "DScanQtyDataGridViewTextBoxColumn"
        '
        'FuelQtyDataGridViewTextBoxColumn
        '
        Me.FuelQtyDataGridViewTextBoxColumn.DataPropertyName = "FuelQty"
        Me.FuelQtyDataGridViewTextBoxColumn.HeaderText = "FuelQty"
        Me.FuelQtyDataGridViewTextBoxColumn.Name = "FuelQtyDataGridViewTextBoxColumn"
        '
        'RFuelQtyDataGridViewTextBoxColumn
        '
        Me.RFuelQtyDataGridViewTextBoxColumn.DataPropertyName = "RFuelQty"
        Me.RFuelQtyDataGridViewTextBoxColumn.HeaderText = "RFuelQty"
        Me.RFuelQtyDataGridViewTextBoxColumn.Name = "RFuelQtyDataGridViewTextBoxColumn"
        '
        'DFuelQtyDataGridViewTextBoxColumn
        '
        Me.DFuelQtyDataGridViewTextBoxColumn.DataPropertyName = "DFuelQty"
        Me.DFuelQtyDataGridViewTextBoxColumn.HeaderText = "DFuelQty"
        Me.DFuelQtyDataGridViewTextBoxColumn.Name = "DFuelQtyDataGridViewTextBoxColumn"
        '
        'GoodsQtyDataGridViewTextBoxColumn
        '
        Me.GoodsQtyDataGridViewTextBoxColumn.DataPropertyName = "GoodsQty"
        Me.GoodsQtyDataGridViewTextBoxColumn.HeaderText = "GoodsQty"
        Me.GoodsQtyDataGridViewTextBoxColumn.Name = "GoodsQtyDataGridViewTextBoxColumn"
        '
        'RGoodsQtyDataGridViewTextBoxColumn
        '
        Me.RGoodsQtyDataGridViewTextBoxColumn.DataPropertyName = "RGoodsQty"
        Me.RGoodsQtyDataGridViewTextBoxColumn.HeaderText = "RGoodsQty"
        Me.RGoodsQtyDataGridViewTextBoxColumn.Name = "RGoodsQtyDataGridViewTextBoxColumn"
        '
        'DGoodsQtyDataGridViewTextBoxColumn
        '
        Me.DGoodsQtyDataGridViewTextBoxColumn.DataPropertyName = "DGoodsQty"
        Me.DGoodsQtyDataGridViewTextBoxColumn.HeaderText = "DGoodsQty"
        Me.DGoodsQtyDataGridViewTextBoxColumn.Name = "DGoodsQtyDataGridViewTextBoxColumn"
        '
        'StartFullTaxNbrDataGridViewTextBoxColumn
        '
        Me.StartFullTaxNbrDataGridViewTextBoxColumn.DataPropertyName = "StartFullTaxNbr"
        Me.StartFullTaxNbrDataGridViewTextBoxColumn.HeaderText = "StartFullTaxNbr"
        Me.StartFullTaxNbrDataGridViewTextBoxColumn.Name = "StartFullTaxNbrDataGridViewTextBoxColumn"
        '
        'EndFullTaxNbrDataGridViewTextBoxColumn
        '
        Me.EndFullTaxNbrDataGridViewTextBoxColumn.DataPropertyName = "EndFullTaxNbr"
        Me.EndFullTaxNbrDataGridViewTextBoxColumn.HeaderText = "EndFullTaxNbr"
        Me.EndFullTaxNbrDataGridViewTextBoxColumn.Name = "EndFullTaxNbrDataGridViewTextBoxColumn"
        '
        'StartManualNbrDataGridViewTextBoxColumn
        '
        Me.StartManualNbrDataGridViewTextBoxColumn.DataPropertyName = "StartManualNbr"
        Me.StartManualNbrDataGridViewTextBoxColumn.HeaderText = "StartManualNbr"
        Me.StartManualNbrDataGridViewTextBoxColumn.Name = "StartManualNbrDataGridViewTextBoxColumn"
        '
        'EndManualNbrDataGridViewTextBoxColumn
        '
        Me.EndManualNbrDataGridViewTextBoxColumn.DataPropertyName = "EndManualNbr"
        Me.EndManualNbrDataGridViewTextBoxColumn.HeaderText = "EndManualNbr"
        Me.EndManualNbrDataGridViewTextBoxColumn.Name = "EndManualNbrDataGridViewTextBoxColumn"
        '
        'StartCnNbrDataGridViewTextBoxColumn
        '
        Me.StartCnNbrDataGridViewTextBoxColumn.DataPropertyName = "StartCnNbr"
        Me.StartCnNbrDataGridViewTextBoxColumn.HeaderText = "StartCnNbr"
        Me.StartCnNbrDataGridViewTextBoxColumn.Name = "StartCnNbrDataGridViewTextBoxColumn"
        '
        'EndCnNbrDataGridViewTextBoxColumn
        '
        Me.EndCnNbrDataGridViewTextBoxColumn.DataPropertyName = "EndCnNbr"
        Me.EndCnNbrDataGridViewTextBoxColumn.HeaderText = "EndCnNbr"
        Me.EndCnNbrDataGridViewTextBoxColumn.Name = "EndCnNbrDataGridViewTextBoxColumn"
        '
        'CustomerQtyDataGridViewTextBoxColumn
        '
        Me.CustomerQtyDataGridViewTextBoxColumn.DataPropertyName = "CustomerQty"
        Me.CustomerQtyDataGridViewTextBoxColumn.HeaderText = "CustomerQty"
        Me.CustomerQtyDataGridViewTextBoxColumn.Name = "CustomerQtyDataGridViewTextBoxColumn"
        '
        'CustPlateQtyDataGridViewTextBoxColumn
        '
        Me.CustPlateQtyDataGridViewTextBoxColumn.DataPropertyName = "custPlateQty"
        Me.CustPlateQtyDataGridViewTextBoxColumn.HeaderText = "custPlateQty"
        Me.CustPlateQtyDataGridViewTextBoxColumn.Name = "CustPlateQtyDataGridViewTextBoxColumn"
        '
        'CustomerHqQtyDataGridViewTextBoxColumn
        '
        Me.CustomerHqQtyDataGridViewTextBoxColumn.DataPropertyName = "CustomerHqQty"
        Me.CustomerHqQtyDataGridViewTextBoxColumn.HeaderText = "CustomerHqQty"
        Me.CustomerHqQtyDataGridViewTextBoxColumn.Name = "CustomerHqQtyDataGridViewTextBoxColumn"
        '
        'CustPlateHqQtyDataGridViewTextBoxColumn
        '
        Me.CustPlateHqQtyDataGridViewTextBoxColumn.DataPropertyName = "custPlateHqQty"
        Me.CustPlateHqQtyDataGridViewTextBoxColumn.HeaderText = "custPlateHqQty"
        Me.CustPlateHqQtyDataGridViewTextBoxColumn.Name = "CustPlateHqQtyDataGridViewTextBoxColumn"
        '
        'VersionDataGridViewTextBoxColumn
        '
        Me.VersionDataGridViewTextBoxColumn.DataPropertyName = "Version"
        Me.VersionDataGridViewTextBoxColumn.HeaderText = "Version"
        Me.VersionDataGridViewTextBoxColumn.Name = "VersionDataGridViewTextBoxColumn"
        '
        'UpdateUserDataGridViewTextBoxColumn
        '
        Me.UpdateUserDataGridViewTextBoxColumn.DataPropertyName = "UpdateUser"
        Me.UpdateUserDataGridViewTextBoxColumn.HeaderText = "UpdateUser"
        Me.UpdateUserDataGridViewTextBoxColumn.Name = "UpdateUserDataGridViewTextBoxColumn"
        '
        'UpdateDttmDataGridViewTextBoxColumn
        '
        Me.UpdateDttmDataGridViewTextBoxColumn.DataPropertyName = "UpdateDttm"
        Me.UpdateDttmDataGridViewTextBoxColumn.HeaderText = "UpdateDttm"
        Me.UpdateDttmDataGridViewTextBoxColumn.Name = "UpdateDttmDataGridViewTextBoxColumn"
        '
        'SummaryReconcileBindingSource
        '
        Me.SummaryReconcileBindingSource.DataSource = GetType(FTS_Reconcile.HQComponent.BO.SummaryReconcile)
        '
        'cbbStatus
        '
        Me.cbbStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbbStatus.FormattingEnabled = True
        Me.cbbStatus.Location = New System.Drawing.Point(12, 469)
        Me.cbbStatus.Name = "cbbStatus"
        Me.cbbStatus.Size = New System.Drawing.Size(154, 21)
        Me.cbbStatus.TabIndex = 19
        Me.cbbStatus.Visible = False
        '
        'lblStatus
        '
        Me.lblStatus.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.lblStatus.Location = New System.Drawing.Point(25, 445)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(634, 53)
        Me.lblStatus.TabIndex = 20
        Me.lblStatus.Text = "Status:"
        '
        'bgwShowData
        '
        '
        'frmEOD
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(680, 505)
        Me.Controls.Add(Me.cbbStatus)
        Me.Controls.Add(Me.lblStatus)
        Me.Controls.Add(Me.dgwResult)
        Me.Controls.Add(Me.dtpToDate)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.dtpStartDate)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.rbOil)
        Me.Controls.Add(Me.rbNgv)
        Me.Controls.Add(Me.cbbSite)
        Me.Controls.Add(Me.btnRefreshEOD)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "frmEOD"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
        Me.Text = "EOD Summary"
        CType(Me.dgwResult, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SummaryReconcileBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnRefreshEOD As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cbbSite As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents rbOil As System.Windows.Forms.RadioButton
    Friend WithEvents rbNgv As System.Windows.Forms.RadioButton
    Friend WithEvents dtpStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dtpToDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dgwResult As DataGridView
    Friend WithEvents SummaryReconcileBindingSource As BindingSource
    Friend WithEvents cbbStatus As ComboBox
    Friend WithEvents lblStatus As Label
    Friend WithEvents bgwShowData As System.ComponentModel.BackgroundWorker
    Friend WithEvents StatusDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents SiteNoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DocDateDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DFullTaxQtyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents FullTaxQtyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents RFullTaxQtyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ShiftNoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents SiteTypeIdDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents SiteIdDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents StationIdDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CreateDateDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CancelQtyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents RCancelQtyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DCancelQtyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents VoidQtyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents RVoidQtyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DVoidQtyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ReplaceQtyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents RReplaceQtyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DReplaceQtyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CnQtyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents RCnQtyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DCnQtyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents FleetQtyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents RFleetQtyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DFleetQtyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ManualQtyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents RManualQtyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DManualQtyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ScanQtyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents RScanQtyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DScanQtyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents FuelQtyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents RFuelQtyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DFuelQtyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents GoodsQtyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents RGoodsQtyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DGoodsQtyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents StartFullTaxNbrDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents EndFullTaxNbrDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents StartManualNbrDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents EndManualNbrDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents StartCnNbrDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents EndCnNbrDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CustomerQtyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CustPlateQtyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CustomerHqQtyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CustPlateHqQtyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents VersionDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents UpdateUserDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents UpdateDttmDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
End Class
