﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmRequestDataFromSite
    Inherits AsyncDialog.AsyncBaseDialog

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.cbbFixIp = New System.Windows.Forms.ComboBox()
        Me.chbFixIp = New System.Windows.Forms.CheckBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dtpStartDate = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.rbOil = New System.Windows.Forms.RadioButton()
        Me.rbNgv = New System.Windows.Forms.RadioButton()
        Me.cbbSite = New System.Windows.Forms.ComboBox()
        Me.btnShow = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnSync = New System.Windows.Forms.Button()
        Me.lblTotalSite = New System.Windows.Forms.Label()
        Me.lblTotalHQ = New System.Windows.Forms.Label()
        Me.lblTotalDiff = New System.Windows.Forms.Label()
        Me.bwSync = New System.ComponentModel.BackgroundWorker()
        Me.bwShowData = New System.ComponentModel.BackgroundWorker()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReconcileByRangNBR = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSummaryEOD = New System.Windows.Forms.ToolStripMenuItem()
        Me.tabCtrlMain = New System.Windows.Forms.TabControl()
        Me.tabMissing = New System.Windows.Forms.TabPage()
        Me.dtgSiteData = New System.Windows.Forms.DataGridView()
        Me.ReceiptNbrDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ReceiptDateDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ReceiptTypeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PlateNumberDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NetAmtDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CustPlateDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CompanyTaxIdDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CompanyAddressDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CompanyBranchNameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CompanyNameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CustTaxIdDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FullTaxNbrRefDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PosNbrDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProdNameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PumpNbrDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QtyDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RefNbrDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.StationIdDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.StatusDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TotalAmtDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VatAmtDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CnRefDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CustBranchNameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CustBranchNoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CustNameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FullTaxReceiptUIBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.tabCompare = New System.Windows.Forms.TabPage()
        Me.dgwHQ = New System.Windows.Forms.DataGridView()
        Me.ReceiptNbrDataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PlateNoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ReceiptDateDataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CustomerTaxIdDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NetAmtDataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ShiftNoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HQReceiptBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.dgwSite = New System.Windows.Forms.DataGridView()
        Me.ReceiptNbrDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PlateNumberDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ReceiptDateDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CompanyTaxIdDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CompanyNameDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NetAmtDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ReceiptTypeDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CompanyAddressDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CompanyBranchNoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FullTaxNbrRefDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RefNbrDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CustBranchNoDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CustNameDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CustTaxIdDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FullTaxReceiptUIBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.lblInfo = New System.Windows.Forms.Label()
        Me.MenuStrip1.SuspendLayout()
        Me.tabCtrlMain.SuspendLayout()
        Me.tabMissing.SuspendLayout()
        CType(Me.dtgSiteData, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FullTaxReceiptUIBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabCompare.SuspendLayout()
        CType(Me.dgwHQ, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.HQReceiptBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgwSite, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FullTaxReceiptUIBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cbbFixIp
        '
        Me.cbbFixIp.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cbbFixIp.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbbFixIp.Enabled = False
        Me.cbbFixIp.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.cbbFixIp.FormattingEnabled = True
        Me.cbbFixIp.Location = New System.Drawing.Point(559, 88)
        Me.cbbFixIp.Name = "cbbFixIp"
        Me.cbbFixIp.Size = New System.Drawing.Size(106, 28)
        Me.cbbFixIp.TabIndex = 40
        Me.cbbFixIp.Visible = False
        '
        'chbFixIp
        '
        Me.chbFixIp.AutoSize = True
        Me.chbFixIp.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.chbFixIp.Location = New System.Drawing.Point(482, 90)
        Me.chbFixIp.Name = "chbFixIp"
        Me.chbFixIp.Size = New System.Drawing.Size(71, 24)
        Me.chbFixIp.TabIndex = 39
        Me.chbFixIp.Text = "Fix IP:"
        Me.chbFixIp.UseVisualStyleBackColor = True
        Me.chbFixIp.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.Label3.Location = New System.Drawing.Point(501, 55)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(52, 19)
        Me.Label3.TabIndex = 36
        Me.Label3.Text = "Date: "
        '
        'dtpStartDate
        '
        Me.dtpStartDate.CustomFormat = "yyyy-MM-dd"
        Me.dtpStartDate.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpStartDate.Location = New System.Drawing.Point(559, 51)
        Me.dtpStartDate.Name = "dtpStartDate"
        Me.dtpStartDate.Size = New System.Drawing.Size(126, 27)
        Me.dtpStartDate.TabIndex = 35
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.Location = New System.Drawing.Point(22, 43)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(104, 19)
        Me.Label2.TabIndex = 34
        Me.Label2.Text = "Station Type:"
        '
        'rbOil
        '
        Me.rbOil.AutoSize = True
        Me.rbOil.Checked = True
        Me.rbOil.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.rbOil.Location = New System.Drawing.Point(147, 42)
        Me.rbOil.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.rbOil.Name = "rbOil"
        Me.rbOil.Size = New System.Drawing.Size(40, 21)
        Me.rbOil.TabIndex = 33
        Me.rbOil.TabStop = True
        Me.rbOil.Text = "Oil"
        Me.rbOil.UseVisualStyleBackColor = True
        '
        'rbNgv
        '
        Me.rbNgv.AutoSize = True
        Me.rbNgv.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.rbNgv.Location = New System.Drawing.Point(207, 42)
        Me.rbNgv.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.rbNgv.Name = "rbNgv"
        Me.rbNgv.Size = New System.Drawing.Size(51, 21)
        Me.rbNgv.TabIndex = 32
        Me.rbNgv.Text = "Ngv"
        Me.rbNgv.UseVisualStyleBackColor = True
        '
        'cbbSite
        '
        Me.cbbSite.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cbbSite.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbbSite.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbbSite.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.cbbSite.FormattingEnabled = True
        Me.cbbSite.Location = New System.Drawing.Point(131, 74)
        Me.cbbSite.Name = "cbbSite"
        Me.cbbSite.Size = New System.Drawing.Size(321, 28)
        Me.cbbSite.TabIndex = 31
        '
        'btnShow
        '
        Me.btnShow.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnShow.Location = New System.Drawing.Point(724, 57)
        Me.btnShow.Name = "btnShow"
        Me.btnShow.Size = New System.Drawing.Size(140, 59)
        Me.btnShow.TabIndex = 30
        Me.btnShow.Text = "Show Data"
        Me.btnShow.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.Label1.Location = New System.Drawing.Point(84, 79)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 19)
        Me.Label1.TabIndex = 29
        Me.Label1.Text = "Site:"
        '
        'btnSync
        '
        Me.btnSync.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSync.Location = New System.Drawing.Point(672, 476)
        Me.btnSync.Name = "btnSync"
        Me.btnSync.Size = New System.Drawing.Size(192, 59)
        Me.btnSync.TabIndex = 43
        Me.btnSync.Text = "Update to HQ"
        Me.btnSync.UseVisualStyleBackColor = True
        '
        'lblTotalSite
        '
        Me.lblTotalSite.AutoSize = True
        Me.lblTotalSite.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.lblTotalSite.Location = New System.Drawing.Point(62, 476)
        Me.lblTotalSite.Name = "lblTotalSite"
        Me.lblTotalSite.Size = New System.Drawing.Size(82, 19)
        Me.lblTotalSite.TabIndex = 45
        Me.lblTotalSite.Text = "Total Site:"
        '
        'lblTotalHQ
        '
        Me.lblTotalHQ.AutoSize = True
        Me.lblTotalHQ.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.lblTotalHQ.Location = New System.Drawing.Point(65, 508)
        Me.lblTotalHQ.Name = "lblTotalHQ"
        Me.lblTotalHQ.Size = New System.Drawing.Size(79, 19)
        Me.lblTotalHQ.TabIndex = 46
        Me.lblTotalHQ.Text = "Total HQ:"
        '
        'lblTotalDiff
        '
        Me.lblTotalDiff.AutoSize = True
        Me.lblTotalDiff.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.lblTotalDiff.Location = New System.Drawing.Point(258, 476)
        Me.lblTotalDiff.Name = "lblTotalDiff"
        Me.lblTotalDiff.Size = New System.Drawing.Size(40, 19)
        Me.lblTotalDiff.TabIndex = 47
        Me.lblTotalDiff.Text = "Diff:"
        '
        'bwSync
        '
        '
        'bwShowData
        '
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem1})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(893, 24)
        Me.MenuStrip1.TabIndex = 48
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuReconcileByRangNBR, Me.mnuSummaryEOD})
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(50, 20)
        Me.ToolStripMenuItem1.Text = "Menu"
        '
        'mnuReconcileByRangNBR
        '
        Me.mnuReconcileByRangNBR.Name = "mnuReconcileByRangNBR"
        Me.mnuReconcileByRangNBR.Size = New System.Drawing.Size(194, 22)
        Me.mnuReconcileByRangNBR.Text = "Reconcile by rang NBR"
        '
        'mnuSummaryEOD
        '
        Me.mnuSummaryEOD.Name = "mnuSummaryEOD"
        Me.mnuSummaryEOD.Size = New System.Drawing.Size(194, 22)
        Me.mnuSummaryEOD.Text = "Summary Status (EOD)"
        '
        'tabCtrlMain
        '
        Me.tabCtrlMain.Controls.Add(Me.tabMissing)
        Me.tabCtrlMain.Controls.Add(Me.tabCompare)
        Me.tabCtrlMain.Location = New System.Drawing.Point(26, 122)
        Me.tabCtrlMain.Name = "tabCtrlMain"
        Me.tabCtrlMain.SelectedIndex = 0
        Me.tabCtrlMain.Size = New System.Drawing.Size(842, 334)
        Me.tabCtrlMain.TabIndex = 49
        '
        'tabMissing
        '
        Me.tabMissing.Controls.Add(Me.dtgSiteData)
        Me.tabMissing.Location = New System.Drawing.Point(4, 22)
        Me.tabMissing.Name = "tabMissing"
        Me.tabMissing.Padding = New System.Windows.Forms.Padding(3)
        Me.tabMissing.Size = New System.Drawing.Size(834, 308)
        Me.tabMissing.TabIndex = 0
        Me.tabMissing.Text = "Missing Data at HQ"
        Me.tabMissing.UseVisualStyleBackColor = True
        '
        'dtgSiteData
        '
        Me.dtgSiteData.AllowUserToAddRows = False
        Me.dtgSiteData.AllowUserToDeleteRows = False
        Me.dtgSiteData.AllowUserToOrderColumns = True
        Me.dtgSiteData.AutoGenerateColumns = False
        Me.dtgSiteData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgSiteData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ReceiptNbrDataGridViewTextBoxColumn, Me.ReceiptDateDataGridViewTextBoxColumn, Me.ReceiptTypeDataGridViewTextBoxColumn, Me.PlateNumberDataGridViewTextBoxColumn, Me.NetAmtDataGridViewTextBoxColumn, Me.CustPlateDataGridViewTextBoxColumn, Me.CompanyTaxIdDataGridViewTextBoxColumn, Me.CompanyAddressDataGridViewTextBoxColumn, Me.CompanyBranchNameDataGridViewTextBoxColumn, Me.CompanyNameDataGridViewTextBoxColumn, Me.CustTaxIdDataGridViewTextBoxColumn, Me.FullTaxNbrRefDataGridViewTextBoxColumn, Me.PosNbrDataGridViewTextBoxColumn, Me.ProdNameDataGridViewTextBoxColumn, Me.PumpNbrDataGridViewTextBoxColumn, Me.QtyDataGridViewTextBoxColumn, Me.RefNbrDataGridViewTextBoxColumn, Me.StationIdDataGridViewTextBoxColumn, Me.StatusDataGridViewTextBoxColumn, Me.TotalAmtDataGridViewTextBoxColumn, Me.VatAmtDataGridViewTextBoxColumn, Me.CnRefDataGridViewTextBoxColumn, Me.CustBranchNameDataGridViewTextBoxColumn, Me.CustBranchNoDataGridViewTextBoxColumn, Me.CustNameDataGridViewTextBoxColumn})
        Me.dtgSiteData.DataSource = Me.FullTaxReceiptUIBindingSource
        Me.dtgSiteData.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dtgSiteData.Location = New System.Drawing.Point(15, 11)
        Me.dtgSiteData.Name = "dtgSiteData"
        Me.dtgSiteData.RowHeadersVisible = False
        Me.dtgSiteData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgSiteData.Size = New System.Drawing.Size(787, 291)
        Me.dtgSiteData.TabIndex = 46
        '
        'ReceiptNbrDataGridViewTextBoxColumn
        '
        Me.ReceiptNbrDataGridViewTextBoxColumn.DataPropertyName = "ReceiptNbr"
        Me.ReceiptNbrDataGridViewTextBoxColumn.HeaderText = "ReceiptNbr"
        Me.ReceiptNbrDataGridViewTextBoxColumn.Name = "ReceiptNbrDataGridViewTextBoxColumn"
        Me.ReceiptNbrDataGridViewTextBoxColumn.ReadOnly = True
        Me.ReceiptNbrDataGridViewTextBoxColumn.Width = 130
        '
        'ReceiptDateDataGridViewTextBoxColumn
        '
        Me.ReceiptDateDataGridViewTextBoxColumn.DataPropertyName = "ReceiptDate"
        Me.ReceiptDateDataGridViewTextBoxColumn.HeaderText = "ReceiptDate"
        Me.ReceiptDateDataGridViewTextBoxColumn.Name = "ReceiptDateDataGridViewTextBoxColumn"
        Me.ReceiptDateDataGridViewTextBoxColumn.ReadOnly = True
        Me.ReceiptDateDataGridViewTextBoxColumn.Width = 120
        '
        'ReceiptTypeDataGridViewTextBoxColumn
        '
        Me.ReceiptTypeDataGridViewTextBoxColumn.DataPropertyName = "ReceiptType"
        Me.ReceiptTypeDataGridViewTextBoxColumn.HeaderText = "ReceiptType"
        Me.ReceiptTypeDataGridViewTextBoxColumn.Name = "ReceiptTypeDataGridViewTextBoxColumn"
        Me.ReceiptTypeDataGridViewTextBoxColumn.ReadOnly = True
        Me.ReceiptTypeDataGridViewTextBoxColumn.Width = 70
        '
        'PlateNumberDataGridViewTextBoxColumn
        '
        Me.PlateNumberDataGridViewTextBoxColumn.DataPropertyName = "PlateNumber"
        Me.PlateNumberDataGridViewTextBoxColumn.HeaderText = "PlateNumber"
        Me.PlateNumberDataGridViewTextBoxColumn.Name = "PlateNumberDataGridViewTextBoxColumn"
        Me.PlateNumberDataGridViewTextBoxColumn.ReadOnly = True
        '
        'NetAmtDataGridViewTextBoxColumn
        '
        Me.NetAmtDataGridViewTextBoxColumn.DataPropertyName = "NetAmt"
        Me.NetAmtDataGridViewTextBoxColumn.HeaderText = "NetAmt"
        Me.NetAmtDataGridViewTextBoxColumn.Name = "NetAmtDataGridViewTextBoxColumn"
        Me.NetAmtDataGridViewTextBoxColumn.ReadOnly = True
        '
        'CustPlateDataGridViewTextBoxColumn
        '
        Me.CustPlateDataGridViewTextBoxColumn.DataPropertyName = "CustPlate"
        Me.CustPlateDataGridViewTextBoxColumn.HeaderText = "CustPlate"
        Me.CustPlateDataGridViewTextBoxColumn.Name = "CustPlateDataGridViewTextBoxColumn"
        Me.CustPlateDataGridViewTextBoxColumn.ReadOnly = True
        '
        'CompanyTaxIdDataGridViewTextBoxColumn
        '
        Me.CompanyTaxIdDataGridViewTextBoxColumn.DataPropertyName = "CompanyTaxId"
        Me.CompanyTaxIdDataGridViewTextBoxColumn.HeaderText = "CompanyTaxId"
        Me.CompanyTaxIdDataGridViewTextBoxColumn.Name = "CompanyTaxIdDataGridViewTextBoxColumn"
        Me.CompanyTaxIdDataGridViewTextBoxColumn.ReadOnly = True
        '
        'CompanyAddressDataGridViewTextBoxColumn
        '
        Me.CompanyAddressDataGridViewTextBoxColumn.DataPropertyName = "CompanyAddress"
        Me.CompanyAddressDataGridViewTextBoxColumn.HeaderText = "CompanyAddress"
        Me.CompanyAddressDataGridViewTextBoxColumn.Name = "CompanyAddressDataGridViewTextBoxColumn"
        Me.CompanyAddressDataGridViewTextBoxColumn.ReadOnly = True
        '
        'CompanyBranchNameDataGridViewTextBoxColumn
        '
        Me.CompanyBranchNameDataGridViewTextBoxColumn.DataPropertyName = "CompanyBranchName"
        Me.CompanyBranchNameDataGridViewTextBoxColumn.HeaderText = "CompanyBranchName"
        Me.CompanyBranchNameDataGridViewTextBoxColumn.Name = "CompanyBranchNameDataGridViewTextBoxColumn"
        Me.CompanyBranchNameDataGridViewTextBoxColumn.ReadOnly = True
        '
        'CompanyNameDataGridViewTextBoxColumn
        '
        Me.CompanyNameDataGridViewTextBoxColumn.DataPropertyName = "CompanyName"
        Me.CompanyNameDataGridViewTextBoxColumn.HeaderText = "CompanyName"
        Me.CompanyNameDataGridViewTextBoxColumn.Name = "CompanyNameDataGridViewTextBoxColumn"
        Me.CompanyNameDataGridViewTextBoxColumn.ReadOnly = True
        '
        'CustTaxIdDataGridViewTextBoxColumn
        '
        Me.CustTaxIdDataGridViewTextBoxColumn.DataPropertyName = "CustTaxId"
        Me.CustTaxIdDataGridViewTextBoxColumn.HeaderText = "CustTaxId"
        Me.CustTaxIdDataGridViewTextBoxColumn.Name = "CustTaxIdDataGridViewTextBoxColumn"
        Me.CustTaxIdDataGridViewTextBoxColumn.ReadOnly = True
        '
        'FullTaxNbrRefDataGridViewTextBoxColumn
        '
        Me.FullTaxNbrRefDataGridViewTextBoxColumn.DataPropertyName = "FullTaxNbrRef"
        Me.FullTaxNbrRefDataGridViewTextBoxColumn.HeaderText = "FullTaxNbrRef"
        Me.FullTaxNbrRefDataGridViewTextBoxColumn.Name = "FullTaxNbrRefDataGridViewTextBoxColumn"
        Me.FullTaxNbrRefDataGridViewTextBoxColumn.ReadOnly = True
        '
        'PosNbrDataGridViewTextBoxColumn
        '
        Me.PosNbrDataGridViewTextBoxColumn.DataPropertyName = "PosNbr"
        Me.PosNbrDataGridViewTextBoxColumn.HeaderText = "PosNbr"
        Me.PosNbrDataGridViewTextBoxColumn.Name = "PosNbrDataGridViewTextBoxColumn"
        Me.PosNbrDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ProdNameDataGridViewTextBoxColumn
        '
        Me.ProdNameDataGridViewTextBoxColumn.DataPropertyName = "ProdName"
        Me.ProdNameDataGridViewTextBoxColumn.HeaderText = "ProdName"
        Me.ProdNameDataGridViewTextBoxColumn.Name = "ProdNameDataGridViewTextBoxColumn"
        Me.ProdNameDataGridViewTextBoxColumn.ReadOnly = True
        '
        'PumpNbrDataGridViewTextBoxColumn
        '
        Me.PumpNbrDataGridViewTextBoxColumn.DataPropertyName = "PumpNbr"
        Me.PumpNbrDataGridViewTextBoxColumn.HeaderText = "PumpNbr"
        Me.PumpNbrDataGridViewTextBoxColumn.Name = "PumpNbrDataGridViewTextBoxColumn"
        Me.PumpNbrDataGridViewTextBoxColumn.ReadOnly = True
        '
        'QtyDataGridViewTextBoxColumn
        '
        Me.QtyDataGridViewTextBoxColumn.DataPropertyName = "Qty"
        Me.QtyDataGridViewTextBoxColumn.HeaderText = "Qty"
        Me.QtyDataGridViewTextBoxColumn.Name = "QtyDataGridViewTextBoxColumn"
        Me.QtyDataGridViewTextBoxColumn.ReadOnly = True
        '
        'RefNbrDataGridViewTextBoxColumn
        '
        Me.RefNbrDataGridViewTextBoxColumn.DataPropertyName = "RefNbr"
        Me.RefNbrDataGridViewTextBoxColumn.HeaderText = "RefNbr"
        Me.RefNbrDataGridViewTextBoxColumn.Name = "RefNbrDataGridViewTextBoxColumn"
        Me.RefNbrDataGridViewTextBoxColumn.ReadOnly = True
        '
        'StationIdDataGridViewTextBoxColumn
        '
        Me.StationIdDataGridViewTextBoxColumn.DataPropertyName = "StationId"
        Me.StationIdDataGridViewTextBoxColumn.HeaderText = "StationId"
        Me.StationIdDataGridViewTextBoxColumn.Name = "StationIdDataGridViewTextBoxColumn"
        Me.StationIdDataGridViewTextBoxColumn.ReadOnly = True
        '
        'StatusDataGridViewTextBoxColumn
        '
        Me.StatusDataGridViewTextBoxColumn.DataPropertyName = "Status"
        Me.StatusDataGridViewTextBoxColumn.HeaderText = "Status"
        Me.StatusDataGridViewTextBoxColumn.Name = "StatusDataGridViewTextBoxColumn"
        Me.StatusDataGridViewTextBoxColumn.ReadOnly = True
        '
        'TotalAmtDataGridViewTextBoxColumn
        '
        Me.TotalAmtDataGridViewTextBoxColumn.DataPropertyName = "TotalAmt"
        Me.TotalAmtDataGridViewTextBoxColumn.HeaderText = "TotalAmt"
        Me.TotalAmtDataGridViewTextBoxColumn.Name = "TotalAmtDataGridViewTextBoxColumn"
        Me.TotalAmtDataGridViewTextBoxColumn.ReadOnly = True
        '
        'VatAmtDataGridViewTextBoxColumn
        '
        Me.VatAmtDataGridViewTextBoxColumn.DataPropertyName = "VatAmt"
        Me.VatAmtDataGridViewTextBoxColumn.HeaderText = "VatAmt"
        Me.VatAmtDataGridViewTextBoxColumn.Name = "VatAmtDataGridViewTextBoxColumn"
        Me.VatAmtDataGridViewTextBoxColumn.ReadOnly = True
        '
        'CnRefDataGridViewTextBoxColumn
        '
        Me.CnRefDataGridViewTextBoxColumn.DataPropertyName = "CnRef"
        Me.CnRefDataGridViewTextBoxColumn.HeaderText = "CnRef"
        Me.CnRefDataGridViewTextBoxColumn.Name = "CnRefDataGridViewTextBoxColumn"
        Me.CnRefDataGridViewTextBoxColumn.ReadOnly = True
        '
        'CustBranchNameDataGridViewTextBoxColumn
        '
        Me.CustBranchNameDataGridViewTextBoxColumn.DataPropertyName = "CustBranchName"
        Me.CustBranchNameDataGridViewTextBoxColumn.HeaderText = "CustBranchName"
        Me.CustBranchNameDataGridViewTextBoxColumn.Name = "CustBranchNameDataGridViewTextBoxColumn"
        Me.CustBranchNameDataGridViewTextBoxColumn.ReadOnly = True
        '
        'CustBranchNoDataGridViewTextBoxColumn
        '
        Me.CustBranchNoDataGridViewTextBoxColumn.DataPropertyName = "CustBranchNo"
        Me.CustBranchNoDataGridViewTextBoxColumn.HeaderText = "CustBranchNo"
        Me.CustBranchNoDataGridViewTextBoxColumn.Name = "CustBranchNoDataGridViewTextBoxColumn"
        Me.CustBranchNoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'CustNameDataGridViewTextBoxColumn
        '
        Me.CustNameDataGridViewTextBoxColumn.DataPropertyName = "CustName"
        Me.CustNameDataGridViewTextBoxColumn.HeaderText = "CustName"
        Me.CustNameDataGridViewTextBoxColumn.Name = "CustNameDataGridViewTextBoxColumn"
        Me.CustNameDataGridViewTextBoxColumn.ReadOnly = True
        '
        'FullTaxReceiptUIBindingSource
        '
        Me.FullTaxReceiptUIBindingSource.DataSource = GetType(FTS_Services.Domain.BO.UI.FullTaxReceiptUI)
        '
        'tabCompare
        '
        Me.tabCompare.Controls.Add(Me.dgwHQ)
        Me.tabCompare.Controls.Add(Me.dgwSite)
        Me.tabCompare.Location = New System.Drawing.Point(4, 22)
        Me.tabCompare.Name = "tabCompare"
        Me.tabCompare.Padding = New System.Windows.Forms.Padding(3)
        Me.tabCompare.Size = New System.Drawing.Size(834, 308)
        Me.tabCompare.TabIndex = 1
        Me.tabCompare.Text = "Compare Site <--> HQ"
        Me.tabCompare.UseVisualStyleBackColor = True
        '
        'dgwHQ
        '
        Me.dgwHQ.AllowUserToAddRows = False
        Me.dgwHQ.AllowUserToDeleteRows = False
        Me.dgwHQ.AllowUserToOrderColumns = True
        Me.dgwHQ.AutoGenerateColumns = False
        Me.dgwHQ.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgwHQ.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ReceiptNbrDataGridViewTextBoxColumn2, Me.PlateNoDataGridViewTextBoxColumn, Me.ReceiptDateDataGridViewTextBoxColumn2, Me.CustomerTaxIdDataGridViewTextBoxColumn, Me.NetAmtDataGridViewTextBoxColumn2, Me.ShiftNoDataGridViewTextBoxColumn})
        Me.dgwHQ.DataSource = Me.HQReceiptBindingSource
        Me.dgwHQ.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dgwHQ.Location = New System.Drawing.Point(424, 15)
        Me.dgwHQ.Name = "dgwHQ"
        Me.dgwHQ.RowHeadersVisible = False
        Me.dgwHQ.Size = New System.Drawing.Size(400, 277)
        Me.dgwHQ.TabIndex = 1
        '
        'ReceiptNbrDataGridViewTextBoxColumn2
        '
        Me.ReceiptNbrDataGridViewTextBoxColumn2.DataPropertyName = "receiptNbr"
        Me.ReceiptNbrDataGridViewTextBoxColumn2.HeaderText = "receiptNbr"
        Me.ReceiptNbrDataGridViewTextBoxColumn2.Name = "ReceiptNbrDataGridViewTextBoxColumn2"
        Me.ReceiptNbrDataGridViewTextBoxColumn2.Width = 130
        '
        'PlateNoDataGridViewTextBoxColumn
        '
        Me.PlateNoDataGridViewTextBoxColumn.DataPropertyName = "plateNo"
        Me.PlateNoDataGridViewTextBoxColumn.HeaderText = "plateNo"
        Me.PlateNoDataGridViewTextBoxColumn.Name = "PlateNoDataGridViewTextBoxColumn"
        '
        'ReceiptDateDataGridViewTextBoxColumn2
        '
        Me.ReceiptDateDataGridViewTextBoxColumn2.DataPropertyName = "receiptDate"
        Me.ReceiptDateDataGridViewTextBoxColumn2.HeaderText = "receiptDate"
        Me.ReceiptDateDataGridViewTextBoxColumn2.Name = "ReceiptDateDataGridViewTextBoxColumn2"
        Me.ReceiptDateDataGridViewTextBoxColumn2.Width = 130
        '
        'CustomerTaxIdDataGridViewTextBoxColumn
        '
        Me.CustomerTaxIdDataGridViewTextBoxColumn.DataPropertyName = "customerTaxId"
        Me.CustomerTaxIdDataGridViewTextBoxColumn.HeaderText = "customerTaxId"
        Me.CustomerTaxIdDataGridViewTextBoxColumn.Name = "CustomerTaxIdDataGridViewTextBoxColumn"
        '
        'NetAmtDataGridViewTextBoxColumn2
        '
        Me.NetAmtDataGridViewTextBoxColumn2.DataPropertyName = "netAmt"
        Me.NetAmtDataGridViewTextBoxColumn2.HeaderText = "netAmt"
        Me.NetAmtDataGridViewTextBoxColumn2.Name = "NetAmtDataGridViewTextBoxColumn2"
        '
        'ShiftNoDataGridViewTextBoxColumn
        '
        Me.ShiftNoDataGridViewTextBoxColumn.DataPropertyName = "shiftNo"
        Me.ShiftNoDataGridViewTextBoxColumn.HeaderText = "shiftNo"
        Me.ShiftNoDataGridViewTextBoxColumn.Name = "ShiftNoDataGridViewTextBoxColumn"
        '
        'HQReceiptBindingSource
        '
        Me.HQReceiptBindingSource.DataSource = GetType(FTS_Reconcile.Domain.BO.UI.HQReceipt)
        '
        'dgwSite
        '
        Me.dgwSite.AllowUserToAddRows = False
        Me.dgwSite.AllowUserToDeleteRows = False
        Me.dgwSite.AllowUserToOrderColumns = True
        Me.dgwSite.AutoGenerateColumns = False
        Me.dgwSite.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgwSite.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ReceiptNbrDataGridViewTextBoxColumn1, Me.PlateNumberDataGridViewTextBoxColumn1, Me.ReceiptDateDataGridViewTextBoxColumn1, Me.CompanyTaxIdDataGridViewTextBoxColumn1, Me.CompanyNameDataGridViewTextBoxColumn1, Me.NetAmtDataGridViewTextBoxColumn1, Me.ReceiptTypeDataGridViewTextBoxColumn1, Me.CompanyAddressDataGridViewTextBoxColumn1, Me.CompanyBranchNoDataGridViewTextBoxColumn, Me.FullTaxNbrRefDataGridViewTextBoxColumn1, Me.RefNbrDataGridViewTextBoxColumn1, Me.CustBranchNoDataGridViewTextBoxColumn1, Me.CustNameDataGridViewTextBoxColumn1, Me.CustTaxIdDataGridViewTextBoxColumn1})
        Me.dgwSite.DataSource = Me.FullTaxReceiptUIBindingSource1
        Me.dgwSite.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dgwSite.Location = New System.Drawing.Point(14, 15)
        Me.dgwSite.Name = "dgwSite"
        Me.dgwSite.Size = New System.Drawing.Size(400, 277)
        Me.dgwSite.TabIndex = 0
        '
        'ReceiptNbrDataGridViewTextBoxColumn1
        '
        Me.ReceiptNbrDataGridViewTextBoxColumn1.DataPropertyName = "ReceiptNbr"
        Me.ReceiptNbrDataGridViewTextBoxColumn1.HeaderText = "ReceiptNbr"
        Me.ReceiptNbrDataGridViewTextBoxColumn1.Name = "ReceiptNbrDataGridViewTextBoxColumn1"
        Me.ReceiptNbrDataGridViewTextBoxColumn1.ReadOnly = True
        Me.ReceiptNbrDataGridViewTextBoxColumn1.Width = 130
        '
        'PlateNumberDataGridViewTextBoxColumn1
        '
        Me.PlateNumberDataGridViewTextBoxColumn1.DataPropertyName = "PlateNumber"
        Me.PlateNumberDataGridViewTextBoxColumn1.HeaderText = "PlateNumber"
        Me.PlateNumberDataGridViewTextBoxColumn1.Name = "PlateNumberDataGridViewTextBoxColumn1"
        Me.PlateNumberDataGridViewTextBoxColumn1.ReadOnly = True
        '
        'ReceiptDateDataGridViewTextBoxColumn1
        '
        Me.ReceiptDateDataGridViewTextBoxColumn1.DataPropertyName = "ReceiptDate"
        Me.ReceiptDateDataGridViewTextBoxColumn1.HeaderText = "ReceiptDate"
        Me.ReceiptDateDataGridViewTextBoxColumn1.Name = "ReceiptDateDataGridViewTextBoxColumn1"
        Me.ReceiptDateDataGridViewTextBoxColumn1.ReadOnly = True
        Me.ReceiptDateDataGridViewTextBoxColumn1.Width = 130
        '
        'CompanyTaxIdDataGridViewTextBoxColumn1
        '
        Me.CompanyTaxIdDataGridViewTextBoxColumn1.DataPropertyName = "CompanyTaxId"
        Me.CompanyTaxIdDataGridViewTextBoxColumn1.HeaderText = "CompanyTaxId"
        Me.CompanyTaxIdDataGridViewTextBoxColumn1.Name = "CompanyTaxIdDataGridViewTextBoxColumn1"
        Me.CompanyTaxIdDataGridViewTextBoxColumn1.ReadOnly = True
        '
        'CompanyNameDataGridViewTextBoxColumn1
        '
        Me.CompanyNameDataGridViewTextBoxColumn1.DataPropertyName = "CompanyName"
        Me.CompanyNameDataGridViewTextBoxColumn1.HeaderText = "CompanyName"
        Me.CompanyNameDataGridViewTextBoxColumn1.Name = "CompanyNameDataGridViewTextBoxColumn1"
        Me.CompanyNameDataGridViewTextBoxColumn1.ReadOnly = True
        '
        'NetAmtDataGridViewTextBoxColumn1
        '
        Me.NetAmtDataGridViewTextBoxColumn1.DataPropertyName = "NetAmt"
        Me.NetAmtDataGridViewTextBoxColumn1.HeaderText = "NetAmt"
        Me.NetAmtDataGridViewTextBoxColumn1.Name = "NetAmtDataGridViewTextBoxColumn1"
        Me.NetAmtDataGridViewTextBoxColumn1.ReadOnly = True
        '
        'ReceiptTypeDataGridViewTextBoxColumn1
        '
        Me.ReceiptTypeDataGridViewTextBoxColumn1.DataPropertyName = "ReceiptType"
        Me.ReceiptTypeDataGridViewTextBoxColumn1.HeaderText = "ReceiptType"
        Me.ReceiptTypeDataGridViewTextBoxColumn1.Name = "ReceiptTypeDataGridViewTextBoxColumn1"
        Me.ReceiptTypeDataGridViewTextBoxColumn1.ReadOnly = True
        '
        'CompanyAddressDataGridViewTextBoxColumn1
        '
        Me.CompanyAddressDataGridViewTextBoxColumn1.DataPropertyName = "CompanyAddress"
        Me.CompanyAddressDataGridViewTextBoxColumn1.HeaderText = "CompanyAddress"
        Me.CompanyAddressDataGridViewTextBoxColumn1.Name = "CompanyAddressDataGridViewTextBoxColumn1"
        Me.CompanyAddressDataGridViewTextBoxColumn1.ReadOnly = True
        '
        'CompanyBranchNoDataGridViewTextBoxColumn
        '
        Me.CompanyBranchNoDataGridViewTextBoxColumn.DataPropertyName = "CompanyBranchNo"
        Me.CompanyBranchNoDataGridViewTextBoxColumn.HeaderText = "CompanyBranchNo"
        Me.CompanyBranchNoDataGridViewTextBoxColumn.Name = "CompanyBranchNoDataGridViewTextBoxColumn"
        Me.CompanyBranchNoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'FullTaxNbrRefDataGridViewTextBoxColumn1
        '
        Me.FullTaxNbrRefDataGridViewTextBoxColumn1.DataPropertyName = "FullTaxNbrRef"
        Me.FullTaxNbrRefDataGridViewTextBoxColumn1.HeaderText = "FullTaxNbrRef"
        Me.FullTaxNbrRefDataGridViewTextBoxColumn1.Name = "FullTaxNbrRefDataGridViewTextBoxColumn1"
        Me.FullTaxNbrRefDataGridViewTextBoxColumn1.ReadOnly = True
        '
        'RefNbrDataGridViewTextBoxColumn1
        '
        Me.RefNbrDataGridViewTextBoxColumn1.DataPropertyName = "RefNbr"
        Me.RefNbrDataGridViewTextBoxColumn1.HeaderText = "RefNbr"
        Me.RefNbrDataGridViewTextBoxColumn1.Name = "RefNbrDataGridViewTextBoxColumn1"
        Me.RefNbrDataGridViewTextBoxColumn1.ReadOnly = True
        '
        'CustBranchNoDataGridViewTextBoxColumn1
        '
        Me.CustBranchNoDataGridViewTextBoxColumn1.DataPropertyName = "CustBranchNo"
        Me.CustBranchNoDataGridViewTextBoxColumn1.HeaderText = "CustBranchNo"
        Me.CustBranchNoDataGridViewTextBoxColumn1.Name = "CustBranchNoDataGridViewTextBoxColumn1"
        Me.CustBranchNoDataGridViewTextBoxColumn1.ReadOnly = True
        '
        'CustNameDataGridViewTextBoxColumn1
        '
        Me.CustNameDataGridViewTextBoxColumn1.DataPropertyName = "CustName"
        Me.CustNameDataGridViewTextBoxColumn1.HeaderText = "CustName"
        Me.CustNameDataGridViewTextBoxColumn1.Name = "CustNameDataGridViewTextBoxColumn1"
        Me.CustNameDataGridViewTextBoxColumn1.ReadOnly = True
        '
        'CustTaxIdDataGridViewTextBoxColumn1
        '
        Me.CustTaxIdDataGridViewTextBoxColumn1.DataPropertyName = "CustTaxId"
        Me.CustTaxIdDataGridViewTextBoxColumn1.HeaderText = "CustTaxId"
        Me.CustTaxIdDataGridViewTextBoxColumn1.Name = "CustTaxIdDataGridViewTextBoxColumn1"
        Me.CustTaxIdDataGridViewTextBoxColumn1.ReadOnly = True
        '
        'FullTaxReceiptUIBindingSource1
        '
        Me.FullTaxReceiptUIBindingSource1.DataSource = GetType(FTS_Services.Domain.BO.UI.FullTaxReceiptUI)
        '
        'lblInfo
        '
        Me.lblInfo.AutoSize = True
        Me.lblInfo.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.lblInfo.Location = New System.Drawing.Point(258, 508)
        Me.lblInfo.Name = "lblInfo"
        Me.lblInfo.Size = New System.Drawing.Size(144, 19)
        Me.lblInfo.TabIndex = 50
        Me.lblInfo.Text = "Missing Data at HQ"
        '
        'frmRequestDataFromSite
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(893, 547)
        Me.Controls.Add(Me.lblInfo)
        Me.Controls.Add(Me.tabCtrlMain)
        Me.Controls.Add(Me.lblTotalDiff)
        Me.Controls.Add(Me.lblTotalHQ)
        Me.Controls.Add(Me.lblTotalSite)
        Me.Controls.Add(Me.btnSync)
        Me.Controls.Add(Me.cbbFixIp)
        Me.Controls.Add(Me.chbFixIp)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.dtpStartDate)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.rbOil)
        Me.Controls.Add(Me.rbNgv)
        Me.Controls.Add(Me.cbbSite)
        Me.Controls.Add(Me.btnShow)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmRequestDataFromSite"
        Me.Text = "Request Data From Site"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.tabCtrlMain.ResumeLayout(False)
        Me.tabMissing.ResumeLayout(False)
        CType(Me.dtgSiteData, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FullTaxReceiptUIBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabCompare.ResumeLayout(False)
        CType(Me.dgwHQ, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.HQReceiptBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgwSite, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FullTaxReceiptUIBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents cbbFixIp As ComboBox
    Friend WithEvents chbFixIp As CheckBox
    Friend WithEvents Label3 As Label
    Friend WithEvents dtpStartDate As DateTimePicker
    Friend WithEvents Label2 As Label
    Friend WithEvents rbOil As RadioButton
    Friend WithEvents rbNgv As RadioButton
    Friend WithEvents cbbSite As ComboBox
    Friend WithEvents btnShow As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents btnSync As Button
    Friend WithEvents FullTaxReceiptUIBindingSource As BindingSource
    Friend WithEvents lblTotalSite As Label
    Friend WithEvents lblTotalHQ As Label
    Friend WithEvents lblTotalDiff As Label
    Friend WithEvents bwSync As System.ComponentModel.BackgroundWorker
    Friend WithEvents bwShowData As System.ComponentModel.BackgroundWorker
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents ToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents mnuReconcileByRangNBR As ToolStripMenuItem
    Friend WithEvents tabCtrlMain As TabControl
    Friend WithEvents tabMissing As TabPage
    Friend WithEvents dtgSiteData As DataGridView
    Friend WithEvents tabCompare As TabPage
    Friend WithEvents lblInfo As Label
    Friend WithEvents dgwSite As DataGridView
    Friend WithEvents ReceiptNbrDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ReceiptDateDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ReceiptTypeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents PlateNumberDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents NetAmtDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CustPlateDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CompanyTaxIdDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CompanyAddressDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CompanyBranchNameDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CompanyNameDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CustTaxIdDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents FullTaxNbrRefDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents PosNbrDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ProdNameDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents PumpNbrDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents QtyDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents RefNbrDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents StationIdDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents StatusDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TotalAmtDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents VatAmtDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CnRefDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CustBranchNameDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CustBranchNoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CustNameDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents dgwHQ As DataGridView
    Friend WithEvents FullTaxReceiptUIBindingSource1 As BindingSource
    Friend WithEvents HQReceiptBindingSource As BindingSource
    Friend WithEvents ReceiptNbrDataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents PlateNoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ReceiptDateDataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents CustomerTaxIdDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents NetAmtDataGridViewTextBoxColumn2 As DataGridViewTextBoxColumn
    Friend WithEvents ShiftNoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents ReceiptNbrDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents PlateNumberDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents ReceiptDateDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents CompanyTaxIdDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents CompanyNameDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents NetAmtDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents ReceiptTypeDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents CompanyAddressDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents CompanyBranchNoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents FullTaxNbrRefDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents RefNbrDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents CustBranchNoDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents CustNameDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents CustTaxIdDataGridViewTextBoxColumn1 As DataGridViewTextBoxColumn
    Friend WithEvents mnuSummaryEOD As ToolStripMenuItem
End Class
