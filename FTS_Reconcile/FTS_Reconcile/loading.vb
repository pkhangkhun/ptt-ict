﻿Imports MRG.Controls.UI

Public Class loading

    Public Delegate Sub PictureVisibilityDelegate(ByVal visibility As Boolean)
    Dim ChangePictureVisibility As PictureVisibilityDelegate
    'Dim d As AsyncProcessDelegate

    Dim frm As loadingTop
    Dim OL As Overlay

    Public Sub ChangeVisibility(ByVal visibility As Boolean)
        'Show animate picture
        PictureBox1.Visible = visibility

        If visibility Then
            OL = New Overlay(Me)
            OL.Show()
        Else
            OL.Dispose()
        End If

    End Sub

    Private Sub BackgroundWorker1_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        'On process
        Me.Invoke(ChangePictureVisibility, True)

        loadingProcess()
    End Sub

    Private Sub BackgroundWorker1_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
        'job done, restore UI
        Me.Invoke(ChangePictureVisibility, False)
    End Sub

    Private Sub loading_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        PictureBox1.Visible = False
        ChangePictureVisibility = AddressOf ChangeVisibility

        'd = AddressOf loadingProcess

    End Sub

    Private Sub loadingProcess()
        For i As Integer = 0 To 50
            System.Threading.Thread.Sleep(100)
        Next
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If Not BackgroundWorker1.IsBusy = True Then
            BackgroundWorker1.RunWorkerAsync()
        End If

        'If Not IsAsyncBusy Then
        '    RunAsyncOperation(d)
        'End If
    End Sub


    Private Class Overlay
        Inherits System.Windows.Forms.Form

        Private WithEvents frm As Form

        Public Sub New(ByVal FormToDisable As Form)
            Me.StartPosition = FormStartPosition.Manual
            Me.frm = FormToDisable
            Me.FormBorderStyle = Windows.Forms.FormBorderStyle.None
            Me.ShowInTaskbar = False
            Me.Opacity = 0.5
            Me.BackColor = Color.Gray
            Me.Owner = FormToDisable
        End Sub

        Private Sub Overlay_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
            FocusOverlay()
        End Sub

        Private Sub FocusOverlay()
            Dim menuSize As Integer = 30

            Me.Size = New Point(frm.Size.Width, frm.Size.Height - menuSize)
            Me.Location = New Point(frm.Location.X, frm.Location.Y + menuSize)
            Me.BringToFront()
            Me.Focus()
        End Sub

        Private Sub frm_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles frm.Activated
            FocusOverlay()
        End Sub

        Private Sub frm_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles frm.GotFocus
            FocusOverlay()
        End Sub

        Private Sub frm_Move(ByVal sender As Object, ByVal e As System.EventArgs) Handles frm.Move
            FocusOverlay()
        End Sub

        Private Sub frm_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles frm.Resize
            If frm.WindowState = FormWindowState.Minimized Then
                Me.Hide()
            Else
                Me.Show()
                FocusOverlay()
            End If
        End Sub
    End Class

End Class