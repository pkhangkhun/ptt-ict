Imports FTS_Reconcile.App.Dao
Imports FTS_Reconcile.App.Utility
Imports System.IO
Imports System.Messaging

Public Class fmReconcile

    Public Shared App_Path As String = New System.IO.FileInfo(Application.ExecutablePath).DirectoryName

    Dim maxReconcileCount As Integer = 5

    Private Sub Reconcile()

        Dim sql As String = ""
        Dim sqlUpdate As String = ""
        Dim status = "C"
        '    pbLoad.Style = ProgressBarStyle.Blocks

        Try
            WriteLogFile("Start Reconcile Version 1.1.2")

            sql = "select shift_no,site_no,site_type_id,reconcile_count," & _
                    "full_tax_qty,scan_qty,void_qty,replace_qty,cancel_qty,manual_qty,fleet_qty,fuel_qty,goods_qty,cn_qty,reconcile_count" & _
                    " from SUMMARY_RECONCILE where status='W'"
            sql = "select  shift_no,site_no,site_type_id,reconcile_count,full_tax_qty,scan_qty,void_qty,replace_qty,cancel_qty,manual_qty,fleet_qty,fuel_qty,goods_qty,cn_qty,reconcile_count from SUMMARY_RECONCILE where status='W' and SITE_TYPE_ID <> 0 and site_no <> '00000'"
            Dim dsShift As DataSet = QueryDao.sqlInquiry(sql)

            WriteLogFile("Total Shift to Reconcile : " + dsShift.Tables(0).Rows.Count.ToString)


            Dim reconcileCount As Integer = 0
            Dim reconcileValue As Integer = 0
            '    pbLoad.Minimum = 0
            '      pbLoad.Maximum = dsShift.Tables(0).Rows.Count


            For i As Integer = 0 To dsShift.Tables(0).Rows.Count - 1

                Me.Text = "FTS Reconcile : " + CStr(i) + " ShiftNo : " + dsShift.Tables(0).Rows(i).Item("shift_no") + " siteNo : " + dsShift.Tables(0).Rows(i).Item("site_no")

                Application.DoEvents()

                WriteLogFile("Reconcile shiftNo : " + dsShift.Tables(0).Rows(i).Item("shift_no") + " siteNo : " + dsShift.Tables(0).Rows(i).Item("site_no"))

                reconcileCount = dsShift.Tables(0).Rows(i).Item("reconcile_count")

                If reconcileCount < maxReconcileCount Then

                    sql = "select receipt_type,status,is_manual_entry,is_fleet_card,qty,is_scaned from RECEIPT " & _
                    " where Shift_no=" + DBUtil.SqlQuote(dsShift.Tables(0).Rows(i).Item("shift_no")) & _
                    " and site_no=" + DBUtil.SqlQuote(dsShift.Tables(0).Rows(i).Item("site_no")) & _
                    " and site_type_id=" + dsShift.Tables(0).Rows(i).Item("site_type_id").ToString

                    Dim dsFullTax As DataSet = QueryDao.sqlInquiry(sql)

                    Dim FullTaxQty As Integer = dsShift.Tables(0).Rows(i).Item("full_tax_qty")
                    Dim ScanQty As Integer = dsShift.Tables(0).Rows(i).Item("scan_qty")
                    Dim VoidQty As Integer = dsShift.Tables(0).Rows(i).Item("void_qty")
                    Dim ReplaceQty As Integer = dsShift.Tables(0).Rows(i).Item("replace_qty")
                    Dim CancelQty As Integer = dsShift.Tables(0).Rows(i).Item("cancel_qty")
                    Dim ManualQty As Integer = dsShift.Tables(0).Rows(i).Item("manual_qty")
                    Dim FleetQty As Integer = dsShift.Tables(0).Rows(i).Item("fleet_qty")
                    Dim FuelQty As Integer = dsShift.Tables(0).Rows(i).Item("fuel_qty")
                    Dim GoodsQty As Integer = dsShift.Tables(0).Rows(i).Item("goods_qty")
                    Dim CnQty As Integer = dsShift.Tables(0).Rows(i).Item("cn_qty")

                    Dim RFullTaxQty As Integer = 0
                    Dim RScanQty As Integer = 0
                    Dim RVoidQty As Integer = 0
                    Dim RReplaceQty As Integer = 0
                    Dim RCancelQty As Integer = 0
                    Dim RManualQty As Integer = 0
                    Dim RFleetQty As Integer = 0
                    Dim RFuelQty As Integer = 0
                    Dim RGoodsQty As Integer = 0
                    Dim RCnQty As Integer = 0

                    Dim DFullTaxQty As Integer = 0
                    Dim DScanQty As Integer = 0
                    Dim DVoidQty As Integer = 0
                    Dim DReplaceQty As Integer = 0
                    Dim DCancelQty As Integer = 0
                    Dim DManualQty As Integer = 0
                    Dim DFleetQty As Integer = 0
                    Dim DFuelQty As Integer = 0
                    Dim DGoodsQty As Integer = 0
                    Dim DCnQty As Integer = 0


                    For j As Integer = 0 To dsFullTax.Tables(0).Rows.Count - 1


                        Application.DoEvents()

                        With dsFullTax.Tables(0).Rows(j)

                            If .Item("receipt_type") = "F" Then

                                RFullTaxQty += 1

                                If .Item("is_scaned") Then
                                    RScanQty += 1
                                End If

                                If .Item("status") = "V" Then
                                    RVoidQty += 1
                                    RReplaceQty += 1
                                End If

                                If .Item("status") = "C" Then
                                    RCancelQty += 1
                                End If

                                If .Item("is_manual_entry") Then
                                    RManualQty += 1
                                End If
                                If .Item("is_fleet_card") Then
                                    RFleetQty += 1
                                End If
                                If .Item("qty") > 0 Then
                                    RFuelQty += 1
                                Else
                                    RGoodsQty += 1
                                End If

                            ElseIf .Item("receipt_type") = "D" Then

                                RCnQty += 1

                            End If

                        End With
                    Next

                    DFullTaxQty = RFullTaxQty - FullTaxQty
                    DScanQty = RScanQty - ScanQty
                    DVoidQty = RVoidQty - VoidQty
                    DReplaceQty = RReplaceQty - ReplaceQty
                    DCancelQty = RCancelQty - CancelQty
                    DManualQty = RManualQty - ManualQty
                    DFleetQty = RFleetQty - FleetQty
                    DFuelQty = RFuelQty - FuelQty
                    DGoodsQty = RGoodsQty - GoodsQty
                    DCnQty = RCnQty - CnQty

                    status = "C"

                    If DFullTaxQty <> 0 Then
                        status = "D"
                    End If
                    If DCnQty <> 0 Then
                        status = "D"
                    End If

                    WriteLogFile("Reconcile Status : " + status)

                    reconcileCount += 1
                    If DFullTaxQty = 0 Then
                        sql = "update SUMMARY_RECONCILE set " & _
                        "R_Full_Tax_Qty=" + RFullTaxQty.ToString + "," & _
                        "R_Scan_Qty=" + RFullTaxQty.ToString + "," & _
                        "R_Void_Qty=" + RVoidQty.ToString + "," & _
                        "R_Replace_Qty=" + RReplaceQty.ToString + "," & _
                        "R_Cancel_Qty=" + RCancelQty.ToString + "," & _
                        "R_Manual_Qty=" + RManualQty.ToString + "," & _
                        "R_Fleet_Qty=" + RFleetQty.ToString + "," & _
                        "R_Fuel_Qty=" + RFuelQty.ToString + "," & _
                        "R_Goods_Qty=" + RGoodsQty.ToString + "," & _
                        "R_Cn_Qty=" + RCnQty.ToString + "," & _
                        "D_Full_Tax_Qty=" + DFullTaxQty.ToString + "," & _
                        "D_Scan_Qty=" + DFullTaxQty.ToString + "," & _
                        "D_Void_Qty=" + DVoidQty.ToString + "," & _
                        "D_Replace_Qty=" + DReplaceQty.ToString + "," & _
                        "D_Cancel_Qty=" + DCancelQty.ToString + "," & _
                        "D_Manual_Qty=" + DManualQty.ToString + "," & _
                        "D_Fleet_Qty=" + DFleetQty.ToString + "," & _
                        "D_Fuel_Qty=" + DFuelQty.ToString + "," & _
                        "D_Goods_Qty=" + DGoodsQty.ToString + "," & _
                        "D_Cn_Qty=" + DCnQty.ToString + "," & _
                        "Reconcile_Count=" + reconcileCount.ToString + "," & _
                        "Status='" + status + "'" & _
                        " where Shift_no=" + DBUtil.SqlQuote(dsShift.Tables(0).Rows(i).Item("shift_no")) & _
                        " and site_no=" + DBUtil.SqlQuote(dsShift.Tables(0).Rows(i).Item("site_no")) & _
                        " and site_type_id=" + dsShift.Tables(0).Rows(i).Item("site_type_id").ToString

                        QueryDao.sqlUpdate(sql)

                        If status = "D" Then
                            Try
                                MSMQ_SendRequest(dsShift.Tables(0).Rows(i).Item("shift_no"), dsShift.Tables(0).Rows(i).Item("site_no"))
                            Catch ex As Exception
                            End Try

                        End If
                    End If
                    WriteLogFile("Reconcile Complete")

                    dsFullTax.Dispose()
                Else
                    WriteLogFile("Reconcile over limit " + maxReconcileCount.ToString + " Times")
                End If
                reconcileValue += 1
                '   pbLoad.Value = reconcileValue
            Next

            dsShift.Dispose()

        Catch ex As Exception
            WriteLogFile("Error Reconcile : " + ex.Message)
        End Try

    End Sub

    Private Shared Sub WriteLogFile(ByVal _msg As String)

        'Dim logPath As String = "c:\FTS_HQ_MsmqServ\Log"
        Dim logPath As String = App_Path + "\log"

        Dim s As String = Format(Date.Today, "yyyyMMdd")
        Dim myFile As String = "\FTS_Reconcile_" + s + ".txt"
        Dim objStreamWriter As StreamWriter
        If Not Directory.Exists(logPath) Then
            Directory.CreateDirectory(logPath)
        End If
        If File.Exists(logPath + myFile) Then
            objStreamWriter = File.AppendText(logPath + myFile)
        Else
            objStreamWriter = File.CreateText(logPath + myFile)
        End If

        _msg = Format(Date.Now, "HH:mm:ss --> ") + _msg
        objStreamWriter.WriteLine(_msg)
        objStreamWriter.Close()
    End Sub

    Private Sub MSMQ_SendRequest(ByVal shiftNo As String, ByVal siteNo As String)

        WriteLogFile("Start Send Request")

        Dim clientIP As String = ""

        Try
            Dim dsStation As DataSet = QueryDao.sqlInquiry("select ip_address from STATION where site_no=" + DBUtil.SqlQuote(siteNo))
            If dsStation.Tables(0).Rows.Count > 0 Then
                clientIP = dsStation.Tables(0).Rows(0).Item("ip_address")
            End If
            dsStation.Dispose()
        Catch ex As Exception
            WriteLogFile("Error : " + ex.Message)
        End Try

        If clientIP = "" Then
            WriteLogFile("Can not get IP-Address from SiteNo:" + siteNo)
            Exit Sub
        End If

        WriteLogFile("SiteNo:" + siteNo + " ShiftNo:" + shiftNo + " IP-Address:" + clientIP)

        Dim mqSource As String = "SERVER"
        Dim messageQ As MessageQueue
        Dim message As System.Messaging.Message
        'Dim mqTran As New MessageQueueTransaction()

        Dim qPath As String = "FormatName:DIRECT=TCP:" + clientIP + "\private$\FTS_POS_NT_IB"
        messageQ = New MessageQueue(qPath)


        message = New System.Messaging.Message(shiftNo, New BinaryMessageFormatter)

        message.Label = "REFT:" + " ShiftNo:" + shiftNo + " SiteNo:" + siteNo + " Date:" + Format(Date.Now, "yyMMdd-HHmm")

        message.AcknowledgeType = AcknowledgeTypes.None
        message.UseJournalQueue = False
        message.UseDeadLetterQueue = True

        'mqTran.Begin()
        Dim success As Boolean = True
        Try
            messageQ.Send(message)

            'messageQ.Send(message, mqTran)
            'mqTran.Commit()

            WriteLogFile("Success Send Request")
        Catch ex As Exception
            'mqTran.Abort()
            success = False

            WriteLogFile("Error Send Request : " + ex.Message)
        Finally
            messageQ.Close()
        End Try
    End Sub

    Private Sub fmReconcile_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Button1_Click(sender, e)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Reconcile()
        Catch ex As Exception
        End Try

        Me.Close()
    End Sub
End Class
