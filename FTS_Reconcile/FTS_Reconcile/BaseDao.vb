Imports System.Data.SqlClient
Imports FTS_Reconcile.App.Utility

Namespace App.Dao

    Public Class BaseDao


        Public Shared ReadOnly Property GetDBConnection() As SqlConnection
            Get
                Return DBUtil.GetDBConnection()
            End Get
        End Property

    End Class

End Namespace

