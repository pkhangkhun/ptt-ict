﻿Imports System.Globalization
Imports System.Threading
Imports FTS_Reconcile.Domain.BO.UI
Imports FTS_Reconcile.fts.constant.FTSConstant
Imports FTS_Reconcile.fts.service
Imports FTS_Reconcile.fts.util
Imports FTS_Reconcile.HQComponent.BO
Imports FTS_Services
Imports FTS_Services.App.Service
Imports FTS_Services.Domain.BO.UI
Imports FTS_Services.UI.Factory
Imports Ie.IeComponent.BO

Public Class frmRequestDataFromSite
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

    Private isInit As Boolean = False
    Private baseConStr As String

    Private siteObjectDic As New List(Of FullTaxReceipt)
    Private diffList As New List(Of FullTaxReceiptUI)
    Private siteList As New List(Of FullTaxReceiptUI)
    Private hqList As New List(Of HQReceipt)

    Public Delegate Sub UIVisibilityDelegate(ByVal siteNo As String, ByVal dataDate As Date)
    Dim ChangeUIVisibility As UIVisibilityDelegate

    Dim processStatus As Integer
    Dim processMsg As String
    Dim totalSite As Integer
    Dim totalHq As Integer


    Dim pSiteNo As String
    Dim pStartDate As Date
    Dim pToDate As Date

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        init()
    End Sub

    Private Sub init()
        ' Sets the culture to English
        Thread.CurrentThread.CurrentCulture = New CultureInfo("en-US")
        ' Sets the UI culture to English
        Thread.CurrentThread.CurrentUICulture = New CultureInfo("en-US")

        ChangeUIVisibility = AddressOf ChangeVisibility

        log.Info("Request data from SITE app is init v." + (GetType(frmMain).Assembly.GetName().Version).ToString)

        '// load connection string template (need to replace IP)
        baseConStr = System.Configuration.ConfigurationManager.AppSettings.Get("postgresConStr")

        isInit = True

        Me.Text = "Request Data From Site v." + (GetType(frmMain).Assembly.GetName().Version).ToString

        LoadSite(StationConstant.STATION_TYPE_OID)
        LoadLocalIpAddr()

    End Sub

    Private Sub LoadSite(ByVal SiteTypeId As Integer)
        Try
            cbbSite.DataSource = SiteService.gets(SiteTypeId)
            cbbSite.DisplayMember = "site_name"
            cbbSite.ValueMember = "site_code"
            cbbSite.Show()
        Catch ex As Exception
            log.Error("load site master", ex)
            Throw ex
        End Try
    End Sub

    Private Sub LoadLocalIpAddr()
        Dim ipAddr As String

        ipAddr = String.Empty
        Dim strHostName As String = System.Net.Dns.GetHostName()
        Dim iphe As System.Net.IPHostEntry = System.Net.Dns.GetHostEntry(strHostName)

        For Each ipheal As System.Net.IPAddress In iphe.AddressList
            If ipheal.AddressFamily = System.Net.Sockets.AddressFamily.InterNetwork Then
                ipAddr = ipheal.ToString()
                cbbFixIp.Items.Add(ipAddr)
            End If
        Next
    End Sub

    '// override server connection string base on selected site
    Private Sub updateConStrBySite(siteNo As String)
        Dim siteIp As String
        siteIp = SiteService.getSiteIP(siteNo)
        updateConStr(siteIp)
    End Sub

    '// override server connection with specific ip
    Private Sub updateConStr(ipAddr As String)
        FTS_Services.App.Utility.DBUtil.conStr = String.Format(baseConStr, ipAddr)
    End Sub

    Public Sub ChangeVisibility(ByVal siteNo As String, ByVal dataDate As Date)
        dtgSiteData.DataSource = Nothing
        dtgSiteData.Refresh()

        Me.siteList.Sort(Function(x, y) x.ReceiptNbr.CompareTo(y.ReceiptNbr))
        dgwSite.DataSource = Me.siteList
        dgwSite.Refresh()

        Me.hqList.Sort(Function(x, y) x.receiptNbr.CompareTo(y.receiptNbr))
        dgwHQ.DataSource = Me.hqList
        dgwHQ.Refresh()

        If Me.processStatus = statusLoad.NotSelect Then
            MessageBox.Show("Please select site", "Invalid", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        ElseIf Me.processStatus = statusLoad.NotFound Then
            MessageBox.Show("Not found reconcile data", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else

            If Me.processStatus = statusLoad.FoundDiff Then
                'diff data
                'display loaded data
                dtgSiteData.DataSource = Me.diffList
                dtgSiteData.Refresh()

                Dim msg As String
                If (siteObjectDic.Count > 0) Then
                    'Found diff transaction
                    msg = String.Format("Total data at site {0} of {1}", siteObjectDic.Count, totalSite)
                Else
                    msg = String.Format("Not found diff data, found {0} records at site", totalSite)
                End If

                lblInfo.Text = "Missing Data at HQ [Site:" + siteNo + "] - Date: " + dataDate.ToString("yyyy-MM-dd")
                lblTotalSite.Text = String.Format("Total Site: {0,5}", totalSite)
                lblTotalHQ.Text = String.Format("Total HQ: {0,5}", totalHq)
                lblTotalDiff.Text = String.Format("Total Diff: {0,5}", siteObjectDic.Count)
                MessageBox.Show(msg)
            ElseIf Me.processStatus = statusLoad.NoEOD Then
                MessageBox.Show("Not found reconcile data", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            ElseIf Me.processStatus = statusLoad.Complate Then
                MessageBox.Show("Reconcile was matched")
            Else
                MessageBox.Show(Me.processStatus.ToString + " : " + Me.processMsg)
            End If
        End If

    End Sub

    Private Sub frmRequestDataFromSite_Load(sender As Object, e As EventArgs) Handles Me.Load
        'load data initial from New method due to implement loading screen (C# base)
        init()
    End Sub

    Private Sub rbOil_CheckedChanged(sender As Object, e As EventArgs) Handles rbOil.CheckedChanged
        If isInit AndAlso rbOil.Checked Then
            cbbSite.Enabled = True
            LoadSite(StationConstant.STATION_TYPE_OID)
        End If
    End Sub

    Private Sub rbNgv_CheckedChanged(sender As Object, e As EventArgs) Handles rbNgv.CheckedChanged
        If isInit AndAlso rbNgv.Checked Then
            cbbSite.Enabled = True
            LoadSite(StationConstant.STATION_TYPE_NGV)
        End If
    End Sub

    Private Sub chbFixIp_CheckedChanged(sender As Object, e As EventArgs) Handles chbFixIp.CheckedChanged
        cbbFixIp.Enabled = chbFixIp.Checked

    End Sub

    Private Sub btnShow_Click(sender As Object, e As EventArgs) Handles btnShow.Click
        'mapping select criteria
        pSiteNo = cbbSite.SelectedValue
        pStartDate = dtpStartDate.Value.Date

        btnSync.Text = "Update to HQ"   'prepare UI wording for resend

        'display loading screen
        BeginAsyncIndication()

        '***** Backgroup process *****
        If Not bwShowData.IsBusy = True Then
            bwShowData.RunWorkerAsync()
        End If
        '***** End Backgroup process *****

    End Sub

    Private Sub bwShowData_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bwShowData.DoWork
        clearSearchResult()

        'check diff data between site and HQ
        showDataDiff(pSiteNo, pStartDate)
    End Sub

    Private Sub bwShowData_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bwShowData.RunWorkerCompleted
        'disable loading
        EndAsyncIndication()

        'display result in UI
        Me.Invoke(ChangeUIVisibility, pSiteNo, pStartDate)
    End Sub

    Private Sub clearSearchResult()
        Me.processStatus = -1
        Me.processMsg = ""

        Me.totalSite = 0
        Me.totalHq = 0

        Me.siteObjectDic.Clear()
        Me.diffList.Clear()
        Me.siteList.Clear()
        Me.hqList.Clear()

    End Sub

    'load data from site & hq for compare missing record
    Private Sub showDataDiff(siteNo As String, dataDate As Date)
        processMsg = ""

        If pSiteNo = "" Then
            processStatus = statusLoad.NotSelect
            Exit Sub
        End If

        Dim siteIp As String = ""

        'check HQ alread receive all data from summary status
        Dim sumRList As SummaryReconcileCollection = ReconcileService.checkLatestSummary(siteNo, dataDate)

        Dim str As String
        str = String.Format("Reconcile Site:{0}, Date:{1}", siteNo, dataDate.ToString("yyyy-MM-dd"))
        log.Info(str)

        If sumRList Is Nothing OrElse sumRList.Count = 0 Then
            processStatus = statusLoad.NotFound
            Exit Sub
        End If

        Try
            siteIp = SiteService.getSiteIP(siteNo)
            'Check postgres is available or not
            CommonUtils.checkRemoteAddrIsValid(siteIp, 5432)
        Catch ex As Exception
            log.Error("error when load site data : " + ex.Message, ex)
        End Try

        updateConStr(siteIp)

        'initial data list
        Dim list As New List(Of FullTaxReceiptUI)

        Dim diffCount As Integer = 0
        Dim noEODCount As Integer = 0

        Try
            For Each sumR As SummaryReconcile In sumRList
                str = String.Format("EOD shift_no:{0} - status:{1}", sumR.ShiftNo, sumR.Status)
                log.Info(str)
                If sumR Is Nothing OrElse sumR.Status = "X" Then
                    noEODCount += 1
                ElseIf sumR.Status = "C" Then
                    'skip complate summary
                    Continue For
                Else
                    diffCount += 1
                    '// load data from site
                    Dim FullTaxServ As IFullTaxService = BaseServiceFactory.GetInstance(GetType(FullTaxService))
                    Dim siteData As FullTaxReceiptCollection
                    Dim hqData As Dictionary(Of String, HQReceipt)

                    hqData = HQFullTaxSvc.getFullTaxByShiftNo(siteNo, sumR.ShiftNo)
                    siteData = FullTaxServ.GetByShiftNo(sumR.ShiftNo)

                    totalSite += siteData.Count
                    totalHq += hqData.Count

                    For Each obj As FullTaxReceipt In siteData
                        Me.siteList.Add(New FullTaxReceiptUI(obj))

                        Dim outObj As FullTaxReceiptUI

                        If Not hqData.ContainsKey(obj.ReceiptNbr) Then
                            outObj = New FullTaxReceiptUI(obj) '// UI object
                            list.Add(outObj)
                            siteObjectDic.Add(obj)  'store data into memory
                        End If
                        System.Threading.Thread.Sleep(1)
                    Next

                    For Each obj As HQReceipt In hqData.Values
                        Me.hqList.Add(obj)
                    Next
                End If
            Next
        Catch ex As Exception
            processStatus = statusLoad.Fail
            Me.processMsg = "Error : " + ex.Message
            log.Error("Error : " + ex.Message, ex)
            Exit Sub
        End Try

        Dim msg As String
        If diffCount > 0 Then
            Me.processStatus = statusLoad.FoundDiff

            'store data to global value for display
            Me.diffList = list

            If (siteObjectDic.Count > 0) Then
                'Found diff transaction
                msg = String.Format("Total data at site {0} of {1}", siteObjectDic.Count, totalSite)
            Else
                msg = String.Format("Not found diff data, found {0} records at site", totalSite)
            End If

        ElseIf noEODCount > 0 Then
            Me.processStatus = statusLoad.NoEOD
            msg = "Site not send EOD"
        Else
            Me.processStatus = statusLoad.Complate
            msg = "Reconcile was matched"
        End If

        Me.processMsg = msg
    End Sub

    Private Sub btnSync_Click(sender As Object, e As EventArgs) Handles btnSync.Click
        BeginAsyncIndication()

        If Not bwSync.IsBusy = True Then
            bwSync.RunWorkerAsync()
        End If
    End Sub

    Private Sub bwSync_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bwSync.DoWork
        'on process
        syncDataToHq()
    End Sub

    Private Sub bwSync_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bwSync.RunWorkerCompleted
        'job done, restore UI
        EndAsyncIndication()

        Dim msg As String
        msg = String.Format("Resend {0} records, complate", siteObjectDic.Count)

        btnSync.Text = "Update to HQ (Done)"
        MessageBox.Show(msg, "Complate", MessageBoxButtons.OK, MessageBoxIcon.Information)

    End Sub

    Private Sub syncDataToHq()

        Dim PaymentTranServ As IPaymentTransService = BaseServiceFactory.GetInstance(GetType(PaymentTransService))

        Try
            Dim msg As String
            'Put data on data table into local queue and send to HQ
            log.Info("Total resend data : " + siteObjectDic.Count.ToString)

            For Each fullTax As FullTaxReceipt In siteObjectDic
                msg = String.Format("Resend {0}:{1}:{2}", fullTax.SiteNo, fullTax.ReceiptDate.ToString("yyyyMMdd"), fullTax.ReceiptNbr)
                log.Info(msg)

                Dim mLabel As String = ""

                If fullTax.Status = "C" Then
                    mLabel = "FT_C_" + fullTax.ReceiptNbr
                ElseIf fullTax.PrintNo > 0 Then
                    mLabel = "FT_P_" + fullTax.ReceiptNbr
                ElseIf fullTax.ReplaceRef <> "" Then
                    mLabel = "FT_R_" + fullTax.ReceiptNbr + ":" + fullTax.ReplaceRef
                ElseIf fullTax.IsManualEntry Then
                    mLabel = "FT_M_" + fullTax.ReceiptNbr
                Else
                    mLabel = "FT_N_" + fullTax.ReceiptNbr
                End If

                Dim BitmapConverter As System.ComponentModel.TypeConverter = System.ComponentModel.TypeDescriptor.GetConverter(My.Resources.FTS_Logo.[GetType]())
                fullTax.LogoImg = DirectCast(BitmapConverter.ConvertTo(My.Resources.FTS_Logo, GetType(Byte())), Byte())

                Dim qFullTax As New Q_FullTax
                qFullTax.RePost = 1
                qFullTax.FullTax = fullTax
                qFullTax.PaymentTrans = PaymentTranServ.GetByFullTaxNbr(qFullTax.FullTax.ReceiptNbr)
                If qFullTax.FullTax.IsScaned Then
                    qFullTax.FullTax.CardImg = FTS_Services.App.Dao.CardImgDao.Retrieve(qFullTax.FullTax.ReceiptNbr)
                End If

                MQService.MSMQ_FullTaxTrans(mLabel, qFullTax)
            Next
            msg = String.Format("Resend {0} records, complate", siteObjectDic.Count)
            log.Info(msg)

        Catch ex As Exception
            Dim msg As String
            msg = String.Format("Error send q : ", ex.Message)
            log.Error(msg, ex)
            Throw ex
        End Try
    End Sub

    Private Sub mnuReconcileByRangNBR_Click(sender As Object, e As EventArgs) Handles mnuReconcileByRangNBR.Click
        My.Forms.frmMain.StartPosition = FormStartPosition.Manual
        My.Forms.frmMain.Location = Me.Location
        My.Forms.frmMain.Show()
    End Sub

    Private Sub mnuSummaryEOD_Click(sender As Object, e As EventArgs) Handles mnuSummaryEOD.Click
        My.Forms.frmEOD.StartPosition = FormStartPosition.Manual
        My.Forms.frmEOD.Location = Me.Location
        My.Forms.frmEOD.Show()
    End Sub
End Class