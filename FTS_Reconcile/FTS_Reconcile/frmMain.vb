Imports FTS_Reconcile.App.Dao
Imports FTS_Reconcile.App.Utility
Imports System.IO
Imports System.Messaging
Imports FTS_Reconcile.Helper.Services
Imports FTS_Reconcile.fts.util
Imports FTS_Reconcile.fts.service
Imports System.Net.NetworkInformation
Imports System.Threading
Imports System.Globalization

Public Class frmMain
    Private Shared ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

    Dim maxReconcileCount As Integer = 10
    Dim TimeCount As Integer = 0

    Private isInit As Boolean = False

    Private Sub Reconcile(ByVal isSendQueue As Boolean)

        Dim sql As String = ""
        Dim sqlUpdate As String = ""
        Dim status = "C"
        pbLoad.Style = ProgressBarStyle.Blocks

        Try
            log.Info("Start Reconcile Version " + (GetType(frmMain).Assembly.GetName().Version).ToString)

            sql = "select SITE_TYPE_ID, SITE_NO, SITE_ID, STATION_ID "
            sql += ", SHIFT_NO, DOC_DATE, FULL_TAX_QTY, R_FULL_TAX_QTY, D_FULL_TAX_QTY "
            sql += ", CANCEL_QTY, R_CANCEL_QTY, D_CANCEL_QTY, VOID_QTY, R_VOID_QTY, D_VOID_QTY "
            sql += ", REPLACE_QTY, R_REPLACE_QTY, D_REPLACE_QTY, CN_QTY, R_CN_QTY, D_CN_QTY "
            sql += ", FLEET_QTY, R_FLEET_QTY, D_FLEET_QTY, MANUAL_QTY, R_MANUAL_QTY, D_MANUAL_QTY "
            sql += ", SCAN_QTY, R_SCAN_QTY, D_SCAN_QTY, FUEL_QTY, R_FUEL_QTY, D_FUEL_QTY "
            sql += ", GOODS_QTY, R_GOODS_QTY, D_GOODS_QTY "
            sql += ", STATUS, START_FULL_TAX_NBR, END_FULL_TAX_NBR, START_MANUAL_NBR, END_MANUAL_NBR "
            sql += ", START_CN_NBR, END_CN_NBR, RECONCILE_COUNT, DUP_QTY, LAST_RECONCILE, TOTAL_STATION "
            sql += " from SUMMARY_RECONCILE"
            sql += " where status <> 'C'"
            sql += " and site_no not in ( '0','4','5')"
            If cbbSite.SelectedValue.ToString <> "0" Then
                sql += " and site_no ='" + cbbSite.SelectedValue.ToString + "'"
            End If

            Dim startdate As Long = CLng(dtpStartDate.Value.Date.ToString("yyyyMMdd"))
            Dim startdateValue As Date = dtpStartDate.Value.Date
            Dim toDate As Long = CLng(dtpToDate.Value.Date.ToString("yyyyMMdd"))
            Dim toDateValue As Date = dtpToDate.Value.Date
            toDateValue = toDateValue.AddDays(1)
            toDateValue = toDateValue.AddMilliseconds(-100)

            If startdate > 25000000 Then
                '-5430000
                startdate = CLng(dtpStartDate.Value.Date.ToString("yyyyMMdd")) - 5430000
                toDate = CLng(dtpToDate.Value.Date.ToString("yyyyMMdd")) - 5430000
            End If

            'sql += " and convert(varchar(8),doc_date,112) between '" + startdate.ToString + "' and '" + toDate.ToString + "'"
            sql += " and doc_date between '" + startdateValue.ToString("yyyy-MM-dd") + "' and '" + toDateValue.ToString("yyyy-MM-dd hh:mm:ss") + "'"
            sql += " order by shift_no desc"
            Dim dsShift As DataSet = QueryDao.sqlInquiry(sql)

            log.Info(String.Format("Total Shift to Reconcile : {0}", dsShift.Tables(0).Rows.Count.ToString))

            Dim reconcileCount As Integer = 0
            Dim reconcileValue As Integer = 0
            Dim countC As Integer = 0
            Dim countD As Integer = 0
            Dim countX As Integer = 0
            pbLoad.Value = 0
            If dsShift.Tables(0).Rows.Count > 0 Then

                pbLoad.Maximum = dsShift.Tables(0).Rows.Count
                pbLoad.Show()
                For i As Integer = 0 To dsShift.Tables(0).Rows.Count - 1
                    Application.DoEvents()
                    log.Info("-------------------------------------------------------")

                    Dim siteNo As String = dsShift.Tables(0).Rows(i).Item("site_no")
                    Dim docDate As Date = CDate(dsShift.Tables(0).Rows(i).Item("doc_date"))

                    If dsShift.Tables(0).Rows(i).Item("status") = "X" Then
                        '// request site resend EOD

                        log.Info(String.Format("site No: {0} No EOD", dsShift.Tables(0).Rows(i).Item("site_no")))

                        If chbFixIp.Checked Then
                            Dim fixIp As String = cbbFixIp.Text
                            MQService.MSMQ_SendRequestEOD(fixIp, siteNo, docDate, docDate)
                        Else
                            MQService.MSMQ_SendRequestEOD(siteNo, docDate, docDate)
                        End If
                        log.Info("Send SendRequest EOD Date :" + docDate.ToShortDateString)
                        status = "X"
                    Else
                        log.Info("Reconcile shiftNo : " + dsShift.Tables(0).Rows(i).Item("shift_no") + " siteNo : " + dsShift.Tables(0).Rows(i).Item("site_no"))

                        reconcileCount = dsShift.Tables(0).Rows(i).Item("reconcile_count")

                        If reconcileCount < maxReconcileCount OrElse Not isSendQueue Then ' And dsShift.Tables(0).Rows(i).Item("status").ToString = "D" Then
                            '// query transaction that already receive in HQ
                            sql = "select RECEIPT_NBR_REF, receipt_type,status,is_manual_entry,is_fleet_card,qty,is_scaned "
                            sql += " from RECEIPT "
                            sql += " where Shift_no=" + DBUtil.SqlQuote(dsShift.Tables(0).Rows(i).Item("shift_no"))
                            sql += " And site_no=" + DBUtil.SqlQuote(dsShift.Tables(0).Rows(i).Item("site_no"))
                            sql += " And site_type_id=" + dsShift.Tables(0).Rows(i).Item("site_type_id").ToString
                            sql += " order by RECEIPT_NBR_REF"

                            '// RECEIPT_NBR_REF add more column and store into list

                            Dim dsFullTax As DataSet = QueryDao.sqlInquiry(sql)

                            Dim FullTaxQty As Integer = dsShift.Tables(0).Rows(i).Item("full_tax_qty")
                            Dim ScanQty As Integer = dsShift.Tables(0).Rows(i).Item("scan_qty")
                            Dim VoidQty As Integer = dsShift.Tables(0).Rows(i).Item("void_qty")
                            Dim ReplaceQty As Integer = dsShift.Tables(0).Rows(i).Item("replace_qty")
                            Dim CancelQty As Integer = dsShift.Tables(0).Rows(i).Item("cancel_qty")
                            Dim ManualQty As Integer = dsShift.Tables(0).Rows(i).Item("manual_qty")
                            Dim FleetQty As Integer = dsShift.Tables(0).Rows(i).Item("fleet_qty")
                            Dim FuelQty As Integer = dsShift.Tables(0).Rows(i).Item("fuel_qty")
                            Dim GoodsQty As Integer = dsShift.Tables(0).Rows(i).Item("goods_qty")
                            Dim CnQty As Integer = dsShift.Tables(0).Rows(i).Item("cn_qty")

                            Dim RFullTaxQty As Integer = 0
                            Dim RScanQty As Integer = 0
                            Dim RVoidQty As Integer = 0
                            Dim RReplaceQty As Integer = 0
                            Dim RCancelQty As Integer = 0
                            Dim RManualQty As Integer = 0
                            Dim RFleetQty As Integer = 0
                            Dim RFuelQty As Integer = 0
                            Dim RGoodsQty As Integer = 0
                            Dim RCnQty As Integer = 0

                            Dim DFullTaxQty As Integer = 0
                            Dim DScanQty As Integer = 0
                            Dim DVoidQty As Integer = 0
                            Dim DReplaceQty As Integer = 0
                            Dim DCancelQty As Integer = 0
                            Dim DManualQty As Integer = 0
                            Dim DFleetQty As Integer = 0
                            Dim DFuelQty As Integer = 0
                            Dim DGoodsQty As Integer = 0
                            Dim DCnQty As Integer = 0

                            Dim receiptList As New Hashtable
                            Dim receiptNbrRef As String

                            For j As Integer = 0 To dsFullTax.Tables(0).Rows.Count - 1
                                Application.DoEvents()

                                With dsFullTax.Tables(0).Rows(j)
                                    If .Item("receipt_type") = "F" Then
                                        RFullTaxQty += 1

                                        If .Item("is_scaned") Then
                                            RScanQty += 1
                                        End If

                                        If .Item("status") = "V" Then
                                            RVoidQty += 1
                                            RReplaceQty += 1
                                        End If

                                        If .Item("status") = "C" Then
                                            RCancelQty += 1
                                        End If

                                        If .Item("is_manual_entry") Then
                                            RManualQty += 1
                                        End If
                                        If .Item("is_fleet_card") Then
                                            RFleetQty += 1
                                        End If
                                        If .Item("qty") > 0 Then
                                            RFuelQty += 1
                                        Else
                                            RGoodsQty += 1
                                        End If
                                    ElseIf .Item("receipt_type") = "C" Then
                                        RCnQty += 1
                                    End If

                                    receiptNbrRef = CStr(.Item("RECEIPT_NBR_REF"))
                                    If Not receiptList.ContainsKey(receiptNbrRef) Then
                                        receiptList.Add(receiptNbrRef, receiptNbrRef)
                                    End If
                                End With
                            Next

                            If RFullTaxQty > FullTaxQty Then
                                FullTaxQty = RFullTaxQty
                            End If

                            DFullTaxQty = RFullTaxQty - FullTaxQty
                            DScanQty = RScanQty - ScanQty
                            DVoidQty = RVoidQty - VoidQty
                            DReplaceQty = RReplaceQty - ReplaceQty
                            DCancelQty = RCancelQty - CancelQty
                            DManualQty = RManualQty - ManualQty
                            DFleetQty = RFleetQty - FleetQty
                            DFuelQty = RFuelQty - FuelQty
                            DGoodsQty = RGoodsQty - GoodsQty
                            DCnQty = RCnQty - CnQty

                            status = "C"

                            If DFullTaxQty <> 0 Then
                                status = "D"
                            End If
                            If DCnQty <> 0 Then
                                status = "D"
                            End If

                            log.Info("Reconcile Status : " + status)

                            reconcileCount += 1

                            '// update reconcile to lastest data
                            ' If DFullTaxQty = 0 Then
                            sql = "update SUMMARY_RECONCILE set " &
                            "R_Full_Tax_Qty=" + RFullTaxQty.ToString + "," &
                            "R_Scan_Qty=" + RFullTaxQty.ToString + "," &
                            "R_Void_Qty=" + RVoidQty.ToString + "," &
                            "R_Replace_Qty=" + RReplaceQty.ToString + "," &
                            "R_Cancel_Qty=" + RCancelQty.ToString + "," &
                            "R_Manual_Qty=" + RManualQty.ToString + "," &
                            "R_Fleet_Qty=" + RFleetQty.ToString + "," &
                            "R_Fuel_Qty=" + RFuelQty.ToString + "," &
                            "R_Goods_Qty=" + RGoodsQty.ToString + "," &
                            "R_Cn_Qty=" + RCnQty.ToString + "," &
                            "D_Full_Tax_Qty=" + DFullTaxQty.ToString + "," &
                            "D_Scan_Qty=" + DFullTaxQty.ToString + "," &
                            "D_Void_Qty=" + DVoidQty.ToString + "," &
                            "D_Replace_Qty=" + DReplaceQty.ToString + "," &
                            "D_Cancel_Qty=" + DCancelQty.ToString + "," &
                            "D_Manual_Qty=" + DManualQty.ToString + "," &
                            "D_Fleet_Qty=" + DFleetQty.ToString + "," &
                            "D_Fuel_Qty=" + DFuelQty.ToString + "," &
                            "D_Goods_Qty=" + DGoodsQty.ToString + "," &
                            "D_Cn_Qty=" + DCnQty.ToString + "," &
                            "Reconcile_Count=" + reconcileCount.ToString + "," &
                            "Status='" + status + "'" &
                            " where Shift_no=" + DBUtil.SqlQuote(dsShift.Tables(0).Rows(i).Item("shift_no")) &
                            " and site_no=" + DBUtil.SqlQuote(dsShift.Tables(0).Rows(i).Item("site_no")) &
                            " and site_type_id=" + dsShift.Tables(0).Rows(i).Item("site_type_id").ToString

                            QueryDao.sqlUpdate(sql)

                            If status = "D" AndAlso Not chkNoEOD.Checked Then
                                If isSendQueue Then

                                    '// still found diff send request to resend data
                                    Try
                                        'If CDec(dsShift.Tables(0).Rows(i).Item("Shift_no")) > 2.1 Then
                                        '    MSMQ_SendRequestByReceiptNbr()
                                        'Else ' Service ที่เป็น Ver2.1 ยังไม่รองรับการส่งเป็นใบ
                                        '    MSMQ_SendRequest(dsShift.Tables(0).Rows(i).Item("Shift_no"), dsShift.Tables(0).Rows(i).Item("site_no"))
                                        'End If

                                        '// resend request full tax
                                        If dsShift.Tables(0).Rows(i).Item("start_full_tax_nbr").ToString.Trim <> "" Then
                                            Dim startFullTax As String = dsShift.Tables(0).Rows(i).Item("start_full_tax_nbr")
                                            Dim endFullTax As String = dsShift.Tables(0).Rows(i).Item("end_full_tax_nbr")

                                            MSMQ_SendRequestByReceiptNbr(startFullTax, endFullTax, siteNo, startdate.ToString(), receiptList)
                                        End If

                                        '// resend request credit note
                                        If dsShift.Tables(0).Rows(i).Item("start_cn_nbr").ToString.Trim <> "" Then
                                            Dim startNbr As String = dsShift.Tables(0).Rows(i).Item("start_cn_nbr")
                                            Dim endNbr As String = dsShift.Tables(0).Rows(i).Item("end_cn_nbr")

                                            MSMQ_SendRequestByReceiptNbr(startNbr, endNbr, siteNo, startdate.ToString, receiptList)
                                        End If

                                    Catch ex As Exception
                                        log.Info("unable send request to SITE:" + siteNo + " Date:" + startdate.ToString)
                                    End Try
                                End If
                            End If
                            'End If
                            dsFullTax.Dispose()
                        Else
                            log.Info("Reconcile over limit " + maxReconcileCount.ToString + " Times")
                        End If
                    End If

                    If status = "C" Then
                        countC += 1
                    ElseIf status = "D" Then
                        countD += 1
                    ElseIf status = "X" Then
                        countX += 1
                    End If

                    reconcileValue += 1
                    pbLoad.Value = reconcileValue
                    lbStatus.Text = "reconciling... " + reconcileValue.ToString("##,##0") + " of " + dsShift.Tables(0).Rows.Count.ToString("##,##0")
                    lblC.Text = "Complete : " + countC.ToString("##,##0")
                    lblD.Text = "Diff : " + countD.ToString("##,##0")
                    lbNoEod.Text = "NO EOD: " + countX.ToString("##,##0")
                Next
            Else
                If GlobalInfo.isUI Then MsgBox("ไม่พบข้อมูลที่จะ Reconcile")
            End If
            dsShift.Dispose()

        Catch ex As Exception
            log.Error("Error Reconcile", ex)
        End Try

    End Sub

    Private Sub MSMQ_SendRequestByReceiptNbr(ByVal startNbr As String, ByVal endNbr As String, ByVal siteNo As String, ByVal transDate As String, receriptCurrentList As Hashtable)

        log.Info("Start Send Request")

        Dim ipAddress As String = ""
        Try
            If chbFixIp.Checked Then
                ipAddress = cbbFixIp.Text
                log.Debug("fix ip for resend to : " + ipAddress)
            Else
                ipAddress = SiteService.getSiteIP(siteNo)
                log.Debug("ip from actual value : " + ipAddress)
            End If

            If Not CommonUtils.IsIpAddressValid(ipAddress) Then
                log.Warn("Can not get IP-Address from SiteNo:" + siteNo)
                Exit Sub
            End If

            If Not chbFixIp.Checked Then
                '// verify can connect to site
                CommonUtils.checkRemoteAddrIsValid(ipAddress, 1801)
            End If

        Catch ex As Exception
            log.Error("Error to connect client:" + ex.Message, ex)
            Throw ex
        End Try

        log.Info("Start Find Receipt In FTS HQ Start Nbr : " + startNbr + " To " + endNbr)

        Dim prefixFullTax As String = startNbr.Substring(0, 12)
        Dim iStartNbr As Integer = CInt(startNbr.Substring(12, 6))
        Dim iEndNbr As Integer = CInt(endNbr.Substring(12, 6))
        Dim receiptNbr As String

        '  วนลูปหาใบที่ไม่มีในระบบ
        If startNbr.Substring(0, 12) <> endNbr.Substring(0, 12) Then ' กรณีที่ใบกำกับมีเลขที่เริ่มต้นข้ามเดือน 
            ' วนลูปหาใบกำกับใน HQ สองรอบ หาตัวเริ่มต้นของเดือนก่อนหน้าก่อน

            ' ====== หาเลขที่สิ้นสุดของ Receipt Nbr ===========
            Dim sql As String = "select max(END_FULL_TAX_NBR) from SUMMARY_RECONCILE where SITE_NO = '" + siteNo + "' and SUBSTRING(END_FULL_TAX_NBR,1,12) = '" + startNbr.Substring(0, 12) + "'"
            Dim dsMaxNbr As DataSet = QueryDao.sqlInquiry(sql)
            Dim maxReceiptNbr As String = dsMaxNbr.Tables(0).Rows(0).Item(0).ToString ' เลขที่ล่าสุดของเดือนก่อนหน้า

            ' ======= วนลูปเดือนก่อนหน้า ======================
            prefixFullTax = startNbr.Substring(0, 12)
            For i As Integer = iStartNbr To maxReceiptNbr.Substring(12, 6)
                Application.DoEvents()

                '// request transaction that no found on HQ
                receiptNbr = prefixFullTax + i.ToString("000000")
                If Not receriptCurrentList.ContainsKey(receiptNbr) Then
                    MQService.MSMQ_SendRequestReceiptNbrByIP(ipAddress, receiptNbr)
                    log.Info("Send Q Request Receipt Nbr : " + receiptNbr)
                End If
            Next
            ' ======= วนลูปเดือนปัจจุบัน ======================
            prefixFullTax = endNbr.Substring(0, 12)
            Dim startTax As Integer = CInt(startNbr.Substring(12, 1) + "00001")
            For i As Integer = startTax To iEndNbr
                Application.DoEvents()

                '// request transaction that no found on HQ
                receiptNbr = prefixFullTax + i.ToString("000000")
                If Not receriptCurrentList.ContainsKey(receiptNbr) Then
                    MQService.MSMQ_SendRequestReceiptNbrByIP(ipAddress, receiptNbr)
                    log.Info("Send Q Request Receipt Nbr : " + receiptNbr)
                End If
            Next
        Else ' ข้อมูลปกติ
            For i As Integer = iStartNbr To iEndNbr
                Application.DoEvents()

                '// request transaction that no found on HQ
                receiptNbr = prefixFullTax + i.ToString("000000")
                If Not receriptCurrentList.ContainsKey(receiptNbr) Then
                    MQService.MSMQ_SendRequestReceiptNbrByIP(ipAddress, receiptNbr)
                    log.Info("Send Q Request Receipt Nbr : " + receiptNbr)
                End If
            Next
        End If
    End Sub

    Private Sub LoadLocalIpAddr()
        Dim ipAddr As String

        ipAddr = String.Empty
        Dim strHostName As String = System.Net.Dns.GetHostName()
        Dim iphe As System.Net.IPHostEntry = System.Net.Dns.GetHostEntry(strHostName)

        For Each ipheal As System.Net.IPAddress In iphe.AddressList
            If ipheal.AddressFamily = System.Net.Sockets.AddressFamily.InterNetwork Then
                ipAddr = ipheal.ToString()
                cbbFixIp.Items.Add(ipAddr)
            End If
        Next
    End Sub

    Private Sub init()
        isInit = True

        FileUtil.DeleteLogFile("logs", 100)

        'Set default application culter
        LoadSite(StationConstant.STATION_TYPE_OID)
        LoadLocalIpAddr()
    End Sub

    Private Sub fmReconcile_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' Sets the culture to English
        Thread.CurrentThread.CurrentCulture = New CultureInfo("en-US")
        ' Sets the UI culture to English
        Thread.CurrentThread.CurrentUICulture = New CultureInfo("en-US")


        log.Info("Reconcile Version " + (GetType(frmMain).Assembly.GetName().Version).ToString)
        ' Button1_Click(sender, e)
        '  Timer1.Start()
        init()

    End Sub

    Private Sub rbNgv_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbNgv.CheckedChanged
        If isInit AndAlso rbNgv.Checked Then
            cbbSite.Enabled = True
            LoadSite(StationConstant.STATION_TYPE_NGV)
        End If
    End Sub

    Private Sub rbOil_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbOil.CheckedChanged
        If isInit AndAlso rbOil.Checked Then
            cbbSite.Enabled = True
            LoadSite(StationConstant.STATION_TYPE_OID)
        End If
    End Sub

    Private Sub LoadSite(ByVal SiteTypeId As Integer)
        Try
            cbbSite.DataSource = SiteService.getsForComboBox(SiteTypeId)
            cbbSite.DisplayMember = "site_name"
            cbbSite.ValueMember = "site_code"
            cbbSite.Show()
        Catch ex As Exception
            log.Error("load site master", ex)
            Throw ex
        End Try
    End Sub

    Private Sub btnSend_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSend.Click
        Try
            GlobalInfo.isUI = True

            log.Info("Manual reconcile start")
            Dim siteNo As String

            btnSend.Enabled = False

            siteNo = cbbSite.SelectedValue.ToString

            If chkNoEOD.Checked Then
                CreateSiteNotEOD(dtpStartDate.Value, dtpToDate.Value)
            Else
                Reconcile(Not chkNoQ.Checked)
            End If
            btnSend.Enabled = True
        Catch ex As Exception
            MsgBox("error: " + ex.Message)
        End Try

    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        TimeCount += 1
        If TimeCount = 2 Then
            Timer1.Stop()
            btnSend_Click(sender, e)
        End If
    End Sub

    '// Add summary reconcile (EOD)
    Private Sub CreateSiteNotEOD(startDate As Date, endDate As Date, Optional ByVal siteNo As String = "")
        Try
            Dim startp As Long = CLng(startDate.ToString("yyyyMMdd"))
            Dim endp As Long = CLng(endDate.ToString("yyyyMMdd"))
            Dim CurrD As Long = startp

            Dim curDate As Date

            curDate = dtpStartDate.Value

            Dim x As Long = startp + 5430000
            If (startp + 5430000) > 30000000 Then
                CurrD = (startp - 5430000)
            End If
            If (endp + 5430000) > 30000000 Then
                endp = endp - 5430000
            End If

            pbLoad.Value = 0
            pbLoad.Maximum = dtpToDate.Value.Subtract(dtpStartDate.Value).TotalDays + 1
            pbLoad.Style = ProgressBarStyle.Marquee
            pbLoad.Show()

            Dim cntByDate As Integer = 0
            While (CurrD <= endp)
                cntByDate = 0
                Application.DoEvents()

                'curDate.ToString("yyyy-MM-dd")
                SiteService.createSiteSummaryNotSendEOD(curDate)

                pbLoad.Value += 1
                curDate = curDate.AddDays(1)
                CurrD = CLng(curDate.ToString("yyyyMMdd"))
            End While

            pbLoad.Value = pbLoad.Maximum
            log.Info("Update EOD success")
        Catch ex As Exception
            log.Error("Error Sub SearchSiteNotEOD", ex)

            If GlobalInfo.isUI Then MsgBox("Error Sub SearchSiteNotEOD : " + ex.Message)
        End Try
    End Sub

    Private Sub SiteNotEod()

        'หาจำนวนสถานีทั้งหมด
        Dim stationToTal As Integer = 0
        Dim sql As String = ""
        sql = "select count(*) as stationToTal from station where status = 'A' and site_no not in ('0','4','5')"
        Dim dsstationToTal As DataSet = QueryDao.sqlInquiry(sql)
        If dsstationToTal.Tables(0).Rows.Count > 0 Then
            stationToTal = CInt(dsstationToTal.Tables(0).Rows(0).ToString)
        End If
        dsstationToTal.Dispose()

        'หาจำนวนสถานีที่ได้รับ Eod
        sql = "select distinct(doc_date) as doc_date,count(*) as totalSiteNo from SUMMARY_RECONCILE where year(doc_date) >= 2015 group by doc_date order by doc_date desc"
        Dim dsEod As DataSet = QueryDao.sqlInquiry(sql)
        If dsEod.Tables(0).Rows.Count > 0 Then

            For i As Integer = 0 To dsEod.Tables(0).Rows.Count - 1

                'เทียบค่า จำนวนสถานีทั้งหมด กับ จำนวนสถานีที่ได้รับ Eod 
                'ถ้าจำนวนสถานีที่ได้รับ Eod มีน้อยกว่า ให้ วนลูป ส่งคิวไปขอ Eod ใหม่ แล้ว Add ค่าลงไปใน SUMMARY_RECONCILE เป็น X

                If stationToTal > CInt(dsEod.Tables(0).Rows("totalSiteNo").ToString) Then

                    sql = " select site_no,IP_ADDRESS from station where SITE_NO not in "
                    sql += " (select site_no  from SUMMARY_RECONCILE where convert(varchar(8),doc_date,112) = " + dsEod.Tables(0).Rows("totalSiteNo").ToString("yyyyMMdd")
                    sql += ") and status = 'A' and  SITE_NO not in ('0','4','5')"
                    Dim dsStationNoEod As DataSet = QueryDao.sqlInquiry(sql)
                    'insert ข้อมูลลง ฐานข้อมูล

                End If

            Next

            stationToTal = CInt(dsstationToTal.Tables(0).Rows(0).ToString)
            dsstationToTal.Dispose()
        End If
    End Sub

    Private Sub btnSendByReceiptNBR_Click(sender As Object, e As EventArgs) Handles btnSendByReceiptNBR.Click
        Dim destIp As String = ""
        Dim receipt As String = ""

        Try
            GlobalInfo.isUI = True

            receipt = txtReceiptNBR.Text
            If rdoIp.Checked Then
                destIp = txtDestAddress.Text
            Else
                '// loopup destination IP from input site no
                destIp = SiteService.getSiteIP(txtDestAddress.Text)
            End If

            If Not CommonUtils.IsIpAddressValid(destIp) Then
                MessageBox.Show("Invalid SITE No or IP Address", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If

            CommonUtils.checkRemoteAddrIsValid(destIp, 1801)

        Catch ex As Exception
            log.Error("error get " + ex.Message, ex)
            If (MessageBox.Show(ex.Message + vbCrLf + vbCrLf + "Are you want to continue send data ?", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = DialogResult.No) Then
                Exit Sub
            End If
        End Try

        MQService.MSMQ_SendRequestReceiptNbrByIP(destIp, receipt)
        MessageBox.Show("Complate")
    End Sub

    Private Sub chbFixIp_CheckedChanged(sender As Object, e As EventArgs) Handles chbFixIp.CheckedChanged
        cbbFixIp.Enabled = chbFixIp.Checked
    End Sub

End Class