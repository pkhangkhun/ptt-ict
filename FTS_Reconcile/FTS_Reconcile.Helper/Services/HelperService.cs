﻿using FTS_Reconcile.Helper.Interfaces;
using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;

namespace FTS_Reconcile.Helper.Services
{
    public class HelperService:IHelperService
    {
        public int CheckServiceStatus(string ipAddress, int port)
        {
            //1:connect success and service started
            //2:cannot connect or firewall blocked
            //3:connect success and service stoped

            try
            {
                TcpClient client = new TcpClient();
                client.Connect(ipAddress, port);
                client.Close();
                return 0;//success
            }
            catch (ArgumentNullException e)
            {
                throw new Exception("ArgumentNullException: {0}", e);
            }
            catch (SocketException e)
            {
                
                if (e.SocketErrorCode.ToString() == "TimedOut")
                {
                    return 1;//occure on: wrong ip, server wrong port, server service stoped.
                }
                else if (e.SocketErrorCode.ToString() == "ConnectionRefused")
                {
                    return 2;//occure on: local wrong port, local service stoped.
                }
                else if (e.ErrorCode == 10064)//ServerDown
                {
                    return 3;
                }
                else //NetworkUnreachable
                {
                    return 4;
                }
                
            }
            
        }

        public bool isPingSuccess(string ipAddress)
        {
            bool ret;
            PingReply Result;
            Ping SendPing = new Ping();

            Result = SendPing.Send(ipAddress);
            if (Result.Status == IPStatus.Success)
                ret = true;
            else
                ret = false;

            return ret;
        }
    }
}
