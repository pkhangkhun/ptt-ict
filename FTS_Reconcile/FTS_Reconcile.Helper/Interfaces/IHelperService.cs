﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FTS_Reconcile.Helper.Interfaces
{
    public interface IHelperService
    {
        int CheckServiceStatus(string ipAddress, int port);
    }
}
