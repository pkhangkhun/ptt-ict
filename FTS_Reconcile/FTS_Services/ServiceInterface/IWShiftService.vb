Imports Ie.IeComponent.BO

Public Interface IWShiftService

    Function GetByShiftDate(ByVal fromdate As String, ByVal toDate As String) As WShiftCollection

End Interface
