Imports Ie.IeComponent.BO

Public Interface IFullTaxService

    Function GetByShiftNo(ByVal shiftNo As String) As FullTaxReceiptCollection

    Function GetByNbr(ByVal FullTaxNbr As String) As FullTaxReceiptCollection

    Function GetByManualEntry(ByVal shiftNo As String) As FullTaxReceiptCollection

    Function GetByDate(ByVal dataDate As Date) As FullTaxReceiptCollection
End Interface
