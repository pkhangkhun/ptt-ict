Imports System.Data.Common
Imports FTS_Services.App.Utility
Imports Npgsql

Namespace App.Dao

    Public Class BaseDao


        Public Shared ReadOnly Property GetDBConnection() As NpgsqlConnection
            Get
                Return DBUtil.GetDBConnection()
            End Get
        End Property

        Public Shared ReadOnly Property GetDBConnectionMaster() As NpgsqlConnection
            Get
                Return DBUtil.GetDBConnectionMaster()
            End Get
        End Property

        Public Shared Function HasColumn(ByRef reader As DbDataReader, ByVal columnName As String) As Boolean
            For i As Integer = 0 To reader.FieldCount - 1
                If reader.GetName(i).Equals(columnName, StringComparison.InvariantCultureIgnoreCase) Then
                    Return Not IsDBNull(reader(columnName))
                End If
            Next

            Return False
        End Function

    End Class

End Namespace

