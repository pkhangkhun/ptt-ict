
Imports Ie.IeComponent.BO
Imports Npgsql
Imports Ie.UI.Utility

Namespace App.Dao

    Public Class QueryDao
        Inherits BaseDao

        Public Shared Sub sqlUpdate(ByVal sql As String)

            Dim cn As NpgsqlConnection = GetDBConnection()
            cn.Open()

            Dim txn As NpgsqlTransaction = cn.BeginTransaction(IsolationLevel.Serializable)
            Try
                Dim command As New NpgsqlCommand(sql, cn, txn)
                command.CommandType = CommandType.Text
                command.ExecuteNonQuery()
                txn.Commit()
            Catch ex As Exception
                txn.Rollback()
                Throw ex
            Finally
                cn.Close()
            End Try

        End Sub

        Public Shared Function sqlInquiry(ByVal sql As String) As DataSet
            Dim ds As New DataSet
            Dim cn As NpgsqlConnection = GetDBConnection()
            cn.Open()

            Dim command As New NpgsqlCommand(sql, cn)
            command.CommandType = CommandType.Text


            Dim dap As NpgsqlDataAdapter = New NpgsqlDataAdapter()
            dap.SelectCommand = command

            Try
                dap.Fill(ds, "MASTER")
            Catch ex As Exception
                Throw ex
            Finally
                dap.Dispose()
                cn.Close()
            End Try

            Return ds
        End Function
        Public Shared Function sqlInquiry(ByVal sql As String, ByVal cn As NpgsqlConnection, ByVal txn As NpgsqlTransaction) As DataSet
            Dim ds As New DataSet
        
            Dim command As New NpgsqlCommand(sql, cn)
            command.CommandType = CommandType.Text


            Dim dap As NpgsqlDataAdapter = New NpgsqlDataAdapter()
            dap.SelectCommand = command

            Try
                dap.Fill(ds, "MASTER")
            Catch ex As Exception
                Throw ex
            Finally
                dap.Dispose()
                cn.Close()
            End Try

            Return ds
        End Function
    End Class



End Namespace