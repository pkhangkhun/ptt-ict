Imports Ie.IeComponent.BO
Imports Npgsql
Imports Ie.UI.Utility

Namespace App.Dao

    Public Class FullTaxDao
        Inherits BaseDao

        Private Class ColumnName
            Public Const ReceiptNbr = "RECEIPT_NBR"
            Public Const CustName = "CUST_NAME"
            Public Const Address = "ADDRESS"
            Public Const CustTaxId = "CUST_TAX_ID"
            Public Const CustBranchNo = "CUST_BRANCH_NO"
            Public Const CustId = "CUST_ID"
            Public Const ProvinceId = "PROVINCE_ID"
            Public Const CustBranchName = "CUST_BRANCH_NAME"
            'Ref Receipt
            Public Const PlateNumber = "PLATE_NUMBER"
            Public Const PlateProvinceId = "PLATE_PROVINCE_ID"
            Public Const ReceiptType = "RECEIPT_TYPE"
            Public Const ReceiptDate = "RECEIPT_DATE"
            Public Const UnitPrice = "UNIT_PRICE"
            Public Const UnitName = "UNIT_NAME"
            Public Const ProdCode = "PROD_CODE"
            Public Const Qty = "QTY"
            Public Const ProdName = "PROD_NAME"
            Public Const TotalAmt = "TOTAL_AMT"
            Public Const DiscountAmt = "DISCOUNT_AMT"
            Public Const TotalAfterDiscountAmt = "TOTAL_AFTER_DISCOUNT_AMT"
            Public Const VatAmt = "VAT_AMT"
            Public Const NetAmt = "NET_AMT"
            Public Const PrintNo = "PRINT_NO"
            Public Const VoidReason = "VOID_REASON"
            Public Const IsScaned = "IS_SCANED"
            Public Const VoidUserId = "VOID_USER_ID"
            Public Const RefNbr = "REF_NBR"
            Public Const IsManualEntry = "IS_MANUAL_ENTRY"
            Public Const StationId = "STATION_ID"
            Public Const VoidDttm = "VOID_DTTM"
            Public Const CustType = "CUST_TYPE"
            Public Const IssueDate = "ISSUE_DATE"
            Public Const Status = "STATUS"
            Public Const ShiftId = "SHIFT_ID"
            Public Const ObjectId = "OBJECT_ID"
            Public Const IsConVerted = "IS_CONVERTED"
            Public Const CreateUser = "CREATE_USER"
            Public Const ReplaceNo = "REPLACE_NO"
            Public Const ReplaceDate = "REPLACE_DATE"
            Public Const LogoImg = "LOGO_IMG"
            Public Const SiteId = "SITE_ID"
            Public Const CompanyName = "COMPANY_NAME"
            Public Const CompanyTaxId = "COMPANY_TAX_ID"
            Public Const CompanyBranchNo = "COMPANY_BRANCH_NO"
            Public Const CompanyBranchName = "COMPANY_BRANCH_NAME"
            Public Const CompanyAddress = "COMPANY_ADDRESS"
            Public Const RdId = "RD_ID"
            Public Const PosNbr = "POS_NBR"
            Public Const TranNbr = "TRAN_NBR"
            Public Const PumpNbr = "PUMP_NBR"
            Public Const HashTag = "HASH_TAG"
            Public Const EmployeeName = "EMPLOYEE_NAME"
            Public Const ReplaceRef = "REPLACE_REF"
            Public Const Lcustomer = "LCUSTOMER"
            Public Const Lcardno = "LCARDNO"
            Public Const Ltotalpoint = "LTOTALPOINT"
            Public Const Ltrnpoint = "LTRNPOINT"
            Public Const Lbalance = "LBALANCE"
            Public Const Lrepoint = "LREPOINT"
            Public Const Lpointtoday = "LPOINTTODAY"
            Public Const IsFleetCard = "IS_FLEET_CARD"
            Public Const HostId = "HOST_ID"
            Public Const SaleTypeDesc = "SALE_TYPE_DESC"
            Public Const SiteTypeId = "SITE_TYPE_ID"
            Public Const IsManualFill = "IS_MANUAL_FILL"
            Public Const SiteNo = "SITE_NO"
            Public Const Version = "VERSION"
            Public Const UpdateUser = "UPDATE_USER"
            Public Const UpdateDttm = "UPDATE_DTTM"

            Public Const RegionCode = "REGION_CODE"
            Public Const DistrictCode = "DISTRICT_CODE"

            Public Const CnRef = "CN_REF"
            Public Const FullTaxNbrRef = "FULL_TAX_NBR_REF"
            Public Const ReasonDesc = "REASON_DESC"

        End Class

        Private Class ParamName
            Public Const ReceiptNbr = "@ReceiptNbr"
            Public Const ReceiptDate = "@ReceiptDate"
            Public Const PosNbr = "@PosNbr"
            Public Const CustName = "@CustName"
            Public Const Address = "@Address"
            Public Const CustTaxId = "@CustTaxId"
            Public Const CustBranchNo = "@CustBranchNo"
            Public Const CustId = "@CustId"
            Public Const ProvinceId = "@ProvinceId"
            Public Const CustBranchName = "@CustBranchName"
            'Ref
            Public Const ShiftId = "@ShiftId"
            Public Const ShiftNo = "@ShiftNo"
        End Class

        Private Shared Sub PopulateData(ByVal fullTaxReceipt As FullTaxReceipt, ByVal dr As NpgsqlDataReader, ftsVersion As Double)
            If Not (dr.Item(ColumnName.ReceiptNbr) Is System.DBNull.Value) Then fullTaxReceipt.ReceiptNbr = CStr(dr.Item(ColumnName.ReceiptNbr))
            If Not (dr.Item(ColumnName.CustName) Is System.DBNull.Value) Then fullTaxReceipt.CustName = CStr(dr.Item(ColumnName.CustName))
            If Not (dr.Item(ColumnName.Address) Is System.DBNull.Value) Then fullTaxReceipt.Address = CStr(dr.Item(ColumnName.Address))
            If Not (dr.Item(ColumnName.CustTaxId) Is System.DBNull.Value) Then fullTaxReceipt.CustTaxId = CStr(dr.Item(ColumnName.CustTaxId))
            If Not (dr.Item(ColumnName.CustBranchNo) Is System.DBNull.Value) Then fullTaxReceipt.CustBranchNo = CInt(dr.Item(ColumnName.CustBranchNo))
            If Not (dr.Item(ColumnName.CustId) Is System.DBNull.Value) Then fullTaxReceipt.CustId = CLng(dr.Item(ColumnName.CustId))
            If Not (dr.Item(ColumnName.ProvinceId) Is System.DBNull.Value) Then fullTaxReceipt.ProvinceId = CLng(dr.Item(ColumnName.ProvinceId))

            'read Receipt
            fullTaxReceipt.ObjectID.ID = CType(dr.Item(ColumnName.ObjectId), Long)
            If Not (dr.Item(ColumnName.ReceiptNbr) Is System.DBNull.Value) Then fullTaxReceipt.ReceiptNbr = CStr(dr.Item(ColumnName.ReceiptNbr))
            If Not (dr.Item(ColumnName.PlateNumber) Is System.DBNull.Value) Then fullTaxReceipt.PlateNumber = CStr(dr.Item(ColumnName.PlateNumber))
            If Not (dr.Item(ColumnName.PlateProvinceId) Is System.DBNull.Value) Then fullTaxReceipt.PlateProvinceId = CInt(dr.Item(ColumnName.PlateProvinceId))
            If Not (dr.Item(ColumnName.ReceiptType) Is System.DBNull.Value) Then fullTaxReceipt.ReceiptType = CStr(dr.Item(ColumnName.ReceiptType))
            If Not (dr.Item(ColumnName.ReceiptDate) Is System.DBNull.Value) Then fullTaxReceipt.ReceiptDate = CDate(dr.Item(ColumnName.ReceiptDate))
            If Not (dr.Item(ColumnName.UnitPrice) Is System.DBNull.Value) Then fullTaxReceipt.UnitPrice = CDec(dr.Item(ColumnName.UnitPrice))
            If Not (dr.Item(ColumnName.UnitName) Is System.DBNull.Value) Then fullTaxReceipt.UnitName = CStr(dr.Item(ColumnName.UnitName))
            If Not (dr.Item(ColumnName.ProdCode) Is System.DBNull.Value) Then fullTaxReceipt.ProdCode = CStr(dr.Item(ColumnName.ProdCode))
            If Not (dr.Item(ColumnName.Qty) Is System.DBNull.Value) Then fullTaxReceipt.Qty = CDec(dr.Item(ColumnName.Qty))
            If Not (dr.Item(ColumnName.ProdName) Is System.DBNull.Value) Then fullTaxReceipt.ProdName = CStr(dr.Item(ColumnName.ProdName))
            If Not (dr.Item(ColumnName.TotalAmt) Is System.DBNull.Value) Then fullTaxReceipt.TotalAmt = CDec(dr.Item(ColumnName.TotalAmt))
            If Not (dr.Item(ColumnName.DiscountAmt) Is System.DBNull.Value) Then fullTaxReceipt.DiscountAmt = CDec(dr.Item(ColumnName.DiscountAmt))
            If Not (dr.Item(ColumnName.TotalAfterDiscountAmt) Is System.DBNull.Value) Then fullTaxReceipt.TotalAfterDiscountAmt = CDec(dr.Item(ColumnName.TotalAfterDiscountAmt))

            If Not (dr.Item(ColumnName.RegionCode) Is System.DBNull.Value) Then fullTaxReceipt.RegionCode = CStr(dr.Item(ColumnName.RegionCode))
            If Not (dr.Item(ColumnName.DistrictCode) Is System.DBNull.Value) Then fullTaxReceipt.DistrictCode = CStr(dr.Item(ColumnName.DistrictCode))

            If Not (dr.Item(ColumnName.VatAmt) Is System.DBNull.Value) Then fullTaxReceipt.VatAmt = CDec(dr.Item(ColumnName.VatAmt))
            If Not (dr.Item(ColumnName.NetAmt) Is System.DBNull.Value) Then fullTaxReceipt.NetAmt = CDec(dr.Item(ColumnName.NetAmt))
            If Not (dr.Item(ColumnName.PrintNo) Is System.DBNull.Value) Then fullTaxReceipt.PrintNo = CInt(dr.Item(ColumnName.PrintNo))
            If Not (dr.Item(ColumnName.VoidReason) Is System.DBNull.Value) Then fullTaxReceipt.VoidReason = CStr(dr.Item(ColumnName.VoidReason))
            If Not (dr.Item(ColumnName.IsScaned) Is System.DBNull.Value) Then fullTaxReceipt.IsScaned = CBool(dr.Item(ColumnName.IsScaned))
            If Not (dr.Item(ColumnName.VoidUserId) Is System.DBNull.Value) Then fullTaxReceipt.VoidUserId = CStr(dr.Item(ColumnName.VoidUserId))
            If Not (dr.Item(ColumnName.RefNbr) Is System.DBNull.Value) Then fullTaxReceipt.RefNbr = CStr(dr.Item(ColumnName.RefNbr))
            If Not (dr.Item(ColumnName.IsManualEntry) Is System.DBNull.Value) Then fullTaxReceipt.IsManualEntry = CBool(dr.Item(ColumnName.IsManualEntry))
            If Not (dr.Item(ColumnName.StationId) Is System.DBNull.Value) Then fullTaxReceipt.StationId = CLng(dr.Item(ColumnName.StationId))
            If Not (dr.Item(ColumnName.VoidDttm) Is System.DBNull.Value) Then fullTaxReceipt.VoidDttm = CDate(dr.Item(ColumnName.VoidDttm))
            If Not (dr.Item(ColumnName.CustType) Is System.DBNull.Value) Then fullTaxReceipt.CustType = CStr(dr.Item(ColumnName.CustType))
            If Not (dr.Item(ColumnName.IssueDate) Is System.DBNull.Value) Then fullTaxReceipt.IssueDate = CDate(dr.Item(ColumnName.IssueDate))
            If Not (dr.Item(ColumnName.Status) Is System.DBNull.Value) Then fullTaxReceipt.Status = CStr(dr.Item(ColumnName.Status))
            If Not (dr.Item(ColumnName.ShiftId) Is System.DBNull.Value) Then fullTaxReceipt.ShiftId = CLng(dr.Item(ColumnName.ShiftId))
            If Not (dr.Item(ColumnName.SiteId) Is System.DBNull.Value) Then fullTaxReceipt.SiteId = CLng(dr.Item(ColumnName.SiteId))
            If Not (dr.Item(ColumnName.IsConVerted) Is System.DBNull.Value) Then fullTaxReceipt.IsConverted = CBool(dr.Item(ColumnName.IsConVerted))
            If Not (dr.Item(ColumnName.ReplaceNo) Is System.DBNull.Value) Then fullTaxReceipt.ReplaceNo = CInt(dr.Item(ColumnName.ReplaceNo))
            If Not (dr.Item(ColumnName.ReplaceDate) Is System.DBNull.Value) Then fullTaxReceipt.ReplaceDate = CDate(dr.Item(ColumnName.ReplaceDate))
            If Not (dr.Item(ColumnName.CreateUser) Is System.DBNull.Value) Then fullTaxReceipt.CreateUser = CStr(dr.Item(ColumnName.CreateUser))
            If Not (dr.Item(ColumnName.LogoImg) Is System.DBNull.Value) Then fullTaxReceipt.LogoImg = dr.Item(ColumnName.LogoImg)

            If Not (dr.Item(ColumnName.CompanyName) Is System.DBNull.Value) Then fullTaxReceipt.CompanyName = CStr(dr.Item(ColumnName.CompanyName))
            If Not (dr.Item(ColumnName.CompanyTaxId) Is System.DBNull.Value) Then fullTaxReceipt.CompanyTaxId = CStr(dr.Item(ColumnName.CompanyTaxId))
            If Not (dr.Item(ColumnName.CompanyBranchNo) Is System.DBNull.Value) Then fullTaxReceipt.CompanyBranchNo = CStr(dr.Item(ColumnName.CompanyBranchNo))
            If Not (dr.Item(ColumnName.CompanyBranchName) Is System.DBNull.Value) Then fullTaxReceipt.CompanyBranchName = CStr(dr.Item(ColumnName.CompanyBranchName))
            If Not (dr.Item(ColumnName.CompanyAddress) Is System.DBNull.Value) Then fullTaxReceipt.CompanyAddress = CStr(dr.Item(ColumnName.CompanyAddress))
            If Not (dr.Item(ColumnName.RdId) Is System.DBNull.Value) Then fullTaxReceipt.RdId = CStr(dr.Item(ColumnName.RdId))
            If Not (dr.Item(ColumnName.PosNbr) Is System.DBNull.Value) Then fullTaxReceipt.PosNbr = CInt(dr.Item(ColumnName.PosNbr))
            If Not (dr.Item(ColumnName.TranNbr) Is System.DBNull.Value) Then fullTaxReceipt.TranNbr = CInt(dr.Item(ColumnName.TranNbr))
            If Not (dr.Item(ColumnName.PumpNbr) Is System.DBNull.Value) Then fullTaxReceipt.PumpNbr = CInt(dr.Item(ColumnName.PumpNbr))
            If Not (dr.Item(ColumnName.HashTag) Is System.DBNull.Value) Then fullTaxReceipt.HashTag = CStr(dr.Item(ColumnName.HashTag))
            If Not (dr.Item(ColumnName.EmployeeName) Is System.DBNull.Value) Then fullTaxReceipt.EmployeeName = CStr(dr.Item(ColumnName.EmployeeName))
            If Not (dr.Item(ColumnName.ReplaceRef) Is System.DBNull.Value) Then fullTaxReceipt.ReplaceRef = CStr(dr.Item(ColumnName.ReplaceRef))
            If Not (dr.Item(ColumnName.Lcustomer) Is System.DBNull.Value) Then fullTaxReceipt.Lcustomer = CStr(dr.Item(ColumnName.Lcustomer))
            If Not (dr.Item(ColumnName.Lcardno) Is System.DBNull.Value) Then fullTaxReceipt.Lcardno = CStr(dr.Item(ColumnName.Lcardno))
            If Not (dr.Item(ColumnName.Ltotalpoint) Is System.DBNull.Value) Then fullTaxReceipt.Ltotalpoint = CDec(dr.Item(ColumnName.Ltotalpoint))
            If Not (dr.Item(ColumnName.Ltrnpoint) Is System.DBNull.Value) Then fullTaxReceipt.Ltrnpoint = CDec(dr.Item(ColumnName.Ltrnpoint))
            If Not (dr.Item(ColumnName.Lbalance) Is System.DBNull.Value) Then fullTaxReceipt.Lbalance = CDec(dr.Item(ColumnName.Lbalance))
            If Not (dr.Item(ColumnName.Lrepoint) Is System.DBNull.Value) Then fullTaxReceipt.Lrepoint = CDec(dr.Item(ColumnName.Lrepoint))
            If Not (dr.Item(ColumnName.Lpointtoday) Is System.DBNull.Value) Then fullTaxReceipt.Lpointtoday = CDec(dr.Item(ColumnName.Lpointtoday))
            If Not (dr.Item(ColumnName.IsFleetCard) Is System.DBNull.Value) Then fullTaxReceipt.IsFleetCard = CBool(dr.Item(ColumnName.IsFleetCard))
            If Not (dr.Item(ColumnName.HostId) Is System.DBNull.Value) Then fullTaxReceipt.HostId = CStr(dr.Item(ColumnName.HostId))
            If Not (dr.Item(ColumnName.SaleTypeDesc) Is System.DBNull.Value) Then fullTaxReceipt.SaleTypeDesc = CStr(dr.Item(ColumnName.SaleTypeDesc))
            If Not (dr.Item(ColumnName.SiteTypeId) Is System.DBNull.Value) Then fullTaxReceipt.SiteTypeId = CInt(dr.Item(ColumnName.SiteTypeId))
            If Not (dr.Item(ColumnName.IsManualFill) Is System.DBNull.Value) Then fullTaxReceipt.IsManualFill = CBool(dr.Item(ColumnName.IsManualFill))
            If Not (dr.Item(ColumnName.SiteNo) Is System.DBNull.Value) Then fullTaxReceipt.SiteNo = CStr(dr.Item(ColumnName.SiteNo))

            If Not (dr.Item(ColumnName.Version) Is System.DBNull.Value) Then fullTaxReceipt.Version = CLng(dr.Item(ColumnName.Version))
            If Not (dr.Item(ColumnName.UpdateUser) Is System.DBNull.Value) Then fullTaxReceipt.UpdateUser = CStr(dr.Item(ColumnName.UpdateUser))
            If Not (dr.Item(ColumnName.UpdateDttm) Is System.DBNull.Value) Then fullTaxReceipt.UpdateDttm = CDate(dr.Item(ColumnName.UpdateDttm))

            If ftsVersion >= 2.3 Then
                'receipt table
                If Not (dr.Item(ColumnName.CnRef) Is System.DBNull.Value) Then fullTaxReceipt.CnRef = CStr(dr.Item(ColumnName.CnRef))
                If Not (dr.Item(ColumnName.FullTaxNbrRef) Is System.DBNull.Value) Then fullTaxReceipt.FullTaxNbrRef = CStr(dr.Item(ColumnName.FullTaxNbrRef))
                'If Not (dr.Item(ColumnName.ReasonDesc) Is System.DBNull.Value) Then fullTaxReceipt.ReasonDesc = CStr(dr.Item(ColumnName.ReasonDesc))

                'full tax receive table
                If Not (dr.Item(ColumnName.CustBranchName) Is System.DBNull.Value) Then fullTaxReceipt.CustBranchName = CStr(dr.Item(ColumnName.CustBranchName))
            End If

        End Sub

        Private Shared Sub PopulateCollection(ByVal fullTaxReceipts As FullTaxReceiptCollection, ByVal dr As NpgsqlDataReader)
            Dim ftsVersion As Double = 1.0

            Do While dr.Read
                Dim fullTaxReceipt As New FullTaxReceipt

                If ftsVersion = 1.0 Then
                    'verify has new column or not (only first record
                    If HasColumn(dr, ColumnName.CnRef) Then
                        ftsVersion = 2.3
                    Else
                        ftsVersion = 2.2
                    End If
                End If
                PopulateData(fullTaxReceipt, dr, ftsVersion)
                fullTaxReceipts.Add(fullTaxReceipt)

                System.Threading.Thread.Sleep(1) 'pause CPU peak load
            Loop
        End Sub

        Private Shared Sub MapCommandParameter(ByVal command As NpgsqlCommand, ByVal fullTaxReceipt As FullTaxReceipt)
            If IsDBNull(fullTaxReceipt.ReceiptNbr) Or IsNothing(fullTaxReceipt.ReceiptNbr) Then
                command.Parameters.AddWithValue(ParamName.ReceiptNbr, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.ReceiptNbr, fullTaxReceipt.ReceiptNbr)
            End If
            If IsDBNull(fullTaxReceipt.CustName) Or IsNothing(fullTaxReceipt.CustName) Then
                command.Parameters.AddWithValue(ParamName.CustName, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.CustName, fullTaxReceipt.CustName)
            End If
            If IsDBNull(fullTaxReceipt.Address) Or IsNothing(fullTaxReceipt.Address) Then
                command.Parameters.AddWithValue(ParamName.Address, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.Address, fullTaxReceipt.Address)
            End If
            If IsDBNull(fullTaxReceipt.CustTaxId) Or IsNothing(fullTaxReceipt.CustTaxId) Then
                command.Parameters.AddWithValue(ParamName.CustTaxId, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.CustTaxId, fullTaxReceipt.CustTaxId)
            End If
            If IsDBNull(fullTaxReceipt.CustBranchNo) Or IsNothing(fullTaxReceipt.CustBranchNo) Then
                command.Parameters.AddWithValue(ParamName.CustBranchNo, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.CustBranchNo, fullTaxReceipt.CustBranchNo)
            End If
            If IsDBNull(fullTaxReceipt.CustId) Or IsNothing(fullTaxReceipt.CustId) Then
                command.Parameters.AddWithValue(ParamName.CustId, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.CustId, fullTaxReceipt.CustId)
            End If
            If IsDBNull(fullTaxReceipt.ProvinceId) Or IsNothing(fullTaxReceipt.ProvinceId) Then
                command.Parameters.AddWithValue(ParamName.ProvinceId, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.ProvinceId, fullTaxReceipt.ProvinceId)
            End If
            If IsDBNull(fullTaxReceipt.CustBranchName) Or IsNothing(fullTaxReceipt.CustBranchName) Then
                command.Parameters.AddWithValue(ParamName.CustBranchName, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.CustBranchName, fullTaxReceipt.CustBranchName)
            End If
        End Sub

#Region "Common Function Method"

        Public Shared Function GetByShiftNo(ByVal ShiftNo As String) As FullTaxReceiptCollection
            Dim fullTaxReceipts As New FullTaxReceiptCollection
            Dim cn As NpgsqlConnection = GetDBConnection()
            cn.Open()

            Dim sql As String = "select a.*,b.* from RECEIPT a "
            sql += "join FULL_TAX_RECEIPT b on a.RECEIPT_NBR = b.RECEIPT_NBR "
            sql += "join W_SHIFT c on a.SHIFT_ID=c.W_SHIFT_OID "
            sql += " where c.Shift_NO = @ShiftNo "
            sql += "  order by a.receipt_nbr"

            Dim command As New NpgsqlCommand(sql, cn)
            command.CommandType = CommandType.Text
            command.Parameters.AddWithValue(ParamName.ShiftNo, ShiftNo)
            Dim dr As NpgsqlDataReader = command.ExecuteReader

            PopulateCollection(fullTaxReceipts, dr)

            dr.Close()
            cn.Close()
            Return fullTaxReceipts
        End Function

        Public Shared Function GetByDate(ByVal receiptDate As Date) As FullTaxReceiptCollection
            Dim fullTaxReceipts As New FullTaxReceiptCollection
            Dim cn As NpgsqlConnection = GetDBConnection()
            cn.Open()

            Dim sql As String = "select a.*,b.* from RECEIPT a "
            sql += " join FULL_TAX_RECEIPT b on a.RECEIPT_NBR = b.RECEIPT_NBR "
            sql += " where 1 = 1"
            sql += String.Format(" and {0} between '{1}' and '{1} 23:59:58'", ColumnName.ReceiptDate, receiptDate.ToString("yyyy-MM-dd"))
            sql += "  order by a.receipt_nbr"

            Dim command As New NpgsqlCommand(sql, cn)
            command.CommandType = CommandType.Text
            Dim dr As NpgsqlDataReader = command.ExecuteReader

            PopulateCollection(fullTaxReceipts, dr)

            dr.Close()
            cn.Close()
            Return fullTaxReceipts
        End Function

        Public Shared Function GetFullTax(ByVal receiptDate As Date, posId As String) As FullTaxReceiptCollection
            Dim fullTaxReceipts As New FullTaxReceiptCollection
            Dim cn As NpgsqlConnection = GetDBConnection()
            cn.Open()

            Dim sql As String = "select a.*,b.* from RECEIPT a "
            sql += " join FULL_TAX_RECEIPT b on a.RECEIPT_NBR = b.RECEIPT_NBR "
            sql += " where 1 = 1"
            sql += String.Format(" and {0} between '{1}' and '{1} 23:59:59'", ColumnName.ReceiptDate, receiptDate.ToString("yyyy-MM-dd"))
            sql += String.Format(" and {0} = {1}", ColumnName.PosNbr, ParamName.PosNbr)
            sql += "  order by a.receipt_nbr"

            Dim command As New NpgsqlCommand(sql, cn)
            command.CommandType = CommandType.Text
            command.Parameters.AddWithValue(ParamName.PosNbr, posId)
            Dim dr As NpgsqlDataReader = command.ExecuteReader

            PopulateCollection(fullTaxReceipts, dr)

            dr.Close()
            cn.Close()
            Return fullTaxReceipts
        End Function

        Public Shared Function GetByNbr(ByVal FullTaxNbr As String) As FullTaxReceiptCollection
            Dim fullTaxReceipts As New FullTaxReceiptCollection
            Dim cn As NpgsqlConnection = GetDBConnection()
            cn.Open()

            Dim sql As String = "select a.*,b.* from RECEIPT a " & _
            "join FULL_TAX_RECEIPT b on a.RECEIPT_NBR = b.RECEIPT_NBR " & _
            " where a.RECEIPT_NBR=@ReceiptNbr"

            Dim command As New NpgsqlCommand(sql, cn)
            command.CommandType = CommandType.Text
            command.Parameters.AddWithValue(ParamName.ReceiptNbr, FullTaxNbr)
            Dim dr As NpgsqlDataReader = command.ExecuteReader

            PopulateCollection(fullTaxReceipts, dr)

            dr.Close()
            cn.Close()
            Return fullTaxReceipts
        End Function

        Public Shared Function GetByManualEntry(ByVal ShiftNo As String) As FullTaxReceiptCollection
            Dim fullTaxReceipts As New FullTaxReceiptCollection
            Dim cn As NpgsqlConnection = GetDBConnection()
            cn.Open()

            Dim sql As String = "select a.*,b.* from RECEIPT a " & _
            "join FULL_TAX_RECEIPT b on a.RECEIPT_NBR = b.RECEIPT_NBR " & _
            "join W_SHIFT c on a.SHIFT_ID=c.W_SHIFT_OID " & _
            " where c.Shift_NO=@ShiftNo " & _
            " and a.IS_MANUAL_ENTRY=true"

            Dim command As New NpgsqlCommand(sql, cn)
            command.CommandType = CommandType.Text
            command.Parameters.AddWithValue(ParamName.ShiftNo, ShiftNo)
            Dim dr As NpgsqlDataReader = command.ExecuteReader

            PopulateCollection(fullTaxReceipts, dr)

            dr.Close()
            cn.Close()
            Return fullTaxReceipts
        End Function

        
#End Region

    End Class

End Namespace
