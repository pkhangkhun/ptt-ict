Imports Ie.IeComponent.BO
Imports Ie.UI.Utility
Imports Npgsql

Namespace App.Dao

    Public Class CardImgDao
        Inherits BaseDao

        'Private Const CreateSproc = "spPos_CardImgAddNew"
        Private Const UpdateSproc = "spPos_CardImgUpdate"
        Private Const DeleteSproc = "spPos_CardImgDelete"
        Private Const RetrieveSproc = "spPos_CardImgRetrieve"
        Private Const RetrieveAllSproc = "spPos_CardImgRetrieveAll"

        Private Class ColumnName
            Public Const ReceiptNbr = "RECEIPT_NBR"
            Public Const CardImg = "CARD_IMG"
        End Class

        Private Class ParamName
            Public Const ReceiptNbr = "@ReceiptNbr"
            Public Const CardImg = "@CardImg"
        End Class

        Private Shared Sub PopulateData(ByVal cardImg As CardImg, ByVal dr As NpgsqlDataReader)

            If Not (dr.Item(ColumnName.ReceiptNbr) Is System.DBNull.Value) Then cardImg.ReceiptNbr = CStr(dr.Item(ColumnName.ReceiptNbr))
            If Not (dr.Item(ColumnName.CardImg) Is System.DBNull.Value) Then cardImg.CardImg = dr.Item(ColumnName.CardImg)

        End Sub

        Private Shared Sub PopulateCollection(ByVal cardImgs As CardImgCollection, ByVal dr As NpgsqlDataReader)
            Do While (dr.Read())
                Dim cardImg As New CardImg
                PopulateData(cardImg, dr)
                cardImgs.Add(cardImg)

            Loop
        End Sub

        Private Shared Sub MapCommandParameter(ByVal command As NpgsqlCommand, ByVal cardImg As CardImg)
        
            If IsDBNull(cardImg.ReceiptNbr) Or IsNothing(cardImg.ReceiptNbr) Then
                command.Parameters.Add(ParamName.ReceiptNbr, System.DBNull.Value)
            Else
                command.Parameters.Add(ParamName.ReceiptNbr, cardImg.ReceiptNbr)
            End If

            Dim img As Byte() = cardImg.CardImg

            Dim imgPara As NpgsqlParameter = New NpgsqlParameter("@CardImg", SqlDbType.Image)

            imgPara.Value = img

            command.Parameters.Add(imgPara)
        End Sub

#Region "Common Function Method"

        Public Shared Sub Create(ByVal cardImg As CardImg, ByVal cn As NpgsqlConnection, ByVal txn As NpgsqlTransaction)
            Dim CreateSproc As String = "" 'My.Resources.rs_CardImg.CreateSproc
            Dim command As New NpgsqlCommand(CreateSproc, cn, txn)
            command.CommandType = CommandType.Text
            MapCommandParameter(command, cardImg)
            command.ExecuteNonQuery()
        End Sub

        Public Shared Sub Update(ByVal cardImg As CardImg, ByVal cn As NpgSqlConnection, ByVal txn As NpgSqlTransaction)
            Dim command As New NpgsqlCommand(UpdateSproc, cn, txn)
            command.CommandType = CommandType.Text
            MapCommandParameter(command, cardImg)
            command.ExecuteNonQuery()
        End Sub

        Public Shared Sub Delete(ByVal cardImg As CardImg, ByVal cn As NpgsqlConnection, ByVal txn As NpgsqlTransaction)
            Dim command As New NpgsqlCommand(DeleteSproc, cn, txn)
            command.CommandType = CommandType.Text
            command.Parameters.Add(ParamName.ReceiptNbr, cardImg.ReceiptNbr)
            command.ExecuteNonQuery()
        End Sub

        Public Shared Function Retrieve(ByVal receiptNbr As String) As CardImg
            Dim cardImg As New CardImg
            Dim cn As NpgSqlConnection = GetDBConnection()
            cn.Open()
            Dim sql As String = " select * from Card_Img where receipt_Nbr=" + Utility.DBUtil.SqlQuote(receiptNbr)
            Dim command As New NpgsqlCommand(sql, cn)
            command.CommandType = CommandType.Text
            command.Parameters.Add(ParamName.ReceiptNbr, receiptNbr)
            Dim dr As NpgsqlDataReader = command.ExecuteReader

            If dr.Read() Then PopulateData(cardImg, dr)

            dr.Close()
            cn.Close()
            Return cardImg
        End Function

        Public Shared Function RetrieveAll() As CardImgCollection
            Dim cardImgs As New CardImgCollection
            Dim cn As NpgsqlConnection = GetDBConnection()
            cn.Open()

            Dim command As New NpgsqlCommand(RetrieveAllSproc, cn)
            command.CommandType = CommandType.Text
            Dim dr As NpgsqlDataReader = command.ExecuteReader

            PopulateCollection(cardImgs, dr)

            dr.Close()
            cn.Close()
            Return cardImgs
        End Function

#End Region

    End Class

End Namespace
