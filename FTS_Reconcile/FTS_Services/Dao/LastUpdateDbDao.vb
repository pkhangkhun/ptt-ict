Imports Ie.IeComponent.BO
Imports FTS_HQ_MsmqServ.HQComponent.BO
Imports Npgsql
Imports Ie.UI.Utility

Namespace App.Dao

    Public Class LastUpdateDbDao
       

#Region "Common Function Method"


        Public Shared Sub Create(ByVal tableName As String, ByVal fileName As String, ByVal lastUpdateDttm As Date, ByVal cn As NpgsqlConnection, ByVal txn As NpgsqlTransaction)
            Dim sql As String = " insert into last_update_db ( table_name,file_name,last_update_dttm)"
            sql += " VALUES (@tableName,@fileName,@lastUpdateDttm)"

            Dim command As New NpgsqlCommand(sql, cn, txn)
            command.CommandType = CommandType.Text
            command.Parameters.Add("@tableName", tableName)
            command.Parameters.Add("@fileName", fileName)
            command.Parameters.Add("@lastUpdateDttm", lastUpdateDttm)
            command.ExecuteNonQuery()
        End Sub

        Public Shared Sub Update(ByVal tableName As String, ByVal fileName As String, ByVal lastUpdateDttm As Date, ByVal cn As NpgsqlConnection, ByVal txn As NpgsqlTransaction)
            Dim sql As String = " Update last_update_db SET"
            sql += " file_name=@fileName,last_update_dttm=@lastUpdateDttm"
            sql += " WHERE table_name=@tableName"

            Dim command As New NpgsqlCommand(sql, cn, txn)
            command.CommandType = CommandType.Text
            command.Parameters.Add("@tableName", tableName)
            command.Parameters.Add("@fileName", fileName)
            command.Parameters.Add("@lastUpdateDttm", lastUpdateDttm)
            command.ExecuteNonQuery()
        End Sub
        Public Shared Function CheckExits(ByVal tableName As String, ByVal cn As NpgsqlConnection, ByVal txn As NpgsqlTransaction) As Boolean

            Dim isExists As Boolean = False

            Dim sqlCmd As String = "select * from last_update_db where table_Name=" + Utility.DBUtil.SqlQuote(tableName)
            Dim command As New NpgsqlCommand(sqlCmd, cn, txn)
            command.CommandType = CommandType.Text
            Dim dr As NpgsqlDataReader = command.ExecuteReader
            If dr.Read Then
                isExists = True
            End If

            dr.Close()
            Return isExists
        End Function
#End Region

    End Class

End Namespace
