Imports Ie.IeComponent.BO
Imports Npgsql
Imports Ie.UI.Utility

Namespace App.Dao

    Public Class WShiftDao
        Inherits BaseDao

        Private Class ColumnName
            Public Const WShiftOId = "W_SHIFT_OID"
            Public Const SiteNo = "SITE_NO"
            Public Const SiteTypeId = "SITE_TYPE_ID"
            Public Const ShiftNo = "SHIFT_NO"
            Public Const OpenShift = "OPEN_SHIFT"
            Public Const CloseShift = "CLOSE_SHIFT"
            Public Const IsOpen = "IS_OPEN"
            Public Const OpenBy = "OPEN_BY"
            Public Const CloseBy = "CLOSE_BY"
            Public Const FullTaxQty = "FULL_TAX_QTY"
            Public Const CancelQty = "CANCEL_QTY"
            Public Const VoidQty = "VOID_QTY"
            Public Const ReplaceQty = "REPLACE_QTY"
            Public Const CnQty = "CN_QTY"
            Public Const FleetQty = "FLEET_QTY"
            Public Const ManualQty = "MANUAL_QTY"
            Public Const ScanQty = "SCAN_QTY"
            Public Const FuelQty = "FUEL_QTY"
            Public Const GoodsQty = "GOODS_QTY"
            Public Const StartFullTaxNbr = "START_FULL_TAX_NBR"
            Public Const EndFullTaxNbr = "END_FULL_TAX_NBR"
            Public Const StartManualNbr = "START_MANUAL_NBR"
            Public Const EndManualNbr = "END_MANUAL_NBR"
            Public Const StartCnNbr = "START_CN_NBR"
            Public Const EndCnNbr = "END_CN_NBR"

        End Class

        Private Class ParamName
            Public Const WShiftOId = "@WShiftOId"
            Public Const SiteNo = "@SiteNo"
            Public Const SiteTypeId = "@SiteTypeId"
            Public Const ShiftNo = "@ShiftNo"
            Public Const OpenShift = "@OpenShift"
            Public Const CloseShift = "@CloseShift"
            Public Const IsOpen = "@IsOpen"
            Public Const OpenBy = "@OpenBy"
            Public Const CloseBy = "@CloseBy"
            Public Const FullTaxQty = "@FullTaxQty"
            Public Const CancelQty = "@CancelQty"
            Public Const VoidQty = "@VoidQty"
            Public Const ReplaceQty = "@ReplaceQty"
            Public Const CnQty = "@CnQty"
            Public Const FleetQty = "@FleetQty"
            Public Const ManualQty = "@ManualQty"
            Public Const ScanQty = "@ScanQty"
            Public Const FuelQty = "@FuelQty"
            Public Const GoodsQty = "@GoodsQty"
            Public Const StartFullTaxNbr = "@StartFullTaxNbr"
            Public Const EndFullTaxNbr = "@EndFullTaxNbr"
            Public Const StartManualNbr = "@StartManualNbr"
            Public Const EndManualNbr = "@EndManualNbr"
            Public Const StartCnNbr = "@StartCnNbr"
            Public Const EndCnNbr = "@EndCnNbr"
        End Class

        Private Shared Sub PopulateData(ByVal shift As WShift, ByVal dr As NpgsqlDataReader)

            If Not (dr.Item(ColumnName.WShiftOId) Is System.DBNull.Value) Then shift.WShiftOid = CInt(dr.Item(ColumnName.WShiftOId))
            If Not (dr.Item(ColumnName.SiteNo) Is System.DBNull.Value) Then shift.SiteNo = CStr(dr.Item(ColumnName.SiteNo))
            If Not (dr.Item(ColumnName.SiteTypeId) Is System.DBNull.Value) Then shift.SiteTypeId = CInt(dr.Item(ColumnName.SiteTypeId))
            If Not (dr.Item(ColumnName.OpenShift) Is System.DBNull.Value) Then shift.OpenShift = CDate(dr.Item(ColumnName.OpenShift))
            If Not (dr.Item(ColumnName.CloseShift) Is System.DBNull.Value) Then shift.CloseShift = CDate(dr.Item(ColumnName.CloseShift))
            If Not (dr.Item(ColumnName.ShiftNo) Is System.DBNull.Value) Then shift.ShiftNo = CStr(dr.Item(ColumnName.ShiftNo))
            If Not (dr.Item(ColumnName.IsOpen) Is System.DBNull.Value) Then shift.IsOpen = CBool(dr.Item(ColumnName.IsOpen))
            If Not (dr.Item(ColumnName.OpenBy) Is System.DBNull.Value) Then shift.OpenBy = CStr(dr.Item(ColumnName.OpenBy))
            If Not (dr.Item(ColumnName.CloseBy) Is System.DBNull.Value) Then shift.CloseBy = CStr(dr.Item(ColumnName.CloseBy))
            If Not (dr.Item(ColumnName.FullTaxQty) Is System.DBNull.Value) Then shift.FullTaxQty = CInt(dr.Item(ColumnName.FullTaxQty))
            If Not (dr.Item(ColumnName.CancelQty) Is System.DBNull.Value) Then shift.CancelQty = CInt(dr.Item(ColumnName.CancelQty))
            If Not (dr.Item(ColumnName.VoidQty) Is System.DBNull.Value) Then shift.VoidQty = CInt(dr.Item(ColumnName.VoidQty))
            If Not (dr.Item(ColumnName.ReplaceQty) Is System.DBNull.Value) Then shift.ReplaceQty = CInt(dr.Item(ColumnName.ReplaceQty))
            If Not (dr.Item(ColumnName.CnQty) Is System.DBNull.Value) Then shift.CnQty = CInt(dr.Item(ColumnName.CnQty))
            If Not (dr.Item(ColumnName.FleetQty) Is System.DBNull.Value) Then shift.FleetQty = CInt(dr.Item(ColumnName.FleetQty))
            If Not (dr.Item(ColumnName.ManualQty) Is System.DBNull.Value) Then shift.ManualQty = CInt(dr.Item(ColumnName.ManualQty))
            If Not (dr.Item(ColumnName.ScanQty) Is System.DBNull.Value) Then shift.ScanQty = CInt(dr.Item(ColumnName.ScanQty))
            If Not (dr.Item(ColumnName.FuelQty) Is System.DBNull.Value) Then shift.FuelQty = CInt(dr.Item(ColumnName.FuelQty))
            If Not (dr.Item(ColumnName.GoodsQty) Is System.DBNull.Value) Then shift.GoodsQty = CInt(dr.Item(ColumnName.GoodsQty))

            If Not (dr.Item(ColumnName.StartFullTaxNbr) Is System.DBNull.Value) Then shift.StartFullTaxNbr = CStr(dr.Item(ColumnName.StartFullTaxNbr))
            If Not (dr.Item(ColumnName.EndFullTaxNbr) Is System.DBNull.Value) Then shift.EndFullTaxNbr = CStr(dr.Item(ColumnName.EndFullTaxNbr))
            If Not (dr.Item(ColumnName.StartManualNbr) Is System.DBNull.Value) Then shift.StartManualNbr = CStr(dr.Item(ColumnName.StartManualNbr))
            If Not (dr.Item(ColumnName.EndManualNbr) Is System.DBNull.Value) Then shift.EndManualNbr = CStr(dr.Item(ColumnName.EndManualNbr))
            If Not (dr.Item(ColumnName.StartCnNbr) Is System.DBNull.Value) Then shift.StartCnNbr = CStr(dr.Item(ColumnName.StartCnNbr))
            If Not (dr.Item(ColumnName.EndCnNbr) Is System.DBNull.Value) Then shift.EndCnNbr = CStr(dr.Item(ColumnName.EndCnNbr))

        End Sub

        Private Shared Sub PopulateCollection(ByVal shifts As WShiftCollection, ByVal dr As NpgsqlDataReader)
            Do While (dr.Read())
                Dim shift As New WShift
                PopulateData(shift, dr)
                shifts.Add(shift)
            Loop
        End Sub

        Private Shared Sub MapCommandParameter(ByVal command As NpgsqlCommand, ByVal shift As WShift)

            If IsDBNull(shift.WShiftOid) Or IsNothing(shift.WShiftOid) Then
                command.Parameters.Add(ParamName.WShiftOId, System.DBNull.Value)
            Else
                command.Parameters.Add(ParamName.WShiftOId, shift.WShiftOid)
            End If
            If IsDBNull(shift.SiteNo) Or IsNothing(shift.SiteNo) Then
                command.Parameters.Add(ParamName.SiteNo, System.DBNull.Value)
            Else
                command.Parameters.Add(ParamName.SiteNo, shift.SiteNo)
            End If
            If IsDBNull(shift.SiteTypeId) Or IsNothing(shift.SiteTypeId) Then
                command.Parameters.Add(ParamName.SiteTypeId, System.DBNull.Value)
            Else
                command.Parameters.Add(ParamName.SiteTypeId, shift.SiteTypeId)
            End If
            If IsDBNull(shift.ShiftNo) Or IsNothing(shift.ShiftNo) Then
                command.Parameters.Add(ParamName.ShiftNo, System.DBNull.Value)
            Else
                command.Parameters.Add(ParamName.ShiftNo, shift.ShiftNo)
            End If
            If Not InputValidator.IsDate(shift.OpenShift) Then
                command.Parameters.Add(ParamName.OpenShift, System.DBNull.Value)
            Else
                command.Parameters.Add(ParamName.OpenShift, shift.OpenShift)
            End If
            If Not InputValidator.IsDate(shift.CloseShift) Then
                command.Parameters.Add(ParamName.CloseShift, System.DBNull.Value)
            Else
                command.Parameters.Add(ParamName.CloseShift, shift.CloseShift)
            End If
            If IsDBNull(shift.IsOpen) Or IsNothing(shift.IsOpen) Then
                command.Parameters.Add(ParamName.IsOpen, System.DBNull.Value)
            Else
                command.Parameters.Add(ParamName.IsOpen, shift.IsOpen)
            End If
            If IsDBNull(shift.OpenBy) Or IsNothing(shift.OpenBy) Then
                command.Parameters.Add(ParamName.OpenBy, System.DBNull.Value)
            Else
                command.Parameters.Add(ParamName.OpenBy, shift.OpenBy)
            End If
            If IsDBNull(shift.CloseBy) Or IsNothing(shift.CloseBy) Then
                command.Parameters.Add(ParamName.CloseBy, System.DBNull.Value)
            Else
                command.Parameters.Add(ParamName.CloseBy, shift.CloseBy)
            End If
            If IsDBNull(shift.FullTaxQty) Or IsNothing(shift.FullTaxQty) Then
                command.Parameters.Add(ParamName.FullTaxQty, System.DBNull.Value)
            Else
                command.Parameters.Add(ParamName.FullTaxQty, shift.FullTaxQty)
            End If
            If IsDBNull(shift.CancelQty) Or IsNothing(shift.CancelQty) Then
                command.Parameters.Add(ParamName.CancelQty, System.DBNull.Value)
            Else
                command.Parameters.Add(ParamName.CancelQty, shift.CancelQty)
            End If
            If IsDBNull(shift.VoidQty) Or IsNothing(shift.VoidQty) Then
                command.Parameters.Add(ParamName.VoidQty, System.DBNull.Value)
            Else
                command.Parameters.Add(ParamName.VoidQty, shift.VoidQty)
            End If
            If IsDBNull(shift.ReplaceQty) Or IsNothing(shift.ReplaceQty) Then
                command.Parameters.Add(ParamName.ReplaceQty, System.DBNull.Value)
            Else
                command.Parameters.Add(ParamName.ReplaceQty, shift.ReplaceQty)
            End If
            If IsDBNull(shift.CnQty) Or IsNothing(shift.CnQty) Then
                command.Parameters.Add(ParamName.CnQty, System.DBNull.Value)
            Else
                command.Parameters.Add(ParamName.CnQty, shift.CnQty)
            End If
            If IsDBNull(shift.FleetQty) Or IsNothing(shift.FleetQty) Then
                command.Parameters.Add(ParamName.FleetQty, System.DBNull.Value)
            Else
                command.Parameters.Add(ParamName.FleetQty, shift.FleetQty)
            End If
            If IsDBNull(shift.ManualQty) Or IsNothing(shift.ManualQty) Then
                command.Parameters.Add(ParamName.ManualQty, System.DBNull.Value)
            Else
                command.Parameters.Add(ParamName.ManualQty, shift.ManualQty)
            End If
            If IsDBNull(shift.ScanQty) Or IsNothing(shift.ScanQty) Then
                command.Parameters.Add(ParamName.ScanQty, System.DBNull.Value)
            Else
                command.Parameters.Add(ParamName.ScanQty, shift.ScanQty)
            End If
            If IsDBNull(shift.FuelQty) Or IsNothing(shift.FuelQty) Then
                command.Parameters.Add(ParamName.FuelQty, System.DBNull.Value)
            Else
                command.Parameters.Add(ParamName.FuelQty, shift.FuelQty)
            End If
            If IsDBNull(shift.GoodsQty) Or IsNothing(shift.GoodsQty) Then
                command.Parameters.Add(ParamName.GoodsQty, System.DBNull.Value)
            Else
                command.Parameters.Add(ParamName.GoodsQty, shift.GoodsQty)
            End If
            If IsDBNull(shift.StartFullTaxNbr) Or IsNothing(shift.StartFullTaxNbr) Then
                command.Parameters.Add(ParamName.StartFullTaxNbr, System.DBNull.Value)
            Else
                command.Parameters.Add(ParamName.StartFullTaxNbr, shift.StartFullTaxNbr)
            End If
            If IsDBNull(shift.EndFullTaxNbr) Or IsNothing(shift.EndFullTaxNbr) Then
                command.Parameters.Add(ParamName.EndFullTaxNbr, System.DBNull.Value)
            Else
                command.Parameters.Add(ParamName.EndFullTaxNbr, shift.EndFullTaxNbr)
            End If
            If IsDBNull(shift.StartManualNbr) Or IsNothing(shift.StartManualNbr) Then
                command.Parameters.Add(ParamName.StartManualNbr, System.DBNull.Value)
            Else
                command.Parameters.Add(ParamName.StartManualNbr, shift.StartManualNbr)
            End If
            If IsDBNull(shift.EndManualNbr) Or IsNothing(shift.EndManualNbr) Then
                command.Parameters.Add(ParamName.EndManualNbr, System.DBNull.Value)
            Else
                command.Parameters.Add(ParamName.EndManualNbr, shift.EndManualNbr)
            End If
            If IsDBNull(shift.StartCnNbr) Or IsNothing(shift.StartCnNbr) Then
                command.Parameters.Add(ParamName.StartCnNbr, System.DBNull.Value)
            Else
                command.Parameters.Add(ParamName.StartCnNbr, shift.StartCnNbr)
            End If
            If IsDBNull(shift.EndCnNbr) Or IsNothing(shift.EndCnNbr) Then
                command.Parameters.Add(ParamName.EndCnNbr, System.DBNull.Value)
            Else
                command.Parameters.Add(ParamName.EndCnNbr, shift.EndCnNbr)
            End If

        End Sub

#Region "Common Function Method"


        Public Shared Function GetByShiftNo(ByVal shiftNo As String) As WShift
            Dim shift As New WShift
            Dim cn As NpgsqlConnection = GetDBConnection()
            cn.Open()

            Dim RetrieveSproc As String = "select * from w_shift where shift_no='" + shiftNo + "'"
            Dim command As New NpgsqlCommand(RetrieveSproc, cn)
            command.CommandType = CommandType.Text

            Dim dr As NpgsqlDataReader = command.ExecuteReader

            If dr.Read() Then PopulateData(shift, dr)

            dr.Close()
            cn.Close()
            Return shift
        End Function

        Public Shared Function GetByShiftDate(ByVal fromdate As String, ByVal toDate As String) As WShiftCollection
            Dim shifts As New WShiftCollection
            Dim cn As NpgsqlConnection = GetDBConnection()
            cn.Open()

            Dim RetrieveSproc As String = "select * from w_shift where open_shift between '" + fromdate + "' and '" + toDate + "' order by open_shift"
            Dim command As New NpgsqlCommand(RetrieveSproc, cn)
            command.CommandType = CommandType.Text

            Dim dr As NpgsqlDataReader = command.ExecuteReader

            PopulateCollection(shifts, dr)

            dr.Close()
            cn.Close()
            Return shifts

        End Function

#End Region

    End Class

End Namespace
