Imports Npgsql
Imports Ie.IeComponent.BO
Namespace App.Utility

    Public Class ObjectIDGenerator

        ' ObjectID Generator for all Document ID (such as Invoice, PO,etc.)

        Public Shared Function GetNextObjectID(ByVal type As Type) As ObjectID

            Dim cn As NpgsqlConnection = DBUtil.GetDBConnection()
            cn.Open()
            Dim txn As NpgsqlTransaction = cn.BeginTransaction(IsolationLevel.Serializable)

            Dim newObjectID As New ObjectID(0)

            Try
                Dim command As New NpgsqlCommand("select object_id from object_info where class_name='" + type.FullName + "'", cn, txn)
                command.CommandType = CommandType.Text
                Dim dr As NpgsqlDataReader = command.ExecuteReader
                If dr.Read Then
                    newObjectID.ID = dr.Item(0) + 1
                End If
                dr.Close()

                If newObjectID.ID > 0 Then
                    Dim command2 As New NpgsqlCommand("update object_info set object_id=" + CStr(newObjectID.ID) + " where class_name='" + type.FullName + "'", cn, txn)
                    command2.CommandType = CommandType.Text
                    command2.ExecuteNonQuery()
                Else
                    Dim sql As String = "insert into object_info (object_id,class_name,parent_class_name) values ("
                    sql += "1,"
                    sql += DBUtil.SqlQuote(type.FullName) + ","
                    sql += DBUtil.SqlQuote(" ") + ")"
                    Dim command2 As New NpgsqlCommand(sql, cn, txn)
                    command2.CommandType = CommandType.Text
                    command2.ExecuteNonQuery()

                    newObjectID.ID = 1

                End If
                

                txn.Commit()
            Catch ex As Exception
                txn.Rollback()
            Finally
                cn.Close()
            End Try

            Return newObjectID
        End Function

        Public Shared Function GetNextObjectID(ByVal type As Type, ByVal cn As NpgsqlConnection, ByVal txn As NpgsqlTransaction) As ObjectID

            Dim newObjectID As New ObjectID(0)


            Dim command As New NpgsqlCommand("select object_id from object_info where class_name='" + type.FullName + "'", cn, txn)
            command.CommandType = CommandType.Text
            Dim dr As NpgsqlDataReader = command.ExecuteReader
            If dr.Read Then
                newObjectID.ID = dr.Item(0) + 1
            End If
            dr.Close()

            If newObjectID.ID > 0 Then
                Dim command2 As New NpgsqlCommand("update object_info set object_id=" + CStr(newObjectID.ID) + " where class_name='" + type.FullName + "'", cn, txn)
                command2.CommandType = CommandType.Text
                command2.ExecuteNonQuery()
            Else
                Dim sql As String = "insert into object_info (object_id,class_name,parent_class_name) values ("
                sql += "1,"
                sql += DBUtil.SqlQuote(type.FullName) + ","
                sql += DBUtil.SqlQuote(" ") + ")"
                Dim command2 As New NpgsqlCommand(sql, cn, txn)
                command2.CommandType = CommandType.Text
                command2.ExecuteNonQuery()

                newObjectID.ID = 1

            End If

            Return newObjectID
        End Function

    End Class

End Namespace
