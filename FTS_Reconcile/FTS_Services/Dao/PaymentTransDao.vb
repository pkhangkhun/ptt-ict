Imports Ie.IeComponent.BO
Imports Npgsql
Imports Ie.UI.Utility

Namespace App.Dao

    Public Class PaymentTransDao
        Inherits BaseDao

        Private Class ColumnName
            Public Const ObjectId = "OBJECT_ID"
            Public Const PayAmt = "PAY_AMT"
            Public Const CreditCardNo = "CREDIT_CARD_NO"
            Public Const AprvCode = "APRV_CODE"
            Public Const ExpireDate = "EXPIRE_DATE"
            Public Const PaymentMthdId = "PAYMENT_MTHD_ID"
            Public Const Remark = "REMARK"
            Public Const ReceiptNbr = "RECEIPT_NBR"
            Public Const Version = "VERSION"
            Public Const CreateUser = "CREATE_USER"
            Public Const CreateDttm = "CREATE_DTTM"
            Public Const UpdateUser = "UPDATE_USER"
            Public Const UpdateDttm = "UPDATE_DTTM"
        End Class

        Private Class ParamName
            Public Const ObjectId = "@ObjectId"
            Public Const PayAmt = "@PayAmt"
            Public Const CreditCardNo = "@CreditCardNo"
            Public Const AprvCode = "@AprvCode"
            Public Const ExpireDate = "@ExpireDate"
            Public Const PaymentMthdId = "@PaymentMthdId"
            Public Const Remark = "@Remark"
            Public Const ReceiptNbr = "@ReceiptNbr"
            Public Const Version = "@Version"
            Public Const CreateUser = "@CreateUser"
            Public Const CreateDttm = "@CreateDttm"
            Public Const UpdateUser = "@UpdateUser"
            Public Const UpdateDttm = "@UpdateDttm"
        End Class

        Private Shared Sub PopulateData(ByVal paymentTrans As PaymentTrans, ByVal dr As NpgsqlDataReader)

            paymentTrans.ObjectID.ID = CType(dr.Item(ColumnName.ObjectId), Long)
            If Not (dr.Item(ColumnName.PayAmt) Is System.DBNull.Value) Then paymentTrans.PayAmt = CDec(dr.Item(ColumnName.PayAmt))
            If Not (dr.Item(ColumnName.CreditCardNo) Is System.DBNull.Value) Then paymentTrans.CreditCardNo = CStr(dr.Item(ColumnName.CreditCardNo))
            If Not (dr.Item(ColumnName.AprvCode) Is System.DBNull.Value) Then paymentTrans.AprvCode = CStr(dr.Item(ColumnName.AprvCode))
            If Not (dr.Item(ColumnName.ExpireDate) Is System.DBNull.Value) Then paymentTrans.ExpireDate = CStr(dr.Item(ColumnName.ExpireDate))
            If Not (dr.Item(ColumnName.PaymentMthdId) Is System.DBNull.Value) Then paymentTrans.PaymentMthdId = CLng(dr.Item(ColumnName.PaymentMthdId))
            If Not (dr.Item(ColumnName.Remark) Is System.DBNull.Value) Then paymentTrans.Remark = CStr(dr.Item(ColumnName.Remark))
            If Not (dr.Item(ColumnName.ReceiptNbr) Is System.DBNull.Value) Then paymentTrans.ReceiptNbr = CStr(dr.Item(ColumnName.ReceiptNbr))
            If Not (dr.Item(ColumnName.Version) Is System.DBNull.Value) Then paymentTrans.Version = CLng(dr.Item(ColumnName.Version))
            If Not (dr.Item(ColumnName.CreateUser) Is System.DBNull.Value) Then paymentTrans.CreateUser = CStr(dr.Item(ColumnName.CreateUser))
            If Not (dr.Item(ColumnName.CreateDttm) Is System.DBNull.Value) Then paymentTrans.CreateDttm = CDate(dr.Item(ColumnName.CreateDttm))
            If Not (dr.Item(ColumnName.UpdateUser) Is System.DBNull.Value) Then paymentTrans.UpdateUser = CStr(dr.Item(ColumnName.UpdateUser))
            If Not (dr.Item(ColumnName.UpdateDttm) Is System.DBNull.Value) Then paymentTrans.UpdateDttm = CDate(dr.Item(ColumnName.UpdateDttm))

        End Sub

        Private Shared Sub PopulateCollection(ByVal paymentTranss As PaymentTransCollection, ByVal dr As NpgsqlDataReader)
            Do While (dr.Read())
                Dim paymentTrans As New PaymentTrans
                PopulateData(paymentTrans, dr)
                paymentTranss.Add(paymentTrans)
            Loop
        End Sub

        Private Shared Sub MapCommandParameter(ByVal command As NpgsqlCommand, ByVal paymentTrans As PaymentTrans)
            command.Parameters.Add(ParamName.ObjectId, paymentTrans.ObjectID.ID)
            If IsDBNull(paymentTrans.PayAmt) Or IsNothing(paymentTrans.PayAmt) Then
                command.Parameters.Add(ParamName.PayAmt, System.DBNull.Value)
            Else
                command.Parameters.Add(ParamName.PayAmt, paymentTrans.PayAmt)
            End If
            If IsDBNull(paymentTrans.CreditCardNo) Or IsNothing(paymentTrans.CreditCardNo) Then
                command.Parameters.Add(ParamName.CreditCardNo, System.DBNull.Value)
            Else
                command.Parameters.Add(ParamName.CreditCardNo, paymentTrans.CreditCardNo)
            End If
            If IsDBNull(paymentTrans.AprvCode) Or IsNothing(paymentTrans.AprvCode) Then
                command.Parameters.Add(ParamName.AprvCode, System.DBNull.Value)
            Else
                command.Parameters.Add(ParamName.AprvCode, paymentTrans.AprvCode)
            End If
            If IsDBNull(paymentTrans.ExpireDate) Or IsNothing(paymentTrans.ExpireDate) Then
                command.Parameters.Add(ParamName.ExpireDate, System.DBNull.Value)
            Else
                command.Parameters.Add(ParamName.ExpireDate, paymentTrans.ExpireDate)
            End If
            If IsDBNull(paymentTrans.PaymentMthdId) Or IsNothing(paymentTrans.PaymentMthdId) Then
                command.Parameters.Add(ParamName.PaymentMthdId, System.DBNull.Value)
            Else
                command.Parameters.Add(ParamName.PaymentMthdId, paymentTrans.PaymentMthdId)
            End If
            If IsDBNull(paymentTrans.Remark) Or IsNothing(paymentTrans.Remark) Then
                command.Parameters.Add(ParamName.Remark, System.DBNull.Value)
            Else
                command.Parameters.Add(ParamName.Remark, paymentTrans.Remark)
            End If
            If IsDBNull(paymentTrans.ReceiptNbr) Or IsNothing(paymentTrans.ReceiptNbr) Then
                command.Parameters.Add(ParamName.ReceiptNbr, System.DBNull.Value)
            Else
                command.Parameters.Add(ParamName.ReceiptNbr, paymentTrans.ReceiptNbr)
            End If
            If IsDBNull(paymentTrans.Version) Or IsNothing(paymentTrans.Version) Then
                command.Parameters.Add(ParamName.Version, System.DBNull.Value)
            Else
                command.Parameters.Add(ParamName.Version, paymentTrans.Version)
            End If
            If IsDBNull(paymentTrans.CreateUser) Or IsNothing(paymentTrans.CreateUser) Then
                command.Parameters.Add(ParamName.CreateUser, System.DBNull.Value)
            Else
                command.Parameters.Add(ParamName.CreateUser, paymentTrans.CreateUser)
            End If
            If Not InputValidator.IsDate(paymentTrans.CreateDttm) Then
                command.Parameters.Add(ParamName.CreateDttm, System.DBNull.Value)
            Else
                command.Parameters.Add(ParamName.CreateDttm, paymentTrans.CreateDttm)
            End If
            If IsDBNull(paymentTrans.UpdateUser) Or IsNothing(paymentTrans.UpdateUser) Then
                command.Parameters.Add(ParamName.UpdateUser, System.DBNull.Value)
            Else
                command.Parameters.Add(ParamName.UpdateUser, paymentTrans.UpdateUser)
            End If
            If Not InputValidator.IsDate(paymentTrans.UpdateDttm) Then
                command.Parameters.Add(ParamName.UpdateDttm, System.DBNull.Value)
            Else
                command.Parameters.Add(ParamName.UpdateDttm, paymentTrans.UpdateDttm)
            End If
        End Sub

#Region "Common Function Method"

        Public Shared Function GetByFullTaxNbr(ByVal fullTaxNbr As String) As PaymentTransCollection
            Dim paymentTranss As New PaymentTransCollection
            Dim cn As NpgsqlConnection = GetDBConnection()
            cn.Open()

            Dim GetByReceiptNbrSproc As String = "select * from payment_trans where receipt_nbr=@ReceiptNbr"
            Dim command As New NpgsqlCommand(GetByReceiptNbrSproc, cn)
            command.CommandType = CommandType.Text
            command.Parameters.Add(ParamName.ReceiptNbr, fullTaxNbr)
            Dim dr As NpgsqlDataReader = command.ExecuteReader

            PopulateCollection(paymentTranss, dr)

            dr.Close()
            cn.Close()
            Return paymentTranss
        End Function

#End Region

    End Class

End Namespace
