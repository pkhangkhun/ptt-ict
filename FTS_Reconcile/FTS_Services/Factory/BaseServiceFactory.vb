Namespace UI.Factory
    Public Class BaseServiceFactory

        Public Shared Function GetInstance(ByVal localClass As Type) As Object
            Return Activator.CreateInstance(localClass)
        End Function

    End Class
End Namespace
