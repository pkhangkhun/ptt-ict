Imports Ie.IeComponent.BO
Imports Npgsql
Imports System.IO
Imports FTS_Services.App.Dao

Namespace App.Service

    Public Class FullTaxService
        Inherits System.ComponentModel.Component
        Implements IFullTaxService

        Public Function GetByShiftNo(ByVal shiftNo As String) As Ie.IeComponent.BO.FullTaxReceiptCollection Implements IFullTaxService.GetByShiftNo

            Return FullTaxDao.GetByShiftNo(shiftNo)
        End Function

        Public Function GetByNbr(ByVal FullTaxNbr As String) As Ie.IeComponent.BO.FullTaxReceiptCollection Implements IFullTaxService.GetByNbr

            Return FullTaxDao.GetByNbr(FullTaxNbr)
        End Function

        Public Function GetBySManualEntry(ByVal shiftNo As String) As Ie.IeComponent.BO.FullTaxReceiptCollection Implements IFullTaxService.GetByManualEntry

            Return FullTaxDao.GetByManualEntry(shiftNo)
        End Function

        Public Function GetByDate(ByVal dataDate As Date) As Ie.IeComponent.BO.FullTaxReceiptCollection Implements IFullTaxService.GetByDate
            Return FullTaxDao.GetByDate(dataDate)
        End Function

    End Class

End Namespace


