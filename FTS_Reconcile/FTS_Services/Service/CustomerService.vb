Imports Ie.IeComponent.BO
Imports Npgsql
Imports System.IO
Imports FTS_ServiceApp.App.Utility

Namespace App.Service

    Public Class CustomerService
        Inherits System.ComponentModel.Component
        Implements ICustomerService


        Public Sub Save(ByRef customers As Ie.IeComponent.BO.CustomerCollection, ByRef msgBody As String) Implements ICustomerService.Save
            Dim isLab As Boolean = DBUtil.GetIslab
            Dim cnMaster As NpgsqlConnection = DBUtil.GetDBConnectionMaster
            Dim cnFTS As NpgsqlConnection = DBUtil.GetDBConnection
            cnMaster.Open()
            cnFTS.Open()
            Dim txnMaster As NpgsqlTransaction = cnMaster.BeginTransaction(IsolationLevel.Serializable)
            Dim txnFTS As NpgsqlTransaction = cnFTS.BeginTransaction(IsolationLevel.Serializable)
            Dim log As String = ""

            Try
                Dim customer As Customer

                'Dim i As Integer = 1
                Dim newCount As Integer = 0
                Dim updateCount As Integer = 0
                Dim CountIsLabCreate As Integer = 0
                Dim CountIsLabUpdate As Integer = 0
                MqHandler.WriteLogFile("Total Customer : " + customers.Count.ToString)

                Dim iR As Integer = 0
                Dim tableName As String = "CUSTOMER"
                Dim fileName As String = msgBody
                Dim updateDttm As New Date

                For Each customer In customers

                    '   MqHandler.WriteLogFile("Save Customer : " + customer.Tin + " : " + customer.Name)
                    If Not CustomerDao.CheckExits(customer.OID, cnMaster, txnMaster) Then
                        '   MqHandler.WriteLogFile("Create New Customer.")
                        '  log = "NEW--> " + CStr(customer.OID) + " , " + customer.Name
                        CustomerDao.Create(False, customer, cnMaster, txnMaster)

                        newCount += 1
                    Else
                        '   MqHandler.WriteLogFile("Upate Customer :" + customer.Name.ToString)
                        ' log = "UPDATE--> " + CStr(customer.OID) + " , " + customer.Name
                        CustomerDao.Update(False, customer, cnMaster, txnMaster)

                        updateCount += 1
                    End If

                    If isLab Then
                        If Not CustomerDao.CheckExits(customer.OID, cnFTS, txnFTS) Then
                            '   MqHandler.WriteLogFile("Create New Customer.")
                            '  log = "NEW--> " + CStr(customer.OID) + " , " + customer.Name
                            CustomerDao.Create(isLab, customer, cnFTS, txnFTS)
                            CountIsLabCreate += 1
                        Else
                            ' log = "UPDATE--> " + CStr(customer.OID) + " , " + customer.Name
                            CustomerDao.Update(isLab, customer, cnFTS, txnFTS)
                            CountIsLabUpdate += 1
                        End If

                    End If
                    updateDttm = customer.UpdateDttm

                    '  MqHandler.WriteLogFile(log)
                Next


                MqHandler.WriteLogFile("Is lab : " + isLab.ToString)

                If isLab Then
                    If LastUpdateDbDao.CheckExits(tableName, cnMaster, txnMaster) Then
                        MqHandler.WriteLogFile("Is lab : Update")
                        MqHandler.WriteLogFile("Last Date : " + updateDttm.ToShortDateString)

                        LastUpdateDbDao.Update(tableName, fileName, updateDttm, cnMaster, txnMaster)
                    Else
                        MqHandler.WriteLogFile("Is lab : Create")
                        LastUpdateDbDao.Create(tableName, fileName, updateDttm, cnMaster, txnMaster)
                    End If

                    txnFTS.Commit()
                    MqHandler.WriteLogFile("Save FTS_DB Completed : New=" + CountIsLabCreate.ToString + " , Update=" + CountIsLabUpdate.ToString)

                End If

                txnMaster.Commit()
                MqHandler.WriteLogFile("Save FTS_MASTER_DB Completed : New=" + newCount.ToString + " , Update=" + updateCount.ToString)
            Catch ex As Exception
                log = "ERROR : " + ex.Message
                MqHandler.WriteLogFile(log)
                txnMaster.Rollback()
                txnFTS.Rollback()
                Throw ex
            Finally
                cnFTS.Close()
                cnMaster.Close()
            End Try
        End Sub

    End Class

End Namespace

