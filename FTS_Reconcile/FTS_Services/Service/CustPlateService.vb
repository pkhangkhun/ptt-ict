Imports Ie.IeComponent.BO
Imports Npgsql
Imports System.IO
Imports FTS_ServiceApp.App.Utility

Namespace App.Service

    Public Class CustPlateService
        Inherits System.ComponentModel.Component
        Implements ICustPlateService


        Public Sub Save(ByRef custPlates As Ie.IeComponent.BO.CustPlateCollection, ByRef msgBody As String) Implements ICustPlateService.Save
            Dim isLab As Boolean = DBUtil.GetIslab
            Dim cn As NpgsqlConnection = DBUtil.GetDBConnectionMaster
            Dim cnFts As NpgsqlConnection = DBUtil.GetDBConnection
            cn.Open()
            cnFts.Open()
            Dim txn As NpgsqlTransaction = cn.BeginTransaction(IsolationLevel.Serializable)
            Dim txnFTS As NpgsqlTransaction = cnFts.BeginTransaction(IsolationLevel.Serializable)
            Dim log As String = ""

            Try
                Dim custPlate As CustPlate
                'Dim i As Integer = 1
                Dim newCount As Integer = 0
                Dim updateCount As Integer = 0
                Dim ftsNewCount As Integer = 0
                Dim ftsUpdateCount As Integer = 0
                Dim iR As Integer = 0
                Dim tableName As String = "CUST_PLATE"
                Dim fileName As String = msgBody
                Dim updateDttm As New Date

                For Each custPlate In custPlates
                    If Not CustPlateDao.CheckExists(custPlate.PlateNumber, custPlate.ProvinceId, custPlate.CustId, cn, txn) Then
                        CustPlateDao.Create(custPlate, cn, txn)
                        'log = CStr(i) + "  NEW--> " + CStr(custPlate.OID) + " , " + custPlate.PlateNumber + " , " + custPlate.ProvinceId.ToString + " , " + custPlate.CustId.ToString
                        newCount += 1
                    Else
                        'log = CStr(i) + "  UPDATE--> " + CStr(custPlate.OID) + " , " + custPlate.PlateNumber + " , " + custPlate.ProvinceId.ToString + " , " + custPlate.CustId.ToString
                        CustPlateDao.Update(custPlate, cn, txn)
                        updateCount += 1
                    End If
                    updateDttm = custPlate.UpdateDttm



                    If isLab Then
                        If Not CustPlateDao.CheckExists(custPlate.PlateNumber, custPlate.ProvinceId, custPlate.CustId, cn, txn) Then
                            CustPlateDao.Create(custPlate, cnFts, txnFTS)
                            'log = CStr(i) + "  NEW--> " + CStr(custPlate.OID) + " , " + custPlate.PlateNumber + " , " + custPlate.ProvinceId.ToString + " , " + custPlate.CustId.ToString
                            ftsNewCount += 1
                        Else
                            'log = CStr(i) + "  UPDATE--> " + CStr(custPlate.OID) + " , " + custPlate.PlateNumber + " , " + custPlate.ProvinceId.ToString + " , " + custPlate.CustId.ToString
                            CustPlateDao.Update(custPlate, cnFts, txnFTS)
                            ftsUpdateCount += 1
                        End If
                    End If
                Next



                If isLab Then
                    If LastUpdateDbDao.CheckExits(tableName, cn, txn) Then
                        LastUpdateDbDao.Update(tableName, fileName, updateDttm, cn, txn)
                    Else
                        LastUpdateDbDao.Create(tableName, fileName, updateDttm, cn, txn)
                    End If
                    txnFTS.Commit()
                    MqHandler.WriteLogFile("Save FTS_DB Completed : New=" + ftsNewCount.ToString + " , Update=" + ftsUpdateCount.ToString)

                End If


                txn.Commit()

                MqHandler.WriteLogFile("Save FTS_MASTER_DB Completed : New=" + newCount.ToString + " , Update=" + updateCount.ToString)

            Catch ex As Exception
                log = "ERROR : " + ex.Message
                MqHandler.WriteLogFile(log)
                txn.Rollback()
                txnFTS.Rollback()
                Throw ex
            Finally
                cn.Close()
                cnFts.Close()
            End Try
        End Sub
    End Class

End Namespace


