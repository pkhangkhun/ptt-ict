Imports Ie.IeComponent.ServiceInterface
Imports Ie.IeComponent.BO
Imports System.Data.SqlClient
Imports System.IO
Imports FTS_Services.App.Dao

Namespace App.Service

    Public Class QueryService
        Inherits System.ComponentModel.Component
        Implements IQueryService

        Public Function sqlInquery(ByVal sql As String) As System.Data.DataSet Implements IQueryService.sqlInquery
            Return QueryDao.sqlInquiry(sql)
        End Function

        Public Sub sqlUpdate(ByVal sql As String) Implements IQueryService.sqlUpdate
            Try
                QueryDao.sqlUpdate(sql)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
    End Class

End Namespace
