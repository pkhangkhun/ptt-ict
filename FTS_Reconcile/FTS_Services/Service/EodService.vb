

Imports Ie.IeComponent.BO
Imports Npgsql
Imports System.IO
Imports FTS_Services.App.Dao

Namespace App.Service

    Public Class EodService
        Inherits System.ComponentModel.Component
        Implements IEodService


        Public Function GetByShiftNo(ByVal shiftNo As String) As Ie.IeComponent.BO.WShift Implements IEodService.GetByShiftNo
            Return WShiftDao.GetByShiftNo(shiftNo)
        End Function

    End Class

End Namespace

