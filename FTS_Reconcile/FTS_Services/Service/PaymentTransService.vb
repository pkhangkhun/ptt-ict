Imports Ie.IeComponent.BO
Imports Npgsql
Imports System.IO
Imports FTS_Services.App.Dao

Namespace App.Service

    Public Class PaymentTransService
        Inherits System.ComponentModel.Component
        Implements IPaymentTransService

        Public Function GetByFullTaxNbr(ByVal fullTaxNbr As String) As Ie.IeComponent.BO.PaymentTransCollection Implements IPaymentTransService.GetByFullTaxNbr
            Return PaymentTransDao.GetByFullTaxNbr(fullTaxNbr)
        End Function

    End Class

End Namespace



