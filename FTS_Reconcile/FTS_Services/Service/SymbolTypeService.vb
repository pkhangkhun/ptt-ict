Imports Ie.IeComponent.BO
Imports FTS_HQ_MsmqServ.HQComponent.BO
Imports Npgsql
Imports FTS_HQ_MsmqServ.App.Utility
Imports FTS_HQ_MsmqServ.App.Dao
Imports System.IO 

Namespace App.Service

    Public Class SymbolTypeService
        Inherits System.ComponentModel.Component
        Implements FTS_HQ_MsmqServ.ISymbolTypeService


        Public Sub Save(ByRef symbolTypes As SymbolTypeCollection, ByRef msgBody As String) Implements ISymbolTypeService.Save

            Dim cn As NpgsqlConnection = DBUtil.GetDBConnectionMaster
            cn.Open()
            Dim txn As NpgsqlTransaction = cn.BeginTransaction(IsolationLevel.Serializable)
            Dim log As String = ""

            Try
                Dim symbolType As SymbolType
                'Dim i As Integer = 1
                Dim newCount As Integer = 0
                Dim updateCount As Integer = 0

                MqHandler.WriteLogFile("Total Symbol Type : " + symbolTypes.Count.ToString)

                Dim iR As Integer = 0
                Dim tableName As String = "SYMBOL_TYPE"
                Dim fileName As String = msgBody
                Dim updateDttm As New Date

                For Each symbolType In symbolTypes

                    MqHandler.WriteLogFile("Save Symbol Type : " + symbolType.SymbolName + "ObjectId: " + symbolType.OID.ToString)

                    Dim tmpSymbolType As SymbolType = SymbolTypeDao.RetrieveByOid(symbolType.OID, cn, txn)

                    If tmpSymbolType.OID > 0 Then
                        SymbolTypeDao.Update(symbolType, cn, txn)
                        updateCount += 1
                    Else
                        SymbolTypeDao.Create(symbolType, cn, txn)
                        newCount += 1
                    End If

                    updateDttm = symbolType.UpdateDttm


                    MqHandler.WriteLogFile(log)
                Next

                Dim isLab As Boolean = DBUtil.GetIslab

                If isLab Then
                    If LastUpdateDbDao.CheckExits(tableName, cn, txn) Then
                        LastUpdateDbDao.Update(tableName, fileName, updateDttm, cn, txn)
                    Else
                        LastUpdateDbDao.Create(tableName, fileName, updateDttm, cn, txn)
                    End If
                End If

                txn.Commit()
                MqHandler.WriteLogFile("Save Symbol Type Completed : New =" + newCount.ToString + " , Update=" + updateCount.ToString)
            Catch ex As Exception
                log = "ERROR : " + ex.Message
                MqHandler.WriteLogFile(log)
                txn.Rollback()
                Throw ex
            Finally
                cn.Close()
            End Try
        End Sub
    End Class

End Namespace

