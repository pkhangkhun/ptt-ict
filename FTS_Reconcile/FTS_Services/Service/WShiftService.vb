Imports Ie.IeComponent.BO
Imports Npgsql
Imports System.IO
Imports FTS_Services.App.Dao

Namespace App.Service

    Public Class WShiftService
        Inherits System.ComponentModel.Component
        Implements IWShiftService

        Public Function GetByShiftDate(ByVal fromdate As String, ByVal toDate As String) As Ie.IeComponent.BO.WShiftCollection Implements IWShiftService.GetByShiftDate
            Return WShiftDao.GetByShiftDate(fromdate, toDate)
        End Function
    End Class

End Namespace