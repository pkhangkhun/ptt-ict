﻿Imports Ie.IeComponent.BO

Namespace Domain.BO.UI

    Public Class FullTaxReceiptUI

        Public Property fullTax As FullTaxReceipt

        Public ReadOnly Property CnRef() As String
            Get
                Return fullTax.CnRef
            End Get
        End Property

        Public ReadOnly Property CompanyAddress() As String
            Get
                Return fullTax.CompanyAddress
            End Get
        End Property

        Public ReadOnly Property CompanyBranchName() As String
            Get
                Return fullTax.CompanyBranchName
            End Get
        End Property

        Public ReadOnly Property CompanyBranchNo() As String
            Get
                Return fullTax.CompanyBranchNo
            End Get
        End Property

        Public ReadOnly Property CompanyBranchNo_5() As String
            Get
                Return fullTax.CompanyBranchNo_5
            End Get
        End Property

        Public ReadOnly Property CompanyName() As String
            Get
                Return fullTax.CompanyName
            End Get
        End Property

        Public ReadOnly Property CompanyTaxId() As String
            Get
                Return fullTax.CompanyTaxId
            End Get
        End Property

        Public ReadOnly Property CreateUser() As String
            Get
                Return fullTax.CreateUser
            End Get
        End Property

        Public ReadOnly Property CustPlate() As CustPlate
            Get
                Return fullTax.CustPlate
            End Get
        End Property

        Public ReadOnly Property CustType() As String
            Get
                Return fullTax.CustType
            End Get
        End Property

        Public ReadOnly Property DiscountAmt() As Decimal
            Get
                Return fullTax.DiscountAmt
            End Get
        End Property

        Public ReadOnly Property DistrictCode() As String
            Get
                Return fullTax.DistrictCode
            End Get
        End Property

        Public ReadOnly Property EmployeeName() As String
            Get
                Return fullTax.EmployeeName
            End Get
        End Property

        Public ReadOnly Property FullTaxNbrRef() As String
            Get
                Return fullTax.FullTaxNbrRef
            End Get
        End Property

        Public ReadOnly Property HashTag() As String
            Get
                Return fullTax.HashTag
            End Get
        End Property

        Public ReadOnly Property HostId() As String
            Get
                Return fullTax.HostId
            End Get
        End Property

        Public ReadOnly Property IsConverted() As Boolean
            Get
                Return fullTax.IsConverted
            End Get
        End Property

        Public ReadOnly Property IsFleetCard() As Boolean
            Get
                Return fullTax.IsFleetCard
            End Get
        End Property

        Public ReadOnly Property IsManualEntry() As Boolean
            Get
                Return fullTax.IsManualEntry
            End Get
        End Property

        Public ReadOnly Property IsManualFill() As Boolean
            Get
                Return fullTax.IsManualFill
            End Get
        End Property

        Public ReadOnly Property IsScaned() As Boolean
            Get
                Return fullTax.IsScaned
            End Get
        End Property

        Public ReadOnly Property IssueDate() As Date
            Get
                Return fullTax.IssueDate
            End Get
        End Property

        Public ReadOnly Property Lbalance() As Decimal
            Get
                Return fullTax.Lbalance
            End Get
        End Property

        Public ReadOnly Property Lcardno() As String
            Get
                Return fullTax.Lcardno
            End Get
        End Property

        Public ReadOnly Property Lcustomer() As String
            Get
                Return fullTax.Lcustomer
            End Get
        End Property

        Public ReadOnly Property LogoImg() As Byte()
            Get
                Return fullTax.LogoImg
            End Get
        End Property

        Public ReadOnly Property Lpointtoday() As Decimal
            Get
                Return fullTax.Lpointtoday
            End Get
        End Property

        Public ReadOnly Property Lrepoint() As Decimal
            Get
                Return fullTax.Lrepoint
            End Get
        End Property

        Public ReadOnly Property Ltotalpoint() As Decimal
            Get
                Return fullTax.Ltotalpoint
            End Get
        End Property

        Public ReadOnly Property Ltrnpoint() As Decimal
            Get
                Return fullTax.Ltrnpoint
            End Get
        End Property

        Public ReadOnly Property NetAmt() As Decimal
            Get
                Return fullTax.NetAmt
            End Get
        End Property

        Public ReadOnly Property New_FullTaxNbr() As String
            Get
                Return fullTax.New_FullTaxNbr
            End Get
        End Property

        Public ReadOnly Property PlateNumber() As String
            Get
                Return fullTax.PlateNumber
            End Get
        End Property

        Public ReadOnly Property PlateProvinceId() As Integer
            Get
                Return fullTax.PlateProvinceId
            End Get
        End Property

        Public ReadOnly Property PosNbr() As Integer
            Get
                Return fullTax.PosNbr
            End Get
        End Property

        Public ReadOnly Property PrintNo() As Integer
            Get
                Return fullTax.PrintNo
            End Get
        End Property

        Public ReadOnly Property ProdCode() As String
            Get
                Return fullTax.ProdCode
            End Get
        End Property

        Public ReadOnly Property ProdName() As String
            Get
                Return fullTax.ProdName
            End Get
        End Property

        Public ReadOnly Property PumpNbr() As Integer
            Get
                Return fullTax.PumpNbr
            End Get
        End Property

        Public ReadOnly Property Qty() As Decimal
            Get
                Return fullTax.Qty
            End Get
        End Property

        Public ReadOnly Property RdId() As String
            Get
                Return fullTax.RdId
            End Get
        End Property

        Public ReadOnly Property ReceiptDate() As Date
            Get
                Return fullTax.ReceiptDate
            End Get
        End Property

        Public ReadOnly Property ReceiptNbr() As String
            Get
                Return fullTax.ReceiptNbr
            End Get
        End Property

        Public ReadOnly Property ReceiptType() As String
            Get
                Return fullTax.ReceiptType
            End Get
        End Property

        Public ReadOnly Property RefNbr() As String
            Get
                Return fullTax.RefNbr
            End Get
        End Property

        Public ReadOnly Property RegionCode() As String
            Get
                Return fullTax.RegionCode
            End Get
        End Property

        Public ReadOnly Property ReplaceDate() As Date
            Get
                Return fullTax.ReplaceDate
            End Get
        End Property

        Public ReadOnly Property ReplaceNo() As Integer
            Get
                Return fullTax.ReplaceNo
            End Get
        End Property

        Public ReadOnly Property ReplaceRef() As String
            Get
                Return fullTax.ReplaceRef
            End Get
        End Property

        Public ReadOnly Property SaleTypeDesc() As String
            Get
                Return fullTax.SaleTypeDesc
            End Get
        End Property

        Public ReadOnly Property ShiftId() As Long
            Get
                Return fullTax.ShiftId
            End Get
        End Property

        Public ReadOnly Property SiteId() As Long
            Get
                Return fullTax.SiteId
            End Get
        End Property

        Public ReadOnly Property SiteNo() As String
            Get
                Return fullTax.SiteNo
            End Get
        End Property

        Public ReadOnly Property SiteTypeId() As Integer
            Get
                Return fullTax.SiteTypeId
            End Get
        End Property

        Public ReadOnly Property StationId() As Long
            Get
                Return fullTax.StationId
            End Get
        End Property

        Public ReadOnly Property Status() As String
            Get
                Return fullTax.Status
            End Get
        End Property

        Public ReadOnly Property T_PlateprovinceId() As Long
            Get
                Return fullTax.T_PlateprovinceId
            End Get
        End Property

        Public ReadOnly Property T_PlateProvinceName() As String
            Get
                Return fullTax.T_PlateProvinceName
            End Get
        End Property

        Public ReadOnly Property T_PromotionDesc() As String
            Get
                Return fullTax.T_PromotionDesc
            End Get
        End Property

        Public ReadOnly Property Tmp_VoidDttm() As Date
            Get
                Return fullTax.Tmp_VoidDttm
            End Get
        End Property

        Public ReadOnly Property Tmp_VoidReason() As String
            Get
                Return fullTax.Tmp_VoidReason
            End Get
        End Property

        Public ReadOnly Property TotalAfterDiscountAmt() As Decimal
            Get
                Return fullTax.TotalAfterDiscountAmt
            End Get
        End Property

        Public ReadOnly Property TotalAmt() As Decimal
            Get
                Return fullTax.TotalAmt
            End Get
        End Property

        Public ReadOnly Property TranNbr() As Integer
            Get
                Return fullTax.TranNbr
            End Get
        End Property

        Public ReadOnly Property UnitName() As String
            Get
                Return fullTax.UnitName
            End Get
        End Property

        Public ReadOnly Property UnitPrice() As Decimal
            Get
                Return fullTax.UnitPrice
            End Get
        End Property

        Public ReadOnly Property VatAmt() As Decimal
            Get
                Return fullTax.VatAmt
            End Get
        End Property

        Public ReadOnly Property VoidDttm() As Date
            Get
                Return fullTax.VoidDttm
            End Get
        End Property

        Public ReadOnly Property VoidReason() As String
            Get
                Return fullTax.VoidReason
            End Get
        End Property

        Public ReadOnly Property VoidUserId() As String
            Get
                Return fullTax.VoidUserId
            End Get
        End Property

        Public ReadOnly Property Address() As String
            Get
                Return fullTax.Address
            End Get
        End Property

        Public ReadOnly Property CardImg() As CardImg
            Get
                Return fullTax.CardImg
            End Get
        End Property

        Public ReadOnly Property CustBranchName() As String
            Get
                Return fullTax.CustBranchName
            End Get
        End Property

        Public ReadOnly Property CustBranchNo() As Integer
            Get
                Return fullTax.CustBranchNo
            End Get
        End Property

        Public ReadOnly Property CustId() As Long
            Get
                Return fullTax.CustId
            End Get
        End Property

        Public ReadOnly Property CustName() As String
            Get
                Return fullTax.CustName
            End Get
        End Property

        Public ReadOnly Property CustTaxId() As String
            Get
                Return fullTax.CustTaxId
            End Get
        End Property

        Public ReadOnly Property Province() As Province
            Get
                Return fullTax.Province
            End Get
        End Property

        Public ReadOnly Property ProvinceId() As Long
            Get
                Return fullTax.ProvinceId
            End Get
        End Property

        Public ReadOnly Property T_LogoFile() As String
            Get
                Return fullTax.T_LogoFile
            End Get
        End Property


        Public Sub New(pFullTax As FullTaxReceipt)
            Me.fullTax = pFullTax
        End Sub

    End Class

End Namespace
