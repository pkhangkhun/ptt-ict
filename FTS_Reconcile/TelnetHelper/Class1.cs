﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace TelnetHelper
{
    public class Class1
    {
        public bool check()
        {
            string hostname = "10.92.5.66";
            int portno = 1801;
            IPAddress ipa = (IPAddress)Dns.GetHostAddresses(hostname)[0];

            try
            {
                System.Net.Sockets.Socket sock = new System.Net.Sockets.Socket(System.Net.Sockets.AddressFamily.InterNetwork, System.Net.Sockets.SocketType.Stream, System.Net.Sockets.ProtocolType.Tcp);
                sock.Connect(ipa, portno);
                if (sock.Connected == true)  // Port is in use and connection is successful
                    Console.WriteLine("Port is open");
                sock.Close();

            }
            catch (System.Net.Sockets.SocketException ex)
            {
                Console.WriteLine(ex.ErrorCode);
                if (ex.ErrorCode == 10061)  // Port is unused and could not establish connection 
                    Console.WriteLine("Port is Open!");
                else
                {
                    Console.WriteLine(ex.Message);
                }
            }

            return true;
        }

        public static void Main(string[] args)
        {
            try {
                Class1 test = new Class1();
                Console.WriteLine(test.check());
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
            }

            Console.ReadKey();
        }
    }
}
