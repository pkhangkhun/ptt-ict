﻿Imports FTS_Reconcile
Imports FTS_Reconcile.Domain.BO.UI
Imports FTS_Reconcile.fts.constant.FTSConstant
Imports FTS_Reconcile.fts.service
Imports FTS_Reconcile.fts.util
Imports FTS_Reconcile.Helper.Services

Module Module1
    Private ReadOnly log As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

    Public Const sectionKey As String = "genericAppSettings"

    Sub Main()
        log.Info("Start auto reconcile v." + (GetType(Module1).Assembly.GetName().Version).ToString)

        'get config data
        Dim batchType As String
        Dim fromDate As DateTime
        Dim toDate As DateTime = DateTime.Today
        Dim diffDate As Integer
        Dim siteNo As String = ""

        batchType = ConfigUtil.getString(sectionKey, "type")

        Try
            siteNo = ConfigUtil.getString(sectionKey, "siteNo")

            If batchType.ToLower = "date" Then
                fromDate = DateTime.Parse(ConfigUtil.getString(sectionKey, "fromDate"))
                toDate = DateTime.Parse(ConfigUtil.getString(sectionKey, "toDate")).AddDays(1)
                diffDate = toDate.Subtract(fromDate).Days
            Else
                diffDate = Integer.Parse(ConfigUtil.getString(sectionKey, "numBackDate"), 5)
                fromDate = toDate.AddDays(-1 * diffDate)
            End If

        Catch ex As Exception
            toDate = DateTime.Today
        End Try

        log.Info("Recheck EOD " + fromDate.ToString("yyyyMMdd") + " - " + toDate.ToString("yyyyMMdd"))

        'process resend
        processResend(diffDate, toDate, siteNo)
    End Sub

    Private Sub processResend(numBackDate As Integer, maxDate As DateTime, pSiteNo As String)
        Dim str As String
        Dim statusStr As String
        Dim dataDate As Date
        Dim siteDt As DataTable
        Dim isAlive As Boolean = True
        Dim svcRs As ReconcileResponse
        Dim totalCheck As Integer
        Dim countRec As Integer

        If Not IsNothing(pSiteNo) AndAlso pSiteNo.Length > 1 Then
            siteDt = SiteService.gets(pSiteNo)
        Else
            siteDt = SiteService.gets(status:="A")
        End If

        totalCheck = siteDt.Rows.Count * numBackDate
        countRec = 1
        For Each dr As DataRow In siteDt.Rows
            Dim siteNo As String = dr("site_code").ToString

            isAlive = True 'site alive status

            For i As Integer = 0 To numBackDate - 1
                System.Threading.Thread.Sleep(1)
                dataDate = maxDate.AddDays(i - numBackDate)

                str = String.Format("[{0}/{1}] Site:{2} Date:{3}", countRec, totalCheck, siteNo, dataDate.ToString("yyyy-MM-dd"))
                Console.Write(str)
                Try
                    svcRs = ReconcileService.autoResendMissingData(siteNo, dataDate, isAlive)
                    statusStr = CType(svcRs.processStatus, statusLoad).ToString()

                    If isAlive Then
                        Console.WriteLine(" status:" + statusStr)

                        str = String.Format("Resend siteNo:{0}, date:{1}, status:{2} - {3}", siteNo, dataDate.ToString("yyyy-MM-dd"), statusStr, svcRs.processMsg)
                        If svcRs.processStatus = statusLoad.FoundDiff Then
                            log.Warn(str)
                            'Generate MQ for missing transaction
                            ReconcileService.syncDataToHq(svcRs.siteMissingList)
                        Else
                            log.Info(str)
                        End If
                    Else
                        log.Warn(str + " status:" + statusStr + " --ignore")
                        Console.WriteLine(" status:" + statusStr + " --ignore")
                    End If

                Catch pgEx As Npgsql.NpgsqlException
                    Console.WriteLine(" Error : " + pgEx.Message.ToString)
                    str = String.Format("Error resend data SiteNo:{0}, Date:{1}", siteNo, dataDate.ToString("yyyy-MM-dd"))
                    log.Error(str, pgEx)

                    'set query data from site to fail
                    isAlive = False
                Catch ex As Exception
                    Console.WriteLine(" Error : " + ex.Message.ToString)
                    str = String.Format("Error resend data SiteNo:{0}, Date:{1}", siteNo, dataDate.ToString("yyyy-MM-dd"))
                    log.Error(str, ex)
                End Try

                countRec += 1
            Next

        Next
    End Sub

End Module
