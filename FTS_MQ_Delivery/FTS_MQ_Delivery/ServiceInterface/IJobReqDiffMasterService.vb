Imports FTS_MQ_Delivery.HQComponent.BO

Public Interface IJobReqDiffMasterService

    Function Retrieve(ByVal jobOid As Long) As JobReqDiffMaster

    Function RetrieveAll() As JobReqDiffMasterCollection

    Sub AddNew(ByVal jobReqDiffMaster As JobReqDiffMaster)

    Sub Update(ByVal jobReqDiffMaster As JobReqDiffMaster)

    Sub Delete(ByVal jobReqDiffMaster As JobReqDiffMaster)

    Function GetJob() As JobReqDiffMasterCollection

    Sub CloseJob(ByVal jobOid As Long)

End Interface

