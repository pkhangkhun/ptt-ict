Imports Ie.IeComponent.BO
Public Interface IMqStationService

    Function GetActive() As StationCollection

    Function GetBySiteNo(ByVal siteNo As String) As StationCollection

End Interface
