Public Interface IMqSystemInfoService

    Function GetNextMQID(ByVal keyWord As String, ByVal r1 As Integer, ByVal r2 As Integer) As String

    Function TestDBConnection() As Boolean

End Interface
