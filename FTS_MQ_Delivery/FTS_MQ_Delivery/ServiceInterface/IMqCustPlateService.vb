Imports Ie.IeComponent.BO

Public Interface IMqCustPlateService

    Function GetPublish() As CustPlateCollection

    Function GetDiff(ByVal maxCarPlateOid As Long) As CustPlateCollection

    Sub UpdatePublishDate(ByVal custPlates As CustPlateCollection)

End Interface



