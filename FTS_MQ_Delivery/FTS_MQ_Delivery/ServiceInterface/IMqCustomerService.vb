Imports Ie.IeComponent.BO


Public Interface IMqCustomerService

    Function GetPublish() As CustomerCollection

    Function GetDiff(ByVal maxCustOid As Long) As CustomerCollection

    Sub UpdatePublishDate(ByVal customers As CustomerCollection)

End Interface


