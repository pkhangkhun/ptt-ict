<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.btnLoad = New System.Windows.Forms.Button
        Me.btnSend = New System.Windows.Forms.Button
        Me.lblTotalTran = New System.Windows.Forms.Label
        Me.dgvTrans = New System.Windows.Forms.DataGridView
        Me.queue_id = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.site_no = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.station_no = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.file_no = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.total_record = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.tran_date = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.q_ip = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.status = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtTotalSendTran = New System.Windows.Forms.TextBox
        Me.dgvStation = New System.Windows.Forms.DataGridView
        Me.station = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.SiteName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.send_date = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.RetryNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ping_result = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.rbComplete = New System.Windows.Forms.RadioButton
        Me.rbInActive = New System.Windows.Forms.RadioButton
        Me.rbWait = New System.Windows.Forms.RadioButton
        Me.rbAll = New System.Windows.Forms.RadioButton
        Me.btnResent = New System.Windows.Forms.Button
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.lblStatus = New System.Windows.Forms.Label
        Me.lblTotal = New System.Windows.Forms.Label
        Me.txtStation = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.TimerRefresh = New System.Windows.Forms.Timer(Me.components)
        Me.txtComputerName = New System.Windows.Forms.TextBox
        Me.lblComputerName = New System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.rbBzNgv = New System.Windows.Forms.RadioButton
        Me.rbBzOil = New System.Windows.Forms.RadioButton
        Me.rbBzAll = New System.Windows.Forms.RadioButton
        Me.lblPendingQueue = New System.Windows.Forms.Label
        Me.cbPauseScreen = New System.Windows.Forms.CheckBox
        CType(Me.dgvTrans, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvStation, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnLoad
        '
        Me.btnLoad.Location = New System.Drawing.Point(615, 166)
        Me.btnLoad.Name = "btnLoad"
        Me.btnLoad.Size = New System.Drawing.Size(94, 34)
        Me.btnLoad.TabIndex = 0
        Me.btnLoad.Text = "Load "
        Me.btnLoad.UseVisualStyleBackColor = True
        Me.btnLoad.Visible = False
        '
        'btnSend
        '
        Me.btnSend.Enabled = False
        Me.btnSend.Location = New System.Drawing.Point(494, 174)
        Me.btnSend.Name = "btnSend"
        Me.btnSend.Size = New System.Drawing.Size(94, 34)
        Me.btnSend.TabIndex = 2
        Me.btnSend.Text = "Send"
        Me.btnSend.UseVisualStyleBackColor = True
        Me.btnSend.Visible = False
        '
        'lblTotalTran
        '
        Me.lblTotalTran.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTotalTran.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblTotalTran.Location = New System.Drawing.Point(12, 391)
        Me.lblTotalTran.Name = "lblTotalTran"
        Me.lblTotalTran.Size = New System.Drawing.Size(960, 23)
        Me.lblTotalTran.TabIndex = 3
        Me.lblTotalTran.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dgvTrans
        '
        Me.dgvTrans.AllowUserToAddRows = False
        Me.dgvTrans.AllowUserToDeleteRows = False
        Me.dgvTrans.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvTrans.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTrans.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.queue_id, Me.site_no, Me.station_no, Me.file_no, Me.total_record, Me.tran_date, Me.q_ip, Me.status})
        Me.dgvTrans.Location = New System.Drawing.Point(927, 209)
        Me.dgvTrans.Name = "dgvTrans"
        Me.dgvTrans.ReadOnly = True
        Me.dgvTrans.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvTrans.Size = New System.Drawing.Size(34, 91)
        Me.dgvTrans.TabIndex = 1
        Me.dgvTrans.Visible = False
        '
        'queue_id
        '
        Me.queue_id.HeaderText = "Queue No"
        Me.queue_id.Name = "queue_id"
        Me.queue_id.ReadOnly = True
        Me.queue_id.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'site_no
        '
        Me.site_no.HeaderText = "Station"
        Me.site_no.Name = "site_no"
        Me.site_no.ReadOnly = True
        Me.site_no.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'station_no
        '
        Me.station_no.HeaderText = "Pos No"
        Me.station_no.Name = "station_no"
        Me.station_no.ReadOnly = True
        Me.station_no.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'file_no
        '
        Me.file_no.HeaderText = "File No"
        Me.file_no.Name = "file_no"
        Me.file_no.ReadOnly = True
        Me.file_no.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'total_record
        '
        Me.total_record.HeaderText = "Total Record"
        Me.total_record.Name = "total_record"
        Me.total_record.ReadOnly = True
        Me.total_record.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'tran_date
        '
        Me.tran_date.HeaderText = "Tran Date"
        Me.tran_date.Name = "tran_date"
        Me.tran_date.ReadOnly = True
        Me.tran_date.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'q_ip
        '
        Me.q_ip.HeaderText = "IP-Address"
        Me.q_ip.Name = "q_ip"
        Me.q_ip.ReadOnly = True
        Me.q_ip.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'status
        '
        Me.status.HeaderText = "Status"
        Me.status.Name = "status"
        Me.status.ReadOnly = True
        Me.status.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'txtTotalSendTran
        '
        Me.txtTotalSendTran.Location = New System.Drawing.Point(803, 174)
        Me.txtTotalSendTran.Name = "txtTotalSendTran"
        Me.txtTotalSendTran.Size = New System.Drawing.Size(100, 20)
        Me.txtTotalSendTran.TabIndex = 4
        '
        'dgvStation
        '
        Me.dgvStation.AllowUserToAddRows = False
        Me.dgvStation.AllowUserToDeleteRows = False
        Me.dgvStation.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvStation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvStation.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.station, Me.SiteName, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn4, Me.send_date, Me.DataGridViewTextBoxColumn6, Me.RetryNo, Me.DataGridViewTextBoxColumn8, Me.ping_result})
        Me.dgvStation.Location = New System.Drawing.Point(12, 99)
        Me.dgvStation.Name = "dgvStation"
        Me.dgvStation.ReadOnly = True
        Me.dgvStation.RowHeadersVisible = False
        Me.dgvStation.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvStation.Size = New System.Drawing.Size(960, 254)
        Me.dgvStation.TabIndex = 5
        '
        'station
        '
        Me.station.HeaderText = "Station"
        Me.station.Name = "station"
        Me.station.ReadOnly = True
        Me.station.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.station.Width = 60
        '
        'SiteName
        '
        Me.SiteName.HeaderText = "Station Name"
        Me.SiteName.Name = "SiteName"
        Me.SiteName.ReadOnly = True
        Me.SiteName.Width = 160
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "IP-Address"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn7.Width = 80
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Current Queue"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Width = 140
        '
        'send_date
        '
        Me.send_date.HeaderText = "Sent Date"
        Me.send_date.Name = "send_date"
        Me.send_date.ReadOnly = True
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Last Attempt"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'RetryNo
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.RetryNo.DefaultCellStyle = DataGridViewCellStyle1
        Me.RetryNo.HeaderText = "Times Attempt"
        Me.RetryNo.Name = "RetryNo"
        Me.RetryNo.ReadOnly = True
        '
        'DataGridViewTextBoxColumn8
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.DataGridViewTextBoxColumn8.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridViewTextBoxColumn8.HeaderText = "MQ State"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'ping_result
        '
        Me.ping_result.HeaderText = "Ping Result"
        Me.ping_result.Name = "ping_result"
        Me.ping_result.ReadOnly = True
        Me.ping_result.Width = 130
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.rbComplete)
        Me.GroupBox1.Controls.Add(Me.rbInActive)
        Me.GroupBox1.Controls.Add(Me.rbWait)
        Me.GroupBox1.Controls.Add(Me.rbAll)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(542, 43)
        Me.GroupBox1.TabIndex = 6
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Status"
        '
        'rbComplete
        '
        Me.rbComplete.AutoSize = True
        Me.rbComplete.Location = New System.Drawing.Point(402, 16)
        Me.rbComplete.Name = "rbComplete"
        Me.rbComplete.Size = New System.Drawing.Size(96, 17)
        Me.rbComplete.TabIndex = 3
        Me.rbComplete.Text = "Waiting Queue"
        Me.rbComplete.UseVisualStyleBackColor = True
        '
        'rbInActive
        '
        Me.rbInActive.AutoSize = True
        Me.rbInActive.Location = New System.Drawing.Point(274, 18)
        Me.rbInActive.Name = "rbInActive"
        Me.rbInActive.Size = New System.Drawing.Size(63, 17)
        Me.rbInActive.TabIndex = 2
        Me.rbInActive.Text = "Inactive"
        Me.rbInActive.UseVisualStyleBackColor = True
        '
        'rbWait
        '
        Me.rbWait.AutoSize = True
        Me.rbWait.Checked = True
        Me.rbWait.Location = New System.Drawing.Point(131, 16)
        Me.rbWait.Name = "rbWait"
        Me.rbWait.Size = New System.Drawing.Size(80, 17)
        Me.rbWait.TabIndex = 1
        Me.rbWait.TabStop = True
        Me.rbWait.Text = "On Delivery"
        Me.rbWait.UseVisualStyleBackColor = True
        '
        'rbAll
        '
        Me.rbAll.AutoSize = True
        Me.rbAll.Location = New System.Drawing.Point(39, 16)
        Me.rbAll.Name = "rbAll"
        Me.rbAll.Size = New System.Drawing.Size(36, 17)
        Me.rbAll.TabIndex = 0
        Me.rbAll.Text = "All"
        Me.rbAll.UseVisualStyleBackColor = True
        '
        'btnResent
        '
        Me.btnResent.Enabled = False
        Me.btnResent.Location = New System.Drawing.Point(615, 122)
        Me.btnResent.Name = "btnResent"
        Me.btnResent.Size = New System.Drawing.Size(75, 28)
        Me.btnResent.TabIndex = 11
        Me.btnResent.Text = "Resent"
        Me.btnResent.UseVisualStyleBackColor = True
        Me.btnResent.Visible = False
        '
        'Timer1
        '
        Me.Timer1.Interval = 5000
        '
        'lblStatus
        '
        Me.lblStatus.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(615, 40)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(357, 23)
        Me.lblStatus.TabIndex = 7
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTotal
        '
        Me.lblTotal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblTotal.Location = New System.Drawing.Point(12, 358)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(211, 23)
        Me.lblTotal.TabIndex = 8
        Me.lblTotal.Text = "Total :"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtStation
        '
        Me.txtStation.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtStation.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtStation.Location = New System.Drawing.Point(615, 11)
        Me.txtStation.Name = "txtStation"
        Me.txtStation.Size = New System.Drawing.Size(73, 26)
        Me.txtStation.TabIndex = 9
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(560, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 13)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Station :"
        '
        'TimerRefresh
        '
        Me.TimerRefresh.Interval = 10000
        '
        'txtComputerName
        '
        Me.txtComputerName.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtComputerName.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtComputerName.Location = New System.Drawing.Point(793, 11)
        Me.txtComputerName.Name = "txtComputerName"
        Me.txtComputerName.Size = New System.Drawing.Size(179, 26)
        Me.txtComputerName.TabIndex = 12
        '
        'lblComputerName
        '
        Me.lblComputerName.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblComputerName.AutoSize = True
        Me.lblComputerName.Location = New System.Drawing.Point(698, 17)
        Me.lblComputerName.Name = "lblComputerName"
        Me.lblComputerName.Size = New System.Drawing.Size(89, 13)
        Me.lblComputerName.TabIndex = 13
        Me.lblComputerName.Text = "Computer Name :"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(712, 122)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 28)
        Me.Button1.TabIndex = 14
        Me.Button1.Text = "Send Q"
        Me.Button1.UseVisualStyleBackColor = True
        Me.Button1.Visible = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.rbBzNgv)
        Me.GroupBox2.Controls.Add(Me.rbBzOil)
        Me.GroupBox2.Controls.Add(Me.rbBzAll)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 50)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(542, 43)
        Me.GroupBox2.TabIndex = 15
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Business Type"
        '
        'rbBzNgv
        '
        Me.rbBzNgv.AutoSize = True
        Me.rbBzNgv.Location = New System.Drawing.Point(274, 19)
        Me.rbBzNgv.Name = "rbBzNgv"
        Me.rbBzNgv.Size = New System.Drawing.Size(48, 17)
        Me.rbBzNgv.TabIndex = 3
        Me.rbBzNgv.Text = "NGV"
        Me.rbBzNgv.UseVisualStyleBackColor = True
        '
        'rbBzOil
        '
        Me.rbBzOil.AutoSize = True
        Me.rbBzOil.Location = New System.Drawing.Point(131, 19)
        Me.rbBzOil.Name = "rbBzOil"
        Me.rbBzOil.Size = New System.Drawing.Size(42, 17)
        Me.rbBzOil.TabIndex = 2
        Me.rbBzOil.Text = "OIL"
        Me.rbBzOil.UseVisualStyleBackColor = True
        '
        'rbBzAll
        '
        Me.rbBzAll.AutoSize = True
        Me.rbBzAll.Checked = True
        Me.rbBzAll.Location = New System.Drawing.Point(39, 19)
        Me.rbBzAll.Name = "rbBzAll"
        Me.rbBzAll.Size = New System.Drawing.Size(36, 17)
        Me.rbBzAll.TabIndex = 1
        Me.rbBzAll.TabStop = True
        Me.rbBzAll.Text = "All"
        Me.rbBzAll.UseVisualStyleBackColor = True
        '
        'lblPendingQueue
        '
        Me.lblPendingQueue.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblPendingQueue.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblPendingQueue.Location = New System.Drawing.Point(229, 358)
        Me.lblPendingQueue.Name = "lblPendingQueue"
        Me.lblPendingQueue.Size = New System.Drawing.Size(211, 23)
        Me.lblPendingQueue.TabIndex = 16
        Me.lblPendingQueue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cbPauseScreen
        '
        Me.cbPauseScreen.AutoSize = True
        Me.cbPauseScreen.Location = New System.Drawing.Point(615, 71)
        Me.cbPauseScreen.Name = "cbPauseScreen"
        Me.cbPauseScreen.Size = New System.Drawing.Size(93, 17)
        Me.cbPauseScreen.TabIndex = 17
        Me.cbPauseScreen.Text = "Pause Screen"
        Me.cbPauseScreen.UseVisualStyleBackColor = True
        '
        'fmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(984, 429)
        Me.Controls.Add(Me.cbPauseScreen)
        Me.Controls.Add(Me.lblPendingQueue)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.lblComputerName)
        Me.Controls.Add(Me.txtComputerName)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtStation)
        Me.Controls.Add(Me.lblTotal)
        Me.Controls.Add(Me.lblStatus)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.dgvStation)
        Me.Controls.Add(Me.txtTotalSendTran)
        Me.Controls.Add(Me.lblTotalTran)
        Me.Controls.Add(Me.btnSend)
        Me.Controls.Add(Me.dgvTrans)
        Me.Controls.Add(Me.btnLoad)
        Me.Controls.Add(Me.btnResent)
        Me.Controls.Add(Me.Button1)
        Me.Name = "fmMain"
        Me.Text = "FTS - Queue Delivery  version 2.2.1"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.dgvTrans, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvStation, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnLoad As System.Windows.Forms.Button
    Friend WithEvents btnSend As System.Windows.Forms.Button
    Friend WithEvents lblTotalTran As System.Windows.Forms.Label
    Friend WithEvents dgvTrans As System.Windows.Forms.DataGridView
    Friend WithEvents queue_id As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents site_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents station_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents file_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents total_record As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tran_date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents q_ip As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents status As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtTotalSendTran As System.Windows.Forms.TextBox
    Friend WithEvents dgvStation As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents rbInActive As System.Windows.Forms.RadioButton
    Friend WithEvents rbWait As System.Windows.Forms.RadioButton
    Friend WithEvents rbAll As System.Windows.Forms.RadioButton
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents rbComplete As System.Windows.Forms.RadioButton
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents txtStation As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnResent As System.Windows.Forms.Button
    Friend WithEvents TimerRefresh As System.Windows.Forms.Timer
    Friend WithEvents txtComputerName As System.Windows.Forms.TextBox
    Friend WithEvents lblComputerName As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents rbBzNgv As System.Windows.Forms.RadioButton
    Friend WithEvents rbBzOil As System.Windows.Forms.RadioButton
    Friend WithEvents rbBzAll As System.Windows.Forms.RadioButton
    Friend WithEvents lblPendingQueue As System.Windows.Forms.Label
    Friend WithEvents cbPauseScreen As System.Windows.Forms.CheckBox
    Friend WithEvents station As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SiteName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents send_date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RetryNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ping_result As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
