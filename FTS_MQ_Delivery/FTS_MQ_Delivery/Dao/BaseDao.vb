Imports System.Data.SqlClient
Imports FTS_MQ_Delivery.App.Utility

Namespace App.Dao

    Public Class BaseDao


        Public Shared ReadOnly Property GetDBConnection() As SqlConnection
            Get
                Return DBUtil.GetDBConnection()
            End Get
        End Property

        Public Shared Function ExistsField(ByVal fieldName As String, ByVal dr As SqlDataReader) As Boolean
            Dim i As Integer
            For i = 0 To dr.FieldCount - 1
                If fieldName.ToUpper = dr.GetName(i).ToUpper Then
                    Return True
                End If
            Next
            Return False
        End Function

    End Class

End Namespace

