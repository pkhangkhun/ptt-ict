Imports FTS_MQ_Delivery.HQComponent.BO
Imports Ie.UI.Utility
Imports System.Data.SqlClient

Namespace App.Dao

    Public Class HQMsmqLogDao
        Inherits BaseDao

        Private Const CreateSproc = "spPOS_MsmqLogAddNew"
        Private Const UpdateSproc = "spPOS_MsmqLogUpdate"
        Private Const DeleteSproc = "spPOS_MsmqLogDelete"
        Private Const RetrieveSproc = "spPOS_MsmqLogRetrieve"
        Private Const RetrieveAllSproc = "spPOS_MsmqLogRetrieveAll"

        Private Class ColumnName
            Public Const QueueId = "QUEUE_ID"
            Public Const SiteNo = "SITE_NO"
            Public Const BranchNo = "BRANCH_NO"
            Public Const StationNo = "STATION_NO"
            Public Const QueueDestination = "QUEUE_DESTINATION"
            Public Const QueueType = "QUEUE_TYPE"
            Public Const TotalFile = "TOTAL_FILE"
            Public Const TotalRecord = "TOTAL_RECORD"
            Public Const SendDate = "SEND_DATE"
            Public Const CompleteDate = "COMPLETE_DATE"
            Public Const ReSendFlg = "RE_SEND_FLG"
        End Class

        Private Class ParamName
            Public Const QueueId = "@QueueId"
            Public Const SiteNo = "@SiteNo"
            Public Const BranchNo = "@BranchNo"
            Public Const StationNo = "@StationNo"
            Public Const QueueDestination = "@QueueDestination"
            Public Const QueueType = "@QueueType"
            Public Const TotalFile = "@TotalFile"
            Public Const TotalRecord = "@TotalRecord"
            Public Const SendDate = "@SendDate"
            Public Const CompleteDate = "@CompleteDate"
            Public Const ReSendFlg = "@ReSendFlg"
        End Class

        Private Shared Sub PopulateData(ByVal msmqLog As HQMsmqLog, ByVal dr As SqlDataReader)

            If Not (dr.Item(ColumnName.QueueId) Is System.DBNull.Value) Then msmqLog.QueueId = CStr(dr.Item(ColumnName.QueueId))
            If Not (dr.Item(ColumnName.SiteNo) Is System.DBNull.Value) Then msmqLog.SiteNo = CStr(dr.Item(ColumnName.SiteNo))
            If Not (dr.Item(ColumnName.BranchNo) Is System.DBNull.Value) Then msmqLog.BranchNo = CStr(dr.Item(ColumnName.BranchNo))
            If Not (dr.Item(ColumnName.StationNo) Is System.DBNull.Value) Then msmqLog.StationNo = CInt(dr.Item(ColumnName.StationNo))
            If Not (dr.Item(ColumnName.QueueDestination) Is System.DBNull.Value) Then msmqLog.QueueDestination = CStr(dr.Item(ColumnName.QueueDestination))
            If Not (dr.Item(ColumnName.QueueType) Is System.DBNull.Value) Then msmqLog.QueueType = CInt(dr.Item(ColumnName.QueueType))
            If Not (dr.Item(ColumnName.TotalFile) Is System.DBNull.Value) Then msmqLog.TotalFile = CInt(dr.Item(ColumnName.TotalFile))
            If Not (dr.Item(ColumnName.TotalRecord) Is System.DBNull.Value) Then msmqLog.TotalRecord = CInt(dr.Item(ColumnName.TotalRecord))
            If Not (dr.Item(ColumnName.SendDate) Is System.DBNull.Value) Then msmqLog.SendDate = CDate(dr.Item(ColumnName.SendDate))
            If Not (dr.Item(ColumnName.CompleteDate) Is System.DBNull.Value) Then msmqLog.CompleteDate = CDate(dr.Item(ColumnName.CompleteDate))
            If Not (dr.Item(ColumnName.ReSendFlg) Is System.DBNull.Value) Then msmqLog.ReSendFlg = CBool(dr.Item(ColumnName.ReSendFlg))

        End Sub

        Private Shared Sub PopulateCollection(ByVal msmqLogs As HQMsmqLogCollection, ByVal dr As SqlDataReader)
            Do While (dr.Read())
                Dim msmqLog As New HQMsmqLog
                PopulateData(msmqLog, dr)
                msmqLogs.Add(msmqLog)
            Loop
        End Sub

        Private Shared Sub MapCommandParameter(ByVal command As SqlCommand, ByVal msmqLog As HQMsmqLog)
            If IsDBNull(msmqLog.QueueId) Or IsNothing(msmqLog.QueueId) Then
                command.Parameters.AddWithValue(ParamName.QueueId, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.QueueId, msmqLog.QueueId)
            End If
            If IsDBNull(msmqLog.SiteNo) Or IsNothing(msmqLog.SiteNo) Then
                command.Parameters.AddWithValue(ParamName.SiteNo, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.SiteNo, msmqLog.SiteNo)
            End If
            If IsDBNull(msmqLog.BranchNo) Or IsNothing(msmqLog.BranchNo) Then
                command.Parameters.AddWithValue(ParamName.BranchNo, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.BranchNo, msmqLog.BranchNo)
            End If
            If IsDBNull(msmqLog.StationNo) Or IsNothing(msmqLog.StationNo) Then
                command.Parameters.AddWithValue(ParamName.StationNo, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.StationNo, msmqLog.StationNo)
            End If
            If IsDBNull(msmqLog.QueueDestination) Or IsNothing(msmqLog.QueueDestination) Then
                command.Parameters.AddWithValue(ParamName.QueueDestination, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.QueueDestination, msmqLog.QueueDestination)
            End If
            If IsDBNull(msmqLog.QueueType) Or IsNothing(msmqLog.QueueType) Then
                command.Parameters.AddWithValue(ParamName.QueueType, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.QueueType, msmqLog.QueueType)
            End If
            If IsDBNull(msmqLog.TotalFile) Or IsNothing(msmqLog.TotalFile) Then
                command.Parameters.AddWithValue(ParamName.TotalFile, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.TotalFile, msmqLog.TotalFile)
            End If
            If IsDBNull(msmqLog.TotalRecord) Or IsNothing(msmqLog.TotalRecord) Then
                command.Parameters.AddWithValue(ParamName.TotalRecord, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.TotalRecord, msmqLog.TotalRecord)
            End If
            If Not InputValidator.IsDate(msmqLog.SendDate) Then
                command.Parameters.AddWithValue(ParamName.SendDate, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.SendDate, msmqLog.SendDate)
            End If
            If Not InputValidator.IsDate(msmqLog.CompleteDate) Then
                command.Parameters.AddWithValue(ParamName.CompleteDate, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.CompleteDate, msmqLog.CompleteDate)
            End If
            If IsDBNull(msmqLog.ReSendFlg) Or IsNothing(msmqLog.ReSendFlg) Then
                command.Parameters.AddWithValue(ParamName.ReSendFlg, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.ReSendFlg, msmqLog.ReSendFlg)
            End If
        End Sub

#Region "Common Function Method"

        Public Shared Sub Create(ByVal msmqLog As HQMsmqLog, ByVal cn As SqlConnection, ByVal txn As SqlTransaction)
            Dim CreateSproc As String = My.Resources.rsMsmqLog.sqlCreate
            Dim command As New SqlCommand(CreateSproc, cn, txn)
            command.CommandType = CommandType.Text
            MapCommandParameter(command, msmqLog)
            command.ExecuteNonQuery()
        End Sub

        Public Shared Sub Update(ByVal msmqLog As HQMsmqLog, ByVal cn As SqlConnection, ByVal txn As SqlTransaction)
            Dim UpdateSproc As String = My.Resources.rsMsmqLog.sqlUpdate
            Dim command As New SqlCommand(UpdateSproc, cn, txn)
            command.CommandType = CommandType.Text
            MapCommandParameter(command, msmqLog)
            command.ExecuteNonQuery()
        End Sub

        Public Shared Sub Delete(ByVal msmqLog As HQMsmqLog, ByVal cn As SqlConnection, ByVal txn As SqlTransaction)
            Dim command As New SqlCommand(DeleteSproc, cn, txn)
            command.CommandType = CommandType.StoredProcedure
            command.ExecuteNonQuery()
        End Sub

        Public Shared Function Retrieve(ByVal id As ObjectID) As HQMsmqLog
            Dim msmqLog As New HQMsmqLog
            Dim cn As SqlConnection = GetDBConnection()
            cn.Open()

            Dim command As New SqlCommand(RetrieveSproc, cn)
            command.CommandType = CommandType.StoredProcedure
            Dim dr As SqlDataReader = command.ExecuteReader

            If dr.Read() Then PopulateData(msmqLog, dr)

            cn.Close()
            Return msmqLog
        End Function

        Public Shared Function RetrieveAll() As HQMsmqLogCollection
            Dim msmqLogs As New HQMsmqLogCollection
            Dim cn As SqlConnection = GetDBConnection()
            cn.Open()

            Dim command As New SqlCommand(RetrieveAllSproc, cn)
            command.CommandType = CommandType.StoredProcedure
            Dim dr As SqlDataReader = command.ExecuteReader

            PopulateCollection(msmqLogs, dr)

            cn.Close()
            Return msmqLogs
        End Function

#End Region

    End Class

End Namespace
