Imports Ie.IEComponent.BO
Imports Ie.UI.Utility
Imports System.Data.SqlClient

Namespace App.Dao

    Public Class CustomerDao
        Inherits BaseDao

        Private Class ColumnName
            Public Const ObjectId = "OBJECT_ID"
            Public Const Tin = "TIN"
            Public Const BranchNumber = "BRANCH_NUMBER"
            Public Const TitleName = "TITLE_NAME"
            Public Const Name = "NAME"
            Public Const Surname = "SURNAME"
            Public Const BuildingName = "BUILDING_NAME"
            Public Const RoomNumber = "ROOM_NUMBER"
            Public Const FloorNumber = "FLOOR_NUMBER"
            Public Const VillageName = "VILLAGE_NAME"
            Public Const HouseNumber = "HOUSE_NUMBER"
            Public Const MooNumber = "MOO_NUMBER"
            Public Const SoiName = "SOI_NAME"
            Public Const StreetName = "STREET_NAME"
            Public Const Thambol = "THAMBOL"
            Public Const Amphur = "AMPHUR"
            Public Const ProvinceId = "PROVINCE_ID"
            Public Const ZipCode = "ZIP_CODE"
            Public Const Status = "STATUS"
            Public Const IsExternalData = "IS_EXTERNAL_DATA"
            Public Const CustType = "CUST_TYPE"
            Public Const Remark = "REMARK"
            Public Const PhoneNo = "PHONE_NO"
            Public Const FaxNo = "FAX_NO"
            Public Const Version = "VERSION"
            Public Const CreateUser = "CREATE_USER"
            Public Const CreateDttm = "CREATE_DTTM"
            Public Const UpdateDttm = "UPDATE_DTTM"
            Public Const UpdateUser = "UPDATE_USER"
            Public Const TrDttm = "TR_DTTM"
        End Class

        Private Class ParamName
            Public Const ObjectId = "@ObjectId"
            Public Const Tin = "@Tin"
            Public Const BranchNumber = "@BranchNumber"
            Public Const TitleName = "@TitleName"
            Public Const Name = "@Name"
            Public Const Surname = "@Surname"
            Public Const BuildingName = "@BuildingName"
            Public Const RoomNumber = "@RoomNumber"
            Public Const FloorNumber = "@FloorNumber"
            Public Const VillageName = "@VillageName"
            Public Const HouseNumber = "@HouseNumber"
            Public Const MooNumber = "@MooNumber"
            Public Const SoiName = "@SoiName"
            Public Const StreetName = "@StreetName"
            Public Const Thambol = "@Thambol"
            Public Const Amphur = "@Amphur"
            Public Const ProvinceId = "@ProvinceId"
            Public Const ZipCode = "@ZipCode"
            Public Const Status = "@Status"
            Public Const IsExternalData = "@IsExternalData"
            Public Const CustType = "@CustType"
            Public Const Remark = "@Remark"
            Public Const PhoneNo = "@PhoneNo"
            Public Const FaxNo = "@FaxNo"
            Public Const Version = "@Version"
            Public Const CreateUser = "@CreateUser"
            Public Const CreateDttm = "@CreateDttm"
            Public Const UpdateDttm = "@UpdateDttm"
            Public Const UpdateUser = "@UpdateUser"
            Public Const TrDttm = "@TrDttm"
        End Class

        Private Shared Sub PopulateData(ByVal customer As Customer, ByVal dr As SqlDataReader)

            customer.ObjectID.ID = CType(dr.Item(ColumnName.ObjectID), Long)
            If Not (dr.Item(ColumnName.Tin) Is System.DBNull.Value) Then customer.Tin = CStr(dr.Item(ColumnName.Tin))
            If Not (dr.Item(ColumnName.BranchNumber) Is System.DBNull.Value) Then customer.BranchNumber = CInt(dr.Item(ColumnName.BranchNumber))
            If Not (dr.Item(ColumnName.TitleName) Is System.DBNull.Value) Then customer.TitleName = CStr(dr.Item(ColumnName.TitleName))
            If Not (dr.Item(ColumnName.Name) Is System.DBNull.Value) Then customer.Name = CStr(dr.Item(ColumnName.Name))
            If Not (dr.Item(ColumnName.Surname) Is System.DBNull.Value) Then customer.Surname = CStr(dr.Item(ColumnName.Surname))
            If Not (dr.Item(ColumnName.BuildingName) Is System.DBNull.Value) Then customer.BuildingName = CStr(dr.Item(ColumnName.BuildingName))
            If Not (dr.Item(ColumnName.RoomNumber) Is System.DBNull.Value) Then customer.RoomNumber = CStr(dr.Item(ColumnName.RoomNumber))
            If Not (dr.Item(ColumnName.FloorNumber) Is System.DBNull.Value) Then customer.FloorNumber = CStr(dr.Item(ColumnName.FloorNumber))
            If Not (dr.Item(ColumnName.VillageName) Is System.DBNull.Value) Then customer.VillageName = CStr(dr.Item(ColumnName.VillageName))
            If Not (dr.Item(ColumnName.HouseNumber) Is System.DBNull.Value) Then customer.HouseNumber = CStr(dr.Item(ColumnName.HouseNumber))
            If Not (dr.Item(ColumnName.MooNumber) Is System.DBNull.Value) Then customer.MooNumber = CStr(dr.Item(ColumnName.MooNumber))
            If Not (dr.Item(ColumnName.SoiName) Is System.DBNull.Value) Then customer.SoiName = CStr(dr.Item(ColumnName.SoiName))
            If Not (dr.Item(ColumnName.StreetName) Is System.DBNull.Value) Then customer.StreetName = CStr(dr.Item(ColumnName.StreetName))
            If Not (dr.Item(ColumnName.Thambol) Is System.DBNull.Value) Then customer.Thambol = CStr(dr.Item(ColumnName.Thambol))
            If Not (dr.Item(ColumnName.Amphur) Is System.DBNull.Value) Then customer.Amphur = CStr(dr.Item(ColumnName.Amphur))
            If Not (dr.Item(ColumnName.ProvinceId) Is System.DBNull.Value) Then customer.ProvinceId = CLng(dr.Item(ColumnName.ProvinceId))
            If Not (dr.Item(ColumnName.ZipCode) Is System.DBNull.Value) Then customer.ZipCode = CStr(dr.Item(ColumnName.ZipCode))
            If Not (dr.Item(ColumnName.Status) Is System.DBNull.Value) Then customer.Status = CStr(dr.Item(ColumnName.Status))
            If Not (dr.Item(ColumnName.IsExternalData) Is System.DBNull.Value) Then customer.IsExternalData = CBool(dr.Item(ColumnName.IsExternalData))
            If Not (dr.Item(ColumnName.CustType) Is System.DBNull.Value) Then customer.CustType = CStr(dr.Item(ColumnName.CustType))
            If Not (dr.Item(ColumnName.Remark) Is System.DBNull.Value) Then customer.Remark = CStr(dr.Item(ColumnName.Remark))
            If Not (dr.Item(ColumnName.PhoneNo) Is System.DBNull.Value) Then customer.PhoneNo = CStr(dr.Item(ColumnName.PhoneNo))
            If Not (dr.Item(ColumnName.FaxNo) Is System.DBNull.Value) Then customer.FaxNo = CStr(dr.Item(ColumnName.FaxNo))
            If Not (dr.Item(ColumnName.Version) Is System.DBNull.Value) Then customer.Version = CLng(dr.Item(ColumnName.Version))
            If Not (dr.Item(ColumnName.CreateUser) Is System.DBNull.Value) Then customer.CreateUser = CStr(dr.Item(ColumnName.CreateUser))
            If Not (dr.Item(ColumnName.CreateDttm) Is System.DBNull.Value) Then customer.CreateDttm = CDate(dr.Item(ColumnName.CreateDttm))
            If Not (dr.Item(ColumnName.UpdateDttm) Is System.DBNull.Value) Then customer.UpdateDttm = CDate(dr.Item(ColumnName.UpdateDttm))
            If Not (dr.Item(ColumnName.UpdateUser) Is System.DBNull.Value) Then customer.UpdateUser = CStr(dr.Item(ColumnName.UpdateUser))
            If Not (dr.Item(ColumnName.TrDttm) Is System.DBNull.Value) Then customer.TrDttm = CDate(dr.Item(ColumnName.TrDttm))

        End Sub

        Private Shared Sub PopulateCollection(ByVal customers As CustomerCollection, ByVal dr As SqlDataReader)
            Do While (dr.Read())
                Dim customer As New Customer
                PopulateData(customer, dr)
                customers.Add(customer)
            Loop
        End Sub

        Private Shared Sub MapCommandParameter(ByVal command As SqlCommand, ByVal customer As Customer)
            command.Parameters.AddWithValue(ParamName.ObjectID, customer.ObjectID.ID)
            If IsDbNull(customer.Tin) Or IsNothing(customer.Tin) Then
                command.Parameters.AddWithValue(ParamName.Tin, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.Tin, customer.Tin)
            End If
            If IsDbNull(customer.BranchNumber) Or IsNothing(customer.BranchNumber) Then
                command.Parameters.AddWithValue(ParamName.BranchNumber, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.BranchNumber, customer.BranchNumber)
            End If
            If IsDbNull(customer.TitleName) Or IsNothing(customer.TitleName) Then
                command.Parameters.AddWithValue(ParamName.TitleName, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.TitleName, customer.TitleName)
            End If
            If IsDbNull(customer.Name) Or IsNothing(customer.Name) Then
                command.Parameters.AddWithValue(ParamName.Name, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.Name, customer.Name)
            End If
            If IsDbNull(customer.Surname) Or IsNothing(customer.Surname) Then
                command.Parameters.AddWithValue(ParamName.Surname, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.Surname, customer.Surname)
            End If
            If IsDbNull(customer.BuildingName) Or IsNothing(customer.BuildingName) Then
                command.Parameters.AddWithValue(ParamName.BuildingName, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.BuildingName, customer.BuildingName)
            End If
            If IsDbNull(customer.RoomNumber) Or IsNothing(customer.RoomNumber) Then
                command.Parameters.AddWithValue(ParamName.RoomNumber, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.RoomNumber, customer.RoomNumber)
            End If
            If IsDbNull(customer.FloorNumber) Or IsNothing(customer.FloorNumber) Then
                command.Parameters.AddWithValue(ParamName.FloorNumber, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.FloorNumber, customer.FloorNumber)
            End If
            If IsDbNull(customer.VillageName) Or IsNothing(customer.VillageName) Then
                command.Parameters.AddWithValue(ParamName.VillageName, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.VillageName, customer.VillageName)
            End If
            If IsDbNull(customer.HouseNumber) Or IsNothing(customer.HouseNumber) Then
                command.Parameters.AddWithValue(ParamName.HouseNumber, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.HouseNumber, customer.HouseNumber)
            End If
            If IsDbNull(customer.MooNumber) Or IsNothing(customer.MooNumber) Then
                command.Parameters.AddWithValue(ParamName.MooNumber, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.MooNumber, customer.MooNumber)
            End If
            If IsDbNull(customer.SoiName) Or IsNothing(customer.SoiName) Then
                command.Parameters.AddWithValue(ParamName.SoiName, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.SoiName, customer.SoiName)
            End If
            If IsDbNull(customer.StreetName) Or IsNothing(customer.StreetName) Then
                command.Parameters.AddWithValue(ParamName.StreetName, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.StreetName, customer.StreetName)
            End If
            If IsDbNull(customer.Thambol) Or IsNothing(customer.Thambol) Then
                command.Parameters.AddWithValue(ParamName.Thambol, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.Thambol, customer.Thambol)
            End If
            If IsDbNull(customer.Amphur) Or IsNothing(customer.Amphur) Then
                command.Parameters.AddWithValue(ParamName.Amphur, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.Amphur, customer.Amphur)
            End If
            If IsDbNull(customer.ProvinceId) Or IsNothing(customer.ProvinceId) Then
                command.Parameters.AddWithValue(ParamName.ProvinceId, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.ProvinceId, customer.ProvinceId)
            End If
            If IsDbNull(customer.ZipCode) Or IsNothing(customer.ZipCode) Then
                command.Parameters.AddWithValue(ParamName.ZipCode, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.ZipCode, customer.ZipCode)
            End If
            If IsDbNull(customer.Status) Or IsNothing(customer.Status) Then
                command.Parameters.AddWithValue(ParamName.Status, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.Status, customer.Status)
            End If
            If IsDbNull(customer.IsExternalData) Or IsNothing(customer.IsExternalData) Then
                command.Parameters.AddWithValue(ParamName.IsExternalData, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.IsExternalData, customer.IsExternalData)
            End If
            If IsDbNull(customer.CustType) Or IsNothing(customer.CustType) Then
                command.Parameters.AddWithValue(ParamName.CustType, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.CustType, customer.CustType)
            End If
            If IsDbNull(customer.Remark) Or IsNothing(customer.Remark) Then
                command.Parameters.AddWithValue(ParamName.Remark, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.Remark, customer.Remark)
            End If
            If IsDbNull(customer.PhoneNo) Or IsNothing(customer.PhoneNo) Then
                command.Parameters.AddWithValue(ParamName.PhoneNo, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.PhoneNo, customer.PhoneNo)
            End If
            If IsDbNull(customer.FaxNo) Or IsNothing(customer.FaxNo) Then
                command.Parameters.AddWithValue(ParamName.FaxNo, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.FaxNo, customer.FaxNo)
            End If
            If IsDbNull(customer.Version) Or IsNothing(customer.Version) Then
                command.Parameters.AddWithValue(ParamName.Version, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.Version, customer.Version)
            End If
            If IsDbNull(customer.CreateUser) Or IsNothing(customer.CreateUser) Then
                command.Parameters.AddWithValue(ParamName.CreateUser, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.CreateUser, customer.CreateUser)
            End If
            If Not InputValidator.IsDate(customer.CreateDttm) Then
                command.Parameters.AddWithValue(ParamName.CreateDttm, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.CreateDttm, customer.CreateDttm)
            End If
            If Not InputValidator.IsDate(customer.UpdateDttm) Then
                command.Parameters.AddWithValue(ParamName.UpdateDttm, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.UpdateDttm, customer.UpdateDttm)
            End If
            If IsDbNull(customer.UpdateUser) Or IsNothing(customer.UpdateUser) Then
                command.Parameters.AddWithValue(ParamName.UpdateUser, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.UpdateUser, customer.UpdateUser)
            End If
            If Not InputValidator.IsDate(customer.TrDttm) Then
                command.Parameters.AddWithValue(ParamName.TrDttm, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.TrDttm, customer.TrDttm)
            End If
        End Sub

#Region "Common Function Method"

        Public Shared Sub Create(ByVal customer As Customer, ByVal cn As SqlConnection, ByVal txn As SqlTransaction)
            Dim CreateSproc As String = ""
            Dim command As New SqlCommand(CreateSproc, cn, txn)
            command.CommandType = CommandType.StoredProcedure
            MapCommandParameter(command, customer)
            command.ExecuteNonQuery()
        End Sub

        Public Shared Sub Update(ByVal customer As Customer, ByVal cn As SqlConnection, ByVal txn As SqlTransaction)
            Dim UpdateSproc As String = ""
            Dim command As New SqlCommand(UpdateSproc, cn, txn)
            command.CommandType = CommandType.Text
            MapCommandParameter(command, customer)
            command.ExecuteNonQuery()
        End Sub

        Public Shared Function GetPublish() As CustomerCollection
            Dim customers As New CustomerCollection
            Dim cn As SqlConnection = GetDBConnection()
            cn.Open()

            Dim RetrieveAllSproc As String = "select * from customer where isnull(tr_dttm,getdate()-1)<isnull(update_dttm,getdate())"

            Dim command As New SqlCommand(RetrieveAllSproc, cn)
            command.CommandType = CommandType.Text
            Dim dr As SqlDataReader = command.ExecuteReader

            PopulateCollection(customers, dr)

            cn.Close()
            Return customers
        End Function

        Public Shared Function GetDiff(ByVal maxCustOid As Long) As CustomerCollection
            Dim customers As New CustomerCollection
            Dim cn As SqlConnection = GetDBConnection()
            cn.Open()

            Dim RetrieveAllSproc As String = "select * from customer where object_id>" + maxCustOid.ToString

            Dim command As New SqlCommand(RetrieveAllSproc, cn)
            command.CommandType = CommandType.Text
            Dim dr As SqlDataReader = command.ExecuteReader

            PopulateCollection(customers, dr)

            cn.Close()
            Return customers
        End Function

        Public Shared Sub UpdatePublishDate(ByVal customer As Customer, ByVal cn As SqlConnection, ByVal txn As SqlTransaction)
            Dim UpdateSproc As String = "update customer set tr_dttm=getdate() where object_id=" + customer.ObjectID.ID.ToString
            Dim command As New SqlCommand(UpdateSproc, cn, txn)
            command.CommandType = CommandType.Text
            MapCommandParameter(command, customer)
            command.ExecuteNonQuery()
        End Sub


#End Region

    End Class

End Namespace

