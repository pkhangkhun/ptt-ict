Imports FTS_MQ_Delivery.HQComponent.BO
Imports Ie.UI.Utility
Imports System.Data.SqlClient

Namespace App.Dao

    Public Class HQMsmqTranLogDao
        Inherits BaseDao

        Private Const CreateSproc = "spPOS_MsmqTranLogAddNew"
        Private Const UpdateSproc = "spPOS_MsmqTranLogUpdate"
        Private Const DeleteSproc = "spPOS_MsmqTranLogDelete"
        Private Const RetrieveSproc = "spPOS_MsmqTranLogRetrieve"
        Private Const RetrieveAllSproc = "spPOS_MsmqTranLogRetrieveAll"

        Private Class ColumnName
            Public Const QueueId = "QUEUE_ID"
            Public Const SiteNo = "SITE_NO"
            Public Const BranchNo = "BRANCH_NO"
            Public Const StationNo = "STATION_NO"
            Public Const FileNo = "FILE_NO"
            Public Const TotalRecord = "TOTAL_RECORD"
            Public Const ReceiveDate = "RECEIVE_DATE"
            Public Const CommitDate = "COMMIT_DATE"
            'Ref
            Public Const Q_IP = "Q_IP"
            Public Const Q_Type = "Q_TYPE"
        End Class

        Private Class ParamName
            Public Const QueueId = "@QueueId"
            Public Const SiteNo = "@SiteNo"
            Public Const BranchNo = "@BranchNo"
            Public Const StationNo = "@StationNo"
            Public Const FileNo = "@FileNo"
            Public Const TotalRecord = "@TotalRecord"
            Public Const ReceiveDate = "@ReceiveDate"
            Public Const CommitDate = "@CommitDate"
        End Class

        Private Shared Sub PopulateData(ByVal msmqTranLog As HQMsmqTranLog, ByVal dr As SqlDataReader)

            If Not (dr.Item(ColumnName.QueueId) Is System.DBNull.Value) Then msmqTranLog.QueueId = CStr(dr.Item(ColumnName.QueueId))
            If Not (dr.Item(ColumnName.SiteNo) Is System.DBNull.Value) Then msmqTranLog.SiteNo = CStr(dr.Item(ColumnName.SiteNo))
            If Not (dr.Item(ColumnName.BranchNo) Is System.DBNull.Value) Then msmqTranLog.BranchNo = CStr(dr.Item(ColumnName.BranchNo))
            If Not (dr.Item(ColumnName.StationNo) Is System.DBNull.Value) Then msmqTranLog.StationNo = CInt(dr.Item(ColumnName.StationNo))
            If Not (dr.Item(ColumnName.FileNo) Is System.DBNull.Value) Then msmqTranLog.FileNo = CStr(dr.Item(ColumnName.FileNo))
            If Not (dr.Item(ColumnName.TotalRecord) Is System.DBNull.Value) Then msmqTranLog.TotalRecord = CInt(dr.Item(ColumnName.TotalRecord))
            If Not (dr.Item(ColumnName.ReceiveDate) Is System.DBNull.Value) Then msmqTranLog.ReceiveDate = CDate(dr.Item(ColumnName.ReceiveDate))
            If Not (dr.Item(ColumnName.CommitDate) Is System.DBNull.Value) Then msmqTranLog.CommitDate = CDate(dr.Item(ColumnName.CommitDate))

            If ExistsField(ColumnName.Q_IP, dr) Then
                If Not (dr.Item(ColumnName.Q_IP) Is System.DBNull.Value) Then msmqTranLog.Q_IP = CStr(dr.Item(ColumnName.Q_IP))
            End If
            If ExistsField(ColumnName.Q_Type, dr) Then
                If Not (dr.Item(ColumnName.Q_Type) Is System.DBNull.Value) Then msmqTranLog.Q_Type = CStr(dr.Item(ColumnName.Q_Type))
            End If

        End Sub

        Private Shared Sub PopulateCollection(ByVal msmqTranLogs As HQMsmqTranLogCollection, ByVal dr As SqlDataReader)
            Do While (dr.Read())
                Dim msmqTranLog As New HQMsmqTranLog
                PopulateData(msmqTranLog, dr)
                msmqTranLogs.Add(msmqTranLog)
            Loop
        End Sub

        Private Shared Sub MapCommandParameter(ByVal command As SqlCommand, ByVal msmqTranLog As HQMsmqTranLog)
            If IsDBNull(msmqTranLog.QueueId) Or IsNothing(msmqTranLog.QueueId) Then
                command.Parameters.AddWithValue(ParamName.QueueId, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.QueueId, msmqTranLog.QueueId)
            End If
            If IsDBNull(msmqTranLog.SiteNo) Or IsNothing(msmqTranLog.SiteNo) Then
                command.Parameters.AddWithValue(ParamName.SiteNo, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.SiteNo, msmqTranLog.SiteNo)
            End If
            If IsDBNull(msmqTranLog.BranchNo) Or IsNothing(msmqTranLog.BranchNo) Then
                command.Parameters.AddWithValue(ParamName.BranchNo, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.BranchNo, msmqTranLog.BranchNo)
            End If
            If IsDBNull(msmqTranLog.StationNo) Or IsNothing(msmqTranLog.StationNo) Then
                command.Parameters.AddWithValue(ParamName.StationNo, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.StationNo, msmqTranLog.StationNo)
            End If
            If IsDBNull(msmqTranLog.FileNo) Or IsNothing(msmqTranLog.FileNo) Then
                command.Parameters.AddWithValue(ParamName.FileNo, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.FileNo, msmqTranLog.FileNo)
            End If
            If IsDBNull(msmqTranLog.TotalRecord) Or IsNothing(msmqTranLog.TotalRecord) Then
                command.Parameters.AddWithValue(ParamName.TotalRecord, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.TotalRecord, msmqTranLog.TotalRecord)
            End If
            If Not InputValidator.IsDate(msmqTranLog.ReceiveDate) Then
                command.Parameters.AddWithValue(ParamName.ReceiveDate, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.ReceiveDate, msmqTranLog.ReceiveDate)
            End If
            If Not InputValidator.IsDate(msmqTranLog.CommitDate) Then
                command.Parameters.AddWithValue(ParamName.CommitDate, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.CommitDate, msmqTranLog.CommitDate)
            End If
        End Sub

#Region "Common Function Method"

        Public Shared Sub Create(ByVal msmqTranLog As HQMsmqTranLog, ByVal cn As SqlConnection, ByVal txn As SqlTransaction)
            Dim CreateSproc As String = My.Resources.rsMsmqTranLog.sqlCreate
            Dim command As New SqlCommand(CreateSproc, cn, txn)
            command.CommandType = CommandType.Text
            MapCommandParameter(command, msmqTranLog)
            command.ExecuteNonQuery()
        End Sub

        Public Shared Sub Update(ByVal msmqTranLog As HQMsmqTranLog, ByVal cn As SqlConnection, ByVal txn As SqlTransaction)
            Dim command As New SqlCommand(UpdateSproc, cn, txn)
            command.CommandType = CommandType.StoredProcedure
            MapCommandParameter(command, msmqTranLog)
            command.ExecuteNonQuery()
        End Sub

        Public Shared Sub Delete(ByVal msmqTranLog As HQMsmqTranLog, ByVal cn As SqlConnection, ByVal txn As SqlTransaction)
            Dim command As New SqlCommand(DeleteSproc, cn, txn)
            command.CommandType = CommandType.StoredProcedure
            command.ExecuteNonQuery()
        End Sub

        Public Shared Function Retrieve(ByVal id As ObjectID) As HQMsmqTranLog
            Dim msmqTranLog As New HQMsmqTranLog
            Dim cn As SqlConnection = GetDBConnection()
            cn.Open()

            Dim command As New SqlCommand(RetrieveSproc, cn)
            command.CommandType = CommandType.StoredProcedure
            Dim dr As SqlDataReader = command.ExecuteReader

            If dr.Read() Then PopulateData(msmqTranLog, dr)

            cn.Close()
            Return msmqTranLog
        End Function

        Public Shared Function RetrieveAll() As HQMsmqTranLogCollection
            Dim msmqTranLogs As New HQMsmqTranLogCollection
            Dim cn As SqlConnection = GetDBConnection()
            cn.Open()

            Dim command As New SqlCommand(RetrieveAllSproc, cn)
            command.CommandType = CommandType.StoredProcedure
            Dim dr As SqlDataReader = command.ExecuteReader

            PopulateCollection(msmqTranLogs, dr)

            cn.Close()
            Return msmqTranLogs
        End Function

        Public Shared Function GetPublishQueueNotResponding() As HQMsmqTranLogCollection
            Dim msmqTranLogs As New HQMsmqTranLogCollection
            Dim cn As SqlConnection = GetDBConnection()
            cn.Open()

            Dim command As New SqlCommand("spFTS_GetPublishQueueNotResponding", cn)
            command.CommandType = CommandType.StoredProcedure
            Dim dr As SqlDataReader = command.ExecuteReader

            PopulateCollection(msmqTranLogs, dr)

            cn.Close()
            Return msmqTranLogs
        End Function

#End Region

    End Class

End Namespace
