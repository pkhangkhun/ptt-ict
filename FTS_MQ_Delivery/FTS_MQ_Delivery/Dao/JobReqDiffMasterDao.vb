Imports FTS_MQ_Delivery.HQComponent.BO
Imports Ie.UI.Utility
Imports System.Data.SqlClient

Namespace App.Dao

    Public Class JobReqDiffMasterDao
        Inherits BaseDao

        Private Const CreateSproc = "spPOS_JobReqDiffMasterAddNew"
        Private Const UpdateSproc = "spPOS_JobReqDiffMasterUpdate"
        Private Const DeleteSproc = "spPOS_JobReqDiffMasterDelete"
        Private Const RetrieveSproc = "spPOS_JobReqDiffMasterRetrieve"
        Private Const RetrieveAllSproc = "spPOS_JobReqDiffMasterRetrieveAll"

        Private Class ColumnName
            Public Const JobOid = "JOB_OID"
            Public Const JobDate = "JOB_DATE"
            Public Const SiteNo = "SITE_NO"
            Public Const MaxCustOid = "MAX_CUST_OID"
            Public Const MaxCarplateOid = "MAX_CARPLATE_OID"
            Public Const RunDate = "RUN_DATE"
            Public Const JobStatus = "JOB_STATUS"
        End Class

        Private Class ParamName
            Public Const JobOid = "@JobOid"
            Public Const JobDate = "@JobDate"
            Public Const SiteNo = "@SiteNo"
            Public Const MaxCustOid = "@MaxCustOid"
            Public Const MaxCarplateOid = "@MaxCarplateOid"
            Public Const RunDate = "@RunDate"
            Public Const JobStatus = "@JobStatus"
        End Class

        Private Shared Sub PopulateData(ByVal jobReqDiffMaster As JobReqDiffMaster, ByVal dr As SqlDataReader)

            If Not (dr.Item(ColumnName.JobOid) Is System.DBNull.Value) Then jobReqDiffMaster.JobOid = CLng(dr.Item(ColumnName.JobOid))
            If Not (dr.Item(ColumnName.JobDate) Is System.DBNull.Value) Then jobReqDiffMaster.JobDate = CDate(dr.Item(ColumnName.JobDate))
            If Not (dr.Item(ColumnName.SiteNo) Is System.DBNull.Value) Then jobReqDiffMaster.SiteNo = CStr(dr.Item(ColumnName.SiteNo))
            If Not (dr.Item(ColumnName.MaxCustOid) Is System.DBNull.Value) Then jobReqDiffMaster.MaxCustOid = CLng(dr.Item(ColumnName.MaxCustOid))
            If Not (dr.Item(ColumnName.MaxCarplateOid) Is System.DBNull.Value) Then jobReqDiffMaster.MaxCarplateOid = CLng(dr.Item(ColumnName.MaxCarplateOid))
            If Not (dr.Item(ColumnName.RunDate) Is System.DBNull.Value) Then jobReqDiffMaster.RunDate = CDate(dr.Item(ColumnName.RunDate))
            If Not (dr.Item(ColumnName.JobStatus) Is System.DBNull.Value) Then jobReqDiffMaster.JobStatus = CStr(dr.Item(ColumnName.JobStatus))

        End Sub

        Private Shared Sub PopulateCollection(ByVal jobReqDiffMasters As JobReqDiffMasterCollection, ByVal dr As SqlDataReader)
            Do While (dr.Read())
                Dim jobReqDiffMaster As New JobReqDiffMaster
                PopulateData(jobReqDiffMaster, dr)
                jobReqDiffMasters.Add(jobReqDiffMaster)
            Loop
        End Sub

        Private Shared Sub MapCommandParameter(ByVal command As SqlCommand, ByVal jobReqDiffMaster As JobReqDiffMaster)
            If IsDBNull(jobReqDiffMaster.JobOid) Or IsNothing(jobReqDiffMaster.JobOid) Then
                command.Parameters.AddWithValue(ParamName.JobOid, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.JobOid, jobReqDiffMaster.JobOid)
            End If
            If Not InputValidator.IsDate(jobReqDiffMaster.JobDate) Then
                command.Parameters.AddWithValue(ParamName.JobDate, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.JobDate, jobReqDiffMaster.JobDate)
            End If
            If IsDBNull(jobReqDiffMaster.SiteNo) Or IsNothing(jobReqDiffMaster.SiteNo) Then
                command.Parameters.AddWithValue(ParamName.SiteNo, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.SiteNo, jobReqDiffMaster.SiteNo)
            End If
            If IsDBNull(jobReqDiffMaster.MaxCustOid) Or IsNothing(jobReqDiffMaster.MaxCustOid) Then
                command.Parameters.AddWithValue(ParamName.MaxCustOid, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.MaxCustOid, jobReqDiffMaster.MaxCustOid)
            End If
            If IsDBNull(jobReqDiffMaster.MaxCarplateOid) Or IsNothing(jobReqDiffMaster.MaxCarplateOid) Then
                command.Parameters.AddWithValue(ParamName.MaxCarplateOid, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.MaxCarplateOid, jobReqDiffMaster.MaxCarplateOid)
            End If
            If Not InputValidator.IsDate(jobReqDiffMaster.RunDate) Then
                command.Parameters.AddWithValue(ParamName.RunDate, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.RunDate, jobReqDiffMaster.RunDate)
            End If
            If IsDBNull(jobReqDiffMaster.JobStatus) Or IsNothing(jobReqDiffMaster.JobStatus) Then
                command.Parameters.AddWithValue(ParamName.JobStatus, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.JobStatus, jobReqDiffMaster.JobStatus)
            End If
        End Sub

#Region "Common Function Method"

        Public Shared Sub Create(ByVal jobReqDiffMaster As JobReqDiffMaster, ByVal cn As SqlConnection, ByVal txn As SqlTransaction)
            Dim command As New SqlCommand(CreateSproc, cn, txn)
            command.CommandType = CommandType.StoredProcedure
            MapCommandParameter(command, jobReqDiffMaster)
            command.ExecuteNonQuery()
        End Sub

        Public Shared Sub Update(ByVal jobReqDiffMaster As JobReqDiffMaster, ByVal cn As SqlConnection, ByVal txn As SqlTransaction)
            Dim command As New SqlCommand(UpdateSproc, cn, txn)
            command.CommandType = CommandType.StoredProcedure
            MapCommandParameter(command, jobReqDiffMaster)
            command.ExecuteNonQuery()
        End Sub

        Public Shared Sub Delete(ByVal jobReqDiffMaster As JobReqDiffMaster, ByVal cn As SqlConnection, ByVal txn As SqlTransaction)
            Dim command As New SqlCommand(DeleteSproc, cn, txn)
            command.CommandType = CommandType.StoredProcedure
            'command.Parameters.AddWithValue(ParamName.ObjectID, jobReqDiffMaster.ObjectID.ID)
            'command.Parameters.AddWithValue(ParamName.Version, jobReqDiffMaster.Version)
            command.ExecuteNonQuery()
        End Sub

        Public Shared Function Retrieve(ByVal jobOid As Long) As JobReqDiffMaster
            Dim jobReqDiffMaster As New JobReqDiffMaster
            Dim cn As SqlConnection = GetDBConnection()
            cn.Open()

            Dim command As New SqlCommand(RetrieveSproc, cn)
            command.CommandType = CommandType.StoredProcedure
            'command.Parameters.AddWithValue(ParamName., id.ID)
            Dim dr As SqlDataReader = command.ExecuteReader

            If dr.Read() Then PopulateData(jobReqDiffMaster, dr)

            cn.Close()
            Return jobReqDiffMaster
        End Function

        Public Shared Function RetrieveAll() As JobReqDiffMasterCollection
            Dim jobReqDiffMasters As New JobReqDiffMasterCollection
            Dim cn As SqlConnection = GetDBConnection()
            cn.Open()

            Dim command As New SqlCommand(RetrieveAllSproc, cn)
            command.CommandType = CommandType.StoredProcedure
            Dim dr As SqlDataReader = command.ExecuteReader

            PopulateCollection(jobReqDiffMasters, dr)

            cn.Close()
            Return jobReqDiffMasters
        End Function

        Public Shared Function GetJob() As JobReqDiffMasterCollection
            Dim jobReqDiffMasters As New JobReqDiffMasterCollection
            Dim cn As SqlConnection = GetDBConnection()
            cn.Open()

            Dim sql As String = "select * from JOB_REQ_DIFF_MASTER where job_status='W'"
            Dim command As New SqlCommand(sql, cn)
            command.CommandType = CommandType.Text
            Dim dr As SqlDataReader = command.ExecuteReader

            PopulateCollection(jobReqDiffMasters, dr)

            cn.Close()
            Return jobReqDiffMasters
        End Function

        Public Shared Sub CloseJob(ByVal jobOid As Long, ByVal cn As SqlConnection, ByVal txn As SqlTransaction)


            Dim sql As String = "update JOB_REQ_DIFF_MASTER set run_date=getdate(),job_status='C' where job_oid=" + jobOid.ToString

            Dim command As New SqlCommand(sql, cn, txn)

            command.CommandType = CommandType.Text
            command.ExecuteNonQuery()
        End Sub

#End Region

    End Class

End Namespace
