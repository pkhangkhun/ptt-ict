Imports Ie.IEComponent.BO
Imports Ie.UI.Utility
Imports System.Data.SqlClient

Namespace App.Dao

    Public Class StationDao
        Inherits BaseDao

        Private Class ColumnName
            Public Const ObjectId = "OBJECT_ID"
            Public Const StationId = "STATION_ID"
            Public Const StationName = "STATION_NAME"
            Public Const SiteId = "SITE_ID"
            Public Const SiteNo = "SITE_NO"
            Public Const BranchNo = "BRANCH_NO"
            Public Const IpAddress = "IP_ADDRESS"
            Public Const QueueName = "QUEUE_NAME"
            Public Const Status = "STATUS"
            Public Const LastDocNbr = "LAST_DOC_NBR"
            Public Const LastDocDate = "LAST_DOC_DATE"
            Public Const LastDocUpdate = "LAST_DOC_UPDATE"
            Public Const Version = "VERSION"
            Public Const CreateUser = "CREATE_USER"
            Public Const CreateDttm = "CREATE_DTTM"
            Public Const UpdateUser = "UPDATE_USER"
            Public Const UpdateDttm = "UPDATE_DTTM"
            Public Const TrDate = "TR_DATE"
        End Class

        Private Class ParamName
            Public Const ObjectId = "@ObjectId"
            Public Const StationId = "@StationId"
            Public Const StationName = "@StationName"
            Public Const SiteId = "@SiteId"
            Public Const SiteNo = "@SiteNo"
            Public Const BranchNo = "@BranchNo"
            Public Const IpAddress = "@IpAddress"
            Public Const QueueName = "@QueueName"
            Public Const Status = "@Status"
            Public Const LastDocNbr = "@LastDocNbr"
            Public Const LastDocDate = "@LastDocDate"
            Public Const LastDocUpdate = "@LastDocUpdate"
            Public Const Version = "@Version"
            Public Const CreateUser = "@CreateUser"
            Public Const CreateDttm = "@CreateDttm"
            Public Const UpdateUser = "@UpdateUser"
            Public Const UpdateDttm = "@UpdateDttm"
            Public Const TrDate = "@TrDate"
        End Class

        Private Shared Sub PopulateData(ByVal station As Station, ByVal dr As SqlDataReader)

            station.ObjectID.ID = CType(dr.Item(ColumnName.ObjectID), Long)
            If Not (dr.Item(ColumnName.StationId) Is System.DBNull.Value) Then station.StationId = CInt(dr.Item(ColumnName.StationId))
            If Not (dr.Item(ColumnName.StationName) Is System.DBNull.Value) Then station.StationName = CStr(dr.Item(ColumnName.StationName))
            If Not (dr.Item(ColumnName.SiteId) Is System.DBNull.Value) Then station.SiteId = CLng(dr.Item(ColumnName.SiteId))
            If Not (dr.Item(ColumnName.SiteNo) Is System.DBNull.Value) Then station.SiteNo = CStr(dr.Item(ColumnName.SiteNo))
            If Not (dr.Item(ColumnName.BranchNo) Is System.DBNull.Value) Then station.BranchNo = CStr(dr.Item(ColumnName.BranchNo))
            If Not (dr.Item(ColumnName.IpAddress) Is System.DBNull.Value) Then station.IpAddress = CStr(dr.Item(ColumnName.IpAddress))
            If Not (dr.Item(ColumnName.QueueName) Is System.DBNull.Value) Then station.QueueName = CStr(dr.Item(ColumnName.QueueName))
            If Not (dr.Item(ColumnName.Status) Is System.DBNull.Value) Then station.Status = CStr(dr.Item(ColumnName.Status))
            If Not (dr.Item(ColumnName.LastDocNbr) Is System.DBNull.Value) Then station.LastDocNbr = CStr(dr.Item(ColumnName.LastDocNbr))
            If Not (dr.Item(ColumnName.LastDocDate) Is System.DBNull.Value) Then station.LastDocDate = CDate(dr.Item(ColumnName.LastDocDate))
            If Not (dr.Item(ColumnName.LastDocUpdate) Is System.DBNull.Value) Then station.LastDocUpdate = CDate(dr.Item(ColumnName.LastDocUpdate))
            If Not (dr.Item(ColumnName.Version) Is System.DBNull.Value) Then station.Version = CInt(dr.Item(ColumnName.Version))
            If Not (dr.Item(ColumnName.CreateUser) Is System.DBNull.Value) Then station.CreateUser = CStr(dr.Item(ColumnName.CreateUser))
            If Not (dr.Item(ColumnName.CreateDttm) Is System.DBNull.Value) Then station.CreateDttm = CDate(dr.Item(ColumnName.CreateDttm))
            If Not (dr.Item(ColumnName.UpdateUser) Is System.DBNull.Value) Then station.UpdateUser = CStr(dr.Item(ColumnName.UpdateUser))
            If Not (dr.Item(ColumnName.UpdateDttm) Is System.DBNull.Value) Then station.UpdateDttm = CDate(dr.Item(ColumnName.UpdateDttm))
            If Not (dr.Item(ColumnName.TrDate) Is System.DBNull.Value) Then station.TrDate = CDate(dr.Item(ColumnName.TrDate))

        End Sub

        Private Shared Sub PopulateCollection(ByVal stations As StationCollection, ByVal dr As SqlDataReader)
            Do While (dr.Read())
                Dim station As New Station
                PopulateData(station, dr)
                stations.Add(station)
            Loop
        End Sub

        Private Shared Sub MapCommandParameter(ByVal command As SqlCommand, ByVal station As Station)
            command.Parameters.AddWithValue(ParamName.ObjectID, station.ObjectID.ID)
            If IsDbNull(station.StationId) Or IsNothing(station.StationId) Then
                command.Parameters.AddWithValue(ParamName.StationId, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.StationId, station.StationId)
            End If
            If IsDbNull(station.StationName) Or IsNothing(station.StationName) Then
                command.Parameters.AddWithValue(ParamName.StationName, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.StationName, station.StationName)
            End If
            If IsDbNull(station.SiteId) Or IsNothing(station.SiteId) Then
                command.Parameters.AddWithValue(ParamName.SiteId, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.SiteId, station.SiteId)
            End If
            If IsDbNull(station.SiteNo) Or IsNothing(station.SiteNo) Then
                command.Parameters.AddWithValue(ParamName.SiteNo, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.SiteNo, station.SiteNo)
            End If
            If IsDbNull(station.BranchNo) Or IsNothing(station.BranchNo) Then
                command.Parameters.AddWithValue(ParamName.BranchNo, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.BranchNo, station.BranchNo)
            End If
            If IsDbNull(station.IpAddress) Or IsNothing(station.IpAddress) Then
                command.Parameters.AddWithValue(ParamName.IpAddress, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.IpAddress, station.IpAddress)
            End If
            If IsDbNull(station.QueueName) Or IsNothing(station.QueueName) Then
                command.Parameters.AddWithValue(ParamName.QueueName, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.QueueName, station.QueueName)
            End If
            If IsDbNull(station.Status) Or IsNothing(station.Status) Then
                command.Parameters.AddWithValue(ParamName.Status, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.Status, station.Status)
            End If
            If IsDbNull(station.LastDocNbr) Or IsNothing(station.LastDocNbr) Then
                command.Parameters.AddWithValue(ParamName.LastDocNbr, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.LastDocNbr, station.LastDocNbr)
            End If
            If Not InputValidator.IsDate(station.LastDocDate) Then
                command.Parameters.AddWithValue(ParamName.LastDocDate, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.LastDocDate, station.LastDocDate)
            End If
            If Not InputValidator.IsDate(station.LastDocUpdate) Then
                command.Parameters.AddWithValue(ParamName.LastDocUpdate, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.LastDocUpdate, station.LastDocUpdate)
            End If
            If IsDbNull(station.Version) Or IsNothing(station.Version) Then
                command.Parameters.AddWithValue(ParamName.Version, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.Version, station.Version)
            End If
            If IsDbNull(station.CreateUser) Or IsNothing(station.CreateUser) Then
                command.Parameters.AddWithValue(ParamName.CreateUser, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.CreateUser, station.CreateUser)
            End If
            If Not InputValidator.IsDate(station.CreateDttm) Then
                command.Parameters.AddWithValue(ParamName.CreateDttm, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.CreateDttm, station.CreateDttm)
            End If
            If IsDbNull(station.UpdateUser) Or IsNothing(station.UpdateUser) Then
                command.Parameters.AddWithValue(ParamName.UpdateUser, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.UpdateUser, station.UpdateUser)
            End If
            If Not InputValidator.IsDate(station.UpdateDttm) Then
                command.Parameters.AddWithValue(ParamName.UpdateDttm, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.UpdateDttm, station.UpdateDttm)
            End If
            If Not InputValidator.IsDate(station.TrDate) Then
                command.Parameters.AddWithValue(ParamName.TrDate, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.TrDate, station.TrDate)
            End If
        End Sub

#Region "Common Function Method"

        Public Shared Function GetActive() As StationCollection
            Dim stations As New StationCollection
            Dim cn As SqlConnection = GetDBConnection()
            cn.Open()

            Dim RetrieveAllSproc As String = "select * from station where status='A'"
            Dim command As New SqlCommand(RetrieveAllSproc, cn)
            command.CommandType = CommandType.Text
            Dim dr As SqlDataReader = command.ExecuteReader

            PopulateCollection(stations, dr)

            cn.Close()
            Return stations
        End Function

        Public Shared Function GetBySiteNo(ByVal siteNo As String) As StationCollection
            Dim stations As New StationCollection
            Dim cn As SqlConnection = GetDBConnection()
            cn.Open()

            Dim RetrieveAllSproc As String = "select * from station where status='A' and site_no='" + siteNo + "'"
            Dim command As New SqlCommand(RetrieveAllSproc, cn)
            command.CommandType = CommandType.Text
            Dim dr As SqlDataReader = command.ExecuteReader

            PopulateCollection(stations, dr)

            cn.Close()
            Return stations
        End Function

#End Region

    End Class

End Namespace
