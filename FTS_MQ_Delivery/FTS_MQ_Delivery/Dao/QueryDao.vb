Imports FTS_MQ_Delivery.HQComponent.BO
Imports Ie.UI.Utility
Imports System.Data.SqlClient

Namespace App.Dao
    Public Class QueryDao
        Inherits BaseDao


        Public Shared Function Inquiry(ByVal sql As String) As DataSet
            Dim ds As New DataSet
            Dim cn As SqlConnection = GetDBConnection()
            cn.Open()

            Dim command As New SqlCommand(sql, cn)
            command.CommandType = CommandType.Text


            Dim dap As SqlDataAdapter = New SqlDataAdapter()
            dap.SelectCommand = command

            Try
                dap.Fill(ds, "MASTER")
            Catch ex As Exception
                Throw ex
            Finally
                dap.Dispose()
                cn.Close()
            End Try

            Return ds
        End Function

        Public Shared Sub Update(ByVal sql As String, ByVal cn As SqlConnection, ByVal txn As SqlTransaction)
            Dim command As New SqlCommand(sql, cn, txn)
            command.CommandType = CommandType.Text
            command.ExecuteNonQuery()
        End Sub

    End Class
End Namespace
