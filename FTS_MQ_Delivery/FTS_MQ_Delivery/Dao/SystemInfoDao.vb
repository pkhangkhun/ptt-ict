Imports Ie.IEComponent.BO
Imports Ie.UI.Utility
Imports System.Data.SqlClient

Namespace App.Dao
    Public Class SystemInfoDao
        Inherits BaseDao

        Private Class ColumnName
            Public Const ObjectId = "OBJECT_ID"
            Public Const Keyword = "KEYWORD"
            Public Const KeyDesc = "KEY_DESC"
            Public Const Prefix = "PREFIX"
            Public Const SysValue = "SYS_VALUE"
            Public Const Version = "VERSION"
            Public Const UpdateUser = "UPDATE_USER"
            Public Const UpdateDttm = "UPDATE_DTTM"
        End Class

        Private Class ParamName
            Public Const ObjectId = "@ObjectId"
            Public Const Keyword = "@Keyword"
            Public Const KeyDesc = "@KeyDesc"
            Public Const Prefix = "@Prefix"
            Public Const SysValue = "@SysValue"
            Public Const Version = "@Version"
            Public Const UpdateUser = "@UpdateUser"
            Public Const UpdateDttm = "@UpdateDttm"
        End Class

        Private Shared Sub PopulateData(ByVal systemInfo As SystemInfo, ByVal dr As SqlDataReader)

            systemInfo.ObjectID.ID = CType(dr.Item(ColumnName.ObjectID), Long)
            If Not (dr.Item(ColumnName.Keyword) Is System.DBNull.Value) Then systemInfo.Keyword = CStr(dr.Item(ColumnName.Keyword))
            If Not (dr.Item(ColumnName.KeyDesc) Is System.DBNull.Value) Then systemInfo.KeyDesc = CStr(dr.Item(ColumnName.KeyDesc))
            If Not (dr.Item(ColumnName.Prefix) Is System.DBNull.Value) Then systemInfo.Prefix = CStr(dr.Item(ColumnName.Prefix))
            If Not (dr.Item(ColumnName.SysValue) Is System.DBNull.Value) Then systemInfo.SysValue = CStr(dr.Item(ColumnName.SysValue))
            If Not (dr.Item(ColumnName.Version) Is System.DBNull.Value) Then systemInfo.Version = CLng(dr.Item(ColumnName.Version))
            If Not (dr.Item(ColumnName.UpdateUser) Is System.DBNull.Value) Then systemInfo.UpdateUser = CStr(dr.Item(ColumnName.UpdateUser))
            If Not (dr.Item(ColumnName.UpdateDttm) Is System.DBNull.Value) Then systemInfo.UpdateDttm = CDate(dr.Item(ColumnName.UpdateDttm))

        End Sub

        Private Shared Sub PopulateCollection(ByVal systemInfos As SystemInfoCollection, ByVal dr As SqlDataReader)
            Do While (dr.Read())
                Dim systemInfo As New SystemInfo
                PopulateData(systemInfo, dr)
                systemInfos.Add(systemInfo)
            Loop
        End Sub

        Private Shared Sub MapCommandParameter(ByVal command As SqlCommand, ByVal systemInfo As SystemInfo)
            command.Parameters.AddWithValue(ParamName.ObjectID, systemInfo.ObjectID.ID)
            If IsDbNull(systemInfo.Keyword) Or IsNothing(systemInfo.Keyword) Then
                command.Parameters.AddWithValue(ParamName.Keyword, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.Keyword, systemInfo.Keyword)
            End If
            If IsDbNull(systemInfo.KeyDesc) Or IsNothing(systemInfo.KeyDesc) Then
                command.Parameters.AddWithValue(ParamName.KeyDesc, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.KeyDesc, systemInfo.KeyDesc)
            End If
            If IsDbNull(systemInfo.Prefix) Or IsNothing(systemInfo.Prefix) Then
                command.Parameters.AddWithValue(ParamName.Prefix, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.Prefix, systemInfo.Prefix)
            End If
            If IsDbNull(systemInfo.SysValue) Or IsNothing(systemInfo.SysValue) Then
                command.Parameters.AddWithValue(ParamName.SysValue, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.SysValue, systemInfo.SysValue)
            End If
            If IsDbNull(systemInfo.Version) Or IsNothing(systemInfo.Version) Then
                command.Parameters.AddWithValue(ParamName.Version, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.Version, systemInfo.Version)
            End If
            If IsDbNull(systemInfo.UpdateUser) Or IsNothing(systemInfo.UpdateUser) Then
                command.Parameters.AddWithValue(ParamName.UpdateUser, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.UpdateUser, systemInfo.UpdateUser)
            End If
            If Not InputValidator.IsDate(systemInfo.UpdateDttm) Then
                command.Parameters.AddWithValue(ParamName.UpdateDttm, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.UpdateDttm, systemInfo.UpdateDttm)
            End If
        End Sub

#Region "Common Function Method"

        
        Public Shared Function GetNextMQID(ByVal keyWord As String, ByVal r1 As Integer, ByVal r2 As Integer) As String
            Dim systemInfo As New SystemInfo
            Dim cn As SqlConnection = GetDBConnection()
            cn.Open()

            Dim retValue As String = ""

            Dim RetrieveSproc As String = "spFTS_GetNextMQID"
            Dim command As New SqlCommand(RetrieveSproc, cn)
            command.CommandType = CommandType.StoredProcedure
            command.Parameters.AddWithValue(ParamName.Keyword, keyWord)
            command.Parameters.AddWithValue("r1", r1)
            command.Parameters.AddWithValue("r2", r2)

            Dim dr As SqlDataReader = command.ExecuteReader

            If dr.Read() Then
                retValue = dr.Item("retSysValue").ToString
            End If

            cn.Close()
            Return retValue
        End Function

        Public Shared Function TestDBConnection() As Boolean

            Dim isSuccess As Boolean = False

            Try
                Dim cn As SqlConnection = GetDBConnection()
                cn.Open()
                cn.Close()
                isSuccess = True
            Catch ex As Exception
                isSuccess = False
            End Try

            Return isSuccess
        End Function



#End Region


    End Class

End Namespace