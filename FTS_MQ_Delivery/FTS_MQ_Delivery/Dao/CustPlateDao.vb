Imports Ie.IEComponent.BO
Imports Ie.UI.Utility
Imports System.Data.SqlClient

Namespace App.Dao

    Public Class CustPlateDao
        Inherits BaseDao

        Private Class ColumnName
            Public Const ObjectId = "OBJECT_ID"
            Public Const PlateNumber = "PLATE_NUMBER"
            Public Const ProvinceId = "PROVINCE_ID"
            Public Const CustId = "CUST_ID"
            Public Const Status = "STATUS"
            Public Const Version = "VERSION"
            Public Const CreateUser = "CREATE_USER"
            Public Const CreateDttm = "CREATE_DTTM"
            Public Const UpdateUser = "UPDATE_USER"
            Public Const UpdateDttm = "UPDATE_DTTM"
            Public Const TrDttm = "TR_DTTM"
        End Class

        Private Class ParamName
            Public Const ObjectId = "@ObjectId"
            Public Const PlateNumber = "@PlateNumber"
            Public Const ProvinceId = "@ProvinceId"
            Public Const CustId = "@CustId"
            Public Const Status = "@Status"
            Public Const Version = "@Version"
            Public Const CreateUser = "@CreateUser"
            Public Const CreateDttm = "@CreateDttm"
            Public Const UpdateUser = "@UpdateUser"
            Public Const UpdateDttm = "@UpdateDttm"
            Public Const TrDttm = "@TrDttm"
            Public Const SendDate = "@SendDate"
        End Class

        Private Shared Sub PopulateData(ByVal custPlate As CustPlate, ByVal dr As SqlDataReader)

            custPlate.ObjectID.ID = CType(dr.Item(ColumnName.ObjectId), Long)
            If Not (dr.Item(ColumnName.PlateNumber) Is System.DBNull.Value) Then custPlate.PlateNumber = CStr(dr.Item(ColumnName.PlateNumber))
            If Not (dr.Item(ColumnName.ProvinceId) Is System.DBNull.Value) Then custPlate.ProvinceId = CLng(dr.Item(ColumnName.ProvinceId))
            If Not (dr.Item(ColumnName.CustId) Is System.DBNull.Value) Then custPlate.CustId = CLng(dr.Item(ColumnName.CustId))
            If Not (dr.Item(ColumnName.Status) Is System.DBNull.Value) Then custPlate.Status = CStr(dr.Item(ColumnName.Status))
            If Not (dr.Item(ColumnName.Version) Is System.DBNull.Value) Then custPlate.Version = CLng(dr.Item(ColumnName.Version))
            If Not (dr.Item(ColumnName.CreateUser) Is System.DBNull.Value) Then custPlate.CreateUser = CStr(dr.Item(ColumnName.CreateUser))
            If Not (dr.Item(ColumnName.CreateDttm) Is System.DBNull.Value) Then custPlate.CreateDttm = CDate(dr.Item(ColumnName.CreateDttm))
            If Not (dr.Item(ColumnName.UpdateUser) Is System.DBNull.Value) Then custPlate.UpdateUser = CStr(dr.Item(ColumnName.UpdateUser))
            If Not (dr.Item(ColumnName.UpdateDttm) Is System.DBNull.Value) Then custPlate.UpdateDttm = CDate(dr.Item(ColumnName.UpdateDttm))
            If Not (dr.Item(ColumnName.TrDttm) Is System.DBNull.Value) Then custPlate.TrDttm = CDate(dr.Item(ColumnName.TrDttm))

        End Sub

        Private Shared Sub PopulateCollection(ByVal custPlates As CustPlateCollection, ByVal dr As SqlDataReader)
            Do While (dr.Read())
                Dim custPlate As New CustPlate
                PopulateData(custPlate, dr)
                custPlates.Add(custPlate)
            Loop
        End Sub

        Private Shared Sub MapCommandParameter(ByVal command As SqlCommand, ByVal custPlate As CustPlate)
            command.Parameters.AddWithValue(ParamName.ObjectId, custPlate.ObjectID.ID)
            If IsDBNull(custPlate.PlateNumber) Or IsNothing(custPlate.PlateNumber) Then
                command.Parameters.AddWithValue(ParamName.PlateNumber, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.PlateNumber, custPlate.PlateNumber)
            End If
            If IsDBNull(custPlate.ProvinceId) Or IsNothing(custPlate.ProvinceId) Then
                command.Parameters.AddWithValue(ParamName.ProvinceId, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.ProvinceId, custPlate.ProvinceId)
            End If
            If IsDBNull(custPlate.CustId) Or IsNothing(custPlate.CustId) Then
                command.Parameters.AddWithValue(ParamName.CustId, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.CustId, custPlate.CustId)
            End If
            If IsDBNull(custPlate.Status) Or IsNothing(custPlate.Status) Then
                command.Parameters.AddWithValue(ParamName.Status, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.Status, custPlate.Status)
            End If
            If IsDBNull(custPlate.Version) Or IsNothing(custPlate.Version) Then
                command.Parameters.AddWithValue(ParamName.Version, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.Version, custPlate.Version)
            End If
            If IsDBNull(custPlate.CreateUser) Or IsNothing(custPlate.CreateUser) Then
                command.Parameters.AddWithValue(ParamName.CreateUser, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.CreateUser, custPlate.CreateUser)
            End If
            If Not InputValidator.IsDate(custPlate.CreateDttm) Then
                command.Parameters.AddWithValue(ParamName.CreateDttm, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.CreateDttm, custPlate.CreateDttm)
            End If
            If IsDBNull(custPlate.UpdateUser) Or IsNothing(custPlate.UpdateUser) Then
                command.Parameters.AddWithValue(ParamName.UpdateUser, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.UpdateUser, custPlate.UpdateUser)
            End If
            If Not InputValidator.IsDate(custPlate.UpdateDttm) Then
                command.Parameters.AddWithValue(ParamName.UpdateDttm, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.UpdateDttm, custPlate.UpdateDttm)
            End If
            If Not InputValidator.IsDate(custPlate.TrDttm) Then
                command.Parameters.AddWithValue(ParamName.TrDttm, System.DBNull.Value)
            Else
                command.Parameters.AddWithValue(ParamName.TrDttm, custPlate.TrDttm)
            End If
        End Sub

#Region "Common Function Method"

        Public Shared Sub Create(ByVal custPlate As CustPlate, ByVal cn As SqlConnection, ByVal txn As SqlTransaction)
            Dim CreateSproc As String = My.Resources.rsCustPlate.sqlCreate
            Dim command As New SqlCommand(CreateSproc, cn, txn)
            command.CommandType = CommandType.Text
            MapCommandParameter(command, custPlate)
            command.ExecuteNonQuery()
        End Sub

        Public Shared Sub Update(ByVal custPlate As CustPlate, ByVal cn As SqlConnection, ByVal txn As SqlTransaction)
            Dim UpdateSproc As String = My.Resources.rsCustPlate.sqlUpdate
            Dim command As New SqlCommand(UpdateSproc, cn, txn)
            command.CommandType = CommandType.Text
            MapCommandParameter(command, custPlate)
            command.ExecuteNonQuery()
        End Sub

        Public Shared Function CheckExists(ByVal plateNumber As String, ByVal provinceId As Long, ByVal custId As Long) As Boolean
            Dim sql As String = "select object_id from cust_plate where plate_number=" + "'" + plateNumber + "'" + " and province_id=" + CStr(provinceId) + " and cust_id=" + CStr(custId) + " limit 1"
            Dim cn As SqlConnection = GetDBConnection()
            cn.Open()
            Dim command As New SqlCommand(sql, cn)
            command.CommandType = CommandType.Text
            Dim dr As SqlDataReader = command.ExecuteReader
            Dim isExists As Boolean = False
            If dr.Read Then
                isExists = True
            End If
            dr.Close()
            cn.Close()
            Return isExists
        End Function

        Public Shared Function GetPublish() As CustPlateCollection
            Dim custPlates As New CustPlateCollection
            Dim cn As SqlConnection = GetDBConnection()
            cn.Open()

            Dim RetrieveAllSproc As String = "select * from cust_plate where isnull(tr_dttm,getdate()-1)<isnull(update_dttm,getdate())"

            Dim command As New SqlCommand(RetrieveAllSproc, cn)
            command.CommandType = CommandType.Text
            Dim dr As SqlDataReader = command.ExecuteReader

            PopulateCollection(custPlates, dr)

            cn.Close()
            Return custPlates
        End Function

        Public Shared Function GetDiff(ByVal maxCarPlateOid As Long) As CustPlateCollection
            Dim custPlates As New CustPlateCollection
            Dim cn As SqlConnection = GetDBConnection()
            cn.Open()

            Dim RetrieveAllSproc As String = "select * from cust_plate where object_id>" + maxCarPlateOid.ToString

            Dim command As New SqlCommand(RetrieveAllSproc, cn)
            command.CommandType = CommandType.Text
            Dim dr As SqlDataReader = command.ExecuteReader

            PopulateCollection(custPlates, dr)

            cn.Close()
            Return custPlates
        End Function

        Public Shared Sub UpdatePublishDate(ByVal custPlate As CustPlate, ByVal cn As SqlConnection, ByVal txn As SqlTransaction)
            Dim UpdateSproc As String = "update cust_plate set tr_dttm=getdate() where  plate_number=" + "'" + custPlate.PlateNumber + "'" + " and province_id=" + CStr(custPlate.ProvinceId) + " and cust_id=" + CStr(custPlate.CustId)
            Dim command As New SqlCommand(UpdateSproc, cn, txn)
            command.CommandType = CommandType.Text
            command.ExecuteNonQuery()
        End Sub

#End Region

    End Class

End Namespace
