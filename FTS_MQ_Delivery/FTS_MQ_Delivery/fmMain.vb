Imports Ie.IeComponent.BO
Imports FTS_MQ_Delivery.App.Service
Imports System.Messaging
Imports System.IO
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Threading.Timer
Imports System.Timers.Timer
Imports System.Windows.Forms
Imports FTS_MQ_Delivery.HQComponent.BO
Imports System.Management
Imports System.Net.NetworkInformation
Imports System.Configuration
Imports System.Net.Sockets

Public Class fmMain

    Public Shared App_Path As String = System.Configuration.ConfigurationManager.AppSettings("Path_Obj").ToString

    'Public Shared App_Path As String = "E:\Appl\FTS_HQ_Publisher"

#Region "MAIN"

    Private Sub fmMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        WriteLogFile("Strat " + Me.Text)
        'DisplayUI()

        'Timer1.Start()

        'Try

        '    SyncLock Me

        '        Dim sysInfoServ As IMqSystemInfoService = UI.Factory.BaseServiceFactory.GetInstance(GetType(MqSystemInfoService))

        '        If Not sysInfoServ.TestDBConnection Then
        '            WriteLogFile("Error : Connect to Database")
        '        Else

        '            '=== Publish Not Responding Queue ======
        '            Dim msmqLogServ As IHQMsmqLogService = UI.Factory.BaseServiceFactory.GetInstance(GetType(HQMsmqLogService))
        '            Dim msmqTranLogs As HQMsmqTranLogCollection = msmqLogServ.GetPublishQueueNotResponding
        '            If msmqTranLogs.Count > 0 Then
        '                WriteLogFile("Start Publish Not Responding Queue")
        '            End If

        '            For Each msmqTranLog As HQMsmqTranLog In msmqTranLogs
        '                PublishNotResponding(msmqTranLog)
        '            Next

        '            '=======================================

        '        End If

        '    End SyncLock

        'Catch ex As Exception
        '    WriteLogFile("Error Start FTS HQ Publisher Services : " + ex.Message)
        'Finally
        '    Me.Close()
        'End Try

        Try
            txtComputerName.Text = System.Environment.MachineName.ToString()
        Catch ex As Exception
        End Try

        Try
            Timer1.Start()
        Catch ex As Exception

        End Try

    End Sub

#End Region

#Region "Publish Normal"


    Private Sub PublishData()

        Dim TransactionPerQueue As Integer = System.Configuration.ConfigurationManager.AppSettings.Get("TransactionPerQueue")

        Dim sendDate As Date = Date.Now

        Dim sysInfoServ As IMqSystemInfoService = UI.Factory.BaseServiceFactory.GetInstance(GetType(MqSystemInfoService))

        Dim stationServ As IMqStationService = UI.Factory.BaseServiceFactory.GetInstance(GetType(MqStationService))

        Dim stations As New StationCollection
        Try
            WriteLogFile("Load Client for Publish")
            stations = stationServ.GetActive()
            WriteLogFile("Success Load Client for Publish Total : " + stations.Count.ToString + " station(s)")
        Catch ex As Exception
            WriteLogFile("Error Get Client IP-Address : " + ex.Message)
            Exit Sub
        End Try


        '=== Customer Data ===================================================================================================

        Try
            Dim custServ As IMqCustomerService = UI.Factory.BaseServiceFactory.GetInstance(GetType(MqCustomerService))

            Dim customers As CustomerCollection = custServ.GetPublish
            WriteLogFile("Publish Customer Total : " + customers.Count.ToString + " Record(s)")

            Dim tempCustomers As New CustomerCollection
            Dim queueNo As String = ""
            Dim r1 As Integer = 1
            For Each cust As Customer In customers
                tempCustomers.Add(cust)
                If tempCustomers.Count = TransactionPerQueue Then
                    queueNo = sysInfoServ.GetNextMQID("MQCM", r1, 1)
                    r1 = 0

                    SerializeObject(tempCustomers, queueNo + ".slz", sendDate)

                    MSMQ_Customer(stations, queueNo, tempCustomers, customers.Count, sendDate)

                    custServ.UpdatePublishDate(tempCustomers)

                    tempCustomers.Clear()

                End If
            Next
            If tempCustomers.Count > 0 Then
                queueNo = sysInfoServ.GetNextMQID("MQCM", r1, 1)

                SerializeObject(tempCustomers, queueNo + ".slz", sendDate)

                MSMQ_Customer(stations, queueNo, tempCustomers, customers.Count, sendDate)

                custServ.UpdatePublishDate(tempCustomers)

                tempCustomers.Clear()

            End If

            WriteLogFile("Success Publish Customer")

        Catch ex As Exception
            WriteLogFile("Error Publish Customer " + ex.Message)
        End Try

        '===========================================================================================================

        '===== Car Plate ===========================================================================================

        Try
            Dim custPlateServ As IMqCustPlateService = UI.Factory.BaseServiceFactory.GetInstance(GetType(MqCustPlateService))

            Dim custPlates As CustPlateCollection = custPlateServ.GetPublish

            WriteLogFile("Publish Car Plate Total : " + custPlates.Count.ToString + " Record(s)")

            Dim tempCustPlates As New CustPlateCollection
            Dim queueNo As String = ""
            Dim r1 As Integer = 1
            For Each custPlate As CustPlate In custPlates
                tempCustPlates.Add(custPlate)
                If tempCustPlates.Count = TransactionPerQueue Then
                    queueNo = sysInfoServ.GetNextMQID("MQCP", r1, 1)
                    r1 = 0

                    SerializeObject(tempCustPlates, queueNo + ".slz", sendDate)

                    MSMQ_CustPlate(stations, queueNo, tempCustPlates, custPlates.Count, sendDate)

                    custPlateServ.UpdatePublishDate(tempCustPlates)

                    tempCustPlates.Clear()

                End If
            Next
            If tempCustPlates.Count > 0 Then
                queueNo = sysInfoServ.GetNextMQID("MQCP", r1, 1)

                SerializeObject(tempCustPlates, queueNo + ".slz", sendDate)

                MSMQ_CustPlate(stations, queueNo, tempCustPlates, custPlates.Count, sendDate)

                custPlateServ.UpdatePublishDate(tempCustPlates)

                tempCustPlates.Clear()

            End If

            WriteLogFile("Success Publish Car Plate")

        Catch ex As Exception
            WriteLogFile("Error Publish Car Plate " + ex.Message)
        End Try


        '===========================================================================================================

        WriteLogFile("Success Publish Master Data")
        WriteLogFile("==============================================================================================")
    End Sub

#End Region

#Region "Publish Diff"


    Private Sub PublishData_DIFF(ByVal jobOid As Long, ByVal siteNo As String, ByVal maxCusOid As Long, ByVal maxCarPlateOid As Long)

        Dim TransactionPerQueue As Integer = System.Configuration.ConfigurationManager.AppSettings.Get("TransactionPerQueue")

        Dim sendDate As Date = Date.Now

        Dim sysInfoServ As IMqSystemInfoService = UI.Factory.BaseServiceFactory.GetInstance(GetType(MqSystemInfoService))

        Dim stationServ As IMqStationService = UI.Factory.BaseServiceFactory.GetInstance(GetType(MqStationService))

        Dim stations As New StationCollection
        Try
            WriteLogFile("Load Client for Publish Diff")
            stations = stationServ.GetBySiteNo(siteNo)
            WriteLogFile("Success Load Client for Publish Diff Total : " + stations.Count.ToString + " station(s)")
        Catch ex As Exception
            WriteLogFile("Error Get Client IP-Address : " + ex.Message)
            Exit Sub
        End Try


        '=== Customer Data ===================================================================================================

        Try
            Dim custServ As IMqCustomerService = UI.Factory.BaseServiceFactory.GetInstance(GetType(MqCustomerService))

            Dim customers As CustomerCollection = custServ.GetDiff(maxCusOid)
            WriteLogFile("Publish Customer Total : " + customers.Count.ToString + " Record(s)")

            Dim tempCustomers As New CustomerCollection
            Dim queueNo As String = ""
            Dim r1 As Integer = 1
            For Each cust As Customer In customers
                tempCustomers.Add(cust)
                If tempCustomers.Count = TransactionPerQueue Then
                    queueNo = sysInfoServ.GetNextMQID("MQCM", r1, 1)
                    r1 = 0

                    SerializeObject(tempCustomers, queueNo + ".slz", sendDate)

                    MSMQ_Customer(stations, queueNo, tempCustomers, customers.Count, sendDate)

                    tempCustomers.Clear()

                End If
            Next
            If tempCustomers.Count > 0 Then
                queueNo = sysInfoServ.GetNextMQID("MQCM", r1, 1)

                SerializeObject(tempCustomers, queueNo + ".slz", sendDate)

                MSMQ_Customer(stations, queueNo, tempCustomers, customers.Count, sendDate)

                tempCustomers.Clear()

            End If

            WriteLogFile("Success Publish Customer Diff")

        Catch ex As Exception
            WriteLogFile("Error Publish Customer Diff" + ex.Message)
        End Try

        '===========================================================================================================

        '===== Car Plate ===========================================================================================

        Try
            Dim custPlateServ As IMqCustPlateService = UI.Factory.BaseServiceFactory.GetInstance(GetType(MqCustPlateService))

            Dim custPlates As CustPlateCollection = custPlateServ.GetDiff(maxCarPlateOid)

            WriteLogFile("Publish Car Plate Diff Total : " + custPlates.Count.ToString + " Record(s)")

            Dim tempCustPlates As New CustPlateCollection
            Dim queueNo As String = ""
            Dim r1 As Integer = 1
            For Each custPlate As CustPlate In custPlates
                tempCustPlates.Add(custPlate)
                If tempCustPlates.Count = TransactionPerQueue Then
                    queueNo = sysInfoServ.GetNextMQID("MQCP", r1, 1)
                    r1 = 0

                    SerializeObject(tempCustPlates, queueNo + ".slz", sendDate)

                    MSMQ_CustPlate(stations, queueNo, tempCustPlates, custPlates.Count, sendDate)

                    tempCustPlates.Clear()

                End If
            Next
            If tempCustPlates.Count > 0 Then
                queueNo = sysInfoServ.GetNextMQID("MQCP", r1, 1)

                SerializeObject(tempCustPlates, queueNo + ".slz", sendDate)

                MSMQ_CustPlate(stations, queueNo, tempCustPlates, custPlates.Count, sendDate)

                tempCustPlates.Clear()

            End If

            WriteLogFile("Success Publish Car Plate Diff")

        Catch ex As Exception
            WriteLogFile("Error Publish Car Plate Diff" + ex.Message)
        End Try


        '===========================================================================================================

        WriteLogFile("Success Publish Master Data Diff")
        WriteLogFile("==============================================================================================")
    End Sub

#End Region

#Region "Delivery Queue"

    Private Sub DeliveryQueue(ByVal msmqTranLog As HQMsmqTranLog)

        Select Case msmqTranLog.Q_Type

            Case 1
                '== Customer ====

                Dim tranDate As String = Format(msmqTranLog.CommitDate, "yyyyMMdd")

                Dim fName As String = App_Path + "\OBJ\" + tranDate + "\" + msmqTranLog.QueueId + "_" + msmqTranLog.FileNo + ".slz"
                WriteLogFile("File Name : " + fName)
                If Not File.Exists(fName) Then
                    WriteLogFile("File Not Exists")
                    WriteLogFile("TranDate : " + tranDate.Substring(0, 4))
                    If tranDate.Substring(0, 4) > "2500" Then
                        tranDate = CStr(CInt(Format(msmqTranLog.CommitDate, "yyyy")) - 543) + Format(msmqTranLog.CommitDate, "MMdd")
                    Else
                        tranDate = CStr(CInt(Format(msmqTranLog.CommitDate, "yyyy")) + 543) + Format(msmqTranLog.CommitDate, "MMdd")
                    End If

                    fName = App_Path + "\OBJ\" + tranDate + "\" + msmqTranLog.QueueId + "_" + msmqTranLog.FileNo + ".slz"
                    WriteLogFile("File Name : " + fName)
                    If Not File.Exists(fName) Then

                        WriteLogFile("File Not Found : " + fName)

                        Try
                            Dim sqlServ As IQueryService = UI.Factory.BaseServiceFactory.GetInstance(GetType(QueryService))

                            Dim Sql As String = "update MSMQ_TRAN_LOG set receive_date=getdate(),commit_date=getdate() where site_no='" + msmqTranLog.SiteNo + "' and queue_id='" + msmqTranLog.QueueId + "' and file_no='" + msmqTranLog.FileNo + "'"

                            sqlServ.Update(Sql)

                            Sql = "select top 1 queue_id from MSMQ_TRAN_LOG where " & _
                            " site_no='" + msmqTranLog.SiteNo + "' and queue_id='" + msmqTranLog.QueueId + "'" & _
                            " and COMMIT_DATE is null"

                            Dim dsCheck As DataSet = sqlServ.Query(Sql)
                            If dsCheck.Tables(0).Rows.Count = 0 Then
                                dsCheck.Dispose()

                                Sql = "update MSMQ_LOG set complete_date=getdate() where site_no='" + msmqTranLog.SiteNo + "' and queue_id='" + msmqTranLog.QueueId + "'"

                                sqlServ.Update(Sql)
                            Else
                                dsCheck.Dispose()
                            End If


                        Catch ex As Exception
                            WriteLogFile("Error Update MSMQ Log : " + ex.Message)
                        End Try
                        Exit Select
                    End If


                End If

                WriteLogFile("Sent Time : " + msmqTranLog.Version.ToString)
                WriteLogFile("Read Customer Object File : " + fName)
                WriteLogFile("Destination IP-Address : " + msmqTranLog.Q_IP + " : Site No " + msmqTranLog.SiteNo)

                Dim customers As CustomerCollection = DeSerializeCustomerObject(fName)

                If customers.Count > 0 Then

                    WriteLogFile("Total : " + customers.Count.ToString + " Record(s)")

                    Dim qDestination As String = "FTS_POS_NT_IB"

                    Dim qIP As String = msmqTranLog.Q_IP

                    Dim qPath As String = "FormatName:DIRECT=TCP:" + qIP
                    Dim qName As String = "\private$\" + qDestination
                    Dim mqSource As String = msmqTranLog.SiteNo + ":" + msmqTranLog.BranchNo + ":" + msmqTranLog.StationNo.ToString
                    Dim messageQ As MessageQueue
                    Dim message As System.Messaging.Message
                    qPath += qName

                    WriteLogFile(qPath)

                    messageQ = New MessageQueue(qPath)
                    message = New System.Messaging.Message(customers, New BinaryMessageFormatter)
                    message.Label = msmqTranLog.QueueId + "_" + msmqTranLog.FileNo
                    message.Label += ":" + mqSource + ":" + Format(Date.Now, "yyMMdd-HHmm")

                    message.AcknowledgeType = AcknowledgeTypes.None
                    message.UseJournalQueue = False
                    message.UseDeadLetterQueue = True

                    Dim success As Boolean = True
                    Try

                        messageQ.Send(message)

                    Catch ex As Exception
                        success = False
                        WriteLogFile("Error Sent Queue : " + ex.Message)
                    Finally
                        messageQ.Close()
                    End Try

                    If success Then
                        WriteLogFile("Success Sent")
                    End If
                Else
                    WriteLogFile("File Not Found : " + fName)
                End If

            Case 2
                '== Car Plate ===

                Dim tranDate As String = Format(msmqTranLog.CommitDate, "yyyyMMdd")

                Dim fName As String = App_Path + "\OBJ\" + tranDate + "\" + msmqTranLog.QueueId + "_" + msmqTranLog.FileNo + ".slz"

                If Not File.Exists(fName) Then

                    If tranDate.Substring(0, 4) > "2500" Then
                        tranDate = CStr(CInt(Format(msmqTranLog.CommitDate, "yyyy")) - 543) + Format(msmqTranLog.CommitDate, "MMdd")
                    Else
                        tranDate = CStr(CInt(Format(msmqTranLog.CommitDate, "yyyy")) + 543) + Format(msmqTranLog.CommitDate, "MMdd")
                    End If

                    fName = App_Path + "\OBJ\" + tranDate + "\" + msmqTranLog.QueueId + "_" + msmqTranLog.FileNo + ".slz"

                    If Not File.Exists(fName) Then

                        WriteLogFile("File Not Found : " + fName)

                        Try
                            Dim sqlServ As IQueryService = UI.Factory.BaseServiceFactory.GetInstance(GetType(QueryService))

                            Dim Sql As String = "update MSMQ_TRAN_LOG set receive_date=getdate(),commit_date=getdate() where site_no='" + msmqTranLog.SiteNo + "' and queue_id='" + msmqTranLog.QueueId + "' and file_no='" + msmqTranLog.FileNo + "'"

                            sqlServ.Update(Sql)

                            Sql = "select top 1 queue_id from MSMQ_TRAN_LOG where " & _
                            " site_no='" + msmqTranLog.SiteNo + "' and queue_id='" + msmqTranLog.QueueId + "'" & _
                            " and COMMIT_DATE is null"

                            Dim dsCheck As DataSet = sqlServ.Query(Sql)
                            If dsCheck.Tables(0).Rows.Count = 0 Then
                                dsCheck.Dispose()

                                Sql = "update MSMQ_LOG set complete_date=getdate() where site_no='" + msmqTranLog.SiteNo + "' and queue_id='" + msmqTranLog.QueueId + "'"

                                sqlServ.Update(Sql)
                            Else
                                dsCheck.Dispose()
                            End If


                        Catch ex As Exception
                            WriteLogFile("Error Update MSMQ Log : " + ex.Message)
                        End Try

                        Exit Select

                    End If


                End If



                WriteLogFile("Sent Time : " + msmqTranLog.Version.ToString)
                WriteLogFile("Read Carplate Object File : " + fName)
                WriteLogFile("Destination IP-Address : " + msmqTranLog.Q_IP + " : Site No " + msmqTranLog.SiteNo)

                Dim custPlates As CustPlateCollection = DeSerializeCarPlateObject(fName)

                If custPlates.Count > 0 Then

                    WriteLogFile("Total : " + custPlates.Count.ToString + " Record(s)")

                    Dim qDestination As String = "FTS_POS_NT_IB"

                    Dim qIP As String = msmqTranLog.Q_IP

                    Dim qPath As String = "FormatName:DIRECT=TCP:" + qIP
                    Dim qName As String = "\private$\" + qDestination
                    Dim mqSource As String = msmqTranLog.SiteNo + ":" + msmqTranLog.BranchNo + ":" + msmqTranLog.StationNo.ToString
                    Dim messageQ As MessageQueue
                    Dim message As System.Messaging.Message
                    qPath += qName

                    WriteLogFile(qPath)

                    messageQ = New MessageQueue(qPath)
                    message = New System.Messaging.Message(custPlates, New BinaryMessageFormatter)
                    message.Label = msmqTranLog.QueueId + "_" + msmqTranLog.FileNo
                    message.Label += ":" + mqSource + ":" + Format(Date.Now, "yyMMdd-HHmm")

                    message.AcknowledgeType = AcknowledgeTypes.None
                    message.UseJournalQueue = False
                    message.UseDeadLetterQueue = True

                    Dim success As Boolean = True
                    Try

                        messageQ.Send(message)

                    Catch ex As Exception
                        success = False
                        WriteLogFile("Error Sent Queue : " + ex.Message)
                    Finally
                        messageQ.Close()
                    End Try
                    If success Then
                        WriteLogFile("Success Sent")
                    End If
                Else
                    WriteLogFile("File Not Found : " + fName)
                End If

                ' ==========================================================================================

            Case 3
                '== Symbol Type ====

                Dim tranDate As String = Format(msmqTranLog.CommitDate, "yyyyMMdd")

                Dim fName As String = App_Path + "\OBJ\" + tranDate + "\" + msmqTranLog.QueueId + "_" + msmqTranLog.FileNo + ".slz"

                If Not File.Exists(fName) Then

                    If tranDate.Substring(0, 4) > "2500" Then
                        tranDate = CStr(CInt(Format(msmqTranLog.CommitDate, "yyyy")) - 543) + Format(msmqTranLog.CommitDate, "MMdd")
                    Else
                        tranDate = CStr(CInt(Format(msmqTranLog.CommitDate, "yyyy")) + 543) + Format(msmqTranLog.CommitDate, "MMdd")
                    End If
                    fName = App_Path + "\OBJ\" + tranDate + "\" + msmqTranLog.QueueId + "_" + msmqTranLog.FileNo + ".slz"

                    If Not File.Exists(fName) Then

                        WriteLogFile("File Not Found : " + fName)

                        Try
                            Dim sqlServ As IQueryService = UI.Factory.BaseServiceFactory.GetInstance(GetType(QueryService))

                            Dim Sql As String = "update MSMQ_TRAN_LOG set receive_date=getdate(),commit_date=getdate() where site_no='" + msmqTranLog.SiteNo + "' and queue_id='" + msmqTranLog.QueueId + "' and file_no='" + msmqTranLog.FileNo + "'"

                            sqlServ.Update(Sql)

                            Sql = "select top 1 queue_id from MSMQ_TRAN_LOG where " & _
                            " site_no='" + msmqTranLog.SiteNo + "' and queue_id='" + msmqTranLog.QueueId + "'" & _
                            " and COMMIT_DATE is null"

                            Dim dsCheck As DataSet = sqlServ.Query(Sql)
                            If dsCheck.Tables(0).Rows.Count = 0 Then
                                dsCheck.Dispose()

                                Sql = "update MSMQ_LOG set complete_date=getdate() where site_no='" + msmqTranLog.SiteNo + "' and queue_id='" + msmqTranLog.QueueId + "'"

                                sqlServ.Update(Sql)
                            Else
                                dsCheck.Dispose()
                            End If


                        Catch ex As Exception
                            WriteLogFile("Error Update MSMQ Log : " + ex.Message)
                        End Try
                        Exit Select
                    End If
                End If

                WriteLogFile("Sent Time : " + msmqTranLog.Version.ToString)
                WriteLogFile("Read Symbol Type Object File : " + fName)
                WriteLogFile("Destination IP-Address : " + msmqTranLog.Q_IP + " : Site No " + msmqTranLog.SiteNo)

                Dim symbolTypes As SymbolTypeCollection = DeSerializeSymbolTypeObject(fName)

                If symbolTypes.Count > 0 Then

                    WriteLogFile("symbol Type Total : " + symbolTypes.Count.ToString + " Record(s)")

                    Dim qDestination As String = "FTS_POS_NT_IB"

                    Dim qIP As String = msmqTranLog.Q_IP

                    Dim qPath As String = "FormatName:DIRECT=TCP:" + qIP
                    Dim qName As String = "\private$\" + qDestination
                    Dim mqSource As String = msmqTranLog.SiteNo + ":" + msmqTranLog.BranchNo + ":" + msmqTranLog.StationNo.ToString
                    Dim messageQ As MessageQueue
                    Dim message As System.Messaging.Message
                    qPath += qName

                    WriteLogFile(qPath)

                    messageQ = New MessageQueue(qPath)
                    message = New System.Messaging.Message(symbolTypes, New BinaryMessageFormatter)
                    message.Label = msmqTranLog.QueueId + "_" + msmqTranLog.FileNo
                    message.Label += ":" + mqSource + ":" + Format(Date.Now, "yyMMdd-HHmm")

                    message.AcknowledgeType = AcknowledgeTypes.None
                    message.UseJournalQueue = False
                    message.UseDeadLetterQueue = True

                    Dim success As Boolean = True
                    Try

                        messageQ.Send(message)

                    Catch ex As Exception
                        success = False
                        WriteLogFile("Error Sent Queue : " + ex.Message)
                    Finally
                        messageQ.Close()
                    End Try

                    If success Then
                        WriteLogFile("Success Sent")
                    End If
                Else
                    WriteLogFile("File Not Found : " + fName)
                End If

        End Select

    End Sub

#End Region

#Region "MSMQ_Customer"

    Private Shared Sub MSMQ_Customer(ByVal stations As StationCollection, ByVal queueNo As String, ByVal customers As CustomerCollection, ByVal totalRecords As Integer, ByVal sendDate As Date)

        For Each station As Station In stations

            Dim qLog As New HQMsmqLog

            WriteLogFile("Publish to : " + station.SiteNo + ":" + station.BranchNo + ":" + station.StationId.ToString + ":Queue No#" + queueNo + ":Total " + customers.Count.ToString + " Record(s)")

            Dim qDestination As String = "FTS_POS_NT_IB"
            'If qDestination = "" Then
            '    Exit Sub
            'End If

            Dim qIP As String = station.IpAddress

            Dim qPath As String = "FormatName:DIRECT=TCP:" + qIP
            Dim qName As String = "\private$\" + qDestination
            Dim mqSource As String = station.SiteNo + ":" + station.BranchNo + ":" + station.StationId.ToString
            Dim messageQ As MessageQueue
            Dim message As System.Messaging.Message
            'Dim mqTran As New MessageQueueTransaction()

            qPath += qName

            WriteLogFile(qPath)

            messageQ = New MessageQueue(qPath)
            message = New System.Messaging.Message(customers, New BinaryMessageFormatter)
            message.Label = queueNo
            message.Label += ":" + mqSource + ":" + Format(sendDate, "yyMMdd-HHmm")

            message.AcknowledgeType = AcknowledgeTypes.None
            message.UseJournalQueue = False
            message.UseDeadLetterQueue = True

            'mqTran.Begin()
            Dim success As Boolean = True
            Try

                messageQ.Send(message)

                'messageQ.Send(message, mqTran)
                'mqTran.Commit()


                If CInt(queueNo.Substring(14, 4)) = 1 Then
                    With qLog
                        .QueueId = queueNo.Substring(0, 13)
                        .SiteNo = station.SiteNo
                        .BranchNo = station.BranchNo
                        .StationNo = station.StationId
                        .QueueDestination = station.IpAddress
                        .QueueType = 1 '= Customer
                        .TotalFile = CInt(queueNo.Substring(14, 4))
                        .TotalRecord = totalRecords
                        .SendDate = sendDate
                    End With
                    Dim qTranLog As New HQMsmqTranLog
                    With qTranLog
                        .QueueId = queueNo.Substring(0, 13)
                        .SiteNo = station.SiteNo
                        .BranchNo = station.BranchNo
                        .StationNo = station.StationId
                        .FileNo = queueNo.Substring(14, 4)
                        .TotalRecord = customers.Count
                    End With
                    qLog.LogTrans.Add(qTranLog)
                Else
                    qLog.QueueId = queueNo.Substring(0, 13)
                    qLog.TotalFile = CInt(queueNo.Substring(14, 4))
                    qLog.TotalRecord = totalRecords

                    Dim qTranLog As New HQMsmqTranLog
                    With qTranLog
                        .QueueId = queueNo.Substring(0, 13)
                        .SiteNo = station.SiteNo
                        .BranchNo = station.BranchNo
                        .StationNo = station.StationId
                        .FileNo = queueNo.Substring(14, 4)
                        .TotalRecord = customers.Count
                    End With
                    qLog.LogTrans.Add(qTranLog)
                End If

            Catch ex As Exception
                'mqTran.Abort()
                success = False
                WriteLogFile("Error Send Queue : " + ex.Message)
            Finally
                messageQ.Close()
            End Try

            Try
                Dim qLogServ As IHQMsmqLogService = UI.Factory.BaseServiceFactory.GetInstance(GetType(HQMsmqLogService))
                qLogServ.SaveLog(qLog)
            Catch ex As Exception
                WriteLogFile("Error Save Web Monitoring Log (Customer) : " + ex.Message)
            End Try
        Next
        WriteLogFile("----------------------------")

    End Sub

#End Region

#Region "MSMQ_CustPlate"
    Private Shared Sub MSMQ_CustPlate(ByVal stations As StationCollection, ByVal queueNo As String, ByVal custPlates As CustPlateCollection, ByVal totalRecords As Integer, ByVal sendDate As Date)

        For Each station As Station In stations

            Dim qLog As New HQMsmqLog

            WriteLogFile("Publish to : " + station.SiteNo + ":" + station.BranchNo + ":" + station.StationId.ToString + ":Queue No#" + queueNo + ":Total " + custPlates.Count.ToString + " Record(s)")

            Dim qDestination As String = "FTS_POS_NT_IB"
            If qDestination = "" Then
                Exit Sub
            End If

            Dim qIP As String = station.IpAddress

            Dim qPath As String = "FormatName:DIRECT=TCP:" + qIP
            Dim qName As String = "\private$\" + qDestination
            Dim mqSource As String = station.SiteNo + ":" + station.BranchNo + ":" + station.StationId.ToString
            Dim messageQ As MessageQueue
            Dim message As System.Messaging.Message
            'Dim mqTran As New MessageQueueTransaction()

            qPath += qName
            messageQ = New MessageQueue(qPath)
            message = New System.Messaging.Message(custPlates, New BinaryMessageFormatter)
            message.Label = queueNo
            message.Label += ":" + mqSource + ":" + Format(sendDate, "yyMMdd-HHmm")

            message.AcknowledgeType = AcknowledgeTypes.None
            message.UseJournalQueue = False
            message.UseDeadLetterQueue = True

            'mqTran.Begin()
            Dim success As Boolean = True
            Try
                messageQ.Send(message)

                'messageQ.Send(message, mqTran)
                'mqTran.Commit()

                If CInt(queueNo.Substring(14, 4)) = 1 Then
                    With qLog
                        .QueueId = queueNo.Substring(0, 13)
                        .SiteNo = station.SiteNo
                        .BranchNo = station.BranchNo
                        .StationNo = station.StationId
                        .QueueDestination = station.IpAddress
                        .QueueType = 2 '= Car Plate
                        .TotalFile = CInt(queueNo.Substring(14, 4))
                        .TotalRecord = totalRecords
                        .SendDate = sendDate
                    End With
                    Dim qTranLog As New HQMsmqTranLog
                    With qTranLog
                        .QueueId = queueNo.Substring(0, 13)
                        .SiteNo = station.SiteNo
                        .BranchNo = station.BranchNo
                        .StationNo = station.StationId
                        .FileNo = queueNo.Substring(14, 4)
                        .TotalRecord = custPlates.Count
                    End With
                    qLog.LogTrans.Add(qTranLog)
                Else
                    qLog.QueueId = queueNo.Substring(0, 13)
                    qLog.TotalFile = CInt(queueNo.Substring(14, 4))
                    qLog.TotalRecord = totalRecords


                    Dim qTranLog As New HQMsmqTranLog
                    With qTranLog
                        .QueueId = queueNo.Substring(0, 13)
                        .SiteNo = station.SiteNo
                        .BranchNo = station.BranchNo
                        .StationNo = station.StationId
                        .FileNo = queueNo.Substring(14, 4)
                        .TotalRecord = custPlates.Count
                    End With
                    qLog.LogTrans.Add(qTranLog)
                End If

            Catch ex As Exception
                'mqTran.Abort()
                success = False
                WriteLogFile("Error Send Queue : " + ex.Message)
            Finally
                messageQ.Close()
            End Try

            Try
                Dim qLogServ As IHQMsmqLogService = UI.Factory.BaseServiceFactory.GetInstance(GetType(HQMsmqLogService))
                qLogServ.SaveLog(qLog)
            Catch ex As Exception
                WriteLogFile("Error Save Web Monitoring Log (Car Plate) : " + ex.Message)
            End Try

        Next

    End Sub

#End Region

#Region "Serialize Object"

    Private Shared Sub SerializeObject(ByVal obj As Object, ByVal fileName As String, ByVal publishDate As Date)
        Try

        Catch ex As Exception

        End Try

        Dim folder As String = App_Path + "\OBJ\" + Format(publishDate, "yyyyMMdd")
        If Not Directory.Exists(folder) Then
            Directory.CreateDirectory(folder)
        End If

        fileName = folder + "\" + fileName
        Dim stream As FileStream = File.Open(fileName, FileMode.Create)
        Try
            Dim bFormater As BinaryFormatter = New BinaryFormatter
            bFormater.Serialize(stream, obj)
        Catch ex As Exception
            WriteLogFile("Error Serialize Queue : " + ex.Message)
        Finally
            stream.Close()
        End Try
    End Sub

    Private Shared Sub SaveWebMonitoringLog(ByVal qLog As HQMsmqLog)
        Try

        Catch ex As Exception
            WriteLogFile("Error Save Web Monitoring Log : " + ex.Message)
        End Try


    End Sub

    Private Shared Function DeSerializeCustomerObject(ByVal fileName As String) As CustomerCollection

        Dim objToSerialize As New CustomerCollection

        Try
            Dim stream As FileStream = File.Open(fileName, FileMode.Open)
            Try

                Dim bFormater As BinaryFormatter = New BinaryFormatter
                objToSerialize = bFormater.Deserialize(stream)

            Catch ex As Exception
            Finally
                stream.Close()
            End Try
        Catch ex As Exception
        End Try

        Return objToSerialize
    End Function


    Private Shared Function DeSerializeCarPlateObject(ByVal fileName As String) As CustPlateCollection
        Dim objToSerialize As New CustPlateCollection

        Try
            Dim stream As FileStream = File.Open(fileName, FileMode.Open)

            Try

                Dim bFormater As BinaryFormatter = New BinaryFormatter
                objToSerialize = bFormater.Deserialize(Stream)

            Catch ex As Exception
            Finally
                Stream.Close()
            End Try
        Catch ex As Exception
        End Try

        Return objToSerialize

    End Function

    Private Shared Function DeSerializeSymbolTypeObject(ByVal fileName As String) As SymbolTypeCollection

        Dim objToSerialize As New SymbolTypeCollection

        Try
            Dim stream As FileStream = File.Open(fileName, FileMode.Open)
            Try

                Dim bFormater As BinaryFormatter = New BinaryFormatter
                objToSerialize = bFormater.Deserialize(stream)

            Catch ex As Exception
            Finally
                stream.Close()
            End Try
        Catch ex As Exception

        End Try

        Return objToSerialize
    End Function



#End Region

#Region "Write Log"

    Private Shared Sub WriteLogFile(ByVal _msg As String)

        Dim logPath As String = App_Path + "\Log\"

        Dim s As String = Format(Date.Today, "yyyyMMdd")
        Dim myFile As String = "\FTS_MqDelivery_" + s + ".txt"
        Dim objStreamWriter As StreamWriter
        If Not Directory.Exists(logPath) Then
            Directory.CreateDirectory(logPath)
        End If
        If File.Exists(logPath + myFile) Then
            objStreamWriter = File.AppendText(logPath + myFile)
        Else
            objStreamWriter = File.CreateText(logPath + myFile)
        End If

        _msg = Format(Date.Now, "HH:mm:ss --> ") + _msg
        objStreamWriter.WriteLine(_msg)
        objStreamWriter.Close()
    End Sub

#End Region


    Private Sub btnLoad_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLoad.Click
        btnLoad.Enabled = False
        btnSend.Enabled = False
        Dim sql As String = "select a.queue_id,a.site_no,a.branch_no,a.station_no,a.file_no,a.total_record,b.send_date as tran_date,c.IP_ADDRESS as Q_IP,'' as status" & _
        " from MSMQ_TRAN_LOG a " & _
        " left join MSMQ_LOG b on a.QUEUE_ID=b.QUEUE_ID and a.SITE_NO=b.SITE_NO and a.BRANCH_NO=b.BRANCH_NO and a.STATION_NO=b.STATION_NO " & _
        " left join STATION c on a.SITE_NO=c.SITE_NO " & _
        " where a.commit_date Is null " & _
        " order by b.SEND_DATE,a.QUEUE_ID,a.FILE_NO,a.SITE_NO"

        Dim sqlServ As IQueryService = UI.Factory.BaseServiceFactory.GetInstance(GetType(QueryService))

        Try
            dgvTrans.Rows.Clear()
            Dim ds As DataSet = sqlServ.query(sql)

            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1

                Application.DoEvents()

                Dim gRow As New DataGridViewRow
                With gRow
                    .Cells.Add(New DataGridViewTextBoxCell)
                    .Cells.Add(New DataGridViewTextBoxCell)
                    .Cells.Add(New DataGridViewTextBoxCell)
                    .Cells.Add(New DataGridViewTextBoxCell)
                    .Cells.Add(New DataGridViewTextBoxCell)
                    .Cells.Add(New DataGridViewTextBoxCell)
                    .Cells.Add(New DataGridViewTextBoxCell)
                    .Cells.Add(New DataGridViewTextBoxCell)

                    .Cells(0).Value = ds.Tables(0).Rows(i).Item("queue_id")
                    .Cells(1).Value = ds.Tables(0).Rows(i).Item("site_no")
                    .Cells(2).Value = ds.Tables(0).Rows(i).Item("station_no")
                    .Cells(3).Value = ds.Tables(0).Rows(i).Item("file_no")
                    .Cells(4).Value = ds.Tables(0).Rows(i).Item("total_record")
                    .Cells(5).Value = ds.Tables(0).Rows(i).Item("tran_date")
                    .Cells(6).Value = ds.Tables(0).Rows(i).Item("q_ip")
                    .Cells(7).Value = ds.Tables(0).Rows(i).Item("status")

                End With

                dgvTrans.Rows.Add(gRow)

            Next

            lblTotalTran.Text = "Total : " + ds.Tables(0).Rows.Count.ToString + " Transaction(s)"
        Catch ex As Exception
            MessageBox.Show("Error : " + ex.Message)
        Finally
            btnLoad.Enabled = True
            btnSend.Enabled = dgvTrans.Rows.Count > 0
        End Try

    End Sub

    Private Sub btnSend_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSend.Click
        For i As Integer = 0 To dgvTrans.Rows.Count - 1
            Dim mqTranLog As New HQMsmqTranLog
            With mqTranLog
                .SiteNo = dgvTrans.Rows(i).Cells("site_no").Value
                .Q_IP = dgvTrans.Rows(i).Cells("q_ip").Value
                .QueueId = dgvTrans.Rows(i).Cells("queue_id").Value
                .FileNo = dgvTrans.Rows(i).Cells("file_no").Value
            End With

            dgvTrans.Rows(i).Cells("status").Value = "Success"
            'PublishNotResponding(mqTranLog)

        Next
    End Sub

    Private Sub StartJob()
        Try

            Dim limitResendTime As Integer = 5
            Dim limitRetryTime As Integer = 5

            ' ����ʶҹ� ��� Active
            Dim sqlServ As IQueryService = UI.Factory.BaseServiceFactory.GetInstance(GetType(QueryService))
            Dim sql As String = "  select distinct a.site_no,isnull(b.SERVICE_VERSION,2.1) as SERVICE_VERSION from MSMQ_LOG a,STATION b  " & _
            "where a.SITE_NO = b.SITE_NO And a.COMPLETE_DATE Is null and b.STATUS='A' and b.MQ_STATUS='A' group by a.site_no,b.SERVICE_VERSION order by site_no "

            Dim dsLog As DataSet = sqlServ.query(sql)
            For i As Integer = 0 To dsLog.Tables(0).Rows.Count - 1

                Application.DoEvents()

                lblStatus.Text = "Running Job : " + dsLog.Tables(0).Rows(i).Item("site_no").ToString
                WriteLogFile("Running Job : " + dsLog.Tables(0).Rows(i).Item("site_no").ToString)

                sql = "select top 1 a.queue_id+'_'+a.file_no as queue_no,a.queue_id,a.file_no,b.send_date from MSMQ_TRAN_LOG a " & _
                " left join MSMQ_LOG b on a.queue_id=b.queue_id and a.site_no=b.site_no" & _
                " where a.site_no='" + dsLog.Tables(0).Rows(i).Item("site_no").ToString + "'" & _
                " and a.commit_date is null "
                If CDec(dsLog.Tables(0).Rows(i).Item("SERVICE_VERSION").ToString.Substring(0, 3)) < 2.2 Then
                    sql += " and convert(varchar(4),a.queue_id)<>'MQST'"
                End If
                sql += " order by b.send_date,a.QUEUE_ID,a.FILE_NO"
                Dim dsQueue As DataSet = sqlServ.Query(sql)
                WriteLogFile("Data Count : " + dsQueue.Tables(0).Rows.Count.ToString)
                If dsQueue.Tables(0).Rows.Count > 0 Then

                    sql = "select branch_no,station_id,ip_address,last_queue,isnull(last_send_date,getdate()) as last_send_date,retry_no,mq_status from station where site_no='" + dsLog.Tables(0).Rows(i).Item("site_no").ToString + "'"
                    Dim dsStation As DataSet = sqlServ.Query(sql)
                    WriteLogFile("Queue Count : " + dsStation.Tables(0).Rows.Count.ToString)
                    If dsStation.Tables(0).Rows.Count > 0 Then
                        If dsQueue.Tables(0).Rows(0).Item("queue_no").ToString <> dsStation.Tables(0).Rows(0).Item("last_queue").ToString Then

                            WriteLogFile("Send Q")
                            '== �ʴ���� Queue ��͹˹�� ���º�������� �ӡ�� Update Queue ����������� ������Ѻ�� Queue ��ǹ��仵��
                            WriteLogFile("last Queue : " + dsQueue.Tables(0).Rows(0).Item("queue_no").ToString)

                            sql = "update station set last_queue='" + dsQueue.Tables(0).Rows(0).Item("queue_no").ToString + "',last_send_date=getdate(),retry_no=1,mq_status='A'" & _
                            ",Ping_Result='',mq_state='Connected'" & _
                            " where site_no='" + dsLog.Tables(0).Rows(i).Item("site_no").ToString + "'"

                            sqlServ.Update(sql)

                            SendQueue(dsLog.Tables(0).Rows(i).Item("site_no").ToString, dsStation.Tables(0).Rows(0).Item("branch_no").ToString, dsStation.Tables(0).Rows(0).Item("station_id"), dsStation.Tables(0).Rows(0).Item("ip_address").ToString, dsQueue.Tables(0).Rows(0).Item("queue_id").ToString, dsQueue.Tables(0).Rows(0).Item("file_no").ToString, dsQueue.Tables(0).Rows(0).Item("send_date"), 1)


                        Else
                            '== ��� Queue �ѧ�繵��������� �ʴ�����ѧ����ա�õͺ�Ѻ�ҡ Client 
                            '== �ӡ������� �Թ Limit ����觫��������� ����Թ Limit �ӡ�� Hold Status �ͧ Queue �� Inactive �����͵�Ǩ�ͺ �� Manual
                            'If dsStation.Tables(0).Rows(0).Item("retry_no") + 1 > limitResendTime Then
                            '    If dsQueue.Tables(0).Rows(0).Item("queue_no").ToString.Trim <> "" Then
                            '        '== �Թ Limit �ӡ�� Update Status �ͧ Queue �� Inactive
                            '        sql = "update station set mq_status='I'" & _
                            '        " where site_no='" + dsLog.Tables(0).Rows(i).Item("site_no").ToString + "'"

                            '        sqlServ.Update(sql)
                            '    End If
                            'Else
                            '    '== �� ������ҧ�ͧ����� ��� Queue �����仹ҹ�Թ���ҷ�������������� ����Թ �ӡ���觫���ա����
                            '    If DateDiff(DateInterval.Minute, dsStation.Tables(0).Rows(0).Item("last_send_date"), Date.Now) > limitRetryTime Then
                            '        '== �觫�� ������Ѻ Stamp ���駷���觫��+1, �ѹ����� ���͵�Ǩ�� Limit ���駵���

                            '        sql = "update station set last_send_date=getdate(),retry_no=retry_no+1,mq_status='A'" & _
                            '        " where site_no='" + dsLog.Tables(0).Rows(i).Item("site_no").ToString + "'"

                            '        sqlServ.Update(sql)

                            '        SendQueue(dsLog.Tables(0).Rows(i).Item("site_no").ToString, dsStation.Tables(0).Rows(0).Item("branch_no").ToString, dsStation.Tables(0).Rows(0).Item("station_id"), dsStation.Tables(0).Rows(0).Item("ip_address").ToString, dsQueue.Tables(0).Rows(0).Item("queue_id").ToString, dsQueue.Tables(0).Rows(0).Item("file_no").ToString, dsQueue.Tables(0).Rows(0).Item("send_date"), dsStation.Tables(0).Rows(0).Item("retry_no") + 1)

                            '    End If

                            'End If

                            WriteLogFile("Check Time Send")
                            If DateDiff(DateInterval.Minute, dsStation.Tables(0).Rows(0).Item("last_send_date"), Date.Now) > limitRetryTime Then

                                '==  limitRetryTime ����㹡�����ա���� = 5 �ҷ�
                                '== �� Queue Outgoing �ҡ�դ�ҧ ����ͧ�ӡ�������� �� �ǡ�ӹǹ�ͺ��þ��������������

                                '== �觫�� ������Ѻ Stamp ���駷���觫��+1, �ѹ����� ���͵�Ǩ�� Limit ���駵���

                                '== Ping Test ========================================================

                                sql = "update station set last_send_date=getdate(),retry_no=retry_no+1,mq_status='A'"

                                Dim mqState As String = GetQueueMessageState(dsStation.Tables(0).Rows(0).Item("ip_address").ToString)
                               
                                Dim replyTime As Double = PingTime(dsStation.Tables(0).Rows(0).Item("ip_address").ToString, 4)

                                Dim replayTimeString = "Reply Time : " + CStr(replyTime) + " ms"

                                If mqState <> "Connected" Then
                                    If replyTime > 0 Then

                                        ' If TelNetClient(dsStation.Tables(0).Rows(0).Item("ip_address").ToString) Then
                                        SetResumeQueue(dsStation.Tables(0).Rows(0).Item("ip_address").ToString)
                                        mqState = GetQueueMessageState(dsStation.Tables(0).Rows(0).Item("ip_address").ToString)
                                        'Else
                                        '   SetPauseQueue(dsStation.Tables(0).Rows(0).Item("ip_address").ToString)
                                        '    mqState = "Not Open Port 1801"
                                        '  End If
                                    Else
                                        SetPauseQueue(dsStation.Tables(0).Rows(0).Item("ip_address").ToString)
                                        mqState = "Paused"
                                        replayTimeString = "Time Out"
                                    End If
                                Else
                                    If replyTime = 0 Then
                                        mqState = "Paused"
                                        replayTimeString = "Time Out"
                                    End If
                                End If

                                sql += ",Ping_Result='" + replayTimeString + "',mq_state='" + mqState + "'"

                                sql += " where site_no='" + dsLog.Tables(0).Rows(i).Item("site_no").ToString + "'"

                                sqlServ.Update(sql)

                                If Not IsQueuePendingOutgoing(dsStation.Tables(0).Rows(0).Item("ip_address").ToString) Then
                                    WriteLogFile("Is Queue Pending Out going Send Q")
                                    SendQueue(dsLog.Tables(0).Rows(i).Item("site_no").ToString, dsStation.Tables(0).Rows(0).Item("branch_no").ToString, dsStation.Tables(0).Rows(0).Item("station_id"), dsStation.Tables(0).Rows(0).Item("ip_address").ToString, dsQueue.Tables(0).Rows(0).Item("queue_id").ToString, dsQueue.Tables(0).Rows(0).Item("file_no").ToString, dsQueue.Tables(0).Rows(0).Item("send_date"), dsStation.Tables(0).Rows(0).Item("retry_no") + 1)
                                End If

                                End If

                        End If

                    End If

                Else
                    '== �ʴ��������� Queue ��ҧ �ӡ����ҧ Status �ͧ Queue ��� Table Station

                    sql = "update station set last_queue='',retry_no=0,mq_status='A'" & _
                            " where site_no='" + dsLog.Tables(0).Rows(i).Item("site_no").ToString + "'"

                    sqlServ.Update(sql)

                End If


            Next

            '==== Update Queue Status =============

            sql = "update STATION set last_queue='',RETRY_NO=0,mq_status='A' " & _
            " where site_no  not in (select distinct SITE_NO from MSMQ_LOG where COMPLETE_DATE is null) and LAST_QUEUE <>''"

            sqlServ.Update(sql)

            '======================================

        Catch ex As Exception
        End Try

    End Sub
    Function TelNetClient(ByVal telnetIp As String) As Boolean
        Try
            Dim telnetPort As Integer = 1801
            Dim client As New TcpClient(telnetIp, telnetPort)

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
    Private Function IsQueuePendingOutgoing(ByVal queueIp As String) As Boolean
        Dim _isPending As Boolean = False

        Try
            Dim mgmt As MSMQ.MSMQManagement = New MSMQ.MSMQManagement
            Dim outgoing As MSMQ.MSMQOutgoingQueueManagement

            Dim s As String = txtComputerName.Text
            Dim ss As Object = CType(s, Object)
            Dim pathName As String = "DIRECT=TCP:" + queueIp + "\Private$\fts_pos_nt_ib"

            Dim pn As Object = CType(pathName, Object)
            Dim format As String = Nothing
            Dim f As Object = CType(format, Object)

            mgmt.Init(ss, f, pn)

            outgoing = CType(mgmt, MSMQ.MSMQOutgoingQueueManagement)

            Dim mCount = outgoing.MessageCount

            _isPending = mCount > 0

        Catch ex As Exception
            _isPending = False
            'WriteLogFile("Error Read OutgoingQueue : " + queueIp + " : " + ex.Message.ToString)
        End Try


        Return _isPending
    End Function

    Private Function GetQueueMessageCount(ByVal queueIp As String) As Integer

        Dim _mCount As Integer = 0

        Try
            Dim mgmt As MSMQ.MSMQManagement = New MSMQ.MSMQManagement
            Dim outgoing As MSMQ.MSMQOutgoingQueueManagement

            Dim s As String = txtComputerName.Text
            Dim ss As Object = CType(s, Object)
            Dim pathName As String = "DIRECT=TCP:" + queueIp + "\Private$\fts_pos_nt_ib"

            Dim pn As Object = CType(pathName, Object)
            Dim format As String = Nothing
            Dim f As Object = CType(format, Object)

            mgmt.Init(ss, f, pn)

            outgoing = CType(mgmt, MSMQ.MSMQOutgoingQueueManagement)

            _mCount = outgoing.MessageCount

        Catch ex As Exception
            _mCount = 0
            'WriteLogFile("Error Read OutgoingQueue : " + queueIp + " : " + ex.Message.ToString)
        End Try


        Return _mCount
    End Function

    Private Function GetQueueMessageState(ByVal queueIp As String) As String

        Dim _qState As String = ""

        Try
            Dim mgmt As MSMQ.MSMQManagement = New MSMQ.MSMQManagement
            Dim outgoing As MSMQ.MSMQOutgoingQueueManagement

            Dim s As String = txtComputerName.Text
            Dim ss As Object = CType(s, Object)
            Dim pathName As String = "DIRECT=TCP:" + queueIp + "\Private$\fts_pos_nt_ib"

            Dim pn As Object = CType(pathName, Object)
            Dim format As String = Nothing
            Dim f As Object = CType(format, Object)

            mgmt.Init(ss, f, pn)

            outgoing = CType(mgmt, MSMQ.MSMQOutgoingQueueManagement)

            Dim iState As Integer = outgoing.State
            Select Case iState
                Case 1
                    _qState = CStr(iState)
                Case 2
                    _qState = "Waiting to connect"
                Case 3
                    _qState = CStr(iState)
                Case 4
                    _qState = "Paused"
                Case 5
                    _qState = "Inactive"
                Case 6
                    _qState = "Connected"
                Case Else
                    _qState = CStr(iState)
            End Select

        Catch ex As Exception
            _qState = "Queue not found"
            WriteLogFile("Error Read OutgoingQueue : " + queueIp + " : " + ex.Message.ToString)
        End Try


        Return _qState
    End Function

    Private Function PingTime(ByVal host As String, ByVal echoNum As Integer) As Double
        Dim _result As Double = 0
        Try
            Dim totaltime As Long = 0
            Dim timeOut As Integer = 120
            Dim pingSender As Ping = New Ping
            For i As Integer = 1 To echoNum

                Dim reply As PingReply = pingSender.Send(host, timeOut)
                If reply.Status = IPStatus.Success Then
                    totaltime += reply.RoundtripTime
                End If

            Next

            _result = totaltime / echoNum

        Catch ex As Exception
            _result = 0

        End Try

        Return _result
    End Function

    Private Sub SetResumeQueue(ByVal queueIp As String)

        Try
            Dim mgmt As MSMQ.MSMQManagement = New MSMQ.MSMQManagement
            Dim outgoing As MSMQ.MSMQOutgoingQueueManagement

            Dim s As String = txtComputerName.Text
            Dim ss As Object = CType(s, Object)
            Dim pathName As String = "DIRECT=TCP:" + queueIp + "\Private$\fts_pos_nt_ib"

            Dim pn As Object = CType(pathName, Object)
            Dim format As String = Nothing
            Dim f As Object = CType(format, Object)

            mgmt.Init(ss, f, pn)

            outgoing = CType(mgmt, MSMQ.MSMQOutgoingQueueManagement)

            outgoing.Resume()

        Catch ex As Exception

        End Try

    End Sub

    Private Sub SetPauseQueue(ByVal queueIp As String)

        Try
            Dim mgmt As MSMQ.MSMQManagement = New MSMQ.MSMQManagement
            Dim outgoing As MSMQ.MSMQOutgoingQueueManagement

            Dim s As String = txtComputerName.Text
            Dim ss As Object = CType(s, Object)
            Dim pathName As String = "DIRECT=TCP:" + queueIp + "\Private$\fts_pos_nt_ib"

            Dim pn As Object = CType(pathName, Object)
            Dim format As String = Nothing
            Dim f As Object = CType(format, Object)

            mgmt.Init(ss, f, pn)

            outgoing = CType(mgmt, MSMQ.MSMQOutgoingQueueManagement)

            outgoing.Pause()

        Catch ex As Exception

        End Try

    End Sub

    Private Sub SendQueue(ByVal siteNo As String, ByVal branchNo As String, ByVal stationId As Integer, ByVal ip As String, ByVal queueId As String, ByVal fileNo As String, ByVal sendDate As Date, ByVal sendTime As Integer)

        Try
            Dim mqTranLog As New HQMsmqTranLog
            With mqTranLog
                .CommitDate = sendDate
                .SiteNo = siteNo
                .BranchNo = branchNo
                .StationNo = stationId
                .Q_IP = ip
                .Q_Type = 1
                If queueId.Substring(0, 4) = "MQCP" Then
                    .Q_Type = 2
                End If
                If queueId.Substring(0, 4) = "MQST" Then
                    .Q_Type = 3
                End If
                .QueueId = queueId
                .FileNo = fileNo
                .Version = sendTime
            End With

            DeliveryQueue(mqTranLog)

        Catch ex As Exception

        End Try

    End Sub

    Private Sub DisplayUI()

        Try
            dgvStation.Rows.Clear()

            btnResent.Enabled = False

            Dim sqlServ As IQueryService = UI.Factory.BaseServiceFactory.GetInstance(GetType(QueryService))
            Dim sql As String = "select a.site_no,b.site_name,a.ip_address,a.last_queue,c.send_date,a.last_send_date,a.retry_no,a.mq_status,a.ping_result,a.mq_state from station a " & _
            " left join SITE b on a.site_no=b.site_code " & _
            " left join MSMQ_LOG c on substring(a.last_queue,1,13)=c.queue_id and a.site_no=c.site_no " & _
            " where 1=1 and a.mq_status<>'C' and a.status='A' "
            If rbWait.Checked Then
                sql += " and a.retry_no>0 and a.mq_status='A'"
            End If
            If rbInActive.Checked Then
                sql += " and a.mq_status<>'A'"
            End If
            If rbComplete.Checked Then
                sql += " and a.last_queue='' and a.mq_status='A'"
            End If
            If txtStation.Text.Trim <> "" Then
                sql += " and a.site_no='" + txtStation.Text.Trim + "'"
            End If

            If Not rbBzAll.Checked Then
                If rbBzOil.Checked Then
                    sql += " and substring(a.site_no,1,1)<>'S'"
                Else
                    sql += " and substring(a.site_no,1,1)='S'"
                End If
            End If

            sql += " order by a.mq_state,a.site_no"

            Dim dsUI As DataSet = sqlServ.Query(sql)
            For i As Integer = 0 To dsUI.Tables(0).Rows.Count - 1

                Application.DoEvents()

                Dim gRow As New DataGridViewRow
                With gRow
                    .Cells.Add(New DataGridViewTextBoxCell)
                    .Cells.Add(New DataGridViewTextBoxCell)
                    .Cells.Add(New DataGridViewTextBoxCell)
                    .Cells.Add(New DataGridViewTextBoxCell)
                    .Cells.Add(New DataGridViewTextBoxCell)
                    .Cells.Add(New DataGridViewTextBoxCell)
                    .Cells.Add(New DataGridViewTextBoxCell)
                    .Cells.Add(New DataGridViewTextBoxCell)
                    .Cells.Add(New DataGridViewTextBoxCell)

                    .Cells(0).Value = dsUI.Tables(0).Rows(i).Item("site_no")
                    .Cells(1).Value = dsUI.Tables(0).Rows(i).Item("site_name")
                    .Cells(2).Value = dsUI.Tables(0).Rows(i).Item("ip_address")
                    If dsUI.Tables(0).Rows(i).Item("last_queue") = "" Then
                        .Cells(3).Value = "- No Queue -"
                    Else
                        .Cells(3).Value = dsUI.Tables(0).Rows(i).Item("last_queue")
                    End If

                    .Cells(4).Value = ""

                    If dsUI.Tables(0).Rows(i).Item("send_date") <> Date.MinValue Then
                        .Cells(4).Value = Format(dsUI.Tables(0).Rows(i).Item("send_date"), "dd-MM-yy HH:mm")
                    End If

                    .Cells(5).Value = ""

                    If dsUI.Tables(0).Rows(i).Item("last_send_date") <> Date.MinValue Then
                        .Cells(5).Value = Format(dsUI.Tables(0).Rows(i).Item("last_send_date"), "dd-MM-yy HH:mm")
                    End If

                    .Cells(6).Value = dsUI.Tables(0).Rows(i).Item("retry_no")

                    If dsUI.Tables(0).Rows(i).Item("retry_no") > 2 And dsUI.Tables(0).Rows(i).Item("retry_no") < 6 Then
                        .Cells(6).Style.BackColor = Drawing.Color.Yellow
                        .Cells(6).Style.ForeColor = Drawing.Color.Black
                        .Cells(6).Style.SelectionBackColor = Drawing.Color.Yellow
                        .Cells(6).Style.SelectionForeColor = Drawing.Color.Black
                    Else
                        If dsUI.Tables(0).Rows(i).Item("retry_no") > 5 Then
                            .Cells(6).Style.BackColor = Drawing.Color.Red
                            .Cells(6).Style.ForeColor = Drawing.Color.White
                            .Cells(6).Style.SelectionBackColor = Drawing.Color.Red
                            .Cells(6).Style.SelectionForeColor = Drawing.Color.White
                        End If
                    End If

                    .Cells(7).Value = dsUI.Tables(0).Rows(i).Item("mq_state")
                    .Cells(8).Value = dsUI.Tables(0).Rows(i).Item("ping_result")

                    If .Cells(8).Value = "Time Out" Then
                        .Cells(8).Style.BackColor = Drawing.Color.Red
                        .Cells(8).Style.ForeColor = Drawing.Color.Yellow
                        .Cells(8).Style.SelectionBackColor = Drawing.Color.Red
                        .Cells(8).Style.SelectionForeColor = Drawing.Color.Yellow
                    End If

                End With

                dgvStation.Rows.Add(gRow)

            Next

            dsUI.Dispose()

            lblTotal.Text = "Total : " + dgvStation.Rows.Count.ToString + " Station(s)"
            If rbInActive.Checked Then
                btnResent.Enabled = dgvStation.Rows.Count > 0
            End If

            Dim bizType As String = ""

            If Not rbBzAll.Checked Then
                If rbBzOil.Checked Then
                    bizType = "OIL"
                Else
                    bizType = "NGV"
                End If
            End If

            GetTotalPendingQueue(bizType)

        Catch ex As Exception
            'MessageBox.Show("Error : " + ex.Message)
        End Try

    End Sub

    Private Sub rbAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbAll.Click, rbWait.Click, rbInActive.Click
        DisplayUI()
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Try
            lblStatus.Text = "Running Job"
            Timer1.Stop()
            StartJob()
            If Not cbPauseScreen.Checked Then
                DisplayUI()
            End If

            Timer1.Start()
            lblStatus.Text = ""
        Catch ex As Exception
        End Try

    End Sub

    Private Sub btnResent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnResent.Click
        'If dgvStation.Rows.Count = 0 Then
        '    Exit Sub
        'End If
        'Try
        '    Dim stationId As String = dgvStation.CurrentRow.Cells("station").Value.ToString
        '    Dim sql As String = "update station set retry_no=0,mq_status='A' where site_no='" + stationId + "'"
        '    Dim sqlServ As IQueryService = UI.Factory.BaseServiceFactory.GetInstance(GetType(QueryService))
        '    sqlServ.Update(sql)

        '    DisplayUI()

        'Catch ex As Exception

        'End Try


        SendTestQueue()


    End Sub

    Private Sub TimerRefresh_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TimerRefresh.Tick
        Try
            TimerRefresh.Stop()
            lblStatus.Text = "Refreshing"
            DisplayUI()
        Catch ex As Exception
        Finally
            lblStatus.Text = ""
            TimerRefresh.Start()
        End Try
    End Sub

    Private Sub SendTestQueue()
        Dim _path As String = "FormatName:DIRECT=TCP:10.10.2.67\Private$\fts_pos_nt_ib"

        Dim MQueue As MessageQueue = New MessageQueue(_path)

        Dim Msg As System.Messaging.Message = New System.Messaging.Message("Message")
        Msg.Priority = MessagePriority.Normal
        Msg.UseJournalQueue = False
        Msg.Label = "Test"
        MQueue.Send(Msg)
        MQueue.Close()

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'SendTestQueue()
        IsQueuePendingOutgoing("10.10.2.67")
    End Sub

    Private Sub dgvStation_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvStation.DoubleClick
        lblTotalTran.Text = ""
        If dgvStation.RowCount = 0 Then Exit Sub

        Dim siteNo As String = dgvStation.CurrentRow.Cells("station").Value.ToString
        Dim siteName As String = dgvStation.CurrentRow.Cells("SiteName").Value.ToString

        Try
            Dim sql As String = "select count(queue_id) as pCount from msmq_tran_log where site_no='" + siteNo + "'"
            sql += " and commit_date is null"

            Dim sqlServ As IQueryService = UI.Factory.BaseServiceFactory.GetInstance(GetType(QueryService))
            Dim ds As DataSet = sqlServ.Query(sql)

            Dim pCount As Integer = 0
            If ds.Tables(0).Rows.Count > 0 Then
                pCount = ds.Tables(0).Rows(0).Item("pCount")
            End If

            ds.Dispose()

            lblTotalTran.Text = siteNo + "-" + siteName + " : Pending = " + pCount.ToString + " Queue(s)"

            sql = "select top 1 receipt_nbr,receipt_date,hq_receive_dttm from receipt where site_no='" + siteNo + "' order by receipt_date desc"

            Dim ds2 As DataSet = sqlServ.Query(sql)

            If ds2.Tables(0).Rows.Count > 0 Then
                lblTotalTran.Text += " Last Doc : " + ds2.Tables(0).Rows(0).Item("receipt_nbr") + " Doc Date : " + Format(ds2.Tables(0).Rows(0).Item("receipt_date"), "dd-MM-yy HH:mm:ss") + " Receive Date : " + Format(ds2.Tables(0).Rows(0).Item("hq_receive_dttm"), "dd-MM-yy HH:mm:ss")
            Else
                lblTotalTran.Text += " Last Doc : No Transaction"
            End If

            ds2.Dispose()



        Catch ex As Exception

        End Try

    End Sub


    Sub GetTotalPendingQueue(ByVal bizType As String)

        Dim pCount As Integer = 0

        Try
            Dim sql As String = "select count(a.queue_id) as pCount from msmq_tran_log a,station b where "
            sql += " a.commit_date is null"
            sql += " and a.site_no=b.site_no and b.status='A' and b.mq_status='A'"

            If bizType <> "" Then
                If bizType = "OIL" Then
                    sql += " and substring(site_no,1,1)<>'S'"
                Else
                    sql += " and substring(site_no,1,1)='S'"
                End If
            End If

            Dim sqlServ As IQueryService = UI.Factory.BaseServiceFactory.GetInstance(GetType(QueryService))
            Dim ds As DataSet = sqlServ.Query(sql)


            If ds.Tables(0).Rows.Count > 0 Then
                pCount = ds.Tables(0).Rows(0).Item("pCount")
            End If

            ds.Dispose()

            lblPendingQueue.Text = "Pending = " + pCount.ToString + " Queue(s)"


        Catch ex As Exception

        End Try

    End Sub


End Class