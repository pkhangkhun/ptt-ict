Namespace HQComponent.BO

    <Serializable()> _
    Public Class ObjectID

        ' Implements the ObjectID column in the DB
        ' Current Implementation is Long

        Private _id As Long

        Property ID() As Long
            Get
                Return _id
            End Get
            Set(ByVal Value As Long)
                _id = Value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub New(ByVal id As Long)
            MyBase.New()
            Me.ID = id
        End Sub

    End Class

End Namespace

