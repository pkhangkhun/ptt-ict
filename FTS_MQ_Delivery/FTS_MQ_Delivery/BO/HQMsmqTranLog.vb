Namespace HQComponent.BO
    <Serializable()> _
    Public Class HQMsmqTranLog
        Inherits BaseBO

#Region "Private Fields"
        Private _queueId As String = ""
        Private _siteNo As String = ""
        Private _branchNo As String = ""
        Private _stationNo As Integer
        Private _fileNo As String = ""
        Private _totalRecord As Integer
        Private _receiveDate As Date
        Private _commitDate As Date
        Private _qIp As String = ""
        Private _qType As Integer
#End Region

#Region "Properties"
        Property QueueId() As String
            Get
                Return _queueId
            End Get
            Set(ByVal Value As String)
                _queueId = Value
            End Set
        End Property

        Property SiteNo() As String
            Get
                Return _siteNo
            End Get
            Set(ByVal Value As String)
                _siteNo = Value
            End Set
        End Property

        Property BranchNo() As String
            Get
                Return _branchNo
            End Get
            Set(ByVal Value As String)
                _branchNo = Value
            End Set
        End Property

        Property StationNo() As Integer
            Get
                Return _stationNo
            End Get
            Set(ByVal Value As Integer)
                _stationNo = Value
            End Set
        End Property

        Property FileNo() As String
            Get
                Return _fileNo
            End Get
            Set(ByVal Value As String)
                _fileNo = Value
            End Set
        End Property

        Property TotalRecord() As Integer
            Get
                Return _totalRecord
            End Get
            Set(ByVal Value As Integer)
                _totalRecord = Value
            End Set
        End Property

        Property ReceiveDate() As Date
            Get
                Return _receiveDate
            End Get
            Set(ByVal Value As Date)
                _receiveDate = Value
            End Set
        End Property

        Property CommitDate() As Date
            Get
                Return _commitDate
            End Get
            Set(ByVal Value As Date)
                _commitDate = Value
            End Set
        End Property

        Property Q_IP() As String
            Get
                Return _qIp
            End Get
            Set(ByVal value As String)
                _qIp = value
            End Set
        End Property

        Property Q_Type() As Integer
            Get
                Return _qType
            End Get
            Set(ByVal value As Integer)
                _qType = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

#End Region

    End Class

    <Serializable()> _
    Public Class HQMsmqTranLogCollection
        Inherits BaseBOCollection

        Default Property Item(ByVal index As Integer) As HQMsmqTranLog
            Get
                Return CType(InnerList.Item(index), HQMsmqTranLog)
            End Get
            Set(ByVal value As HQMsmqTranLog)
                InnerList.Item(index) = value
            End Set
        End Property

        Sub Add(ByVal value As HQMsmqTranLog)
            InnerList.Add(value)
        End Sub

    End Class

End Namespace
