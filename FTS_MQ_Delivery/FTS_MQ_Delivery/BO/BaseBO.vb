Namespace HQComponent.BO

    <Serializable()> _
    Public Class BaseBO
        Implements System.IDisposable

        ' Base Business Object for all Business Objects
        ' This will contain the base attribute to be used for all business objects

#Region " Private Fields "

        Private _objectID As ObjectID
        Private _version As Integer
        Private _updateUser As String
        Private _updateDttm As Date

#End Region

#Region " Properties "

        Property ObjectID() As ObjectID
            Get
                Return _objectID
            End Get
            Set(ByVal Value As ObjectID)
                _objectID = Value
            End Set
        End Property

        Property OID() As Long
            Get
                Return Me.ObjectID.ID
            End Get
            Set(ByVal Value As Long)
                Me.ObjectID.ID = Value
            End Set
        End Property

        Property Version() As Integer
            Get
                Return _version
            End Get
            Set(ByVal Value As Integer)
                _version = Value
            End Set
        End Property

        Property UpdateUser() As String
            Get
                Return _updateUser
            End Get
            Set(ByVal Value As String)
                _updateUser = Value
            End Set
        End Property

        Property UpdateDttm() As Date
            Get
                Return _updateDttm
            End Get
            Set(ByVal Value As Date)
                _updateDttm = Value
            End Set
        End Property

#End Region

        Public Sub New()
            MyBase.New()
            Me.ObjectID = New ObjectID
        End Sub

        Public Sub New(ByVal oid As ObjectID)
            MyBase.New()
            Me.ObjectID = oid
        End Sub

        Public Function IsNewObject() As Boolean
            Return Me.Version = -1
        End Function

        ' Implement IDisposable
        ' This is the method called by the client to dispose the object.
        Public Overloads Sub Dispose() _
            Implements System.IDisposable.Dispose

            ' Call the actual dispose method, specify manual dispose
            Dispose(True)
            ' Take the object out of GC
            GC.SuppressFinalize(Me)
        End Sub

        ' This is the method called by the GC
        Protected Overrides Sub Finalize()
            ' Call the actual dispose method, specify GC dispose
            Dispose(False)
        End Sub

        ' This is to be implemented by the subclass
        Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
            If disposing Then
                ' Manually called
                ' Call Dispose() on the contained object
            End If
            ' Call Dispose() on the base object
        End Sub

    End Class

End Namespace

