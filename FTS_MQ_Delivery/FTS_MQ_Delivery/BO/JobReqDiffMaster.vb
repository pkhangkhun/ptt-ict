Namespace HQComponent.BO
    <Serializable()> _
    Public Class JobReqDiffMaster
        Inherits BaseBO

#Region "Private Fields"
        Private _jobOid As Long
        Private _jobDate As Date
        Private _siteNo As String
        Private _maxCustOid As Long
        Private _maxCarplateOid As Long
        Private _runDate As Date
        Private _jobStatus As String
#End Region

#Region "Properties"
        Property JobOid() As Long
            Get
                Return _jobOid
            End Get
            Set(ByVal Value As Long)
                _jobOid = Value
            End Set
        End Property

        Property JobDate() As Date
            Get
                Return _jobDate
            End Get
            Set(ByVal Value As Date)
                _jobDate = Value
            End Set
        End Property

        Property SiteNo() As String
            Get
                Return _siteNo
            End Get
            Set(ByVal Value As String)
                _siteNo = Value
            End Set
        End Property

        Property MaxCustOid() As Long
            Get
                Return _maxCustOid
            End Get
            Set(ByVal Value As Long)
                _maxCustOid = Value
            End Set
        End Property

        Property MaxCarplateOid() As Long
            Get
                Return _maxCarplateOid
            End Get
            Set(ByVal Value As Long)
                _maxCarplateOid = Value
            End Set
        End Property

        Property RunDate() As Date
            Get
                Return _runDate
            End Get
            Set(ByVal Value As Date)
                _runDate = Value
            End Set
        End Property

        Property JobStatus() As String
            Get
                Return _jobStatus
            End Get
            Set(ByVal Value As String)
                _jobStatus = Value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

#End Region

    End Class

    <Serializable()> _
    Public Class JobReqDiffMasterCollection
        Inherits BaseBOCollection

        Default Property Item(ByVal index As Integer) As JobReqDiffMaster
            Get
                Return CType(innerList.Item(index), JobReqDiffMaster)
            End Get
            Set(ByVal value As JobReqDiffMaster)
                InnerList.Item(index) = value
            End Set
        End Property

        Sub Add(ByVal value As JobReqDiffMaster)
            InnerList.Add(value)
        End Sub

    End Class

End Namespace
