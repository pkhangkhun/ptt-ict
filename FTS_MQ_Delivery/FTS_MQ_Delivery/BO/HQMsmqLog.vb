Namespace HQComponent.BO
    <Serializable()> _
    Public Class HQMsmqLog
        Inherits BaseBO

#Region "Private Fields"
        Private _queueId As String = ""
        Private _siteNo As String = ""
        Private _branchNo As String = ""
        Private _stationNo As Integer
        Private _queueDestination As String = ""
        Private _queueType As Integer
        Private _totalFile As Integer
        Private _totalRecord As Integer
        Private _sendDate As Date
        Private _completeDate As Date
        Private _reSendFlg As Boolean = False

        Private _logTrans As HQMsmqTranLogCollection
#End Region

#Region "Properties"
        Property QueueId() As String
            Get
                Return _queueId
            End Get
            Set(ByVal Value As String)
                _queueId = Value
            End Set
        End Property

        Property SiteNo() As String
            Get
                Return _siteNo
            End Get
            Set(ByVal Value As String)
                _siteNo = Value
            End Set
        End Property

        Property BranchNo() As String
            Get
                Return _branchNo
            End Get
            Set(ByVal Value As String)
                _branchNo = Value
            End Set
        End Property

        Property StationNo() As Integer
            Get
                Return _stationNo
            End Get
            Set(ByVal Value As Integer)
                _stationNo = Value
            End Set
        End Property

        Property QueueDestination() As String
            Get
                Return _queueDestination
            End Get
            Set(ByVal Value As String)
                _queueDestination = Value
            End Set
        End Property

        Property QueueType() As Integer
            Get
                Return _queueType
            End Get
            Set(ByVal Value As Integer)
                _queueType = Value
            End Set
        End Property

        Property TotalFile() As Integer
            Get
                Return _totalFile
            End Get
            Set(ByVal Value As Integer)
                _totalFile = Value
            End Set
        End Property

        Property TotalRecord() As Integer
            Get
                Return _totalRecord
            End Get
            Set(ByVal Value As Integer)
                _totalRecord = Value
            End Set
        End Property

        Property SendDate() As Date
            Get
                Return _sendDate
            End Get
            Set(ByVal Value As Date)
                _sendDate = Value
            End Set
        End Property

        Property CompleteDate() As Date
            Get
                Return _completeDate
            End Get
            Set(ByVal Value As Date)
                _completeDate = Value
            End Set
        End Property

        Property ReSendFlg() As Boolean
            Get
                Return _reSendFlg
            End Get
            Set(ByVal Value As Boolean)
                _reSendFlg = Value
            End Set
        End Property

        Property LogTrans() As HQMsmqTranLogCollection
            Get
                Return _logTrans
            End Get
            Set(ByVal value As HQMsmqTranLogCollection)
                _logTrans = value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
            Me.LogTrans = New HQMsmqTranLogCollection
        End Sub

#End Region

    End Class

    <Serializable()> _
    Public Class HQMsmqLogCollection
        Inherits BaseBOCollection

        Default Property Item(ByVal index As Integer) As HQMsmqLog
            Get
                Return CType(InnerList.Item(index), HQMsmqLog)
            End Get
            Set(ByVal value As HQMsmqLog)
                InnerList.Item(index) = value
            End Set
        End Property

        Sub Add(ByVal value As HQMsmqLog)
            InnerList.Add(value)
        End Sub

    End Class

End Namespace

