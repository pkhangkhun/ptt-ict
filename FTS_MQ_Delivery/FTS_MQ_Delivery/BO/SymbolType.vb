Namespace HQComponent.BO
    <Serializable()> _
    Public Class SymbolType
        Inherits BaseBO

#Region "Private Fields"
        Private _symbolName As String
        Private _status As String
        Private _createUser As String
        Private _createDttm As Date
        Private _trDttm As Date
#End Region

#Region "Properties"
        Property SymbolName() As String
            Get
                Return _symbolName
            End Get
            Set(ByVal Value As String)
                _symbolName = Value
            End Set
        End Property

        Property Status() As String
            Get
                Return _status
            End Get
            Set(ByVal Value As String)
                _status = Value
            End Set
        End Property

        Property CreateUser() As String
            Get
                Return _createUser
            End Get
            Set(ByVal Value As String)
                _createUser = Value
            End Set
        End Property

        Property CreateDttm() As Date
            Get
                Return _createDttm
            End Get
            Set(ByVal Value As Date)
                _createDttm = Value
            End Set
        End Property

        Property TrDttm() As Date
            Get
                Return _trDttm
            End Get
            Set(ByVal Value As Date)
                _trDttm = Value
            End Set
        End Property

        Public Sub New()
            MyBase.New()
        End Sub

#End Region

    End Class

    <Serializable()> _
    Public Class SymbolTypeCollection
        Inherits BaseBOCollection

        Default Property Item(ByVal index As Integer) As SymbolType
            Get
                Return CType(innerList.Item(index), SymbolType)
            End Get
            Set(ByVal value As SymbolType)
                InnerList.Item(index) = value
            End Set
        End Property

        Sub Add(ByVal value As SymbolType)
            InnerList.Add(value)
        End Sub

    End Class

End Namespace
