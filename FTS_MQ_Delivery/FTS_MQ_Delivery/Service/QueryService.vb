Imports Ie.IeComponent.ServiceInterface
Imports FTS_MQ_Delivery.HQComponent.BO
Imports FTS_MQ_Delivery.App.Dao
Imports FTS_MQ_Delivery.App.Utility
Imports System.Data.SqlClient

Namespace App.Service
    Public Class QueryService
        Inherits System.ComponentModel.Component
        Implements IQueryService

        Public Function query(ByVal sql As String) As System.Data.DataSet Implements IQueryService.query
            Return QueryDao.Inquiry(sql)
        End Function

        Sub update(ByVal sql As String) Implements IQueryService.update

            Dim cn As SqlConnection = DBUtil.GetDBConnection
            cn.Open()
            Dim txn As SqlTransaction = cn.BeginTransaction(IsolationLevel.Serializable)

            Try
                QueryDao.Update(sql, cn, txn)

                txn.Commit()
            Catch ex As Exception
                txn.Rollback()
                Throw ex
            Finally
                cn.Close()
            End Try

        End Sub

    End Class
End Namespace