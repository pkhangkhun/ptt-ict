Imports Ie.IeComponent.ServiceInterface
Imports Ie.IeComponent.BO
Imports FTS_MQ_Delivery.App.Dao
Imports FTS_MQ_Delivery.App.Utility
Imports System.Data.SqlClient
Imports System.IO

Namespace App.Service

    Public Class MqCustPlateService
        Inherits System.ComponentModel.Component
        Implements IMqCustPlateService

        Public Function GetPublish() As Ie.IeComponent.BO.CustPlateCollection Implements IMqCustPlateService.GetPublish
            Return CustPlateDao.GetPublish
        End Function

        Public Function GetDiff(ByVal maxCarPlateOid As Long) As Ie.IeComponent.BO.CustPlateCollection Implements IMqCustPlateService.GetDiff
            Return CustPlateDao.GetDiff(maxCarPlateOid)
        End Function

        Sub UpdatePublishDate(ByVal custPlates As CustPlateCollection) Implements IMqCustPlateService.UpdatePublishDate

            Dim cn As SqlConnection = DBUtil.GetDBConnection
            cn.Open()
            Dim txn As SqlTransaction = cn.BeginTransaction(IsolationLevel.Serializable)

            Try

                For Each custPlate As CustPlate In custPlates
                    CustPlateDao.UpdatePublishDate(custPlate, cn, txn)
                Next

                txn.Commit()
            Catch ex As Exception
                txn.Rollback()
                Throw ex
            Finally
                cn.Close()
            End Try

        End Sub

    End Class

End Namespace


