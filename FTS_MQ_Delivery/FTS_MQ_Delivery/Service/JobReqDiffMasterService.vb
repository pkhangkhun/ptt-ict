Imports Ie.IeComponent.ServiceInterface
Imports Ie.IeComponent.BO
Imports FTS_MQ_Delivery.HQComponent.BO
Imports FTS_MQ_Delivery.App.Dao
Imports FTS_MQ_Delivery.App.Utility
Imports System.Data.SqlClient
Imports System.IO

Namespace App.Service

    Public Class JobReqDiffMasterService
        Inherits System.ComponentModel.Component
        Implements IJobReqDiffMasterService

        Function Retrieve(ByVal jobOid As Long) As JobReqDiffMaster _
             Implements IJobReqDiffMasterService.Retrieve

            Dim jobReqDiffMaster As JobReqDiffMaster = JobReqDiffMasterDao.Retrieve(jobOid)
            Return jobReqDiffMaster

        End Function

        Function RetrieveAll() As JobReqDiffMasterCollection _
             Implements IJobReqDiffMasterService.RetrieveAll

            Return JobReqDiffMasterDao.RetrieveAll()

        End Function

        Sub AddNew(ByVal jobReqDiffMaster As JobReqDiffMaster) _
             Implements IJobReqDiffMasterService.AddNew

            Dim cn As SqlConnection = DBUtil.GetDBConnection
            cn.Open()
            Dim txn As SqlTransaction = cn.BeginTransaction(IsolationLevel.Serializable)

            Try
                JobReqDiffMasterDao.Create(jobReqDiffMaster, cn, txn)
                txn.Commit()
            Catch ex As Exception
                txn.Rollback()
                Throw ex
            Finally
                cn.Close()
            End Try

        End Sub

        Sub Update(ByVal jobReqDiffMaster As JobReqDiffMaster) _
             Implements IJobReqDiffMasterService.Update

            Dim cn As SqlConnection = DBUtil.GetDBConnection
            cn.Open()
            Dim txn As SqlTransaction = cn.BeginTransaction(IsolationLevel.Serializable)

            Try
                JobReqDiffMasterDao.Update(jobReqDiffMaster, cn, txn)
                txn.Commit()
            Catch ex As Exception
                txn.Rollback()
                Throw ex
            Finally
                cn.Close()
            End Try

        End Sub

        Sub Delete(ByVal jobReqDiffMaster As JobReqDiffMaster) _
             Implements IJobReqDiffMasterService.Delete

            Dim cn As SqlConnection = DBUtil.GetDBConnection
            cn.Open()
            Dim txn As SqlTransaction = cn.BeginTransaction(IsolationLevel.Serializable)

            Try
                JobReqDiffMasterDao.Delete(jobReqDiffMaster, cn, txn)
                txn.Commit()
            Catch ex As Exception
                txn.Rollback()
                Throw ex
            Finally
                cn.Close()
            End Try

        End Sub

        Function GetJob() As JobReqDiffMasterCollection _
             Implements IJobReqDiffMasterService.GetJob

            Return JobReqDiffMasterDao.GetJob

        End Function

        Sub CloseJob(ByVal jobOid As Long) _
             Implements IJobReqDiffMasterService.CloseJob

            Dim cn As SqlConnection = DBUtil.GetDBConnection
            cn.Open()
            Dim txn As SqlTransaction = cn.BeginTransaction(IsolationLevel.Serializable)

            Try
                JobReqDiffMasterDao.CloseJob(jobOid, cn, txn)
                txn.Commit()
            Catch ex As Exception
                txn.Rollback()
                Throw ex
            Finally
                cn.Close()
            End Try

        End Sub

    End Class

End Namespace

