Imports Ie.IeComponent.ServiceInterface
Imports FTS_MQ_Delivery.HQComponent.BO
Imports FTS_MQ_Delivery.App.Dao
Imports FTS_MQ_Delivery.App.Utility
Imports System.Data.SqlClient
Imports System.IO

Namespace App.Service

    Public Class HQMsmqLogService
        Inherits System.ComponentModel.Component
        Implements IHQMsmqLogService

        Sub SaveLog(ByVal qLog As HQMsmqLog) Implements IHQMsmqLogService.SaveLog

            Dim cn As SqlConnection = DBUtil.GetDBConnection
            cn.Open()
            Dim txn As SqlTransaction = cn.BeginTransaction(IsolationLevel.Serializable)

            Try

                If qLog.TotalFile = 1 Then
                    HQMsmqLogDao.Create(qLog, cn, txn)
                End If
                For Each qTranLog As HQMsmqTranLog In qLog.LogTrans
                    HQMsmqTranLogDao.Create(qTranLog, cn, txn)
                Next

                txn.Commit()
            Catch ex As Exception
                txn.Rollback()
                Throw ex
            Finally
                cn.Close()
            End Try

        End Sub

        Function GetPublishQueueNotResponding() As HQMsmqTranLogCollection _
             Implements IHQMsmqLogService.GetPublishQueueNotResponding

            Return HQMsmqTranLogDao.GetPublishQueueNotResponding

        End Function


    End Class

End Namespace

