Imports Ie.IeComponent.ServiceInterface
Imports Ie.IeComponent.BO
Imports FTS_MQ_Delivery.App.Dao
Imports FTS_MQ_Delivery.App.Utility
Imports System.Data.SqlClient
Imports System.IO

Namespace App.Service
    Public Class MqSystemInfoService
        Inherits System.ComponentModel.Component
        Implements IMqSystemInfoService

        Public Function GetNextMQID(ByVal keyWord As String, ByVal r1 As Integer, ByVal r2 As Integer) As String Implements IMqSystemInfoService.GetNextMQID
            Return SystemInfoDao.GetNextMQID(keyWord, r1, r2)
        End Function

        Public Function TestDBConnection() As Boolean Implements IMqSystemInfoService.TestDBConnection
            Return SystemInfoDao.TestDBConnection
        End Function

    End Class
End Namespace