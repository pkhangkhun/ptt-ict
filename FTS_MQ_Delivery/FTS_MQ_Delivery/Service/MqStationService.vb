Imports Ie.IeComponent.ServiceInterface
Imports Ie.IeComponent.BO
Imports FTS_MQ_Delivery.App.Dao
Imports FTS_MQ_Delivery.App.Utility
Imports System.Data.SqlClient
Imports System.IO

Namespace App.Service
    Public Class MqStationService
        Inherits System.ComponentModel.Component
        Implements IMqStationService

        Public Function GetActive() As Ie.IeComponent.BO.StationCollection Implements IMqStationService.GetActive
            Return StationDao.GetActive
        End Function

        Public Function GetBySiteNo(ByVal siteNo As String) As Ie.IeComponent.BO.StationCollection Implements IMqStationService.GetBySiteNo
            Return StationDao.GetBySiteNo(siteNo)
        End Function
    End Class

End Namespace