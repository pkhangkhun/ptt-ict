Imports Ie.IeComponent.ServiceInterface
Imports Ie.IeComponent.BO
Imports FTS_MQ_Delivery.App.Dao
Imports FTS_MQ_Delivery.App.Utility
Imports System.Data.SqlClient
Imports System.IO

Namespace App.Service

    Public Class MqCustomerService
        Inherits System.ComponentModel.Component
        Implements IMqCustomerService

        Public Function GetPublish() As Ie.IeComponent.BO.CustomerCollection Implements IMqCustomerService.GetPublish
            Return CustomerDao.GetPublish
        End Function

        Public Function GetDiff(ByVal maxCusOid As Long) As Ie.IeComponent.BO.CustomerCollection Implements IMqCustomerService.GetDiff
            Return CustomerDao.GetDiff(maxCusOid)
        End Function

        Sub UpdatePublishDate(ByVal customers As CustomerCollection) Implements IMqCustomerService.UpdatePublishDate

            Dim cn As SqlConnection = DBUtil.GetDBConnection
            cn.Open()
            Dim txn As SqlTransaction = cn.BeginTransaction(IsolationLevel.Serializable)

            Try

                For Each cust As Customer In customers
                    CustomerDao.UpdatePublishDate(cust, cn, txn)
                Next

                txn.Commit()
            Catch ex As Exception
                txn.Rollback()
                Throw ex
            Finally
                cn.Close()
            End Try

        End Sub

    End Class

End Namespace

