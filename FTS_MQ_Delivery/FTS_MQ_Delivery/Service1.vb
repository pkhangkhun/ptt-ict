'Imports Ie.IeComponent.BO     
'Imports FTS_HQ_MsmqServ.App.Service
'Imports System.Messaging
'Imports System.IO
'Imports System.Runtime.Serialization.Formatters.Binary
'Imports System.Threading.Timer 
'Imports System.Timers.Timer

Public Class Service1

    'Public thisTimer As System.Timers.Timer
    'Public IsRunning As Boolean

    'Public ScheduleTime As String = System.Configuration.ConfigurationManager.AppSettings.Get("ScheduleTime")

    'Public Shared Sub WriteLogFile(ByVal _msg As String)

    '    'Dim logPath As String = "c:\FTS_HQ_MsmqServ\Log"
    '    Dim logPath As String = System.Configuration.ConfigurationManager.AppSettings.Get("LogPath")

    '    Dim s As String = Format(Date.Today, "yyyyMMdd")
    '    Dim myFile As String = "\FTS_MqLog_" + s + ".txt"
    '    Dim objStreamWriter As StreamWriter
    '    If Not Directory.Exists(logPath) Then
    '        Directory.CreateDirectory(logPath)
    '    End If
    '    If File.Exists(logPath + myFile) Then
    '        objStreamWriter = File.AppendText(logPath + myFile)
    '    Else
    '        objStreamWriter = File.CreateText(logPath + myFile)
    '    End If

    '    _msg = Format(Date.Now, "HH:mm:ss --> ") + _msg
    '    objStreamWriter.WriteLine(_msg)
    '    objStreamWriter.Close()
    'End Sub

    'Private Sub PublishData()

    '    Dim TransactionPerQueue As Integer = System.Configuration.ConfigurationManager.AppSettings.Get("TransactionPerQueue")

    '    Dim sendDate As Date = Date.Now

    '    Dim sysInfoServ As IMqSystemInfoService = UI.Factory.BaseServiceFactory.GetInstance(GetType(MqSystemInfoService))

    '    Dim stationServ As IMqStationService = UI.Factory.BaseServiceFactory.GetInstance(GetType(MqStationService))

    '    Dim stations As New StationCollection
    '    Try
    '        stations = stationServ.GetActive()
    '    Catch ex As Exception
    '    End Try


    '    '=== Customer Data ===================================================================================================

    '    Try
    '        Dim custServ As IMqCustomerService = UI.Factory.BaseServiceFactory.GetInstance(GetType(MqCustomerService))

    '        Dim customers As CustomerCollection = custServ.GetPublish

    '        WriteLogFile("Publish Customer Total : " + customers.Count.ToString + " Record(s)")

    '        Dim tempCustomers As New CustomerCollection
    '        Dim queueNo As String = ""
    '        Dim r1 As Integer = 1
    '        For Each cust As Customer In customers
    '            tempCustomers.Add(cust)
    '            If tempCustomers.Count = TransactionPerQueue Then
    '                queueNo = sysInfoServ.GetNextMQID("MQCM", r1, 1)
    '                r1 = 0

    '                SerializeObject(tempCustomers, queueNo + ".slz", sendDate)

    '                MSMQ_Customer(stations, queueNo, tempCustomers)

    '                custServ.UpdatePublishDate(tempCustomers)

    '                tempCustomers.Clear()

    '            End If
    '        Next
    '        If tempCustomers.Count > 0 Then
    '            queueNo = sysInfoServ.GetNextMQID("MQCM", r1, 1)

    '            SerializeObject(tempCustomers, queueNo + ".slz", sendDate)

    '            MSMQ_Customer(stations, queueNo, tempCustomers)

    '            custServ.UpdatePublishDate(tempCustomers)

    '            tempCustomers.Clear()

    '        End If

    '        WriteLogFile("Success Publish Customer")

    '    Catch ex As Exception
    '        WriteLogFile("Error Publish Customer " + ex.Message)
    '    End Try

    '    '===========================================================================================================

    '    '===== Car Plate ===========================================================================================

    '    Try
    '        Dim custPlateServ As IMqCustPlateService = UI.Factory.BaseServiceFactory.GetInstance(GetType(MqCustPlateService))

    '        Dim custPlates As CustPlateCollection = custPlateServ.GetPublish

    '        WriteLogFile("Publish Car Plate Total : " + custPlates.Count.ToString + " Record(s)")

    '        Dim tempCustPlates As New CustPlateCollection
    '        Dim queueNo As String = ""
    '        Dim r1 As Integer = 1
    '        For Each custPlate As CustPlate In custPlates
    '            tempCustPlates.Add(custPlate)
    '            If tempCustPlates.Count = TransactionPerQueue Then
    '                queueNo = sysInfoServ.GetNextMQID("MQCP", r1, 1)
    '                r1 = 0

    '                SerializeObject(tempCustPlates, queueNo + ".slz", sendDate)

    '                MSMQ_CustPlate(stations, queueNo, tempCustPlates)

    '                custPlateServ.UpdatePublishDate(tempCustPlates)

    '                tempCustPlates.Clear()

    '            End If
    '        Next
    '        If tempCustPlates.Count > 0 Then
    '            queueNo = sysInfoServ.GetNextMQID("MQCP", r1, 1)

    '            SerializeObject(tempCustPlates, queueNo + ".slz", sendDate)

    '            MSMQ_CustPlate(stations, queueNo, tempCustPlates)

    '            custPlateServ.UpdatePublishDate(tempCustPlates)

    '            tempCustPlates.Clear()

    '        End If

    '        WriteLogFile("Success Publish Car Plate")

    '    Catch ex As Exception
    '        WriteLogFile("Error Publish Car Plate " + ex.Message)
    '    End Try


    '    '===========================================================================================================

    '    WriteLogFile("Success Publish Master Data")
    'End Sub

    'Public Shared Sub MSMQ_Customer(ByVal stations As StationCollection, ByVal queueNo As String, ByVal customers As CustomerCollection)

    '    For Each station As Station In stations

    '        WriteLogFile("Publish to : " + station.SiteNo + ":" + station.BranchNo + ":" + station.StationId.ToString + ":Queue No#" + queueNo + ":Total " + customers.Count.ToString + " Record(s)")

    '        Dim qDestination As String = station.QueueName
    '        If qDestination = "" Then
    '            Exit Sub
    '        End If

    '        Dim qIP As String = station.IpAddress

    '        Dim qPath As String = "FormatName:DIRECT=TCP:" + qIP
    '        Dim qName As String = "\private$\" + qDestination
    '        Dim mqSource As String = station.SiteNo + ":" + station.BranchNo + ":" + station.StationId.ToString
    '        Dim messageQ As MessageQueue
    '        Dim message As Message
    '        Dim mqTran As New MessageQueueTransaction()

    '        qPath += qName

    '        WriteLogFile(qPath)

    '        messageQ = New MessageQueue(qPath)
    '        message = New Message(customers, New BinaryMessageFormatter)
    '        message.Label = queueNo
    '        message.Label += ":" + mqSource + ":" + Format(Date.Now, "yyMMdd-HHmm")

    '        message.AcknowledgeType = AcknowledgeTypes.FullReceive
    '        message.UseJournalQueue = False
    '        message.UseDeadLetterQueue = True

    '        mqTran.Begin()
    '        Dim success As Boolean = True
    '        Try
    '            messageQ.Send(message, mqTran)
    '            mqTran.Commit()
    '        Catch ex As Exception
    '            mqTran.Abort()
    '            success = False
    '            WriteLogFile("Error Send Queue : " + ex.Message)
    '        Finally
    '            messageQ.Close()
    '        End Try

    '    Next

    'End Sub

    'Public Shared Sub MSMQ_CustPlate(ByVal stations As StationCollection, ByVal queueNo As String, ByVal custPlates As CustPlateCollection)

    '    For Each station As Station In stations

    '        WriteLogFile("Publish to : " + station.SiteNo + ":" + station.BranchNo + ":" + station.StationId.ToString + ":Queue No#" + queueNo + ":Total " + custPlates.Count.ToString + " Record(s)")

    '        Dim qDestination As String = station.QueueName
    '        If qDestination = "" Then
    '            Exit Sub
    '        End If

    '        Dim qIP As String = station.IpAddress

    '        Dim qPath As String = "FormatName:DIRECT=TCP:" + qIP
    '        Dim qName As String = "\private$\" + qDestination
    '        Dim mqSource As String = station.SiteNo + ":" + station.BranchNo + ":" + station.StationId.ToString
    '        Dim messageQ As MessageQueue
    '        Dim message As Message
    '        Dim mqTran As New MessageQueueTransaction()

    '        qPath += qName
    '        messageQ = New MessageQueue(qPath)
    '        message = New Message(custPlates, New BinaryMessageFormatter)
    '        message.Label = queueNo
    '        message.Label += ":" + mqSource + ":" + Format(Date.Now, "yyMMdd-HHmm")

    '        message.AcknowledgeType = AcknowledgeTypes.FullReceive
    '        message.UseJournalQueue = False
    '        message.UseDeadLetterQueue = True

    '        mqTran.Begin()
    '        Dim success As Boolean = True
    '        Try
    '            messageQ.Send(message, mqTran)
    '            mqTran.Commit()
    '        Catch ex As Exception
    '            mqTran.Abort()
    '            success = False
    '            WriteLogFile("Error Send Queue : " + ex.Message)
    '        Finally
    '            messageQ.Close()
    '        End Try

    '    Next

    'End Sub

    'Public Shared Sub SerializeObject(ByVal obj As Object, ByVal fileName As String, ByVal publishDate As Date)

    '    Dim folder As String = System.Configuration.ConfigurationManager.AppSettings.Get("ObjPath") + "\" + Format(publishDate, "yyyyMMdd")
    '    If Not Directory.Exists(folder) Then
    '        Directory.CreateDirectory(folder)
    '    End If

    '    fileName = folder + "\" + fileName
    '    Dim stream As FileStream = File.Open(fileName, FileMode.Create)
    '    Try
    '        Dim bFormater As BinaryFormatter = New BinaryFormatter
    '        bFormater.Serialize(stream, obj)
    '    Catch ex As Exception
    '    Finally
    '        stream.Close()
    '    End Try
    'End Sub

    ''Private Shared Function DeSerializeObject(ByVal fileName As String) As QueueTransLog

    ''    Dim stream As FileStream = File.Open(fileName, FileMode.Open)
    ''    Dim objToSerialize As New QueueTransLog
    ''    Try

    ''        Dim bFormater As BinaryFormatter = New BinaryFormatter
    ''        objToSerialize = bFormater.Deserialize(stream)

    ''    Catch ex As Exception
    ''        Throw ex
    ''    Finally
    ''        stream.Close()
    ''    End Try

    ''    Return objToSerialize
    ''End Function


    '' This automatically-created subprocedure contains code you want run when the service is started.
    'Protected Overrides Sub OnStart(ByVal args() As String)

    '    Try

    '        WriteLogFile("Start FTS HQ Publisher Services version 1.0.1")

    '        ' Set initial variable values.
    '        Me.IsRunning = False

    '        ' Create and start a timer that checks every 20 seconds.
    '        thisTimer = New System.Timers.Timer()
    '        thisTimer.Enabled = True
    '        thisTimer.Interval = 1000
    '        thisTimer.AutoReset = True
    '        AddHandler thisTimer.Elapsed, AddressOf thisTimer_Tick
    '        thisTimer.Start()

    '    Catch ex As Exception
    '        WriteLogFile("Error Start FTS HQ Publisher Services : " + ex.Message)
    '    End Try

    'End Sub

    '' This automatically-created subprocedure contains code you want run when the service is stopped.
    'Protected Overrides Sub OnStop()
    '    WriteLogFile("Stop FTS HQ Publisher Services")
    'End Sub

    '' This subprocedure is of our own creation, used to store code we want this service to actually execute.
    'Public Sub DoNextExecution()
    '    SyncLock Me
    '        thisTimer.Stop()

    '        ' Another execution cycle begins.

    '        WriteLogFile("Run Publish Master Data")

    '        PublishData()

    '        thisTimer.Start()
    '    End SyncLock
    'End Sub

    '' Each time the timer completes an interval, it executes the code found within this subprocedure.
    ''Private Sub thisTimer_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ''    DoNextExecution()
    ''End Sub

    'Private Sub thisTimer_Tick(ByVal sender As System.Object, ByVal e As System.Timers.ElapsedEventArgs)
    '    WriteLogFile(Format(Date.Now, "HH:mm:ss"))
    '    If Format(Date.Now, "HH:mm:ss") = ScheduleTime Then
    '        DoNextExecution()
    '    End If

    'End Sub


End Class
